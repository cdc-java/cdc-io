# cdc-io

# Benches

# Compress

# Data
DOM-like memory structure.  
It is simpler than DOM, being focused on features that are most useful when no
mixed content is necessary.

# Demos

# JSON

# TXT
Small library used to handle text files and make them appear as a list of lines.

# Tools

# XML
XML writer.  
The API supports configurable pretty printing.