package cdc.io.tools;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.function.Consumer;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.io.data.paths.SPath;
import cdc.io.data.util.PatternReplacement;
import cdc.io.xml.XmlWriter;
import cdc.util.files.Files;

class XmlNormalizerTest {
    private static final Logger LOGGER = LogManager.getLogger(XmlNormalizerTest.class);
    private static final String XML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
    private static final String INDENT = ""; // " ";
    private static final String EOL = ""; // System.lineSeparator();

    private static void test(String expected,
                             String source,
                             boolean delayedProcessing,
                             Consumer<XmlNormalizer.MainArgs> margsSetter) throws IOException {
        LOGGER.debug("expected: {}", expected);
        LOGGER.debug("source: {}", source);
        final File input = File.createTempFile(XmlNormalizerTest.class.getSimpleName(), ".in");
        input.deleteOnExit();
        final File output = File.createTempFile(XmlNormalizerTest.class.getSimpleName(), ".out");
        output.deleteOnExit();
        final XmlNormalizer.MainArgs margs = new XmlNormalizer.MainArgs();
        margs.inputFile = input;
        margs.outputFile = output;
        margs.indentString = INDENT;
        margs.setEnabled(XmlNormalizer.MainArgs.Feature.PRETTY_PRINT, false);
        margs.setEnabled(XmlNormalizer.MainArgs.Feature.DELAYED_PROCESSING, delayedProcessing);
        margsSetter.accept(margs);
        try (final FileWriter writer = new FileWriter(input)) {
            writer.write(source);
            writer.flush();
        }
        XmlNormalizer.execute(margs);

        Files.cat(input, StandardCharsets.UTF_8, LOGGER, Level.DEBUG);
        Files.cat(output, StandardCharsets.UTF_8, LOGGER, Level.DEBUG);

        final String result = Files.readFileAsString(output, StandardCharsets.UTF_8);
        assertEquals(expected, result);
    }

    private static void test(String expected,
                             String source,
                             Consumer<XmlNormalizer.MainArgs> margsSetter) throws IOException {
        test(expected, source, true, margsSetter);
        test(expected, source, false, margsSetter);
    }

    private static void testN(String expected,
                              String source,
                              boolean delayedProcessing,
                              Consumer<XmlNormalizer.MainArgs> margsSetter) throws IOException {
        LOGGER.debug("expected: {}", expected);
        LOGGER.debug("source: {}", source);
        final File input = File.createTempFile(XmlNormalizerTest.class.getSimpleName(), ".in");
        input.deleteOnExit();
        final File output = File.createTempFile(XmlNormalizerTest.class.getSimpleName(), ".out");
        output.deleteOnExit();
        final XmlNormalizer.MainArgs margs = new XmlNormalizer.MainArgs();
        margs.inputFile = input;
        margs.outputFile = output;
        margs.indentString = "   ";
        margs.setEnabled(XmlNormalizer.MainArgs.Feature.PRETTY_PRINT, true);
        margs.setEnabled(XmlNormalizer.MainArgs.Feature.DELAYED_PROCESSING, delayedProcessing);
        margsSetter.accept(margs);
        try (final FileWriter writer = new FileWriter(input)) {
            writer.write(XML + "\n");
            writer.write(source);
            writer.flush();
        }
        XmlNormalizer.execute(margs);

        Files.cat(input, StandardCharsets.UTF_8, LOGGER, Level.DEBUG);
        Files.cat(output, StandardCharsets.UTF_8, LOGGER, Level.DEBUG);

        final String result = Files.readFileAsString(output, StandardCharsets.UTF_8);
        assertEquals(XML + "\n" + expected, result);
    }

    private static void testN(String expected,
                              String source,
                              Consumer<XmlNormalizer.MainArgs> margsSetter) throws IOException {
        testN(expected, source, true, margsSetter);
        testN(expected, source, false, margsSetter);
    }

    @Test
    void testBasic() throws IOException {
        final String source = XML
                + "<root>"
                + "<child/>"
                + "</root>";
        final String expected = XML + EOL
                + "<root>" + EOL
                + INDENT + "<child/>" + EOL
                + "</root>";

        test(expected,
             source,
             margs -> {
                 // Ignore
             });
    }

    @Test
    void testRemove() throws IOException {
        final String source = XML
                + "<root att=''>"
                + "<child/>"
                + "</root>";
        final String expected = XML + EOL
                + "<root/>";

        test(expected,
             source,
             margs -> {
                 margs.removed.add(new SPath("root/child"));
                 margs.removed.add(new SPath("root@att"));
             });
    }

    @Test
    void testDissolve() throws IOException {
        final String source =
                """
                <root>
                   <child1>
                      <child2/>
                   </child1>
                </root>
                """;
        final String expected =
                """
                <root>
                   <child2/>
                </root>""";
        testN(expected,
              source,
              margs -> {
                  margs.dissolved.add(new SPath("child1"));
              });
    }

    @Test
    void testRemoveEmptyAttributes() throws IOException {
        final String source = XML
                + "<root>"
                + "<child att=''/>"
                + "</root>";
        final String expected = XML + EOL
                + "<root>" + EOL
                + INDENT + "<child/>" + EOL
                + "</root>";

        test(expected,
             source,
             margs -> {
                 margs.setEnabled(XmlNormalizer.MainArgs.Feature.REMOVE_EMPTY_ATTRIBUTES, true);
             });
    }

    @Test
    void testRemovePureElements() throws IOException {
        final String source = XML
                + "<root att=''>"
                + "<child/>"
                + "</root>";
        final String expected = XML + EOL
                + "<root att=\"\"/>";

        test(expected,
             source,
             margs -> {
                 margs.setEnabled(XmlNormalizer.MainArgs.Feature.REMOVE_PURE_ELEMENTS, true);
             });
    }

    @Test
    void testRenameElement() throws IOException {
        final String source = XML
                + "<root>"
                + "<child att1='' att2=''/>"
                + "</root>";
        final String expected = XML + EOL
                + "<root>" + EOL
                + INDENT + "<CHILD att1=\"\" att2=\"\"/>" + EOL
                + "</root>";

        test(expected,
             source,
             margs -> {
                 margs.renamed.put(new SPath("root/child"),
                                   s -> "CHILD");
             });
    }

    @Test
    void testRenameAttribute() throws IOException {
        final String source = XML
                + "<root>"
                + "<child att1='' att2=''/>"
                + "</root>";
        final String expected = XML + EOL
                + "<root>" + EOL
                + INDENT + "<child ATT1=\"\" att2=\"\"/>" + EOL
                + "</root>";

        test(expected,
             source,
             margs -> {
                 margs.renamed.put(new SPath("child@att1"), s -> "ATT1");
             });
    }

    @Test
    void testRename1() throws IOException {
        final String source = XML
                + "<root>"
                + "<child att1='' att2=''/>"
                + "</root>";
        final String expected = XML + EOL
                + "<root>" + EOL
                + INDENT + "<CHILD ATT1=\"\" att2=\"\"/>" + EOL
                + "</root>";

        test(expected,
             source,
             margs -> {
                 margs.renamed.put(new SPath("root/child"), s -> "CHILD");
                 margs.renamed.put(new SPath("child@att1"), s -> "ATT1");
             });
    }

    @Test
    void testRename2() throws IOException {
        final String source = XML
                + "<root>"
                + "<child att1='' att2=''/>"
                + "</root>";
        final String expected = XML + EOL
                + "<root>" + EOL
                + INDENT + "<chIld ATT1=\"\" att2=\"\"/>" + EOL
                + "</root>";

        test(expected,
             source,
             margs -> {
                 margs.renamed.put(new SPath("root/child"), new PatternReplacement("i", "I"));
                 margs.renamed.put(new SPath("child@att1"), new PatternReplacement("att", "ATT"));
             });
    }

    @Test
    void testSetAttributesValues() throws IOException {
        final String source = XML
                + "<root>"
                + "<child att='value'/>"
                + "</root>";
        final String expected = XML + EOL
                + "<root>" + EOL
                + INDENT + "<child att=\"VALUE\"/>" + EOL
                + "</root>";

        test(expected,
             source,
             margs -> {
                 margs.set.put(new SPath("@att"), s -> "VALUE");
             });
    }

    @Test
    void testSetElementsContents1() throws IOException {
        final String source = XML
                + "<root>"
                + "<child>a</child>"
                + "</root>";
        final String expected = XML + EOL
                + "<root>" + EOL
                + INDENT + "<child>CONTENT</child>" + EOL
                + "</root>";

        test(expected,
             source,
             margs -> {
                 margs.set.put(new SPath("child"), s -> "CONTENT");
             });
    }

    @Test
    void testSetElementsContents2() throws IOException {
        final String source = XML
                + "<root>"
                + "<child/>"
                + "</root>";
        final String expected = XML + EOL
                + "<root>" + EOL
                + INDENT + "<child>CONTENT</child>" + EOL
                + "</root>";

        test(expected,
             source,
             margs -> {
                 margs.set.put(new SPath("child"), s -> "CONTENT");
             });
    }

    @Test
    void testSortAttributes() throws IOException {
        final String source = XML
                + "<root>"
                + "<child att2='' att1=''/>"
                + "</root>";
        final String expected = XML + EOL
                + "<root>" + EOL
                + INDENT + "<child att1=\"\" att2=\"\"/>" + EOL
                + "</root>";

        test(expected,
             source,
             margs -> {
                 margs.setEnabled(XmlNormalizer.MainArgs.Feature.SORT_ATTRIBUTES, true);
             });
    }

    @Test
    void testSortElements() throws IOException {
        final String source = XML
                + "<root>"
                + "<child3/>"
                + "<child2/>"
                + "<child1/>"
                + "</root>";
        final String expected = XML + EOL
                + "<root>" + EOL
                + INDENT + "<child1/>" + EOL
                + INDENT + "<child2/>" + EOL
                + INDENT + "<child3/>" + EOL
                + "</root>";

        test(expected,
             source,
             margs -> {
                 margs.setEnabled(XmlNormalizer.MainArgs.Feature.SORT_ELEMENTS, true);
             });
    }

    @Test
    void testMoveAttributeFirst() throws IOException {
        final String source = XML
                + "<root>"
                + "<child att3='' att2='' att1=''/>"
                + "</root>";
        final String expected = XML + EOL
                + "<root>" + EOL
                + INDENT + "<child att1=\"\" att3=\"\" att2=\"\"/>" + EOL
                + "</root>";

        test(expected,
             source,
             margs -> {
                 margs.firstAttributes.add("att1");
             });
    }

    @Test
    void testUseSingleQuote() throws IOException {
        final String source = XML
                + "<root>"
                + "<child att3='' att2='' att1=''/>"
                + "</root>";
        final String expected = XML + EOL
                + "<root>" + EOL
                + INDENT + "<child att3='' att2='' att1=''/>" + EOL
                + "</root>";

        test(expected,
             source,
             margs -> {
                 margs.setEnabled(XmlNormalizer.MainArgs.Feature.USE_SINGLE_QUOTE, true);
             });
    }

    @Test
    void testAlwaysEntitizeAttributes() throws IOException {
        final String source = XML
                + "<root>"
                + "<child att='" + XmlWriter.TAB + "'/>"
                + "</root>";
        final String expected = XML + EOL
                + "<root>" + EOL
                + INDENT + "<child att=\"" + XmlWriter.TAB + "\"/>" + EOL
                + "</root>";

        test(expected,
             source,
             margs -> {
                 margs.setEnabled(XmlNormalizer.MainArgs.Feature.ALWAYS_ENTITIZE_ATTRIBUTES, true);
             });
    }
}