package cdc.io.json;

import java.math.BigDecimal;
import java.util.EnumMap;
import java.util.Map;

import javax.json.stream.JsonParser;
import javax.json.stream.JsonParser.Event;

/**
 * JSONP wrapper.
 *
 * @author Damien Carbonne
 */
public class JsonpParser extends AbstractJsonParser {
    private final JsonParser parser;
    private JsonParser.Event xevent;
    private JsonEvent event;

    private static final Map<JsonParser.Event, JsonEvent> TO_EVENT = new EnumMap<>(JsonParser.Event.class);

    static {
        TO_EVENT.put(JsonParser.Event.END_ARRAY, JsonEvent.END_ARRAY);
        TO_EVENT.put(JsonParser.Event.START_ARRAY, JsonEvent.START_ARRAY);
        TO_EVENT.put(JsonParser.Event.END_OBJECT, JsonEvent.END_OBJECT);
        TO_EVENT.put(JsonParser.Event.START_OBJECT, JsonEvent.START_OBJECT);
        TO_EVENT.put(JsonParser.Event.KEY_NAME, JsonEvent.KEY_NAME);
        TO_EVENT.put(JsonParser.Event.VALUE_FALSE, JsonEvent.VALUE_FALSE);
        TO_EVENT.put(JsonParser.Event.VALUE_TRUE, JsonEvent.VALUE_TRUE);
        TO_EVENT.put(JsonParser.Event.VALUE_NULL, JsonEvent.VALUE_NULL);
        TO_EVENT.put(JsonParser.Event.VALUE_STRING, JsonEvent.VALUE_STRING);
    }

    public JsonpParser(JsonParser parser) {
        this.parser = parser;
    }

    @Override
    public JsonEvent getEvent() {
        return event;
    }

    @Override
    public boolean hasNext() {
        return parser.hasNext();
    }

    @Override
    public AbstractJsonParser next() {
        xevent = parser.next();
        event = TO_EVENT.get(xevent);
        if (event == null) {
            if (xevent == Event.VALUE_NUMBER) {
                if (parser.isIntegralNumber()) {
                    event = JsonEvent.VALUE_NUMBER_INTEGRAL;
                } else {
                    event = JsonEvent.VALUE_NUMBER_DECIMAL;

                }
            } else {
                event = JsonEvent.OTHER;
            }
        }
        updateIndentAndTrace();
        return this;
    }

    @Override
    public String getKeyName() {
        return parser.getString();
    }

    @Override
    public String getStringValue() {
        return parser.getString();
    }

    @Override
    public int getIntValue() {
        return parser.getInt();
    }

    @Override
    public long getLongValue() {
        return parser.getLong();
    }

    @Override
    public BigDecimal getBigDecimalValue() {
        return parser.getBigDecimal();
    }

    @Override
    public float getFloatValue() {
        return parser.getBigDecimal().floatValue();
    }

    @Override
    public double getDoubleValue() {
        return parser.getBigDecimal().doubleValue();
    }

    @Override
    public AbstractJsonParser close() {
        parser.close();
        return this;
    }
}