package cdc.io.json;

import java.io.IOException;
import java.io.StringWriter;
import java.util.function.Consumer;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonFactoryBuilder;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import cdc.util.lang.ExceptionWrapper;
import cdc.util.lang.InvalidDataException;

public final class JacksonUtils {
    private JacksonUtils() {
    }

    public static void expect(JsonToken token,
                              JsonToken expected) {
        if (token != expected) {
            throw new InvalidDataException("Unexpected token: " + token + ", expected: " + expected);
        }
    }

    /**
     * Creates a string showing a JsonParser state.
     *
     * @param parser The parser.
     * @return A string representing {@code parser} and {@code event}.
     */
    public static String toString(JsonParser parser) {
        final StringBuilder builder = new StringBuilder();
        builder.append(parser.currentToken());
        if (parser.currentToken() != null) {
            try {
                switch (parser.currentToken()) {
                case FIELD_NAME:
                    builder.append(" '")
                           .append(parser.currentName())
                           .append("'");
                    break;
                case VALUE_STRING:
                    builder.append(' ')
                           .append(parser.getValueAsString());
                    break;
                case VALUE_NUMBER_INT:
                    builder.append(' ')
                           .append(parser.getValueAsLong());
                    break;
                case VALUE_NUMBER_FLOAT:
                    builder.append(' ')
                           .append(parser.getValueAsDouble());
                    break;
                default:
                    break;
                }
            } catch (final IOException e) {
                // Ignore
            }
        }
        return builder.toString();
    }

    /**
     * Creates a JsonGenerator using a StringWriter, pass it to a Consumer and returns the corresponding string.
     *
     * @param consumer The Consumer.
     * @return The string built by {@code consumer}.
     */
    public static String toString(Consumer<JsonGenerator> consumer) {
        final JsonFactoryBuilder builder = new JsonFactoryBuilder();
        final JsonFactory factory = builder.build();
        try (final StringWriter writer = new StringWriter();
                final JsonGenerator generator = factory.createGenerator(writer)) {
            consumer.accept(generator);
            generator.flush();
            return writer.toString();
        } catch (final IOException e) {
            // This should not happen.
            // StringWriter should not throw IOException
            throw new ExceptionWrapper(e);
        }
    }
}