package cdc.io.json;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.EnumMap;
import java.util.Map;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import cdc.util.lang.ExceptionWrapper;

/**
 * Jackson parser wrapper.
 *
 * @author Damien Carbonne
 */
public class JacksonParser extends AbstractJsonParser {
    private final JsonParser parser;

    private static final Map<JsonToken, JsonEvent> TO_EVENT = new EnumMap<>(JsonToken.class);

    static {
        TO_EVENT.put(JsonToken.END_ARRAY, JsonEvent.END_ARRAY);
        TO_EVENT.put(JsonToken.START_ARRAY, JsonEvent.START_ARRAY);
        TO_EVENT.put(JsonToken.END_OBJECT, JsonEvent.END_OBJECT);
        TO_EVENT.put(JsonToken.START_OBJECT, JsonEvent.START_OBJECT);
        TO_EVENT.put(JsonToken.FIELD_NAME, JsonEvent.KEY_NAME);
        TO_EVENT.put(JsonToken.VALUE_FALSE, JsonEvent.VALUE_FALSE);
        TO_EVENT.put(JsonToken.VALUE_TRUE, JsonEvent.VALUE_TRUE);
        TO_EVENT.put(JsonToken.VALUE_NULL, JsonEvent.VALUE_NULL);
        TO_EVENT.put(JsonToken.VALUE_STRING, JsonEvent.VALUE_STRING);
        TO_EVENT.put(JsonToken.VALUE_NUMBER_INT, JsonEvent.VALUE_NUMBER_INTEGRAL);
        TO_EVENT.put(JsonToken.VALUE_NUMBER_FLOAT, JsonEvent.VALUE_NUMBER_DECIMAL);
    }

    public JacksonParser(JsonParser parser) {
        this.parser = parser;
    }

    @Override
    public JsonEvent getEvent() {
        final JsonToken token = parser.currentToken();
        return TO_EVENT.getOrDefault(token, JsonEvent.OTHER);
    }

    @Override
    public boolean hasNext() {
        return parser.hasCurrentToken();
    }

    @Override
    public AbstractJsonParser next() {
        try {
            parser.nextToken();
        } catch (final IOException e) {
            throw new ExceptionWrapper(e);
        }
        updateIndentAndTrace();
        return this;
    }

    @Override
    public String getKeyName() {
        try {
            return parser.currentName();
        } catch (final IOException e) {
            throw new ExceptionWrapper(e);
        }
    }

    @Override
    public String getStringValue() {
        try {
            return parser.getValueAsString();
        } catch (final IOException e) {
            throw new ExceptionWrapper(e);
        }
    }

    @Override
    public int getIntValue() {
        try {
            return parser.getValueAsInt();
        } catch (final IOException e) {
            throw new ExceptionWrapper(e);
        }
    }

    @Override
    public long getLongValue() {
        try {
            return parser.getValueAsLong();
        } catch (final IOException e) {
            throw new ExceptionWrapper(e);
        }
    }

    @Override
    public BigDecimal getBigDecimalValue() {
        try {
            return parser.getDecimalValue();
        } catch (final IOException e) {
            throw new ExceptionWrapper(e);
        }
    }

    @Override
    public float getFloatValue() {
        try {
            return parser.getFloatValue();
        } catch (final IOException e) {
            throw new ExceptionWrapper(e);
        }
    }

    @Override
    public double getDoubleValue() {
        try {
            return parser.getDoubleValue();
        } catch (final IOException e) {
            throw new ExceptionWrapper(e);
        }
    }

    @Override
    public AbstractJsonParser close() {
        try {
            parser.close();
        } catch (final IOException e) {
            throw new ExceptionWrapper(e);
        }
        return this;
    }
}