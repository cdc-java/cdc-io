package cdc.io.json;

import java.math.BigDecimal;

public class JsonParserWrapper extends AbstractJsonParser {
    private final AbstractJsonParser delegate;

    public JsonParserWrapper(AbstractJsonParser delegate) {
        this.delegate = delegate;
    }

    public AbstractJsonParser getDelegate() {
        return delegate;
    }

    @Override
    public final JsonEvent getEvent() {
        return delegate.getEvent();
    }

    @Override
    public final boolean hasNext() {
        return delegate.hasNext();
    }

    @Override
    public JsonParserWrapper next() {
        delegate.next();
        return this;
    }

    @Override
    public final String getKeyName() {
        return delegate.getKeyName();
    }

    @Override
    public final String getStringValue() {
        return delegate.getStringValue();
    }

    @Override
    public final int getIntValue() {
        return delegate.getIntValue();
    }

    @Override
    public final long getLongValue() {
        return delegate.getLongValue();
    }

    @Override
    public final BigDecimal getBigDecimalValue() {
        return delegate.getBigDecimalValue();
    }

    @Override
    public final float getFloatValue() {
        return delegate.getFloatValue();
    }

    @Override
    public final double getDoubleValue() {
        return delegate.getDoubleValue();
    }

    @Override
    public JsonParserWrapper close() {
        delegate.close();
        return this;
    }
}