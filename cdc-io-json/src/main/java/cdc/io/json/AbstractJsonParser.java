package cdc.io.json;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.util.lang.InvalidDataException;
import cdc.util.lang.Procedure;
import cdc.util.strings.StringUtils;

/**
 * Abstract wrapper for different JSON implementations.
 *
 * @author Damien Carbonne
 */
public abstract class AbstractJsonParser {
    protected final Logger logger = LogManager.getLogger(getClass());
    private int level = 0;

    protected AbstractJsonParser() {
        super();
    }

    private String indent() {
        return StringUtils.toString(' ', 2 * level);
    }

    protected void updateIndentAndTrace() {
        final JsonEvent event = getEvent();
        if (event == JsonEvent.START_ARRAY || event == JsonEvent.START_OBJECT) {
            trace();
            level++;
        } else if (event == JsonEvent.END_ARRAY || event == JsonEvent.END_OBJECT) {
            level--;
            trace();
        } else {
            trace();
        }
    }

    public final Logger getLogger() {
        return logger;
    }

    protected JsonException unexpecteState(String message) {
        return new JsonException(message);
    }

    protected JsonException unexpectedKeyName(String keyName) {
        return new JsonException("Unexpected key name: '" + keyName + "'");
    }

    protected final String getState() {
        final StringBuilder builder = new StringBuilder();
        builder.append(getEvent());
        if (getEvent() != null) {
            switch (getEvent()) {
            case KEY_NAME:
                builder.append(" '")
                       .append(getKeyName())
                       .append("'");
                break;
            case VALUE_STRING:
                builder.append(' ')
                       .append(getStringValue());
                break;

            case VALUE_NUMBER_INTEGRAL:
                builder.append(' ')
                       .append(getLongValue());
                break;
            case VALUE_NUMBER_DECIMAL:
                builder.append(' ')
                       .append(getDoubleValue());
                break;
            default:
                break;
            }
        }
        return builder.toString();
    }

    public AbstractJsonParser trace(String message) {
        if (logger.isTraceEnabled()) {
            logger.trace("{}: {}{}", message, indent(), getState());
        }
        return this;
    }

    public AbstractJsonParser trace() {
        return trace("located on");
    }

    /**
     * @return The current event.
     */
    public abstract JsonEvent getEvent();

    /**
     * @return {@code true} if there are more parsing events,
     *         and {@link #next()} can be called. It returns {@code false}
     *         if the end of the text to parse has been reached.
     */
    public abstract boolean hasNext();

    /**
     * Moves to next event.
     *
     * @return This parser.
     */
    public abstract AbstractJsonParser next();

    public abstract String getKeyName();

    public final boolean getBooleanValue() {
        if (isOn(JsonEvent.VALUE_FALSE)) {
            return false;
        } else if (isOn(JsonEvent.VALUE_TRUE)) {
            return true;
        } else {
            throw unexpecteState("Not a booolean");
        }
    }

    public abstract String getStringValue();

    public final <E extends Enum<E>> E getEnumValue(Class<E> cls) {
        final String name = getStringValue();
        return Enum.valueOf(cls, name);
    }

    public abstract int getIntValue();

    public abstract long getLongValue();

    public abstract BigDecimal getBigDecimalValue();

    public abstract float getFloatValue();

    public abstract double getDoubleValue();

    public abstract AbstractJsonParser close();

    /**
     * Returns {@code true} if current event is a particular one.
     *
     * @param event The tested event.
     * @return {@code true} if current event is {@code event}.
     */
    public final boolean isOn(JsonEvent event) {
        return getEvent() == event;
    }

    /**
     * Checks that current event is the expected one.
     *
     * @param expected The expected event.
     * @return This object.
     * @throws InvalidDataException When current event is not {@code expected}.
     */
    public AbstractJsonParser expect(JsonEvent expected) {
        if (getEvent() != expected) {
            throw new InvalidDataException("Unexpected event: " + getEvent() + ", expected: " + expected);
        }
        return this;
    }

    /**
     * Checks that current event is {@link JsonEvent#KEY_NAME} and the name is an expected one.
     *
     * @param expectedName The expected name.
     * @return This object.
     * @throws InvalidDataException When current event is not {@link JsonEvent#KEY_NAME}
     *             and name is not {@code expectedName}.
     */
    public AbstractJsonParser expectKeyName(String expectedName) {
        expect(JsonEvent.KEY_NAME);
        if (!expectedName.equals(getStringValue())) {
            throw new InvalidDataException("Unexpected key name: " + getStringValue() + ", expected: " + expectedName);
        }
        return this;
    }

    /**
     * If this current event is {@link JsonEvent#START_ARRAY} or
     * {@link JsonEvent#START_OBJECT}, move to corresponding
     * {@link JsonEvent#END_ARRAY} or {@link JsonEvent#END_OBJECT}.
     *
     * @return This object.
     */
    public AbstractJsonParser skipToEnd() {
        final JsonEvent event = getEvent();
        if (event == JsonEvent.START_ARRAY || event == JsonEvent.START_OBJECT) {
            int depth = 1;
            while (depth > 0) {
                next();
                switch (getEvent()) {
                case END_ARRAY:
                case END_OBJECT:
                    depth--;
                    break;
                case START_ARRAY:
                case START_OBJECT:
                    depth++;
                    break;
                default:
                    break;
                }
            }
        }
        return this;
    }

    /**
     * Parses an array.
     * <p>
     * Parser must be located on a {@link JsonEvent#START_ARRAY} event.<br>
     * In the end, parser is located on the corresponding {@link JsonEvent#END_ARRAY}.
     *
     * @param <T> The array element type.
     * @param itemParser The function used to parse each array item.
     * @param itemConsumer The item consumer.
     */
    public <T> void parseArray(Function<AbstractJsonParser, T> itemParser,
                               Consumer<T> itemConsumer) {
        expect(JsonEvent.START_ARRAY);
        while (hasNext()) {
            next();
            if (isOn(JsonEvent.END_ARRAY)) {
                return;
            } else {
                final T item = itemParser.apply(this);
                itemConsumer.accept(item);
            }
        }
        throw new JsonException("Failed to parse array.");
    }

    /**
     * Parses an array.
     * <p>
     * Parser must be located on a {@link JsonEvent#START_ARRAY} event.<br>
     * In the end, parser is located on the corresponding {@link JsonEvent#END_ARRAY}.
     *
     * @param <T> The array element type.
     * @param itemParser The function used to parse each array item.
     * @return A list of items.
     */
    public <T> List<T> parseArray(Function<AbstractJsonParser, T> itemParser) {
        final List<T> list = new ArrayList<>();
        parseArray(itemParser, list::add);
        return list;
    }

    public <T> void parseArray(Supplier<T> itemParser,
                               Consumer<T> itemConsumer) {
        expect(JsonEvent.START_ARRAY);
        while (hasNext()) {
            next();
            if (isOn(JsonEvent.END_ARRAY)) {
                return;
            } else {
                final T item = itemParser.get();
                itemConsumer.accept(item);
            }
        }
        throw new JsonException("Failed to parse array.");
    }

    public <T> List<T> parseArray(Supplier<T> itemParser) {
        final List<T> list = new ArrayList<>();
        parseArray(itemParser, list::add);
        return list;
    }

    public void parseArray(Consumer<AbstractJsonParser> itemParserAndConsumer) {
        expect(JsonEvent.START_ARRAY);
        while (hasNext()) {
            next();
            if (isOn(JsonEvent.END_ARRAY)) {
                return;
            } else {
                itemParserAndConsumer.accept(this);
            }
        }
        throw new JsonException("Failed to parse array.");
    }

    public void parseArray(Procedure itemParserAndConsumer) {
        final Consumer<AbstractJsonParser> x =
                parser -> itemParserAndConsumer.invoke();
        parseArray(x);
    }

    /**
     * Parses an object.
     * <p>
     * Parser must be located on a {@link JsonEvent#START_OBJECT} event.<br>
     * In the end, parser is located on the corresponding {@link JsonEvent#END_OBJECT}.
     *
     * @param fieldParserAndConsumer The function that will parse and consume each field.<br>
     *            When a field is not recognized, it should return a {@code null} consumer.<bR>
     *            Each consumer should parse and consume the field value.
     */
    public void parseObject(Function<String, Consumer<AbstractJsonParser>> fieldParserAndConsumer) {
        expect(JsonEvent.START_OBJECT);
        while (hasNext()) {
            next();
            if (isOn(JsonEvent.END_OBJECT)) {
                return;
            } else {
                expect(JsonEvent.KEY_NAME);
                final String keyName = getKeyName();
                next();
                final Consumer<AbstractJsonParser> consumer = fieldParserAndConsumer.apply(keyName);
                if (consumer == null) {
                    throw unexpectedKeyName(keyName);
                } else {
                    consumer.accept(this);
                }
            }
        }
        throw new JsonException("Failed to parse object.");
    }

    public void parseObject(Consumer<String> fieldParserAndConsumer) {
        expect(JsonEvent.START_OBJECT);
        while (hasNext()) {
            next();
            if (isOn(JsonEvent.END_OBJECT)) {
                return;
            } else {
                expect(JsonEvent.KEY_NAME);
                final String keyName = getKeyName();
                next();
                fieldParserAndConsumer.accept(keyName);
            }
        }
        throw new JsonException("Failed to parse object.");
    }

    public static interface ConsumerSupplier<S, R> extends Consumer<S>, Supplier<R> {
        // Ignore
    }

    public <T> T parseObject(ConsumerSupplier<String, T> fieldParserConsumerSupplier) {
        parseObject(fieldParserConsumerSupplier);
        return fieldParserConsumerSupplier.get();
    }
}