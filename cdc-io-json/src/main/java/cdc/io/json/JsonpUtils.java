package cdc.io.json;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import javax.json.Json;
import javax.json.stream.JsonGenerator;
import javax.json.stream.JsonGeneratorFactory;
import javax.json.stream.JsonParser;

import cdc.util.lang.ExceptionWrapper;
import cdc.util.lang.InvalidDataException;

public final class JsonpUtils {
    private JsonpUtils() {
    }

    public static final Map<String, Object> PRETTY_CONFIG;

    static {
        final Map<String, Object> config = new HashMap<>();
        config.put(JsonGenerator.PRETTY_PRINTING, Boolean.TRUE);
        PRETTY_CONFIG = Collections.unmodifiableMap(config);
    }

    public static void expect(JsonParser.Event event,
                              JsonParser.Event expected) {
        if (event != expected) {
            throw new InvalidDataException("Unexpected event: " + event + ", expected: " + expected);
        }
    }

    /**
     * Creates a string showing a JsonParser state.
     *
     * @param parser The parser.
     * @param event The current event.<br>
     *            <b>WARNING:</b> It must be compliant with the real parser state.
     * @return A string representing {@code parser} and {@code event}.
     */
    public static String toString(JsonParser parser,
                                  JsonParser.Event event) {
        final StringBuilder builder = new StringBuilder();
        builder.append(event);
        if (event != null) {
            switch (event) {
            case KEY_NAME:
            case VALUE_STRING:
                builder.append(" '")
                       .append(parser.getString())
                       .append("'");
                break;
            case VALUE_NUMBER:
                builder.append(' ')
                       .append(parser.getBigDecimal());
                break;
            default:
                break;
            }
        }
        return builder.toString();
    }

    /**
     * Creates a JsonGenerator using a StringWriter, pass it to a Consumer and returns the corresponding string.
     *
     * @param consumer The Consumer.
     * @return The string built by {@code consumer}.
     */
    public static String toString(Consumer<JsonGenerator> consumer) {
        final JsonGeneratorFactory factory = Json.createGeneratorFactory(null);
        try (final StringWriter writer = new StringWriter();
                final JsonGenerator generator = factory.createGenerator(writer)) {
            consumer.accept(generator);
            generator.flush();
            return writer.toString();
        } catch (final IOException e) {
            // This should not happen.
            // StringWriter should not throw IOException
            throw new ExceptionWrapper(e);
        }
    }
}