package cdc.io.json;

/**
 * Enumeration of possible JSON events.
 *
 * @author Damien Carbonne
 */
public enum JsonEvent {
    /** Used when '{' has be encountered to signal starting of an object value. */
    START_OBJECT,
    /** Used when '}' has be encountered to signal ending of an object value. */
    END_OBJECT,
    /** Used when '[' has be encountered to signal starting of an array value. */
    START_ARRAY,
    /** Used when ']' has be encountered to signal ending of an array value. */
    END_ARRAY,
    /** Used when a string has be encountered as a key/field name. */
    KEY_NAME,
    /** Used when a string has be encountered in a value context. */
    VALUE_STRING,
    /** Used when an integer number has be encountered in a value context. */
    VALUE_NUMBER_INTEGRAL,
    /** Used when a non-integer number has be encountered in a value context. */
    VALUE_NUMBER_DECIMAL,
    /** Used when literal "false" has be encountered in a value context. */
    VALUE_FALSE,
    /** Used when literal "true" has be encountered in a value context. */
    VALUE_TRUE,
    /** Used when literal "null" has be encountered in a value context. */
    VALUE_NULL,
    /** Used when end of stream or any unrecognized token has been reached. */
    OTHER
}