package cdc.io.demos;

import java.io.File;
import java.io.IOException;

import org.apache.commons.compress.archivers.ArchiveException;

import cdc.io.compress.SevenZUtils;

public final class SevenZUtilsDemo {
    private SevenZUtilsDemo() {
    }

    public static void main(String[] args) throws IOException, ArchiveException {
        SevenZUtils.compress(new File("target/test.7z"),
                             false,
                             new File("src/main/java/cdc/io/compress/Archiver.java"));

        SevenZUtils.compress(new File("target/test1.7z"),
                             false,
                             new File("src/main/java/cdc/io/compress"));
        SevenZUtils.compress(new File("target/test2.7z"),
                             true,
                             new File("src/main/java/cdc/io/compress"));
        SevenZUtils.compress(new File("target/test3.7z"),
                             false,
                             new File("src/main/java/cdc/io"));
        SevenZUtils.compress(new File("target/test4.7z"),
                             true,
                             new File("src/main/java/cdc/io"));

        SevenZUtils.decompress(new File("target/test.7z"),
                               new File("target/test_7z"));

        final org.apache.commons.compress.archivers.examples.Archiver x =
                new org.apache.commons.compress.archivers.examples.Archiver();
        x.create("7z", new File("target/test5.7z"), new File("src/main/java/cdc/io"));
    }
}