package cdc.io.demos;

import java.io.File;
import java.io.IOException;

import cdc.io.compress.ZipUtils;

public final class ZipUtilsDemo {
    private ZipUtilsDemo() {
    }

    public static void main(String[] args) throws IOException {
        ZipUtils.compress(new File("target/test.zip"),
                          false,
                          new File("src/main/java/cdc/io/compress/Archiver.java"));

        ZipUtils.compress(new File("target/test1.zip"),
                          false,
                          new File("src/main/java/cdc/io/compress"));
        ZipUtils.compress(new File("target/test2.zip"),
                          true,
                          new File("src/main/java/cdc/io/compress"));
        ZipUtils.compress(new File("target/test3.zip"),
                          false,
                          new File("src/main/java/cdc/io"));
        ZipUtils.compress(new File("target/test4.zip"),
                          true,
                          new File("src/main/java/cdc/io"));

        ZipUtils.decompress(new File("target/test.zip"),
                            new File("target/test_zip"));
    }
}