package cdc.io.demos;

import java.io.IOException;
import java.net.URL;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.io.data.Document;
import cdc.io.data.Element;
import cdc.io.data.Text;
import cdc.io.data.TextKind;
import cdc.io.data.xml.XmlDataWriter;
import cdc.io.utils.NonCloseableOutputStream;
import cdc.io.xml.XmlWriter;
import cdc.util.debug.Debug;
import cdc.util.files.Resources;

public final class XmlDataWriterDemo {
    private static final Logger LOGGER = LogManager.getLogger(XmlDataWriterDemo.class);

    private XmlDataWriterDemo() {
    }

    public static void main(String[] args) throws IOException {
        LOGGER.info("Start {}", XmlDataWriterDemo.class.getSimpleName());
        Debug.printClassPath();
        for (final URL url : Resources.getResources("log4j2.xml")) {
            LOGGER.info("   {}", url);
        }
        final Document doc = new Document();
        final Element root = new Element("root");
        final Text text1 = new Text("text1");
        final Text text2 = new Text("text2").setKind(TextKind.CDATA);
        final Text text3 = new Text("text3");
        doc.addComment("Hello");
        doc.addChild(root);
        doc.addComment("Hello");
        root.addAttribute("att1", "value1");
        final Element child = new Element("child");
        child.addAttribute("att1", "value1");
        child.addChild(text1);
        child.addChild(text2);
        child.addChild(text3);
        root.addChild(child);

        doc.removeChildren();
        doc.addComment("Hello");
        doc.addChild(root);
        doc.addComment("Hello");

        try (final XmlDataWriter writer = new XmlDataWriter(NonCloseableOutputStream.NON_CLOSABLE_SYSTEM_OUT)) {
            writer.getXmlWriter().setEnabled(XmlWriter.Feature.PRETTY_PRINT, true);
            writer.write(doc);
            writer.flush();
        }
    }
}