package cdc.io.demos;

import java.io.File;
import java.io.IOException;

import cdc.io.data.paths.SPath;
import cdc.io.tools.XmlNormalizer;

public final class XmlNormalizerDemo {
    private XmlNormalizerDemo() {
    }

    public static void main(String[] args) throws IOException {
        final XmlNormalizer.MainArgs margs = new XmlNormalizer.MainArgs();
        margs.indentString = " ";
        margs.inputFile = new File("src/test/resources/test-reader.xml");
        margs.setEnabled(XmlNormalizer.MainArgs.Feature.PRETTY_PRINT, true);
        margs.setEnabled(XmlNormalizer.MainArgs.Feature.APPEND_FINAL_EOL, true);

        XmlNormalizer.execute(margs);

        margs.setEnabled(XmlNormalizer.MainArgs.Feature.DELAYED_PROCESSING, true);
        XmlNormalizer.execute(margs);

        margs.setEnabled(XmlNormalizer.MainArgs.Feature.PRETTY_PRINT, true);

        XmlNormalizer.execute(margs);

        margs.setEnabled(XmlNormalizer.MainArgs.Feature.REMOVE_COMMENTS, true);
        margs.setEnabled(XmlNormalizer.MainArgs.Feature.REMOVE_EMPTY_ATTRIBUTES, true);
        margs.setEnabled(XmlNormalizer.MainArgs.Feature.REMOVE_PURE_ELEMENTS, true);
        margs.setEnabled(XmlNormalizer.MainArgs.Feature.SORT_ATTRIBUTES, true);
        margs.setEnabled(XmlNormalizer.MainArgs.Feature.SORT_ELEMENTS, true);
        margs.setEnabled(XmlNormalizer.MainArgs.Feature.SCRAMBLE_ATTRIBUTES, false);
        margs.setEnabled(XmlNormalizer.MainArgs.Feature.SCRAMBLE_ELEMENTS, false);
        margs.firstAttributes.add("id");
        margs.removed.add(new SPath("to-remove"));
        margs.removed.add(new SPath("@to-remove"));

        XmlNormalizer.execute(margs);

        margs.setEnabled(XmlNormalizer.MainArgs.Feature.DELAYED_PROCESSING, true);
        XmlNormalizer.execute(margs);

        margs.renamed.put(new SPath("@id"), s -> "ID");
        margs.renamed.put(new SPath("element"), s -> "ELEMENT");
        XmlNormalizer.execute(margs);
    }
}