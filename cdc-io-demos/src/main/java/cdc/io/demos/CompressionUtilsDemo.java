package cdc.io.demos;

import java.io.File;
import java.io.IOException;

import cdc.io.compress.Archiver;
import cdc.io.compress.CompressionUtils;

public final class CompressionUtilsDemo {
    private CompressionUtilsDemo() {
    }

    public static void main(String[] args) throws IOException {
        CompressionUtils.compress(new File("target/test.zip"),
                                 Archiver.ZIP,
                                 new File("src/test/java"));
        CompressionUtils.compress(new File("target/.classpath.zip"),
                                 Archiver.ZIP,
                                 new File(".classpath"));
        CompressionUtils.compress(new File("target/test.7z"),
                                 Archiver.SEVEN_Z,
                                 new File("src/test/java"));
    }
}