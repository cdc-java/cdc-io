package cdc.io.demos;

import java.io.IOException;
import java.io.InputStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.io.txt.LinesHandler;
import cdc.io.utils.ProgressControllerInputStream;
import cdc.util.events.ProgressController;
import cdc.util.events.ProgressEvent;
import cdc.util.function.Evaluation;

public final class ProgressControllerInputStreamDemo {
    static final Logger LOGGER = LogManager.getLogger(ProgressControllerInputStreamDemo.class);

    private ProgressControllerInputStreamDemo() {
    }

    public static void main(String[] args) throws IOException {
        final ProgressController controller = new ProgressController() {
            @Override
            public void onProgress(ProgressEvent event) {
                LOGGER.info("onProgress({})", event);
            }

            @Override
            public boolean isCancelled() {
                return false;
            }
        };

        @SuppressWarnings("unused")
        final LinesHandler handler = new LinesHandler() {
            @Override
            public Evaluation processLine(String line,
                                          int number) {
                return Evaluation.CONTINUE;
            }

            @Override
            public void processEnd() {
                LOGGER.info("processEnd()");
            }

            @Override
            public void processBegin() {
                LOGGER.info("processBegin()");
            }
        };

        if (args.length > 0) {
            final String filename = args[0];
            try (final InputStream in = new ProgressControllerInputStream(filename, controller)) {
                while (in.read() >= 0) {
                    // Ignore
                }
            }
        }
    }
}