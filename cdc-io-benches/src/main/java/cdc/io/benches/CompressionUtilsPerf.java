package cdc.io.benches;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.Locale;

import cdc.io.compress.CompressionUtils;
import cdc.io.compress.Compressor;
import cdc.util.time.Chronometer;

public final class CompressionUtilsPerf {
    private static final String ROOT = "root";
    private static final String CHILD = "child";
    private static final String INDENT = "  ";

    private CompressionUtilsPerf() {
    }

    private static File generate(int count) throws IOException {
        final File file = File.createTempFile(CompressionUtilsPerf.class.getCanonicalName(), ".xml");
        try (final OutputStream os = new BufferedOutputStream(new FileOutputStream(file));
                final Writer writer = new OutputStreamWriter(os, StandardCharsets.UTF_8)) {
            writer.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n");
            writer.append("<");
            writer.append(ROOT);
            writer.append(">\n");
            for (int index = 0; index < count; index++) {
                writer.append(INDENT);
                writer.append("<");
                writer.append(CHILD);
                writer.append("/>\n");
            }
            writer.append("</");
            writer.append(ROOT);
            writer.append(">");
            writer.flush();
        }
        return file;
    }

    private static File compress(Compressor compressor,
                                 File file) throws IOException {
        final Chronometer chrono = new Chronometer();
        final File result = new File(file.getPath() + "." + compressor.getExtension());
        chrono.start();
        try (final InputStream is = new BufferedInputStream(new FileInputStream(file));
                final OutputStream os =
                        CompressionUtils.adapt(new BufferedOutputStream(new FileOutputStream(result)), compressor)) {
            int b;
            while ((b = is.read()) != -1) {
                os.write(b);
            }
            os.flush();
        }
        chrono.suspend();
        final double seconds = chrono.getElapsedSeconds();
        System.out.println(String.format(Locale.ENGLISH,
                                         "%10s %6.3f s %,12d B %,12d B %6.1f %% %,10d kB/s",
                                         compressor,
                                         seconds,
                                         file.length(),
                                         result.length(),
                                         100.0 * result.length() / file.length(),
                                         (int) (file.length() / seconds / 1000.0)));

        return result;
    }

    @SuppressWarnings("unused")
    private static void test1() throws IOException {
        final Compressor[] compressors = {
                Compressor.BZIP2,
                Compressor.GZIP,
                Compressor.LZMA,
                Compressor.XZ
        };
        for (final Compressor compressor : compressors) {
            int count = 1;
            for (int i = 0; i < 24; i++) {
                final File sample = generate(count);
                final File compressed = compress(compressor, sample);
                count *= 2;
                sample.delete();
                compressed.delete();
            }
        }
    }

    private static void test2(File sample) throws IOException {
        final Compressor[] compressors = {
                Compressor.BZIP2,
                Compressor.GZIP,
                Compressor.LZMA,
                Compressor.XZ
        };
        for (final Compressor compressor : compressors) {
            final File compressed = compress(compressor, sample);
            compressed.delete();
        }
    }

    public static void main(String[] args) throws IOException {
        // test1();

        test2(new File("target/z100k.xml"));
        test2(new File("target/z200k.xml"));
        test2(new File("target/z400k.xml"));
        test2(new File("target/z800k.xml"));
    }
}
