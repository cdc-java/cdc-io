package cdc.io.html;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.util.lang.Checks;

final class HtmlWriterContext {
    private static final Logger LOGGER = LogManager.getLogger(HtmlWriterContext.class);

    public enum Type {
        IN_OPEN_STREAM,
        IN_TAG,
        IN_CONTENT,
        IN_CLOSED_STREAM
    }

    public enum ContentType {
        EMPTY,
        TEXT
    }

    private Type type;
    private ContentType contentType;
    private Tag tag;
    private final HtmlWriterContext parent;
    private HtmlWriterContext child = null;

    private HtmlWriterContext(Type type,
                              HtmlWriterContext parent) {
        this.type = type;
        this.parent = parent;
        if (parent != null) {
            parent.child = this;
        }
    }

    HtmlWriterContext() {
        this(Type.IN_OPEN_STREAM, null);
    }

    HtmlWriterContext pushContext(Type type) {
        LOGGER.trace("pushContext({})", type);
        if (child == null) {
            return new HtmlWriterContext(type, this);
        } else {
            child.setType(type);
            return child;
        }
    }

    HtmlWriterContext popContext() {
        LOGGER.trace("popContext()");
        type = null;
        contentType = null;
        tag = null;
        return parent;
    }

    Type getType() {
        return type;
    }

    void setType(Type type) {
        LOGGER.trace("setType({})", type);
        this.type = type;
        if (type == Type.IN_CONTENT) {
            this.contentType = ContentType.EMPTY;
        } else {
            this.contentType = null;
        }
    }

    void setContentType(ContentType contentType) {
        LOGGER.trace("setContentType({})", contentType);
        Checks.assertTrue(this.type == Type.IN_CONTENT, "invalid type");
        this.contentType = contentType;
    }

    ContentType getContentType() {
        return contentType;
    }

    void setTag(Tag tag) {
        LOGGER.trace("setTag({})", tag);
        this.tag = tag;
    }

    Tag getTag() {
        return tag;
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append(type);
        if (tag != null) {
            builder.append(' ').append(tag);
        }
        if (contentType != null) {
            builder.append(' ').append(contentType);
        }
        return builder.toString();
    }

    private void print() {
        if (parent != null) {
            parent.print();
        }
        LOGGER.trace("   {}", this);
    }

    public void print(String message) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("---------------------");
            LOGGER.trace(message);
            print();
        }
    }
}