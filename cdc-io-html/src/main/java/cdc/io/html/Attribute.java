package cdc.io.html;

/**
 * Description of an HTML attribute.
 *
 * @author Damien Carbonne
 */
public class Attribute {
    private final String name;

    public static final Attribute CLASS = builder("class").build();
    public static final Attribute CONTENT = builder("content").build();
    public static final Attribute HREF = builder("href").build();
    public static final Attribute HTTP_EQUIV = builder("http-equiv").build();
    public static final Attribute NAME = builder("name").build();
    public static final Attribute REL = builder("rel").build();
    public static final Attribute ROLE = builder("role").build();
    public static final Attribute SIZES = builder("sizes").build();
    public static final Attribute SRC = builder("src").build();
    public static final Attribute STYLE = builder("style").build();
    public static final Attribute TARGET = builder("target").build();
    public static final Attribute TYPE = builder("type").build();
    public static final Attribute USEMAP = builder("usemap").build();

    private Attribute(String label) {
        this.name = label;
    }

    public String getName() {
        return name;
    }

    public static Builder builder(String name) {
        return new Builder(name);
    }

    public static class Builder {
        private final String name;

        Builder(String name) {
            this.name = name;
        }

        public Attribute build() {
            return new Attribute(name);
        }
    }
}