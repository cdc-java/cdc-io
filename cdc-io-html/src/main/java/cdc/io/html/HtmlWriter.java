package cdc.io.html;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.Flushable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.io.html.HtmlWriterContext.ContentType;
import cdc.io.html.HtmlWriterContext.Type;
import cdc.io.utils.NonCloseableOutputStream;
import cdc.util.lang.InvalidStateException;

public class HtmlWriter implements Flushable, Closeable {
    private static final Logger LOGGER = LogManager.getLogger(HtmlWriter.class);
    private final Writer out;
    private final String encoding;
    private HtmlWriterContext context = new HtmlWriterContext();
    private int level = -1;
    private String indent = "\t";
    private static final String EOL = System.lineSeparator();
    private static final String GT = "&gt;";
    private static final String LT = "&lt;";
    private static final String QUOT = "&quot;";
    private static final String APOS = "&apos;";
    private static final String AMP = "&amp;";

    private HtmlWriter(OutputStream os,
                       String encoding,
                       boolean wrap)
            throws IOException {
        this.encoding = encoding;
        if (wrap) {
            out = new BufferedWriter(new OutputStreamWriter(new NonCloseableOutputStream(os), encoding));
        } else {
            out = new BufferedWriter(new OutputStreamWriter(os, encoding));
        }

        out.append("<!DOCTYPE html>");
        eol();
    }

    private void stateError(String message) {
        try {
            out.flush();
        } catch (final IOException e) {
            LOGGER.catching(e);
        }
        throw new InvalidStateException("Invalid state: " + context.getType() + ", when calling: " + message);
    }

    private void indent(int delta) throws IOException {
        for (int i = 0; i < level + delta; i++) {
            out.append(indent);
        }
    }

    private void indent() throws IOException {
        indent(0);
    }

    private void eol() throws IOException {
        out.append(EOL);
    }

    private void writeEscaped(String s) throws IOException {
        if (s != null) {
            final int length = s.length();
            int index = 0;
            while (index < length) {
                final int codePoint = s.codePointAt(index);
                switch (codePoint) {
                case '>':
                    // In fact, never needs to be escaped !
                    out.append(GT);
                    break;

                case '<':
                    out.append(LT);
                    break;

                case '&':
                    out.append(AMP);
                    break;

                case '\'':
                    out.append(APOS);
                    break;

                case '"':
                    out.append(QUOT);
                    break;

                default:
                    out.write(codePoint);
                    break;
                }
                index += Character.charCount(codePoint);
            }
        }
    }

    public HtmlWriter(File file,
                      String encoding)
            throws IOException {
        this(new FileOutputStream(file), encoding, false);
    }

    public HtmlWriter(File file) throws IOException {
        this(file, StandardCharsets.UTF_8.name());
    }

    public String getEncoding() {
        return encoding;
    }

    public HtmlWriter setIndentString(String s) {
        this.indent = s;
        return this;
    }

    public String getIndentString() {
        return indent;
    }

    public HtmlWriter start(Tag tag) throws IOException {
        context.print("start");
        level++;
        switch (context.getType()) {
        case IN_TAG:
            out.append('>');
            if (!tag.isInline()) {
                eol();
                indent();
            }
            context.setType(Type.IN_CONTENT);
            context = context.pushContext(Type.IN_TAG);
            context.setTag(tag);
            break;
        case IN_CONTENT:
            if (!tag.isInline()) {
                eol();
                indent();
            }
            context = context.pushContext(Type.IN_TAG);
            context.setTag(tag);
            break;
        case IN_OPEN_STREAM:
            context = context.pushContext(Type.IN_TAG);
            context.setTag(tag);
            break;
        default:
            stateError("start()");
            break;
        }
        out.append('<')
           .append(tag.getName());
        return this;
    }

    public HtmlWriter end() throws IOException {
        context.print("end");
        switch (context.getType()) {
        case IN_TAG:
            out.append('>');
            if (!context.getTag().isVoid()) {
                out.append("</")
                   .append(context.getTag().getName())
                   .append('>');
            }
            context = context.popContext();
            if (context.getType() == Type.IN_OPEN_STREAM) {
                context.setType(Type.IN_CLOSED_STREAM);
            }
            break;
        case IN_CONTENT:
            if (context.getContentType() == ContentType.EMPTY) {
                eol();
                indent();
            }
            out.append("</")
               .append(context.getTag().getName())
               .append('>');
            context = context.popContext();
            if (context.getType() == Type.IN_OPEN_STREAM) {
                context.setType(Type.IN_CLOSED_STREAM);
            }
            break;
        default:
            stateError("end()");
            break;
        }
        level--;
        return this;
    }

    public HtmlWriter attr(Attribute att,
                           String value) throws IOException {
        context.print("att");
        if (context.getType() == Type.IN_TAG) {
            out.append(' ')
               .append(att.getName())
               .append("=\"");
            writeEscaped(value);
            out.append('\"');
        } else {
            stateError("att()");
        }
        return this;
    }

    public HtmlWriter text(String s) throws IOException {
        context.print("text");
        switch (context.getType()) {
        case IN_TAG:
            out.append('>');
            context.setType(Type.IN_CONTENT);
            context.setContentType(ContentType.TEXT);
            break;
        case IN_CONTENT:
            context.setContentType(ContentType.TEXT);
            break;
        default:
            stateError("text()");
            break;
        }
        writeEscaped(s);
        return this;
    }

    public HtmlWriter text(long x) throws IOException {
        text(Long.toString(x));
        return this;
    }

    public HtmlWriter text(int x) throws IOException {
        text(Integer.toString(x));
        return this;
    }

    public HtmlWriter text(short x) throws IOException {
        text(Short.toString(x));
        return this;
    }

    public HtmlWriter text(byte x) throws IOException {
        text(Byte.toString(x));
        return this;
    }

    public HtmlWriter text(boolean x) throws IOException {
        text(Boolean.toString(x));
        return this;
    }

    public HtmlWriter text(Object x) throws IOException {
        text(x.toString());
        return this;
    }

    public HtmlWriter include(String s,
                              boolean indent,
                              int delta) throws IOException {
        if (indent) {
            final String[] lines = s.split("\\r?\\n");
            for (final String line : lines) {
                eol();
                indent(delta);
                out.append(line);
            }
        } else {
            out.append(s);
        }
        return this;
    }

    public HtmlWriter include(File file,
                              Charset charset,
                              boolean indent,
                              int delta) throws IOException {
        try (final InputStream is = new BufferedInputStream(new FileInputStream(file));
                final BufferedReader reader = new BufferedReader(new InputStreamReader(is, charset))) {
            String line = null;
            if (indent) {
                while ((line = reader.readLine()) != null) {
                    eol();
                    indent(delta);
                    out.append(line);
                }
            } else {
                while ((line = reader.readLine()) != null) {
                    eol();
                    out.append(line);
                }
            }
        }
        return this;
    }

    public final HtmlWriter include(File file,
                                    boolean indent,
                                    int delta) throws IOException {
        return include(file,
                       Charset.defaultCharset(),
                       indent,
                       delta);
    }

    public HtmlWriter wrap(Tag element,
                           HtmlWriterConsumer consumer) throws IOException {
        start(element);
        consumer.accept(this);
        end();
        return this;
    }

    public HtmlWriter wrap(HtmlWriterConsumer consumer) throws IOException {
        consumer.accept(this);
        return this;
    }

    public HtmlWriter a() throws IOException {
        return start(Tag.A);
    }

    public HtmlWriter body() throws IOException {
        return start(Tag.BODY);
    }

    public HtmlWriter br() throws IOException {
        return start(Tag.BR);
    }

    public HtmlWriter div() throws IOException {
        return start(Tag.DIV);
    }

    public HtmlWriter h1() throws IOException {
        return start(Tag.H1);
    }

    public HtmlWriter h2() throws IOException {
        return start(Tag.H2);
    }

    public HtmlWriter h3() throws IOException {
        return start(Tag.H3);
    }

    public HtmlWriter h4() throws IOException {
        return start(Tag.H4);
    }

    public HtmlWriter h5() throws IOException {
        return start(Tag.H5);
    }

    public HtmlWriter h6() throws IOException {
        return start(Tag.H6);
    }

    public HtmlWriter head() throws IOException {
        return start(Tag.HEAD);
    }

    public HtmlWriter header() throws IOException {
        return start(Tag.HEADER);
    }

    public HtmlWriter html() throws IOException {
        return start(Tag.HTML);
    }

    public HtmlWriter iframe() throws IOException {
        return start(Tag.IFRAME);
    }

    public HtmlWriter img() throws IOException {
        return start(Tag.IMG);
    }

    public HtmlWriter li() throws IOException {
        return start(Tag.LI);
    }

    public HtmlWriter link() throws IOException {
        return start(Tag.LINK);
    }

    public HtmlWriter main() throws IOException {
        return start(Tag.MAIN);
    }

    public HtmlWriter meta() throws IOException {
        return start(Tag.META);
    }

    public HtmlWriter nav() throws IOException {
        return start(Tag.NAV);
    }

    public HtmlWriter ol() throws IOException {
        return start(Tag.OL);
    }

    public HtmlWriter span() throws IOException {
        return start(Tag.SPAN);
    }

    public HtmlWriter strong() throws IOException {
        return start(Tag.STRONG);
    }

    public HtmlWriter table() throws IOException {
        return start(Tag.TABLE);
    }

    public HtmlWriter tbody() throws IOException {
        return start(Tag.TBODY);
    }

    public HtmlWriter td() throws IOException {
        return start(Tag.TD);
    }

    public HtmlWriter th() throws IOException {
        return start(Tag.TH);
    }

    public HtmlWriter thead() throws IOException {
        return start(Tag.THEAD);
    }

    public HtmlWriter title() throws IOException {
        return start(Tag.TITLE);
    }

    public HtmlWriter tr() throws IOException {
        return start(Tag.TR);
    }

    public HtmlWriter ul() throws IOException {
        return start(Tag.UL);
    }

    public HtmlWriter attrClass(String value) throws IOException {
        return attr(Attribute.CLASS, value);
    }

    public HtmlWriter attrContent(String value) throws IOException {
        return attr(Attribute.CONTENT, value);
    }

    public HtmlWriter attrHref(String value) throws IOException {
        return attr(Attribute.HREF, value);
    }

    public HtmlWriter attrHttpEquiv(String value) throws IOException {
        return attr(Attribute.HTTP_EQUIV, value);
    }

    public HtmlWriter attrName(String value) throws IOException {
        return attr(Attribute.NAME, value);
    }

    public HtmlWriter attrRel(String value) throws IOException {
        return attr(Attribute.REL, value);
    }

    public HtmlWriter attrRole(String value) throws IOException {
        return attr(Attribute.ROLE, value);
    }

    public HtmlWriter attrSizes(String value) throws IOException {
        return attr(Attribute.SIZES, value);
    }

    public HtmlWriter attrSrc(String value) throws IOException {
        return attr(Attribute.SRC, value);
    }

    public HtmlWriter attrStyle(String value) throws IOException {
        return attr(Attribute.STYLE, value);
    }

    public HtmlWriter attrTarget(String value) throws IOException {
        return attr(Attribute.TARGET, value);
    }

    public HtmlWriter attrType(String value) throws IOException {
        return attr(Attribute.TYPE, value);
    }

    public HtmlWriter attrUsemap(String value) throws IOException {
        return attr(Attribute.USEMAP, value);
    }

    @Override
    public void flush() throws IOException {
        out.flush();
    }

    @Override
    public void close() throws IOException {
        context.print("close");
        out.close();
    }
}