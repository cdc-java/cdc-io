package cdc.io.html;

import java.io.IOException;

@FunctionalInterface
public interface HtmlWriterConsumer {
    public void accept(HtmlWriter writer) throws IOException;
}