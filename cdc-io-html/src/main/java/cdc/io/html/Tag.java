package cdc.io.html;

import java.util.EnumSet;
import java.util.Set;

import cdc.util.lang.IntMasks;

/**
 * Description ofg an HTML tag.
 *
 * @author Damien Carbonne
 */
public class Tag {
    private final String name;
    private final int mask;

    public enum Bit {
        INLINE,
        VOID
    }

    private Tag(String name,
                Set<Bit> bits) {
        this.name = name;
        int m = 0;
        for (final Bit bit : bits) {
            m = m | IntMasks.toMask(bit);
        }
        mask = m;
    }

    public static final Tag A = builder("a").bit(Bit.INLINE).build();
    public static final Tag ABBR = builder("abbr").build();
    public static final Tag ADDRESS = builder("address").build();
    public static final Tag AREA = builder("area").bit(Bit.VOID).build();
    public static final Tag ARTICLE = builder("article").build();
    public static final Tag ASIDE = builder("aside").build();
    public static final Tag AUDIO = builder("audio").build();
    public static final Tag B = builder("b").bit(Bit.INLINE).build();
    public static final Tag BASE = builder("base").bit(Bit.VOID).bit(Bit.INLINE).build();
    public static final Tag BDI = builder("bdi").bit(Bit.INLINE).build();
    public static final Tag BDO = builder("bdo").bit(Bit.INLINE).build();
    public static final Tag BLOCKQUOTE = builder("blockquote").bit(Bit.INLINE).build();
    public static final Tag BODY = builder("body").build();
    public static final Tag BR = builder("br").bit(Bit.VOID).build();
    public static final Tag BUTTON = builder("button").build();
    public static final Tag CANVAS = builder("canvas").build();
    public static final Tag CAPTION = builder("caption").build();
    public static final Tag CITE = builder("cite").build();
    public static final Tag CODE = builder("code").build();
    public static final Tag COL = builder("col").bit(Bit.VOID).build();
    public static final Tag COLGROUP = builder("colgroup").build();
    public static final Tag DATA = builder("data").build();
    public static final Tag DATALIST = builder("datalist").build();
    public static final Tag DD = builder("dd").build();
    public static final Tag DEL = builder("del").build();
    public static final Tag DETAILS = builder("details").build();
    public static final Tag DFN = builder("dfn").build();
    public static final Tag DIALOG = builder("dialog").build();
    public static final Tag DIV = builder("div").build();
    public static final Tag DL = builder("dl").build();
    public static final Tag DT = builder("dt").build();
    public static final Tag EM = builder("em").build();
    public static final Tag EMBED = builder("embed").bit(Bit.VOID).build();
    public static final Tag FIELDSET = builder("fieldset").build();
    public static final Tag FIGCAPTION = builder("figcaption").build();
    public static final Tag FIGURE = builder("figure").build();
    public static final Tag FOOTER = builder("footer").build();
    public static final Tag FORM = builder("form").build();
    public static final Tag H1 = builder("h1").build();
    public static final Tag H2 = builder("h2").build();
    public static final Tag H3 = builder("h3").build();
    public static final Tag H4 = builder("h4").build();
    public static final Tag H5 = builder("h5").build();
    public static final Tag H6 = builder("h6").build();
    public static final Tag HEAD = builder("head").build();
    public static final Tag HEADER = builder("header").build();
    public static final Tag HGROUP = builder("hgroup").build();
    public static final Tag HR = builder("hr").bit(Bit.VOID).build();
    public static final Tag HTML = builder("html").build();
    public static final Tag I = builder("i").bit(Bit.INLINE).build();
    public static final Tag IFRAME = builder("iframe").build();
    public static final Tag IMG = builder("img").bit(Bit.VOID).build();
    public static final Tag INPUT = builder("input").bit(Bit.VOID).build();
    public static final Tag INS = builder("ins").build();
    public static final Tag KBD = builder("kbd").build();
    public static final Tag LABEL = builder("label").build();
    public static final Tag LEGEND = builder("legend").build();
    public static final Tag LI = builder("li").build();
    public static final Tag LINK = builder("link").bit(Bit.VOID).build();
    public static final Tag MAIN = builder("main").build();
    public static final Tag MAP = builder("map").build();
    public static final Tag MARK = builder("mark").build();
    public static final Tag MENU = builder("menu").build();
    public static final Tag META = builder("meta").bit(Bit.VOID).build();
    public static final Tag METER = builder("meter").build();
    public static final Tag NAV = builder("nav").build();
    public static final Tag NOSCRIPT = builder("noscript").build();
    public static final Tag OBJECT = builder("object").build();
    public static final Tag OL = builder("ol").build();
    public static final Tag OPTGROUP = builder("optgroup").build();
    public static final Tag OPTION = builder("option").build();
    public static final Tag OUTPUT = builder("output").build();
    public static final Tag P = builder("p").build();
    public static final Tag PARAM = builder("param").bit(Bit.VOID).build();
    public static final Tag PICTURE = builder("picture").build();
    public static final Tag PRE = builder("pre").build();
    public static final Tag PROGRESS = builder("progress").build();
    public static final Tag Q = builder("q").build();
    public static final Tag RP = builder("rp").build();
    public static final Tag RT = builder("rt").build();
    public static final Tag RUBY = builder("ruby").build();
    public static final Tag S = builder("s").build();
    public static final Tag SAMP = builder("samp").build();
    public static final Tag SCRIPT = builder("script").build();
    public static final Tag SECTION = builder("section").build();
    public static final Tag SELECT = builder("select").build();
    public static final Tag SLOT = builder("slot").build();
    public static final Tag SMALL = builder("small").build();
    public static final Tag SOURCE = builder("source").bit(Bit.VOID).build();
    public static final Tag SPAN = builder("span").build();
    public static final Tag STRONG = builder("strong").build();
    public static final Tag STYLE = builder("style").build();
    public static final Tag SUB = builder("sub").bit(Bit.INLINE).build();
    public static final Tag SUMMARY = builder("summary").build();
    public static final Tag SUP = builder("sup").bit(Bit.INLINE).build();
    public static final Tag TABLE = builder("table").build();
    public static final Tag TBODY = builder("tbody").build();
    public static final Tag TD = builder("td").build();
    public static final Tag TEMPLATE = builder("template").build();
    public static final Tag TEXTAREA = builder("textarea").build();
    public static final Tag TFOOT = builder("tfoot").build();
    public static final Tag TH = builder("th").build();
    public static final Tag THEAD = builder("thead").build();
    public static final Tag TIME = builder("time").build();
    public static final Tag TITLE = builder("title").build();
    public static final Tag TR = builder("tr").build();
    public static final Tag TRACK = builder("track").bit(Bit.VOID).build();
    public static final Tag U = builder("u").build();
    public static final Tag UL = builder("ul").build();
    public static final Tag VAR = builder("var").build();
    public static final Tag VIDEO = builder("video").build();
    public static final Tag WBR = builder("wbr").bit(Bit.VOID).build();

    public String getName() {
        return name;
    }

    public boolean isSet(Bit bit) {
        return IntMasks.isEnabled(mask, bit);
    }

    public boolean isInline() {
        return IntMasks.isEnabled(mask, Bit.INLINE);
    }

    public boolean isVoid() {
        return IntMasks.isEnabled(mask, Bit.VOID);
    }

    public static Builder builder(String name) {
        return new Builder(name);
    }

    public static class Builder {
        private final String name;
        private final Set<Bit> bits = EnumSet.noneOf(Bit.class);

        Builder(String name) {
            this.name = name;
        }

        public Builder bit(Bit bit) {
            this.bits.add(bit);
            return this;
        }

        public Tag build() {
            return new Tag(name, bits);
        }
    }
}