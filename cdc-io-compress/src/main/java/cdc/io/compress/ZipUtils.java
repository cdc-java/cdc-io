package cdc.io.compress;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;

import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveInputStream;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Zip utilities.
 *
 * @author Damien Carbonne
 *
 */
public final class ZipUtils {
    private static final Logger LOGGER = LogManager.getLogger(ZipUtils.class);

    private ZipUtils() {
    }

    /**
     * Recursively compresses a set of files or directories into a Zip archive.
     * <p>
     * Empty directories are included in archive.
     *
     * @param output The destination archive.
     * @param skipRootDirs If {@code true}, for each input that is a directory,
     *            only compresses the content of that input.
     * @param inputs The files to be compressed into {@code output}.
     * @throws IOException When an IO error occurs.
     */
    public static void compress(File output,
                                boolean skipRootDirs,
                                File... inputs) throws IOException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("compress({}, {}, {})", output, skipRootDirs, Arrays.toString(inputs));
        }
        try (ZipArchiveOutputStream out = new ZipArchiveOutputStream(output)) {
            for (final File input : inputs) {
                if (skipRootDirs && input.isDirectory()) {
                    final File[] children = input.listFiles();
                    if (children != null) {
                        for (final File child : children) {
                            addRec(out, input.toPath(), child);
                        }
                    }
                } else {
                    addRec(out, input.toPath().getParent(), input);
                }
            }
        }
    }

    /**
     * Decompress a Zip input file to a destination.
     *
     * @param input The Zip archive to decompress.
     * @param output The destination.
     * @throws IOException When an IO error occurs.
     */
    public static void decompress(File input,
                                  File output) throws IOException {
        LOGGER.debug("decompress({}, {})", input, output);
        try (final ZipArchiveInputStream in = new ZipArchiveInputStream(new BufferedInputStream(new FileInputStream(input)))) {
            ZipArchiveEntry entry;
            while ((entry = in.getNextEntry()) != null) {
                final File file = new File(output, entry.getName());
                if (entry.isDirectory()) {
                    if (!file.exists()) {
                        LOGGER.debug("   create dir: {}", file);
                        file.mkdirs();
                    }
                } else {
                    final File parent = file.getParentFile();
                    if (!parent.exists()) {
                        LOGGER.debug("   create dir: {}", parent);
                        parent.mkdirs();
                    }
                    LOGGER.debug("   create file: {}", file);
                    try (OutputStream out = new BufferedOutputStream(new FileOutputStream(file))) {
                        IOUtils.copy(in, out);
                    }
                }
            }
        }
    }

    private static String getRelativeName(Path root,
                                          File file) {
        return root == null
                ? file.getName()
                : root.relativize(file.toPath()).toFile().getPath().replace('\\', '/');
    }

    /**
     * Recursively adds a file or directory to an archive file.
     *
     * @param out The archive file.
     * @param root The root path relatively to which names are created.
     * @param file The file or directory to add.
     * @throws IOException When an IO error occurs.
     */
    private static void addRec(ZipArchiveOutputStream out,
                               Path root,
                               File file) throws IOException {
        if (file.isFile()) {
            LOGGER.debug("   add file: {}", file);
            final ZipArchiveEntry entry = new ZipArchiveEntry(file, getRelativeName(root, file));
            out.putArchiveEntry(entry);
            try (InputStream i = Files.newInputStream(file.toPath())) {
                IOUtils.copy(i, out);
            }
            out.closeArchiveEntry();
        } else if (file.isDirectory()) {
            LOGGER.debug("   add dir: {}", file);
            final ZipArchiveEntry entry = new ZipArchiveEntry(file, getRelativeName(root, file));
            out.putArchiveEntry(entry);
            out.closeArchiveEntry();

            final File[] children = file.listFiles();
            if (children != null) {
                for (final File child : children) {
                    addRec(out, root, child);
                }
            }
        } else {
            LOGGER.debug("  skip: {}", file);
        }
    }
}