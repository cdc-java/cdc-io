package cdc.io.compress;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;
import java.util.Arrays;

import org.apache.commons.compress.archivers.sevenz.SevenZArchiveEntry;
import org.apache.commons.compress.archivers.sevenz.SevenZFile;
import org.apache.commons.compress.archivers.sevenz.SevenZOutputFile;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * 7Z utilities.
 *
 * @author Damien Carbonne
 *
 */
public final class SevenZUtils {
    private static final Logger LOGGER = LogManager.getLogger(SevenZUtils.class);
    private static final int BUFFER_SIZE = 1024;

    private SevenZUtils() {
    }

    /**
     * Recursively compresses a set of files or directories into a 7Z archive.
     * <p>
     * Empty directories are included in archive.
     *
     * @param output The destination archive.
     * @param skipRootDirs If {@code true}, for each input that is a directory,
     *            only compresses the content of that input.
     * @param inputs The files to be compressed into {@code output}.
     * @throws IOException When an IO error occurs.
     */
    public static void compress(File output,
                                boolean skipRootDirs,
                                File... inputs) throws IOException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("compress({}, {}, {})", output, skipRootDirs, Arrays.toString(inputs));
        }
        try (SevenZOutputFile out = new SevenZOutputFile(output)) {
            for (final File input : inputs) {
                if (skipRootDirs && input.isDirectory()) {
                    final File[] children = input.listFiles();
                    if (children != null) {
                        for (final File child : children) {
                            addRec(out, input.toPath(), child);
                        }
                    }
                } else {
                    addRec(out, input.toPath().getParent(), input);
                }
            }
            out.finish();
        }
    }

    /**
     * Decompress a 7Z input file to a destination.
     *
     * @param input The 7Z archive to decompress.
     * @param output The destination.
     * @throws IOException When an IO error occurs.
     */
    public static void decompress(File input,
                                  File output) throws IOException {
        LOGGER.debug("decompress({}, {})", input, output);
        try (final SevenZFile sevenZFile = SevenZFile.builder().setFile(input).get()) {
            SevenZArchiveEntry entry;
            while ((entry = sevenZFile.getNextEntry()) != null) {
                final File file = new File(output, entry.getName());
                if (entry.isDirectory()) {
                    if (!file.exists()) {
                        LOGGER.debug("   create dir: {}", file);
                        file.mkdirs();
                    }
                } else {
                    final File parent = file.getParentFile();
                    if (!parent.exists()) {
                        LOGGER.debug("   create dir: {}", parent);
                        parent.mkdirs();
                    }
                    LOGGER.debug("   create file: {}", file);
                    try (final OutputStream out = new BufferedOutputStream(new FileOutputStream(file))) {
                        final byte[] b = new byte[BUFFER_SIZE];
                        long remaining = entry.getSize();
                        while (remaining > 0L) {
                            final int count = sevenZFile.read(b, 0, remaining >= BUFFER_SIZE ? BUFFER_SIZE : (int) remaining);
                            out.write(b, 0, count);
                            remaining -= count;
                        }
                        out.flush();
                    }
                }
            }
        }
    }

    private static String getRelativeName(Path root,
                                          File file) {
        return root == null
                ? file.getName()
                : root.relativize(file.toPath()).toFile().getPath().replace('\\', '/');
    }

    /**
     * Recursively adds a file or directory to an archive file.
     *
     * @param out The archive file.
     * @param root The root path relatively to which names are created.
     * @param file The file or directory to add.
     * @throws IOException When an IO error occurs.
     */
    private static void addRec(SevenZOutputFile out,
                               Path root,
                               File file) throws IOException {
        if (file.isFile()) {
            LOGGER.debug("   add file: {}", file);
            final SevenZArchiveEntry entry = out.createArchiveEntry(file, getRelativeName(root, file));
            out.putArchiveEntry(entry);
            try (final InputStream in = new BufferedInputStream(new FileInputStream(file))) {
                final byte[] b = new byte[BUFFER_SIZE];
                int count = 0;
                while ((count = in.read(b)) > 0) {
                    out.write(b, 0, count);
                }
            }
            out.closeArchiveEntry();
        } else if (file.isDirectory()) {
            LOGGER.debug("   add dir: {}", file);
            final File[] children = file.listFiles();
            if (children == null || children.length == 0) {
                // Warning: Adding "/" suffix is necessary to create an empty directory.
                final SevenZArchiveEntry entry = out.createArchiveEntry(file, getRelativeName(root, file) + "/");
                out.putArchiveEntry(entry);
                out.closeArchiveEntry();
            } else {
                for (final File child : children) {
                    addRec(out, root, child);
                }
            }
        } else {
            LOGGER.debug("   skip: {}", file);
        }
    }
}