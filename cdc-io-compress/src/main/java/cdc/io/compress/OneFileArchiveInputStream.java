package cdc.io.compress;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.ArchiveException;
import org.apache.commons.compress.archivers.ArchiveInputStream;
import org.apache.commons.compress.archivers.ArchiveStreamFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.util.lang.UnexpectedValueException;

public class OneFileArchiveInputStream extends FilterInputStream {
    private static final Logger LOGGER = LogManager.getLogger(OneFileArchiveInputStream.class);

    private static ArchiveInputStream<?> create(Archiver archiver,
                                                InputStream in) throws IOException {
        try {
            return new ArchiveStreamFactory().createArchiveInputStream(archiver.getName(), in);
        } catch (final ArchiveException e) {
            throw new IOException(e);
        }
    }

    public OneFileArchiveInputStream(Archiver archiver,
                                     InputStream in)
            throws IOException {
        super(create(archiver, in));
        @SuppressWarnings("unchecked")
        final ArchiveInputStream<ArchiveEntry> ais = (ArchiveInputStream<ArchiveEntry>) this.in;
        switch (archiver) {
        case ZIP:
        case SEVEN_Z:
            ArchiveEntry entry = null;
            while ((entry = ais.getNextEntry()) != null) {
                if (!ais.canReadEntryData(entry)) {
                    LOGGER.error("Can not read entry");
                    continue;
                }
                if (!entry.isDirectory()) {
                    // Return on first file
                    return;
                }
            }
            throw new IOException("No file found in archive");

        default:
            throw new UnexpectedValueException(archiver);
        }
    }
}