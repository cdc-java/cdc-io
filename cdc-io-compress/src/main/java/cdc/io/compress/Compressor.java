package cdc.io.compress;

import org.apache.commons.compress.compressors.CompressorStreamFactory;

/**
 * Enumeration of compressor algorithms.
 * <p>
 * Based on those supported by commons compress.
 *
 * @author Damien Carbonne
 */
public enum Compressor {
    NONE(null),

    /**
     * Depends on installation of Google Brotli dec.
     */
    BROTLI(CompressorStreamFactory.BROTLI),

    BZIP2(CompressorStreamFactory.BZIP2),

    GZIP(CompressorStreamFactory.GZIP),

    /**
     * Depends on installation of XZ for Java.
     */
    LZMA(CompressorStreamFactory.LZMA),

    /**
     * Depends on installation of XZ for Java.
     */
    XZ(CompressorStreamFactory.XZ);

    private final String name;

    private Compressor(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getExtension() {
        if (this == Compressor.BZIP2) {
            return "bz2";
        } else {
            return name;
        }
    }
}