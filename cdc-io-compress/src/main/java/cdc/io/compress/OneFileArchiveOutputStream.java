package cdc.io.compress;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.apache.commons.compress.archivers.ArchiveException;
import org.apache.commons.compress.archivers.ArchiveOutputStream;
import org.apache.commons.compress.archivers.ArchiveStreamFactory;
import org.apache.commons.compress.archivers.sevenz.SevenZArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;

import cdc.util.lang.UnexpectedValueException;

public class OneFileArchiveOutputStream extends FilterOutputStream {
    private boolean closed = false;

    private static ArchiveOutputStream<?> create(Archiver archiver,
                                                 OutputStream out) throws IOException {
        try {
            return new ArchiveStreamFactory().createArchiveOutputStream(archiver.getName(), out);
        } catch (final ArchiveException e) {
            throw new IOException(e);
        }
    }

    public OneFileArchiveOutputStream(Archiver archiver,
                                      String name,
                                      OutputStream out)
            throws IOException {
        super(create(archiver, out));
        switch (archiver) {
        case ZIP:
            @SuppressWarnings("unchecked")
            final ArchiveOutputStream<ZipArchiveEntry> aosZip = (ArchiveOutputStream<ZipArchiveEntry>) this.out;
            aosZip.putArchiveEntry(new ZipArchiveEntry(name));
            break;

        case SEVEN_Z:
            @SuppressWarnings("unchecked")
            final ArchiveOutputStream<SevenZArchiveEntry> aos7Z = (ArchiveOutputStream<SevenZArchiveEntry>) this.out;
            final SevenZArchiveEntry entry = new SevenZArchiveEntry();
            entry.setName(name);
            aos7Z.putArchiveEntry(entry);
            break;

        default:
            throw new UnexpectedValueException(archiver);
        }
    }

    @Override
    public void close() throws IOException {
        if (!closed) {
            ((ArchiveOutputStream<?>) out).closeArchiveEntry();
            ((ArchiveOutputStream<?>) out).finish();
            super.close();
            closed = true;
        }
    }
}