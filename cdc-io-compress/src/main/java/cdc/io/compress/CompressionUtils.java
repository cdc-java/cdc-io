package cdc.io.compress;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.compress.compressors.CompressorException;
import org.apache.commons.compress.compressors.CompressorStreamFactory;

import cdc.io.utils.IOUtils;
import cdc.util.lang.Checks;
import cdc.util.lang.UnexpectedValueException;

/**
 * Utilities related to compression.
 *
 * @author Damien Carbonne
 *
 */
public final class CompressionUtils {
    private static int SIZE = 1024 * 32;

    private CompressionUtils() {
    }

    /**
     * Creates an OutputStream that adds a compression stage to another one.
     * <p>
     * Some compression algorithms need installation of additional jars.
     *
     * @param os The OutputStream to adapt.
     * @param compressor The compressor to use.
     * @return A new OutputStream that adds {@code compressor} compression stage to {@code os}.
     * @throws IOException When an IO exception occurs.
     */
    public static OutputStream adapt(OutputStream os,
                                     Compressor compressor) throws IOException {
        Checks.isNotNull(os, "os");
        Checks.isNotNull(compressor, "compressor");

        final OutputStream out = IOUtils.toBuffered(os, SIZE);

        try {
            switch (compressor) {
            case BZIP2:
            case GZIP:
            case XZ:
            case LZMA:
                return new CompressorStreamFactory().createCompressorOutputStream(compressor.getName(), out);

            case NONE:
                return out;

            case BROTLI:
                throw new UnexpectedValueException("Compression with " + compressor + " is not yet supported by commons compress");

            default:
                throw new UnexpectedValueException(compressor);
            }
        } catch (final CompressorException e) {
            throw new IOException(e);
        }
    }

    /**
     * Creates an InputStream that adds a decompression stage to another one.
     * <p>
     * Some compression algorithms need installation of additional jars.
     *
     * @param is The inputStream to adapt.
     * @param compressor The compressor to use.
     * @return A new InputStream that adds {@code compressor} decompression stage to {@code is}.
     * @throws IOException When an IO exception occurs.
     */
    public static InputStream adapt(InputStream is,
                                    Compressor compressor) throws IOException {
        Checks.isNotNull(is, "is");
        Checks.isNotNull(compressor, "compressor");

        final InputStream in = IOUtils.toBuffered(is, SIZE);

        try {
            switch (compressor) {
            case BROTLI:
            case BZIP2:
            case GZIP:
            case XZ:
            case LZMA:
                return new CompressorStreamFactory().createCompressorInputStream(compressor.getName(), in);

            case NONE:
                return in;

            default:
                throw new UnexpectedValueException(compressor);
            }
        } catch (final CompressorException e) {
            throw new IOException(e);
        }
    }

    /**
     * Creates an OutputStream that is an archive containing one file.
     * <p>
     * <b>Warning:</b> ZIP and 7Z archivers are currently supported.
     *
     * @param os The delegate OutputStream.
     * @param name Name of the file contained in the archive.
     * @param archiver The archiver to use.
     * @return A new OutputStream that appears as an archive containing one file named {@code name}.
     * @throws IOException When an IO exception occurs.
     */
    public static OutputStream adapt(OutputStream os,
                                     String name,
                                     Archiver archiver) throws IOException {
        Checks.isNotNull(os, "os");
        Checks.isNotNull(archiver, "archiver");

        final OutputStream out = IOUtils.toBuffered(os, SIZE);

        switch (archiver) {
        case ZIP:
        case SEVEN_Z:
            return new OneFileArchiveOutputStream(archiver, name, out);

        case NONE:
        default:
            return out;
        }
    }

    /**
     * Creates an InputStream that corresponds to the unique file contained in an archive.
     * <p>
     * <b>Warning:</b> ZIP and 7Z archivers are currently supported.
     *
     * @param is The archive InputStream.
     * @param archiver The Archiver to use.
     * @return A new InputStream that corresponds to the unique file contained in an archive.
     * @throws IOException When an IO exception occurs.
     */
    public static InputStream adapt(InputStream is,
                                    Archiver archiver) throws IOException {
        Checks.isNotNull(is, "is");
        Checks.isNotNull(archiver, "archiver");

        final InputStream in = IOUtils.toBuffered(is, SIZE);

        switch (archiver) {
        case ZIP:
        case SEVEN_Z:
            return new OneFileArchiveInputStream(archiver, in);

        case NONE:
        default:
            return in;
        }
    }

    /**
     * Recursively compresses a set of files or directories into an archive.
     *
     * @param output The output file.
     * @param archiver The Archiver to use.
     * @param skipRootDirs If {@code true}, for each input that is a directory,
     *            only compresses the content of that input.
     * @param inputs The input files or directories.
     * @throws IOException When an IO exception occurs.
     */
    public static void compress(File output,
                                boolean skipRootDirs,
                                Archiver archiver,
                                File... inputs) throws IOException {
        Checks.isNotNull(output, "output");
        Checks.isNotNull(archiver, "archiver");

        switch (archiver) {
        case SEVEN_Z:
            SevenZUtils.compress(output, skipRootDirs, inputs);
            break;
        case ZIP:
            ZipUtils.compress(output, skipRootDirs, inputs);
            break;
        case NONE:
            break;
        default:
            throw new UnexpectedValueException(archiver);
        }
    }

    /**
     * Recursively compresses a set of files or directories into an archive.
     *
     * @param output The output file.
     * @param archiver The Archiver to use.
     * @param inputs The input files or directories.
     * @throws IOException When an IO exception occurs.
     */
    public static void compress(File output,
                                Archiver archiver,
                                File... inputs) throws IOException {
        compress(output, false, archiver, inputs);
    }

    /**
     * Decompress an archive file to a destination.
     *
     * @param input The archive to decompress.
     * @param output The destination.
     * @param archiver the archiver to use.
     * @throws IOException When an IO error occurs.
     */
    public static void decompress(File input,
                                  File output,
                                  Archiver archiver) throws IOException {
        Checks.isNotNull(input, "input");
        Checks.isNotNull(output, "output");
        Checks.isNotNull(archiver, "archiver");

        switch (archiver) {
        case SEVEN_Z:
            SevenZUtils.decompress(input, output);
            break;
        case ZIP:
            ZipUtils.decompress(input, output);
            break;
        case NONE:
            break;
        default:
            throw new UnexpectedValueException(archiver);
        }
    }

    /**
     * Decompress an archive file to a destination.
     * <p>
     * The format is determined from input name.<br>
     * If is can not be determined, an exception is raised.
     *
     * @param input The archive to decompress.
     * @param output The destination.
     * @throws IOException When an IO error occurs.
     */
    public static void decompress(File input,
                                  File output) throws IOException {
        Checks.isNotNull(input, "input");
        Checks.isNotNull(output, "output");

        final String name = input.getName().toLowerCase();
        for (final Archiver archiver : Archiver.values()) {
            if (archiver != Archiver.NONE && name.endsWith("." + archiver.getExtension())) {
                decompress(input, output, archiver);
                return;
            }
        }
        throw new IOException("Can not find archiver associated to " + input);
    }
}