package cdc.io.compress;

import org.apache.commons.compress.archivers.ArchiveStreamFactory;

/**
 * Enumeration of supported archivers.
 * <p>
 * Based on those supported by commons compress.
 *
 * @author Damien Carbonne
 *
 */
public enum Archiver {
    NONE(null),

    ZIP(ArchiveStreamFactory.ZIP),
    SEVEN_Z(ArchiveStreamFactory.SEVEN_Z);

    private final String name;

    private Archiver(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getExtension() {
        return name;
    }
}