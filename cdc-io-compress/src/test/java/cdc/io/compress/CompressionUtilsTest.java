package cdc.io.compress;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Test;

class CompressionUtilsTest {

    static void check(String basename,
                      Archiver archiver) throws IOException {
        final File folder1 = new File("src/test/resources/cdc/io/compress/folder1");
        final File archive = new File("target", basename + "." + archiver.getExtension());
        final File out = new File("target", basename + "-" + archiver);

        FileUtils.deleteQuietly(out);

        CompressionUtils.compress(archive,
                                  false,
                                  archiver,
                                  folder1);
        CompressionUtils.decompress(archive,
                                    out);
        assertTrue(out.isDirectory());
        assertTrue(new File(out, "folder1").isDirectory());
        assertTrue(new File(out, "folder1/file1.txt").isFile());
        assertTrue(new File(out, "folder1/file2.txt").isFile());
    }

    @Test
    void testFolder1() throws IOException {
        check("test-folder1", Archiver.SEVEN_Z);
        check("test-folder1", Archiver.ZIP);
    }
}