<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:output method="html" doctype-public="" doctype-system="" encoding="UTF-8" indent="no"/>
   <xsl:template match="root">
      <html>
         <body>
            <h1>ROOT</h1>
            <xsl:apply-templates/>
         </body>
      </html>
   </xsl:template>

   <xsl:template match="child">
      <h2>CHILD</h2>
   </xsl:template>
</xsl:stylesheet>