package cdc.io.xml;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.util.lang.InvalidDataException;
import cdc.util.lang.InvalidStateException;

class XmlWriterTest {
    private static final Logger LOGGER = LogManager.getLogger(XmlWriterTest.class);
    private static final String XML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
    private static final String DTD = "<!DOCTYPE root [ <!ELEMENT root (child+) ><!-- comment --> ]>";
    private static final String EOL = "";
    private static final String INDENT = "";
    private static final String TARGET = "target";
    private static final boolean ENABLE_FINAL_EOL = true;
    private static final String FINAL_EOL = ENABLE_FINAL_EOL ? EOL : "";

    private static final String LINE = "=============================================================";

    private static void init(XmlWriter xmlw) {
        xmlw.setEndOfLine(EOL);
        xmlw.setIndentString(INDENT);
        xmlw.setEnabled(XmlWriter.Feature.PRETTY_PRINT, true);
        xmlw.setEnabled(XmlWriter.Feature.APPEND_FINAL_EOL, ENABLE_FINAL_EOL);
    }

    private static void check(String expected,
                              Writer writer) {
        if (expected.equals(writer.toString())) {
            LOGGER.debug("expected and obtained:\n{}\n{}\n", LINE, expected);
        } else {
            LOGGER.error("expect:\n{}\n{}\n", LINE, expected);
            LOGGER.error("result:\n{}\n{}\n", LINE, writer);
        }
        assertEquals(expected, writer.toString());
    }

    @FunctionalInterface
    private static interface ConsumerE<T, E extends Exception> {
        void accept(T t) throws E;
    }

    private static void check(String expectedPP,
                              String expectedNoPP,
                              ConsumerE<XmlWriter, IOException> consumer) throws IOException {
        final StringWriter writer = new StringWriter();
        try (final XmlWriter xmlw = new XmlWriter(writer)) {
            xmlw.setEndOfLine("\n");
            xmlw.setIndentString("  ");
            xmlw.setEnabled(XmlWriter.Feature.PRETTY_PRINT, true);
            consumer.accept(xmlw);
            assertEquals(expectedPP, writer.toString());
        }
        writer.getBuffer().setLength(0);
        try (final XmlWriter xmlw = new XmlWriter(writer)) {
            xmlw.setEndOfLine("");
            xmlw.setIndentString("  ");
            xmlw.setEnabled(XmlWriter.Feature.PRETTY_PRINT, false);
            consumer.accept(xmlw);
            assertEquals(expectedNoPP, writer.toString());
        }
    }

    @Test
    void testEmptyDocument() throws IOException {
        final String expectedPP = XML;
        final String expectedNoPP = XML;
        check(expectedPP,
              expectedNoPP,
              w -> {
                  w.beginDocument();
                  w.endDocument();
                  w.close();
              });
    }

    @Test
    void test1Comment() throws IOException {
        final String expectedPP = """
                <?xml version="1.0" encoding="UTF-8"?>
                <!--X-->""";
        final String expectedNoPP = """
                <?xml version="1.0" encoding="UTF-8"?><!--X-->""";
        check(expectedPP,
              expectedNoPP,
              w -> {
                  w.beginDocument();
                  w.addComment("X");
                  w.endDocument();
                  w.close();
              });
    }

    @Test
    void test2Comments() throws IOException {
        final String expectedPP = """
                <?xml version="1.0" encoding="UTF-8"?>
                <!--X-->
                <!--X-->""";
        final String expectedNoPP = """
                <?xml version="1.0" encoding="UTF-8"?><!--X--><!--X-->""";
        check(expectedPP,
              expectedNoPP,
              w -> {
                  w.beginDocument();
                  w.addComment("X");
                  w.addComment("X");
                  w.endDocument();
                  w.close();
              });
    }

    @Test
    void testSimpleContent1() throws IOException {
        final String expectedPP = """
                <?xml version="1.0" encoding="UTF-8"?>
                <r/>""";
        final String expectedNoPP = """
                <?xml version="1.0" encoding="UTF-8"?><r/>""";
        check(expectedPP,
              expectedNoPP,
              w -> {
                  w.beginDocument();
                  w.beginElement("r");
                  w.endElement();
                  w.endDocument();
                  w.close();
              });
    }

    @Test
    void testSimpleContent2() throws IOException {
        final String expectedPP = """
                <?xml version="1.0" encoding="UTF-8"?>
                <r>
                  <c/>
                  <c a="v"/>
                </r>""";
        final String expectedNoPP = """
                <?xml version="1.0" encoding="UTF-8"?><r><c/><c a="v"/></r>""";
        check(expectedPP,
              expectedNoPP,
              w -> {
                  w.beginDocument();
                  w.beginElement("r");
                  w.beginElement("c");
                  w.endElement();
                  w.beginElement("c");
                  w.addAttribute("a", "v");
                  w.endElement();
                  w.endElement();
                  w.endDocument();
                  w.close();
              });
    }

    @Test
    void testSimpleContent1Comments() throws IOException {
        final String expectedPP = """
                <?xml version="1.0" encoding="UTF-8"?>
                <!--X-->
                <r>
                  <!--X-->
                </r>
                <!--X-->""";
        final String expectedNoPP = """
                <?xml version="1.0" encoding="UTF-8"?><!--X--><r><!--X--></r><!--X-->""";
        check(expectedPP,
              expectedNoPP,
              w -> {
                  w.beginDocument();
                  w.addComment("X");
                  w.beginElement("r");
                  w.addComment("X");
                  w.endElement();
                  w.addComment("X");
                  w.endDocument();
                  w.close();
              });
    }

    @Test
    void testSimpleContent2Comments() throws IOException {
        final String expectedPP = """
                <?xml version="1.0" encoding="UTF-8"?>
                <!--X-->
                <r>
                  <!--X-->
                  <c>
                    <!--X-->
                  </c>
                  <!--X-->
                  <c a="v">
                    <!--X-->
                  </c>
                  <!--X-->
                </r>
                <!--X-->""";
        final String expectedNoPP =
                """
                        <?xml version="1.0" encoding="UTF-8"?><!--X--><r><!--X--><c><!--X--></c><!--X--><c a="v"><!--X--></c><!--X--></r><!--X-->""";
        check(expectedPP,
              expectedNoPP,
              w -> {
                  w.beginDocument();
                  w.addComment("X");
                  w.beginElement("r");
                  w.addComment("X");
                  w.beginElement("c");
                  w.addComment("X");
                  w.endElement();
                  w.addComment("X");
                  w.beginElement("c");
                  w.addAttribute("a", "v");
                  w.addComment("X");
                  w.endElement();
                  w.addComment("X");
                  w.endElement();
                  w.addComment("X");
                  w.endDocument();
                  w.close();
              });
    }

    @Test
    void testMixedContent1() throws IOException {
        final String expectedPP = """
                <?xml version="1.0" encoding="UTF-8"?>
                <r>C</r>""";
        final String expectedNoPP = """
                <?xml version="1.0" encoding="UTF-8"?><r>C</r>""";
        check(expectedPP,
              expectedNoPP,
              w -> {
                  w.beginDocument();
                  w.beginElement("r");
                  w.addElementContent("C");
                  w.endElement();
                  w.endDocument();
                  w.close();
              });
    }

    @Test
    void testMixedContent2() throws IOException {
        final String expectedPP = """
                <?xml version="1.0" encoding="UTF-8"?>
                <r>C<b>C</b></r>""";
        final String expectedNoPP = """
                <?xml version="1.0" encoding="UTF-8"?><r>C<b>C</b></r>""";
        check(expectedPP,
              expectedNoPP,
              w -> {
                  w.beginDocument();
                  w.beginElement("r");
                  w.addElementContent("C");
                  w.beginElement("b");
                  w.addElementContent("C");
                  w.endElement();
                  w.endElement();
                  w.endDocument();
                  w.close();
              });
    }

    @Test
    void testMixedContent3() throws IOException {
        final String expectedPP = """
                <?xml version="1.0" encoding="UTF-8"?>
                <r>
                  <b>C</b>
                </r>""";
        final String expectedNoPP = """
                <?xml version="1.0" encoding="UTF-8"?><r><b>C</b></r>""";
        check(expectedPP,
              expectedNoPP,
              w -> {
                  w.beginDocument();
                  w.beginElement("r");
                  w.beginElement("b");
                  w.addElementContent("C");
                  w.endElement();
                  w.endElement();
                  w.endDocument();
                  w.close();
              });
    }

    @Test
    void testMixedContent4() throws IOException {
        final String expectedPP = """
                <?xml version="1.0" encoding="UTF-8"?>
                <r><b>C</b>C</r>""";
        final String expectedNoPP = """
                <?xml version="1.0" encoding="UTF-8"?><r><b>C</b>C</r>""";
        check(expectedPP,
              expectedNoPP,
              w -> {
                  w.beginDocument();
                  w.beginElement("r");
                  w.addElementContent(""); // Force mixed content
                  w.beginElement("b");
                  w.addElementContent("C");
                  w.endElement();
                  w.addElementContent("C");
                  w.endElement();
                  w.endDocument();
                  w.close();
              });
    }

    @Test
    void testIndentMixedContentComment1() throws IOException {
        final StringWriter writer = new StringWriter();
        try (final XmlWriter xmlw = new XmlWriter(writer)) {
            init(xmlw);

            xmlw.beginDocument();
            xmlw.beginElement("root");
            xmlw.addElementContent(""); // Force mixed content
            xmlw.addComment("XXX");
            xmlw.beginElement("b");
            xmlw.addElementContent("CCC");
            xmlw.endElement();
            xmlw.addElementContent("CCC");
            xmlw.endElement();
            xmlw.endDocument();
            xmlw.flush();

            final String expected = XML + EOL
                    + "<root>"
                    + "<!--XXX-->"
                    + "<b>CCC</b>CCC"
                    + "</root>" + FINAL_EOL;

            check(expected, writer);
        }
    }

    @Test
    void testIndentMixedContentComment2() throws IOException {
        final StringWriter writer = new StringWriter();
        try (final XmlWriter xmlw = new XmlWriter(writer)) {
            init(xmlw);

            xmlw.beginDocument();
            xmlw.beginElement("root");
            xmlw.beginElement("b");
            xmlw.addElementContent("CCC");
            xmlw.endElement();
            xmlw.addElementContent("CCC");
            xmlw.endElement();
            xmlw.endDocument();
            xmlw.flush();

            final String expected = XML + EOL
                    + "<root>" + EOL
                    + INDENT + "<b>CCC</b>CCC" + EOL
                    + "</root>" + FINAL_EOL;

            check(expected, writer);
        }
    }

    @Test
    void testIndentMixedContentComment3() throws IOException {
        final StringWriter writer = new StringWriter();
        try (final XmlWriter xmlw = new XmlWriter(writer)) {
            init(xmlw);

            xmlw.beginDocument();
            xmlw.beginElement("root");
            xmlw.addComment("XXX");
            xmlw.beginElement("b");
            xmlw.addElementContent("CCC");
            xmlw.endElement();
            xmlw.addElementContent("CCC");
            xmlw.endElement();
            xmlw.endDocument();
            xmlw.flush();

            final String expected = XML + EOL
                    + "<root>" + EOL
                    + INDENT + "<!--XXX-->" + EOL
                    + INDENT + "<b>CCC</b>CCC" + EOL
                    + "</root>" + FINAL_EOL;

            check(expected, writer);
        }
    }

    @Test
    void testIndentCData1() throws IOException {
        final StringWriter writer = new StringWriter();
        try (final XmlWriter xmlw = new XmlWriter(writer)) {
            init(xmlw);

            xmlw.beginDocument();
            xmlw.beginElement("root");
            xmlw.addCData("DDD");
            xmlw.endElement();
            xmlw.endDocument();
            xmlw.flush();

            final String expected = XML + EOL
                    + "<root>" + EOL
                    + INDENT + "<![CDATA[DDD]]>" + EOL
                    + "</root>" + FINAL_EOL;

            check(expected, writer);
        }
    }

    @Test
    void testIndentCData2() throws IOException {
        final StringWriter writer = new StringWriter();
        try (final XmlWriter xmlw = new XmlWriter(writer)) {
            init(xmlw);

            xmlw.beginDocument();
            xmlw.beginElement("root");
            xmlw.addCData("DDD");
            xmlw.addCData("DDD");
            xmlw.endElement();
            xmlw.endDocument();
            xmlw.flush();

            final String expected = XML + EOL
                    + "<root>" + EOL
                    + INDENT + "<![CDATA[DDD]]>" + EOL
                    + INDENT + "<![CDATA[DDD]]>" + EOL
                    + "</root>" + FINAL_EOL;

            check(expected, writer);
        }
    }

    @Test
    void testIndentCDataComment() throws IOException {
        final StringWriter writer = new StringWriter();
        try (final XmlWriter xmlw = new XmlWriter(writer)) {
            init(xmlw);

            xmlw.beginDocument();
            xmlw.beginElement("root");
            xmlw.addComment("CCC");
            xmlw.addCData("DDD");
            xmlw.addComment("CCC");
            xmlw.addCData("DDD");
            xmlw.addComment("CCC");
            xmlw.endElement();
            xmlw.endDocument();
            xmlw.flush();

            final String expected = XML + EOL
                    + "<root>" + EOL
                    + INDENT + "<!--CCC-->" + EOL
                    + INDENT + "<![CDATA[DDD]]>" + EOL
                    + INDENT + "<!--CCC-->" + EOL
                    + INDENT + "<![CDATA[DDD]]>" + EOL
                    + INDENT + "<!--CCC-->" + EOL
                    + "</root>" + FINAL_EOL;

            check(expected, writer);
        }
    }

    @Test
    void testIndentCDataSimpleContent1() throws IOException {
        final StringWriter writer = new StringWriter();
        try (final XmlWriter xmlw = new XmlWriter(writer)) {
            init(xmlw);

            xmlw.beginDocument();
            xmlw.beginElement("root");
            xmlw.addCData("DDD");
            xmlw.beginElement("child");
            xmlw.endElement();
            xmlw.endElement();
            xmlw.endDocument();
            xmlw.flush();

            final String expected = XML + EOL
                    + "<root>" + EOL
                    + INDENT + "<![CDATA[DDD]]>" + EOL
                    + INDENT + "<child/>" + EOL
                    + "</root>" + FINAL_EOL;

            check(expected, writer);
        }
    }

    @Test
    void testIndentCDataSimpleContent2() throws IOException {
        final StringWriter writer = new StringWriter();
        try (final XmlWriter xmlw = new XmlWriter(writer)) {
            init(xmlw);

            xmlw.beginDocument();
            xmlw.beginElement("root");
            xmlw.addCData("DDD");
            xmlw.beginElement("child");
            xmlw.addCData("DDD");
            xmlw.endElement();
            xmlw.endElement();
            xmlw.endDocument();
            xmlw.flush();

            final String expected = XML + EOL
                    + "<root>" + EOL
                    + INDENT + "<![CDATA[DDD]]>" + EOL
                    + INDENT + "<child>" + EOL
                    + INDENT + INDENT + "<![CDATA[DDD]]>" + EOL
                    + INDENT + "</child>" + EOL
                    + "</root>" + FINAL_EOL;

            check(expected, writer);
        }
    }

    @Test
    void testIndentCDataSimpleContentComment() throws IOException {
        final StringWriter writer = new StringWriter();
        try (final XmlWriter xmlw = new XmlWriter(writer)) {
            init(xmlw);

            xmlw.beginDocument();
            xmlw.beginElement("root");
            xmlw.addComment("CCC");
            xmlw.addCData("DDD");
            xmlw.beginElement("child");
            xmlw.addCData("DDD");
            xmlw.endElement();
            xmlw.endElement();
            xmlw.endDocument();
            xmlw.flush();

            final String expected = XML + EOL
                    + "<root>" + EOL
                    + INDENT + "<!--CCC-->" + EOL
                    + INDENT + "<![CDATA[DDD]]>" + EOL
                    + INDENT + "<child>" + EOL
                    + INDENT + INDENT + "<![CDATA[DDD]]>" + EOL
                    + INDENT + "</child>" + EOL
                    + "</root>" + FINAL_EOL;

            check(expected, writer);
        }
    }

    @Test
    void testIndentCDataMixedContent1() throws IOException {
        final StringWriter writer = new StringWriter();
        try (final XmlWriter xmlw = new XmlWriter(writer)) {
            init(xmlw);

            xmlw.beginDocument();
            xmlw.beginElement("root");
            xmlw.addCData("DDD");
            xmlw.addElementContent("CCC");
            xmlw.endElement();
            xmlw.endDocument();
            xmlw.flush();

            final String expected = XML + EOL
                    + "<root>" + EOL
                    + INDENT + "<![CDATA[DDD]]>" + "CCC" + EOL
                    + "</root>" + FINAL_EOL;

            check(expected, writer);
        }
    }

    @Test
    void testIndentCDataMixedContent2() throws IOException {
        final StringWriter writer = new StringWriter();
        try (final XmlWriter xmlw = new XmlWriter(writer)) {
            init(xmlw);

            xmlw.beginDocument();
            xmlw.beginElement("root");
            xmlw.addElementContent(""); // Force mixed content
            xmlw.addCData("DDD");
            xmlw.addElementContent("CCC");
            xmlw.endElement();
            xmlw.endDocument();
            xmlw.flush();

            final String expected = XML + EOL
                    + "<root>"
                    + "<![CDATA[DDD]]>" + "CCC"
                    + "</root>" + FINAL_EOL;

            check(expected, writer);
        }
    }

    @Test
    void testIndentProcessingInstruction1() throws IOException {
        final StringWriter writer = new StringWriter();
        try (final XmlWriter xmlw = new XmlWriter(writer)) {
            init(xmlw);

            xmlw.beginDocument();
            xmlw.addProcessingInstruction("target", "PPP");
            xmlw.endDocument();
            xmlw.flush();

            final String expected = XML + EOL
                    + "<?" + TARGET + " " + "PPP" + "?>" + FINAL_EOL;

            check(expected, writer);
        }
    }

    @Test
    void testIndentProcessingInstruction2() throws IOException {
        final StringWriter writer = new StringWriter();
        try (final XmlWriter xmlw = new XmlWriter(writer)) {
            init(xmlw);

            xmlw.beginDocument();
            xmlw.addProcessingInstruction("target", "PPP");
            xmlw.addProcessingInstruction("target", "PPP");
            xmlw.endDocument();
            xmlw.flush();

            final String expected = XML + EOL
                    + "<?" + TARGET + " " + "PPP" + "?>" + EOL
                    + "<?" + TARGET + " " + "PPP" + "?>" + FINAL_EOL;

            check(expected, writer);
        }
    }

    @Test
    void testIndentProcessingInstruction3() throws IOException {
        final StringWriter writer = new StringWriter();
        try (final XmlWriter xmlw = new XmlWriter(writer)) {
            init(xmlw);

            xmlw.beginDocument();
            xmlw.addProcessingInstruction("target", "PPP");
            xmlw.beginElement("root");
            xmlw.addProcessingInstruction("target", "PPP");
            xmlw.endElement();
            xmlw.endDocument();
            xmlw.flush();

            final String expected = XML + EOL
                    + "<?" + TARGET + " " + "PPP" + "?>" + EOL
                    + "<root>" + EOL
                    + INDENT + "<?" + TARGET + " " + "PPP" + "?>" + EOL
                    + "</root>" + FINAL_EOL;

            check(expected, writer);
        }
    }

    @Test
    void testPartialComment() throws IOException {
        final StringWriter writer = new StringWriter();
        try (final XmlWriter xmlw = new XmlWriter(writer)) {
            init(xmlw);
            xmlw.setEnabled(XmlWriter.Feature.ALLOW_PARTIAL_XML);

            xmlw.beginComment();
            xmlw.addCommentContent("comment");
            xmlw.endComment();
            xmlw.flush();

            final String expected = "<!--comment-->" + FINAL_EOL;

            check(expected, writer);
        }
    }

    @Test
    void testPartialElement() throws IOException {
        final StringWriter writer = new StringWriter();
        try (final XmlWriter xmlw = new XmlWriter(writer)) {
            init(xmlw);
            xmlw.setEnabled(XmlWriter.Feature.ALLOW_PARTIAL_XML);

            xmlw.beginElement("root");
            xmlw.addAttribute("id", "value");
            xmlw.endElement();
            xmlw.flush();

            final String expected = "<root id=\"value\"/>" + FINAL_EOL;

            check(expected, writer);
        }
    }

    @Test
    void testPartial1() throws IOException {
        final StringWriter writer = new StringWriter();
        try (final XmlWriter xmlw = new XmlWriter(writer)) {
            init(xmlw);
            xmlw.setEnabled(XmlWriter.Feature.ALLOW_PARTIAL_XML);

            xmlw.beginElement("root");
            xmlw.addAttribute("id", "value");
            xmlw.addComment("comment");
            xmlw.endElement();
            xmlw.flush();

            final String expected = "<root id=\"value\">" + EOL
                    + INDENT + "<!--comment-->" + EOL
                    + "</root>" + FINAL_EOL;

            check(expected, writer);
        }
    }

    @Test
    void testPartial2() throws IOException {
        final StringWriter writer = new StringWriter();
        try (final XmlWriter xmlw = new XmlWriter(writer)) {
            init(xmlw);
            xmlw.setEnabled(XmlWriter.Feature.ALLOW_PARTIAL_XML);

            xmlw.beginElement("root");
            xmlw.beginElement("child");
            xmlw.endElement();
            xmlw.beginElement("child");
            xmlw.endElement();
            xmlw.endElement();
            xmlw.flush();

            final String expected = "<root>" + EOL
                    + INDENT + "<child/>" + EOL
                    + INDENT + "<child/>" + EOL
                    + "</root>" + FINAL_EOL;

            check(expected, writer);
        }
    }

    @Test
    void testPartial3() throws IOException {
        final StringWriter writer = new StringWriter();
        try (final XmlWriter xmlw = new XmlWriter(writer)) {
            init(xmlw);
            xmlw.setEnabled(XmlWriter.Feature.ALLOW_PARTIAL_XML);

            xmlw.addComment("comment");
            xmlw.beginElement("root");
            xmlw.beginElement("child");
            xmlw.endElement();
            xmlw.addComment("comment");
            xmlw.beginElement("child");
            xmlw.endElement();
            xmlw.endElement();
            xmlw.addComment("comment");
            xmlw.flush();

            final String expected = "<!--comment-->" + EOL
                    + "<root>" + EOL
                    + INDENT + "<child/>" + EOL
                    + INDENT + "<!--comment-->" + EOL
                    + INDENT + "<child/>" + EOL
                    + "</root>" + EOL
                    + "<!--comment-->" + FINAL_EOL;

            check(expected, writer);
        }
    }

    @Test
    void testElementEscapingEntitizeAlways() throws IOException {
        final StringWriter writer = new StringWriter();
        try (final XmlWriter xmlw = new XmlWriter(writer)) {
            init(xmlw);
            xmlw.setEnabled(XmlWriter.Feature.ALLOW_PARTIAL_XML);
            xmlw.setEnabled(XmlWriter.Feature.ALWAYS_ENTITIZE_ELEMENTS);
            xmlw.beginElement("a");
            xmlw.addElementContent("<>'\"&");
            xmlw.endElement();
            xmlw.flush();

            final String expected = "<a>&lt;&gt;&apos;&quot;&amp;</a>" + FINAL_EOL;
            check(expected, writer);
        }
    }

    @Test
    void testElementEscapingEntitizeMin() throws IOException {
        final StringWriter writer = new StringWriter();
        try (final XmlWriter xmlw = new XmlWriter(writer)) {
            init(xmlw);
            xmlw.setEnabled(XmlWriter.Feature.ALLOW_PARTIAL_XML);
            xmlw.beginElement("a");
            xmlw.addElementContent("<>'\"&");
            xmlw.endElement();
            xmlw.flush();

            final String expected = "<a>&lt;&gt;'\"&amp;</a>" + FINAL_EOL;
            check(expected, writer);
        }
    }

    @Test
    void testAttributeEscapingQuoteEntitizeAlways() throws IOException {
        final StringWriter writer = new StringWriter();
        try (final XmlWriter xmlw = new XmlWriter(writer)) {
            init(xmlw);
            xmlw.setEnabled(XmlWriter.Feature.ALLOW_PARTIAL_XML);
            xmlw.setEnabled(XmlWriter.Feature.ALWAYS_ENTITIZE_ATTRIBUTES);

            xmlw.beginElement("a");
            xmlw.addAttribute("a", "<>'\"&\t\n");
            xmlw.endElement();
            xmlw.flush();

            final String expected = "<a a=\"&lt;&gt;&apos;&quot;&amp;&#x9;&#xA;\"/>" + FINAL_EOL;
            check(expected, writer);
        }
    }

    @Test
    void testUnescapedElement() throws IOException {
        final StringWriter writer = new StringWriter();
        try (final XmlWriter xmlw = new XmlWriter(writer)) {
            init(xmlw);
            xmlw.setEnabled(XmlWriter.Feature.ALLOW_PARTIAL_XML);
            xmlw.beginElement("a");
            xmlw.addUnescapedElementContent("&xxx;");
            xmlw.endElement();
            xmlw.flush();

            final String expected = "<a>&xxx;</a>" + FINAL_EOL;
            check(expected, writer);
        }
    }

    @Test
    void testAttributeEscapingQuoteEntitizeMin() throws IOException {
        final StringWriter writer = new StringWriter();
        try (final XmlWriter xmlw = new XmlWriter(writer)) {
            init(xmlw);
            xmlw.setEnabled(XmlWriter.Feature.ALLOW_PARTIAL_XML);

            xmlw.beginElement("a");
            xmlw.addAttribute("a", "<>'\"&\t\n");
            xmlw.endElement();
            xmlw.flush();

            final String expected = "<a a=\"&lt;>'&quot;&amp;&#x9;&#xA;\"/>" + FINAL_EOL;
            check(expected, writer);
        }
    }

    @Test
    void testUnescapedAttribute() throws IOException {
        final StringWriter writer = new StringWriter();
        try (final XmlWriter xmlw = new XmlWriter(writer)) {
            init(xmlw);
            xmlw.setEnabled(XmlWriter.Feature.ALLOW_PARTIAL_XML);

            xmlw.beginElement("a");
            xmlw.addUnescapedAttribute("a", "&xxx;");
            xmlw.endElement();
            xmlw.flush();

            final String expected = "<a a=\"&xxx;\"/>" + FINAL_EOL;
            check(expected, writer);
        }
    }

    @Test
    void testAttributeEscapingAposEntitizeAlways() throws IOException {
        final StringWriter writer = new StringWriter();
        try (final XmlWriter xmlw = new XmlWriter(writer)) {
            init(xmlw);
            xmlw.setEnabled(XmlWriter.Feature.ALLOW_PARTIAL_XML);
            xmlw.setEnabled(XmlWriter.Feature.USE_SINGLE_QUOTE);
            xmlw.setEnabled(XmlWriter.Feature.ALWAYS_ENTITIZE_ATTRIBUTES);

            xmlw.beginElement("a");
            xmlw.addAttribute("a", "<>'\"&\t\n");
            xmlw.endElement();
            xmlw.flush();

            final String expected = "<a a='&lt;&gt;&apos;&quot;&amp;&#x9;&#xA;'/>" + FINAL_EOL;
            check(expected, writer);
        }
    }

    @Test
    void testAttributeEscapingAposEntitizeMin() throws IOException {
        final StringWriter writer = new StringWriter();
        try (final XmlWriter xmlw = new XmlWriter(writer)) {
            init(xmlw);
            xmlw.setEnabled(XmlWriter.Feature.ALLOW_PARTIAL_XML);
            xmlw.setEnabled(XmlWriter.Feature.USE_SINGLE_QUOTE);

            xmlw.beginElement("a");
            xmlw.addAttribute("a", "<>'\"&\t\n");
            xmlw.endElement();
            xmlw.flush();

            final String expected = "<a a='&lt;>&apos;\"&amp;&#x9;&#xA;'/>" + FINAL_EOL;
            check(expected, writer);
        }
    }

    @Test
    void testCDATAEscaping() throws IOException {
        final StringWriter writer = new StringWriter();
        try (final XmlWriter xmlw = new XmlWriter(writer)) {
            init(xmlw);
            xmlw.setEnabled(XmlWriter.Feature.ALLOW_PARTIAL_XML);

            xmlw.beginElement("a");
            xmlw.addCData("<>'\"&abcd");
            xmlw.endElement();
            xmlw.flush();

            final String expected = "<a><![CDATA[<>'\"&abcd]]></a>" + FINAL_EOL;
            check(expected, writer);
        }
    }

    @Test
    void testInvalidNames() {
        assertThrows(InvalidDataException.class,
                     () -> {
                         final StringWriter writer = new StringWriter();
                         try (final XmlWriter xmlw = new XmlWriter(writer)) {
                             xmlw.setEnabled(XmlWriter.Feature.ALLOW_PARTIAL_XML);
                             xmlw.beginElement("0");
                         }
                     });

        assertThrows(InvalidDataException.class,
                     () -> {
                         final StringWriter writer = new StringWriter();
                         try (final XmlWriter xmlw = new XmlWriter(writer)) {
                             xmlw.setEnabled(XmlWriter.Feature.ALLOW_PARTIAL_XML);
                             xmlw.beginElement("a");
                             xmlw.addAttribute("0", "");
                         }
                     });
    }

    @Test
    void testInvalidContent() {
        assertThrows(InvalidDataException.class,
                     () -> {
                         final StringWriter writer = new StringWriter();
                         try (final XmlWriter xmlw = new XmlWriter(writer)) {
                             xmlw.setEnabled(XmlWriter.Feature.ALLOW_PARTIAL_XML);
                             xmlw.beginElement("a");
                             xmlw.addElementContent(new String(new byte[] { 0x0 }));
                         }
                     });

        assertThrows(InvalidDataException.class,
                     () -> {
                         final StringWriter writer = new StringWriter();
                         try (final XmlWriter xmlw = new XmlWriter(writer)) {
                             xmlw.setEnabled(XmlWriter.Feature.ALLOW_PARTIAL_XML);
                             xmlw.beginElement("a");
                             xmlw.addAttribute("a", new String(new byte[] { 0x0 }));
                         }
                     });
    }

    @Test
    void testReplaceNonPrintableChars() throws IOException {
        assertThrows(InvalidDataException.class,
                     () -> {
                         final StringWriter writer = new StringWriter();
                         try (final XmlWriter xmlw = new XmlWriter(writer)) {
                             xmlw.setEnabled(XmlWriter.Feature.ALLOW_PARTIAL_XML);
                             xmlw.beginElement("root");
                             xmlw.addElementContent(new String(new byte[] { 0x0 }));
                             xmlw.endElement();
                         }
                     });

        final StringWriter writer = new StringWriter();
        try (final XmlWriter xmlw = new XmlWriter(writer)) {
            xmlw.setEnabled(XmlWriter.Feature.ALLOW_PARTIAL_XML);
            xmlw.setEnabled(XmlWriter.Feature.REPLACE_NON_PRINTABLE_CHARS);
            xmlw.beginElement("root");
            xmlw.addElementContent(new String(new byte[] { 0x0 }));
            xmlw.endElement();
            assertEquals("<root>" + XmlWriter.DEFAULT_REPLACEMENT + "</root>", writer.toString());
        }
    }

    @Test
    void testDTD() throws IOException {
        final StringWriter writer = new StringWriter();
        try (final XmlWriter xmlw = new XmlWriter(writer)) {
            init(xmlw);

            xmlw.beginDocument();
            xmlw.addDTD(DTD);
            xmlw.beginElement("root");
            xmlw.endElement();
            xmlw.endDocument();
            xmlw.flush();

            final String expected = XML + EOL + DTD + EOL + "<root/>" + FINAL_EOL;
            check(expected, writer);
        }
    }

    @Test
    void testDTDInvalidState() {
        assertThrows(InvalidStateException.class,
                     () -> {
                         final StringWriter writer = new StringWriter();
                         try (final XmlWriter xmlw = new XmlWriter(writer)) {
                             xmlw.beginDocument();
                             xmlw.addDTD(DTD);
                             xmlw.addDTD(DTD);
                         }
                     });
        assertThrows(InvalidStateException.class,
                     () -> {
                         final StringWriter writer = new StringWriter();
                         try (final XmlWriter xmlw = new XmlWriter(writer)) {
                             xmlw.beginDocument();
                             xmlw.beginElement("root");
                             xmlw.addDTD(DTD);
                         }
                     });
    }
}