package cdc.io.xml;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class XmlUtilsTest {
    @Test
    void testXml10Char() {
        assertTrue(XmlUtils.isXml10Char('a'));
        assertTrue(XmlUtils.isXml10Char('0'));

        assertFalse(XmlUtils.isXml10Char(0x7));
    }

    @Test
    void testXml11Char() {
        assertTrue(XmlUtils.isXml11Char('a'));
        assertTrue(XmlUtils.isXml11Char('0'));

        assertTrue(XmlUtils.isXml11Char(0x7));
    }

    @Test
    void testXmlNameStartChar() {
        assertTrue(XmlUtils.isNameStartChar('a'));
        assertFalse(XmlUtils.isNameStartChar('1'));
        assertFalse(XmlUtils.isNameStartChar('-'));
        assertTrue(XmlUtils.isNameStartChar(':'));
        assertTrue(XmlUtils.isNameStartChar('_'));
    }

    @Test
    void testXmlNameChar() {
        assertTrue(XmlUtils.isNameChar('a'));
        assertTrue(XmlUtils.isNameChar('1'));
        assertTrue(XmlUtils.isNameChar('-'));
        assertTrue(XmlUtils.isNameChar(':'));
        assertTrue(XmlUtils.isNameChar('_'));
    }

    @Test
    void testIsValidName() {
        assertFalse(XmlUtils.isValidName(null));
        assertFalse(XmlUtils.isValidName(""));
        assertFalse(XmlUtils.isValidName("0"));
        assertFalse(XmlUtils.isValidName("-"));

        assertTrue(XmlUtils.isValidName("_"));
        assertTrue(XmlUtils.isValidName("a"));
    }

    @Test
    void testReplaceInvalidChars() {
        assertEquals("abcde", XmlUtils.replaceInvalidChars("abcde", XmlVersion.XML_1_0, "?"));
        for (int i = 0; i < 65000; i++) {
            final char c = (char) i;
            final String s = Character.toString(c);
            if (XmlUtils.isValidXml10(s)) {
                assertEquals(s, XmlUtils.replaceInvalidChars(s, XmlVersion.XML_1_0, "<?>"));
            } else {
                assertEquals("<?>", XmlUtils.replaceInvalidChars(s, XmlVersion.XML_1_0, "<?>"));
            }
            if (XmlUtils.isValidXml11(s)) {
                assertEquals(s, XmlUtils.replaceInvalidChars(s, XmlVersion.XML_1_1, "<?>"));
            } else {
                assertEquals("<?>", XmlUtils.replaceInvalidChars(s, XmlVersion.XML_1_1, "<?>"));
            }
        }
    }
}