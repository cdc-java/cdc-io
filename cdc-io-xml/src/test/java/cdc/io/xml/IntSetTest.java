package cdc.io.xml;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class IntSetTest {

    @Test
    public void testInvalidSet() {
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         new IntSet(0, 3, 5);
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         new IntSet(5, 0);
                     });
    }

    @Test
    public void testContains() {
        final IntSet set = new IntSet(0, 0, 3, 5);
        assertFalse(set.contains(-1));
        assertTrue(set.contains(0));
        assertFalse(set.contains(1));
        assertFalse(set.contains(2));
        assertTrue(set.contains(3));
        assertTrue(set.contains(4));
        assertTrue(set.contains(5));
        assertFalse(set.contains(6));
    }
}