package cdc.io.xml;

import java.io.IOException;

/**
 * Interface implemented by classes that can write an object to an {@link XmlHandler} as an element.
 *
 * @author Damien Carbonne
 *
 * @param <T> The object type.
 * @param <D> The data type.
 */
public interface XmlElementWriter<T, D> {
    /**
     * @return The class of objects that can be written.
     */
    public Class<T> getObjectClass();

    /**
     * @return The class of associated data.
     */
    public Class<D> getDataClass();

    /**
     * Writes an object as an element.
     *
     * @param handler The XML handler.
     * @param name The name of the element.
     * @param object The object to print.
     * @param data Data that can be used to write {@code object}.
     * @throws IOException When an IO error occurs.
     */
    public void writeElement(XmlHandler handler,
                             String name,
                             T object,
                             D data) throws IOException;

    public default void writeElementRaw(XmlHandler handler,
                                        String name,
                                        Object object,
                                        Object data) throws IOException {
        writeElement(handler, name, getObjectClass().cast(object), getDataClass().cast(data));
    }
}