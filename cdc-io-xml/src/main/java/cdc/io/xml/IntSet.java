package cdc.io.xml;

import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class IntSet {
    private static final Logger LOGGER = LogManager.getLogger(IntSet.class);
    private final int[] values;

    public IntSet(int... values) {
        if (values.length > 0) {
            if (values.length % 2 != 0) {
                throw new IllegalArgumentException("values length is not even");
            }
            for (int index = 1; index < values.length; index++) {
                if (values[index] < values[index - 1]) {
                    throw new IllegalArgumentException("values are not sorted: " + Arrays.toString(values));
                }
            }
            for (int index = 2; index < values.length; index++) {
                if (index % 2 == 0 && values[index] == values[index - 1] + 1 && LOGGER.isDebugEnabled()) {
                    LOGGER.warn("merge {} with {} in {}", index, (index - 1), Arrays.toString(values));
                }
            }
        }
        this.values = values.clone();
    }

    public boolean contains(int value) {
        final int index = Arrays.binarySearch(values, value);
        return index >= 0 || index % 2 == 0;
    }

    public int[] getValues() {
        return values.clone();
    }
}