package cdc.io.xml;

import java.io.IOException;

import cdc.util.strings.StringConversion;

public interface XmlHandler {
    /**
     * Begins an XML document, using writer's encoding.
     *
     * @param version The XML version to use.
     * @throws IOException If an I/O error occurs.
     */
    public void beginDocument(XmlVersion version) throws IOException;

    /**
     * Begins an XML document, using writer's encoding and version 1.0.
     *
     * @throws IOException If an I/O error occurs.
     */
    public default void beginDocument() throws IOException {
        beginDocument(XmlVersion.XML_1_0);
    }

    /**
     * Ends current Document.
     *
     * @throws IOException If an I/O error occurs.
     */
    public void endDocument() throws IOException;

    /**
     * Adds a default namespace.
     *
     * @param uri The namespace URI.
     * @throws IOException If an I/O error occurs.
     */
    public default void addDefaultNamespace(String uri) throws IOException {
        addAttribute("xmlns", uri);
    }

    /**
     * Adds a namespace declaration.
     *
     * @param prefix Namespace prefix
     * @param uri Associated URI.
     * @throws IOException If an I/O error occurs.
     */
    public default void addNamespace(String prefix,
                                     String uri) throws IOException {
        addAttribute("xmlns:" + prefix, uri);
    }

    /**
     * Adds a DTD.
     * <p>
     * <b>WARNING:</b> it is the caller responsibility to construct a correct DTD.
     *
     * @param dtd The DTD full content ({@code <!DOCTYPE ... [ ... ]>}).
     * @throws IOException If an I/O error occurs.
     */
    public void addDTD(String dtd) throws IOException;

    /**
     * Begins a Processing Instruction.
     *
     * @param target The PI target.
     * @throws IOException If an I/O error occurs.
     */
    public void beginProcessingInstruction(String target) throws IOException;

    /**
     * Adds a Processing Instruction content.
     *
     * @param content The PI content.
     * @throws IOException If an I/O error occurs.
     */
    public void addProcessingInstructionContent(String content) throws IOException;

    /**
     * Ends a Processing Instruction.
     *
     * @throws IOException If an I/O error occurs.
     */
    public void endProcessingInstruction() throws IOException;

    /**
     * Adds a Processing Instruction.
     *
     * @param target The PI target.
     * @param content The PI content.
     * @throws IOException If an I/O error occurs.
     */
    public void addProcessingInstruction(String target,
                                         String content) throws IOException;

    /**
     * Helper function used to insert a StyleSheet Processing Instruction.
     *
     * @param xsl The URL of the style sheet to use.
     * @throws IOException If an I/O error occurs.
     */
    public default void addStyleSheet(String xsl) throws IOException {
        beginProcessingInstruction("xml-stylesheet");
        addAttribute("type", "text/xsl");
        addAttribute("href", xsl);
        endProcessingInstruction();
    }

    /**
     * Starts the writing of a new comment.
     *
     * @throws IOException If an I/O error occurs.
     */
    public void beginComment() throws IOException;

    /**
     * Adds content to current comment.
     *
     * @param content The text to add to current comment.
     * @throws IOException When IO error occurs.
     */
    public void addCommentContent(String content) throws IOException;

    /**
     * Closes the current comment.
     *
     * @throws IOException If an I/O error occurs.
     */
    public void endComment() throws IOException;

    /**
     * Inserts a comment section with a content.
     *
     * @param content The comment content.
     * @throws IOException If an I/O error occurs.
     */
    public default void addComment(String content) throws IOException {
        beginComment();
        addCommentContent(content);
        endComment();
    }

    /**
     * Starts the writing of a new CData.
     *
     * @throws IOException If an I/O error occurs.
     */
    public void beginCData() throws IOException;

    /**
     * Adds content to current CData.
     *
     * @param content The text to add to current CData.
     * @throws IOException When IO error occurs.
     */
    public void addCDataContent(String content) throws IOException;

    /**
     * Closes the current CData.
     *
     * @throws IOException If an I/O error occurs.
     */
    public void endCData() throws IOException;

    /**
     * Inserts a CData section with a content.
     *
     * @param content The CData content.
     * @throws IOException If an I/O error occurs.
     */
    public default void addCData(String content) throws IOException {
        beginCData();
        addCDataContent(content);
        endCData();
    }

    /**
     * Begins an element.
     *
     * @param name The element name.
     * @throws IOException If an I/O error occurs.
     */
    public void beginElement(String name) throws IOException;

    /**
     * Adds text content to current element.
     * <p>
     * <b>Note:</b> content is escaped if necessary.
     *
     * @param content The content.
     * @throws IOException If an I/O error occurs.
     */
    public void addElementContent(String content) throws IOException;

    /**
     * Adds unescaped text content to current element.
     * <p>
     * <b>Note:</b> content is NOT escaped. This should be used to write specific entities.
     *
     * @param content The content.
     * @throws IOException If an I/O error occurs.
     */
    public void addUnescapedElementContent(String content) throws IOException;

    /**
     * Ends an element.
     *
     * @throws IOException If an I/O error occurs.
     */
    public void endElement() throws IOException;

    /**
     * Adds an element with a text content.
     * <p>
     * <b>Note:</b> Content is escaped if necessary.
     *
     * @param name The element name.
     * @param content The element content.
     * @throws IOException If an I/O error occurs.
     */
    public default void addElement(String name,
                                   String content) throws IOException {
        beginElement(name);
        addElementContent(content);
        endElement();
    }

    public default void addElementIfNonEmpty(String name,
                                             String content) throws IOException {
        if (content != null && content.length() > 0) {
            addElement(name, content);
        }
    }

    /**
     * Adds a String attribute to current element.
     * <p>
     * <b>Note:</b> value is escaped if necessary.
     *
     * @param name Attribute name.
     * @param value Attribute value.
     * @throws IOException If an I/O error occurs.
     */
    public void addAttribute(String name,
                             String value) throws IOException;

    /**
     * Adds a String attribute to current element.
     * <p>
     * <b>Note:</b> value is NOT escaped. This should be used to write specific entities.
     *
     * @param name Attribute name.
     * @param value Attribute value.
     * @throws IOException If an I/O error occurs.
     */
    public void addUnescapedAttribute(String name,
                                      String value) throws IOException;

    /**
     * Adds an Object attribute to current element.
     * <p>
     * If object is null adds an empty attribute.<br>
     * Otherwise, uses toString() to convert value to a String.
     *
     *
     * @param name Attribute name.
     * @param value Attribute value.
     * @throws IOException If an I/O error occurs.
     */
    public default void addAttribute(String name,
                                     Object value) throws IOException {
        if (value == null) {
            addAttribute(name, "");
        } else {
            addAttribute(name, value.toString());
        }
    }

    /**
     * Adds a char attribute to current element.
     *
     * @param name Attribute name.
     * @param value Attribute value.
     * @throws IOException If an I/O error occurs.
     */
    public default void addAttribute(String name,
                                     char value) throws IOException {
        addAttribute(name, Character.toString(value));
    }

    /**
     * Adds a boolean attribute to current element.
     *
     * @param name Attribute name.
     * @param value Attribute value.
     * @throws IOException If an I/O error occurs.
     */
    public default void addAttribute(String name,
                                     boolean value) throws IOException {
        addAttribute(name, StringConversion.asString(value));
    }

    /**
     * Adds a long attribute to current element.
     *
     * @param name Attribute name.
     * @param value Attribute value.
     * @throws IOException If an I/O error occurs.
     */
    public default void addAttribute(String name,
                                     long value) throws IOException {
        addAttribute(name, StringConversion.asString(value));
    }

    /**
     * Adds an int attribute to current element.
     *
     * @param name Attribute name.
     * @param value Attribute value.
     * @throws IOException If an I/O error occurs.
     */
    public default void addAttribute(String name,
                                     int value) throws IOException {
        addAttribute(name, StringConversion.asString(value));
    }

    /**
     * Adds a short attribute to current element.
     *
     * @param name Attribute name.
     * @param value Attribute value.
     * @throws IOException If an I/O error occurs.
     */
    public default void addAttribute(String name,
                                     short value) throws IOException {
        addAttribute(name, StringConversion.asString(value));
    }

    /**
     * Adds a byte attribute to current element.
     *
     * @param name Attribute name.
     * @param value Attribute value.
     * @throws IOException If an I/O error occurs.
     */
    public default void addAttribute(String name,
                                     byte value) throws IOException {
        addAttribute(name, StringConversion.asString(value));
    }

    /**
     * Adds a double attribute to current element.
     *
     * @param name Attribute name.
     * @param value Attribute value.
     * @throws IOException If an I/O error occurs.
     */
    public default void addAttribute(String name,
                                     double value) throws IOException {
        addAttribute(name, StringConversion.asString(value));
    }

    /**
     * Adds a float attribute to current element.
     *
     * @param name Attribute name.
     * @param value Attribute value.
     * @throws IOException If an I/O error occurs.
     */
    public default void addAttribute(String name,
                                     float value) throws IOException {
        addAttribute(name, StringConversion.asString(value));
    }
}