package cdc.io.xml;

import java.util.Arrays;

import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.util.lang.Checks;
import cdc.util.lang.DataException;
import cdc.util.lang.FailureReaction;
import cdc.util.lang.InvalidDataException;
import cdc.util.lang.NotFoundException;
import cdc.util.strings.StringConversion;

/**
 * Class that can facilitate the writing of an XML parser over an XMLStreamReader.
 *
 * @author Damien Carbonne
 * @param <R> The result type.
 */
public abstract class AbstractStAXParser<R> {
    protected final Logger logger = LogManager.getLogger(getClass());
    protected final XMLStreamReader reader;
    protected final String systemId;
    protected final FailureReaction reaction;

    protected AbstractStAXParser(XMLStreamReader reader,
                                 String systemId,
                                 FailureReaction reaction) {
        Checks.isNotNull(reader, "reader");
        this.reader = reader;
        this.systemId = systemId;
        this.reaction = reaction;
    }

    public XMLStreamReader getReader() {
        return reader;
    }

    public String getSystemId() {
        return systemId;
    }

    /**
     * @return The reaction to adopt in case of error.
     */
    public final FailureReaction getReaction() {
        return reaction;
    }

    /**
     * Function that either keeps silent, warns or raises an exception,
     * depending on error reaction.
     *
     * @param message The error message.
     * @throws DataException When error reaction is {@link FailureReaction#FAIL}.
     */
    public final void onError(String message) {
        FailureReaction.onError(message, logger, reaction, DataException::new);
    }

    /**
     * Function that either silently returns a value, warns and returns a value
     * or raises an exception, depending on error reaction.
     *
     * @param <T> The return type.
     * @param message The error message.
     * @param def The default return value.
     * @return {@code def} if error reaction is {@link FailureReaction#DEFAULT}
     *         or {@link FailureReaction#WARN}.
     * @throws DataException When error reaction is {@link FailureReaction#FAIL}.
     */
    public final <T> T onError(String message,
                               T def) {
        return FailureReaction.onError(message, logger, reaction, def, DataException::new);
    }

    /**
     * Function that returns a computed value if it is not {@code null},
     * or silently returns a default value, warns and returns a default value
     * or throws an exception.
     *
     * @param <T> The return type.
     * @param result The computed result.
     * @param message The error message.
     * @param def The default return value.
     * @return {@code result} if it is not {@code null}, or {@code def} if
     *         error reaction is {@link FailureReaction#DEFAULT}
     *         or {@link FailureReaction#WARN}.
     * @throws DataException When {@code result} is {@code null}
     *             and error reaction is {@link FailureReaction#FAIL}.
     */
    public final <T> T onResult(T result,
                                String message,
                                T def) {
        return FailureReaction.onResult(result, message, logger, reaction, def, DataException::new);
    }

    public final Logger getLogger() {
        return logger;
    }

    public void trace(String message) {
        if (logger.isTraceEnabled()) {
            logger.trace("{}: {}", message, StAXSupport.toString(reader));
        }
    }

    public void trace() {
        trace("located on");
    }

    /**
     * Base method called to parse the stream.
     *
     * @return The parsed result.
     * @throws XMLStreamException When there is an error parsing the underlying XML source.
     */
    protected abstract R parse() throws XMLStreamException;

    /**
     * Throws an InvalidDataExeption.
     *
     * @param message The exception message.
     * @return A new instance of InvalidDataException.
     */
    public final InvalidDataException error(String message) {
        logger.error(message);
        return new InvalidDataException(message);
    }

    /**
     * @return A new instance of InvalidDataException.
     */
    public final InvalidDataException unexpectedEvent() {
        return error("Unexpected event: " + StAXSupport.toString(reader));
    }

    /**
     * Gets the next parsing event.
     * <p>
     * It is a simple wrapper of {@link XMLStreamReader#next()}.
     *
     * @return The integer code corresponding to the current parse event.
     *
     * @throws XMLStreamException When there is an error parsing the underlying XML source.
     */
    public int next() throws XMLStreamException {
        trace("next()    called on");
        return reader.next();
    }

    /**
     * Skips any white space (isWhiteSpace() returns true), COMMENT,
     * or PROCESSING_INSTRUCTION, until a START_ELEMENT or END_ELEMENT is reached.
     * <p>
     * It is a simple wrapper of {@link XMLStreamReader#nextTag()}.
     * <p>
     * If other than white space characters, COMMENT, PROCESSING_INSTRUCTION,
     * START_ELEMENT, END_ELEMENT are encountered, an exception is thrown.
     * This method should be used when processing element-only content separeted by white space.
     *
     * @return The event type of the element read (START_ELEMENT or END_ELEMENT).
     * @throws XMLStreamException When the current event is not white space, PROCESSING_INSTRUCTION, START_ELEMENT or END_ELEMENT.
     */
    public int nextTag() throws XMLStreamException {
        trace("nextTag() called on");
        final int result = reader.nextTag();
        // trace(" moved to");
        return result;
    }

    /**
     * Must be called after a START_ELEMENT has been found.
     * <p>
     * Skips all events till the corresponding END_ELEMENT is reached.
     *
     * @throws XMLStreamException If there is a fatal error detecting the next states.
     */
    public void ignoreElement() throws XMLStreamException {
        trace("ignoreElement()");
        int depth = 1;
        final String name = reader.getLocalName();

        boolean go = true;
        while (go) {
            switch (reader.getEventType()) {
            case XMLStreamConstants.ATTRIBUTE:
            case XMLStreamConstants.CDATA:
            case XMLStreamConstants.CHARACTERS:
            case XMLStreamConstants.COMMENT:
            case XMLStreamConstants.SPACE:
                next();
                break;
            case XMLStreamConstants.START_ELEMENT:
                depth++;
                next();
                break;
            case XMLStreamConstants.END_ELEMENT:
                depth--;
                if (depth == 1) {
                    go = false;
                } else {
                    next();
                }
                break;
            default:
                throw new IllegalStateException();
            }
        }

        expectEndElement("ignoreElement()", name);
    }

    /**
     * Interface used by {@link AbstractStAXParser#parseChildren}
     *
     * @author Damien Carbonne
     *
     * @param <C> The context type.
     */
    @FunctionalInterface
    public static interface ContextElementParser<C> {
        public void parseChild(C context) throws XMLStreamException;
    }

    /**
     * Utility used to parse an element and all its children.
     *
     * @param <C> The context type.
     * @param context The context to pass to {@code childParser}.
     * @param name The element name.
     * @param childParser The child parser.
     * @throws XMLStreamException When there is an error parsing the underlying XML source.
     */
    public <C> void parseChildren(C context,
                                  String name,
                                  ContextElementParser<C> childParser) throws XMLStreamException {
        if (isStartElement(name)) {
            nextTag();
            // Now cursor is either on the first child start tag or on the closing tag of parent
            while (reader.isStartElement()) {
                childParser.parseChild(context);
                nextTag();
            }
            expectEndElement("parseChildren()", name);
        } else {
            throw unexpectedEvent();
        }
    }

    @FunctionalInterface
    public static interface ElementParser {
        public void parseChild() throws XMLStreamException;
    }

    public void parseChildren(String name,
                              ElementParser childParser) throws XMLStreamException {
        if (isStartElement(name)) {
            nextTag();
            while (reader.isStartElement()) {
                childParser.parseChild();
                nextTag();
            }
            expectEndElement("parseChildren()", name);
        } else {
            throw unexpectedEvent();
        }
    }

    /**
     * Checks that current event type is an expected one.
     * <p>
     * If {@code false}, raises an exception.
     *
     * @param context The call context.
     * @param eventType The expected event type.
     * @throws InvalidDataException When current event type is not the expected one.
     */
    public void expect(String context,
                       int eventType) {
        if (reader.getEventType() != eventType) {
            throw error("[" + context + "] Unexpected event: " + StAXSupport.toString(reader)
                    + ", expected: " + StAXSupport.eventTypeToString(eventType));
        }
    }

    public void expectStartElement(String context) {
        expect(context, XMLStreamConstants.START_ELEMENT);
    }

    /**
     * Checks that current event type is START_ELEMNT.
     * <p>
     * If {@code false}, raises an exception.
     *
     * @param context The call context.
     * @param name The expected element name.
     * @throws InvalidDataException When current event type is not START_ELEMENT or name does not match.
     */
    public void expectStartElement(String context,
                                   String name) {
        expect(context, XMLStreamConstants.START_ELEMENT);
        if (!name.equals(reader.getLocalName())) {
            throw error("[" + context + "] Unexpected element name: " + reader.getLocalName() + ", expected: " + name);
        }
    }

    public void expectStartElement(String context,
                                   String... names) {
        expect(context, XMLStreamConstants.START_ELEMENT);
        for (final String name : names) {
            if (!name.equals(reader.getLocalName())) {
                return;
            }
        }
        throw error("[" + context + "] Unexpected element name: " + reader.getLocalName() + ", expected: "
                + Arrays.toString(names));
    }

    /**
     * Checks that current event type is END_ELEMNT.
     * <p>
     * If {@code false}, raises an exception.
     *
     * @param context The call context.
     * @param name The expected element name.
     * @throws InvalidDataException When current event type is not END_ELEMENT or name does not match.
     */
    public void expectEndElement(String context,
                                 String name) {
        expect(context, XMLStreamConstants.END_ELEMENT);
        if (!name.equals(reader.getLocalName())) {
            throw error("[" + context + "] Unexpected element name: " + reader.getLocalName() + ", expected: " + name);
        }
    }

    public void expectEndElement(String context,
                                 String... names) {
        expect(context, XMLStreamConstants.END_ELEMENT);
        for (final String name : names) {
            if (!name.equals(reader.getLocalName())) {
                return;
            }
        }
        throw error("[" + context + "] Unexpected element name: " + reader.getLocalName() + ", expected: "
                + Arrays.toString(names));
    }

    /**
     * Checks that current event type is START_DOCUMENT.
     * <p>
     * If {@code false}, raises an exception.
     *
     * @param context The call context.
     * @throws InvalidDataException When current event type is not START_DOCUMENT.
     */
    public void expectStartDocument(String context) {
        expect(context, XMLStreamConstants.START_DOCUMENT);
    }

    /**
     * Checks that current event type is END_DOCUMENT.
     * <p>
     * If {@code false}, raises an exception.
     *
     * @param context The call context.
     * @throws InvalidDataException When current event type is not END_DOCUMENT.
     */
    public void expectEndDocument(String context) {
        expect(context, XMLStreamConstants.END_DOCUMENT);
    }

    /**
     * Returns {@code true} if reader is on a START_ELEMENT that has a given name.
     *
     * @param name The element name.
     * @return {@code true} if reader is on a START_ELEMENT named {@code name}.
     */
    public boolean isStartElement(String name) {
        return reader.isStartElement() && name.equals(reader.getLocalName());
    }

    /**
     * Returns an attribute value.
     *
     * @param name The attribute name.
     * @param def The default value.
     * @param missingReaction The reaction to adopt when no attribute named {@code name} is found.
     * @return The attribute value, or {@code def}.
     * @throws NotFoundException When {@code missingReaction} is FAIL and no attribute is found.
     */
    public String getAttributeValue(String name,
                                    String def,
                                    FailureReaction missingReaction) {
        final String value = reader.getAttributeValue(null, name);
        if (value == null) {
            if (missingReaction == FailureReaction.DEFAULT) {
                return def;
            } else if (missingReaction == FailureReaction.WARN) {
                logger.warn("No attribute named '{}'", name);
                return def;
            } else {
                throw new NotFoundException("No attribute named '" + name + "'");
            }
        } else {
            return value;
        }
    }

    /**
     * Returns an attribute value or a default value.
     *
     * @param name The attribute name.
     * @param def The default value.
     * @return The attribute value, or {@code def}.
     */
    public String getAttributeValue(String name,
                                    String def) {
        return getAttributeValue(name,
                                 def,
                                 FailureReaction.DEFAULT);
    }

    /**
     * Returns an attribute value.
     *
     * @param name The attribute name.
     * @return The attribute value.
     * @throws NotFoundException When no attribute is found.
     */
    public String getAttributeValue(String name) {
        return getAttributeValue(name,
                                 null,
                                 FailureReaction.FAIL);
    }

    public boolean getAttributeAsBoolean(String name,
                                         boolean def,
                                         FailureReaction missingReaction,
                                         FailureReaction errorReaction) {
        return StringConversion.asBoolean(getAttributeValue(name, null),
                                          def,
                                          missingReaction,
                                          errorReaction);
    }

    public boolean getAttributeAsBoolean(String name,
                                         boolean def) {
        return getAttributeAsBoolean(name,
                                     def,
                                     FailureReaction.DEFAULT,
                                     FailureReaction.FAIL);
    }

    public boolean getAttributeAsBoolean(String name) {
        return getAttributeAsBoolean(name,
                                     false,
                                     FailureReaction.FAIL,
                                     FailureReaction.FAIL);
    }

    public Boolean getAttributeAsOptionalBoolean(String name,
                                                 Boolean def,
                                                 FailureReaction errorReaction) {
        return StringConversion.asOptionalBoolean(getAttributeValue(name, null),
                                                  def,
                                                  errorReaction);
    }

    public Boolean getAttributeAsOptionalBoolean(String name,
                                                 Boolean def) {
        return getAttributeAsOptionalBoolean(name, def, FailureReaction.FAIL);
    }

    public long getAttributeAsLong(String name,
                                   long def,
                                   FailureReaction missingReaction,
                                   FailureReaction errorReaction) {
        return StringConversion.asLong(getAttributeValue(name, null),
                                       def,
                                       missingReaction,
                                       errorReaction);
    }

    public long getAttributeAsLong(String name,
                                   long def) {
        return getAttributeAsLong(name,
                                  def,
                                  FailureReaction.DEFAULT,
                                  FailureReaction.FAIL);
    }

    public long getAttributeAsLong(String name) {
        return getAttributeAsLong(name,
                                  0L,
                                  FailureReaction.FAIL,
                                  FailureReaction.FAIL);
    }

    public Long getAttributeAsOptionalLong(String name,
                                           Long def,
                                           FailureReaction errorReaction) {
        return StringConversion.asOptionalLong(getAttributeValue(name, null),
                                               def,
                                               errorReaction);
    }

    public Long getAttributeAsOptionalLong(String name,
                                           Long def) {
        return getAttributeAsOptionalLong(name,
                                          def,
                                          FailureReaction.FAIL);
    }

    public int getAttributeAsInt(String name,
                                 int def,
                                 FailureReaction missingReaction,
                                 FailureReaction errorReaction) {
        return StringConversion.asInt(getAttributeValue(name, null),
                                      def,
                                      missingReaction,
                                      errorReaction);
    }

    public int getAttributeAsInt(String name,
                                 int def) {
        return getAttributeAsInt(name,
                                 def,
                                 FailureReaction.DEFAULT,
                                 FailureReaction.FAIL);
    }

    public int getAttributeAsInt(String name) {
        return getAttributeAsInt(name,
                                 0,
                                 FailureReaction.FAIL,
                                 FailureReaction.FAIL);
    }

    public Integer getAttributeAsOptionalInt(String name,
                                             Integer def,
                                             FailureReaction errorReaction) {
        return StringConversion.asOptionalInt(getAttributeValue(name, null),
                                              def,
                                              errorReaction);
    }

    public Integer getAttributeAsOptionalInt(String name,
                                             Integer def) {
        return getAttributeAsOptionalInt(name,
                                         def,
                                         FailureReaction.FAIL);
    }

    public short getAttributeAsShort(String name,
                                     short def,
                                     FailureReaction missingReaction,
                                     FailureReaction errorReaction) {
        return StringConversion.asShort(getAttributeValue(name, null),
                                        def,
                                        missingReaction,
                                        errorReaction);
    }

    public short getAttributeAsShort(String name,
                                     short def) {
        return getAttributeAsShort(name,
                                   def,
                                   FailureReaction.DEFAULT,
                                   FailureReaction.FAIL);
    }

    public short getAttributeAsShort(String name) {
        return getAttributeAsShort(name,
                                   (short) 0,
                                   FailureReaction.FAIL,
                                   FailureReaction.FAIL);
    }

    public Short getAttributeAsOptionalShort(String name,
                                             Short def,
                                             FailureReaction errorReaction) {
        return StringConversion.asOptionalShort(getAttributeValue(name, null),
                                                def,
                                                errorReaction);
    }

    public Short getAttributeAsOptionalShort(String name,
                                             Short def) {
        return getAttributeAsOptionalShort(name,
                                           def,
                                           FailureReaction.FAIL);
    }

    public byte getAttributeAsByte(String name,
                                   byte def,
                                   FailureReaction missingReaction,
                                   FailureReaction errorReaction) {
        return StringConversion.asByte(getAttributeValue(name, null),
                                       def,
                                       missingReaction,
                                       errorReaction);
    }

    public byte getAttributeAsByte(String name,
                                   byte def) {
        return getAttributeAsByte(name,
                                  def,
                                  FailureReaction.DEFAULT,
                                  FailureReaction.FAIL);
    }

    public byte getAttributeAsByte(String name) {
        return getAttributeAsByte(name,
                                  (byte) 0,
                                  FailureReaction.FAIL,
                                  FailureReaction.FAIL);
    }

    public Byte getAttributeAsOptionalByte(String name,
                                           Byte def,
                                           FailureReaction errorReaction) {
        return StringConversion.asOptionalByte(getAttributeValue(name, null),
                                               def,
                                               errorReaction);
    }

    public Byte getAttributeAsOptionalByte(String name,
                                           Byte def) {
        return getAttributeAsOptionalByte(name,
                                          def,
                                          FailureReaction.FAIL);
    }

    public double getAttributeAsDouble(String name,
                                       double def,
                                       FailureReaction missingReaction,
                                       FailureReaction errorReaction) {
        return StringConversion.asDouble(getAttributeValue(name, null),
                                         def,
                                         missingReaction,
                                         errorReaction);
    }

    public double getAttributeAsDouble(String name,
                                       double def) {
        return getAttributeAsDouble(name,
                                    def,
                                    FailureReaction.DEFAULT,
                                    FailureReaction.FAIL);
    }

    public double getAttributeAsDouble(String name) {
        return getAttributeAsDouble(name,
                                    0.0,
                                    FailureReaction.FAIL,
                                    FailureReaction.FAIL);
    }

    public Double getAttributeAsOptionalDouble(String name,
                                               Double def,
                                               FailureReaction errorReaction) {
        return StringConversion.asOptionalDouble(getAttributeValue(name, null),
                                                 def,
                                                 errorReaction);
    }

    public Double getAttributeAsOptionalDouble(String name,
                                               Double def) {
        return getAttributeAsOptionalDouble(name,
                                            def,
                                            FailureReaction.FAIL);
    }

    public float getAttributeAsFloat(String name,
                                     float def,
                                     FailureReaction missingReaction,
                                     FailureReaction errorReaction) {
        return StringConversion.asFloat(getAttributeValue(name, null),
                                        def,
                                        missingReaction,
                                        errorReaction);
    }

    public float getAttributeAsFloat(String name,
                                     float def) {
        return getAttributeAsFloat(name,
                                   def,
                                   FailureReaction.DEFAULT,
                                   FailureReaction.FAIL);
    }

    public float getAttributeAsFloat(String name) {
        return getAttributeAsFloat(name,
                                   0.0f,
                                   FailureReaction.FAIL,
                                   FailureReaction.FAIL);
    }

    public Float getAttributeAsOptionalFloat(String name,
                                             Float def,
                                             FailureReaction errorReaction) {
        return StringConversion.asOptionalFloat(getAttributeValue(name, null),
                                                def,
                                                errorReaction);
    }

    public Float getAttributeAsOptionalFloat(String name,
                                             Float def) {
        return getAttributeAsOptionalFloat(name,
                                           def,
                                           FailureReaction.FAIL);
    }

    public Enum<?> getAttributeAsRawEnum(String name,
                                         Class<? extends Enum<?>> enumClass,
                                         Enum<?> def,
                                         FailureReaction missingReaction,
                                         FailureReaction errorReaction) {
        return StringConversion.asRawEnum(getAttributeValue(name, null),
                                          enumClass,
                                          def,
                                          missingReaction,
                                          errorReaction);
    }

    public Enum<?> getAttributeAsRawEnum(String name,
                                         Class<? extends Enum<?>> enumClass,
                                         Enum<?> def) {
        return getAttributeAsRawEnum(name,
                                     enumClass,
                                     def,
                                     FailureReaction.DEFAULT,
                                     FailureReaction.FAIL);
    }

    public Enum<?> getAttributeAsRawEnum(String name,
                                         Class<? extends Enum<?>> enumClass) {
        return getAttributeAsRawEnum(name,
                                     enumClass,
                                     null,
                                     FailureReaction.FAIL,
                                     FailureReaction.FAIL);
    }

    public Enum<?> getAttributeAsOptionalRawEnum(String name,
                                                 Class<? extends Enum<?>> enumClass,
                                                 Enum<?> def,
                                                 FailureReaction errorReaction) {
        return StringConversion.asOptionalRawEnum(getAttributeValue(name, null),
                                                  enumClass,
                                                  def,
                                                  errorReaction);
    }

    public Enum<?> getAttributeAsOptionalRawEnum(String name,
                                                 Class<? extends Enum<?>> enumClass,
                                                 Enum<?> def) {
        return getAttributeAsOptionalRawEnum(name,
                                             enumClass,
                                             def,
                                             FailureReaction.FAIL);
    }

    public <E extends Enum<E>> E getAttributeAsEnum(String name,
                                                    Class<E> enumClass,
                                                    E def,
                                                    FailureReaction missingReaction,
                                                    FailureReaction errorReaction) {
        return StringConversion.asEnum(getAttributeValue(name, null),
                                       enumClass,
                                       def,
                                       missingReaction,
                                       errorReaction);
    }

    public <E extends Enum<E>> E getAttributeAsEnum(String name,
                                                    Class<E> enumClass,
                                                    E def) {
        return getAttributeAsEnum(name,
                                  enumClass,
                                  def,
                                  FailureReaction.DEFAULT,
                                  FailureReaction.FAIL);
    }

    public <E extends Enum<E>> E getAttributeAsEnum(String name,
                                                    Class<E> enumClass) {
        return getAttributeAsEnum(name,
                                  enumClass,
                                  null,
                                  FailureReaction.FAIL,
                                  FailureReaction.FAIL);
    }

    public <E extends Enum<E>> E getAttributeAsOptionalEnum(String name,
                                                            Class<E> enumClass,
                                                            E def,
                                                            FailureReaction errorReaction) {
        return StringConversion.asOptionalEnum(getAttributeValue(name, null),
                                               enumClass,
                                               def,
                                               errorReaction);
    }

    public <E extends Enum<E>> E getAttributeAsOptionalEnum(String name,
                                                            Class<E> enumClass,
                                                            E def) {
        return getAttributeAsOptionalEnum(name,
                                          enumClass,
                                          def,
                                          FailureReaction.FAIL);
    }
}