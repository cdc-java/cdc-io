package cdc.io.xml;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import javax.xml.stream.XMLStreamException;

import cdc.util.lang.Checks;
import cdc.util.lang.NotFoundException;

public class StAXReading {
    private static final Map<Class<?>, StAXElementReader<?, ?>> map = new HashMap<>();

    public StAXReading() {
        super();
    }

    public <T, C> void register(StAXElementReader<T, C> reader) {
        Checks.isNotNull(reader, "reader");
        Checks.doesNotContainKey(map, reader.getObjectClass(), "reader.getObjectClass()");

        map.put(reader.getObjectClass(), reader);
    }

    public <T, C> StAXElementReader<T, C> getElementReaderOrNull(Class<T> objectClass,
                                                                 Class<C> contextClass) {
        Checks.isNotNull(objectClass, "objectClass");
        Checks.isNotNull(contextClass, "contextClass");

        @SuppressWarnings("unchecked")
        final StAXElementReader<T, C> result = (StAXElementReader<T, C>) map.get(objectClass);
        if (result != null) {
            Checks.assertTrue(result.getContextClass().isAssignableFrom(contextClass),
                              "Context class mismatch, expected: {}, found: {}",
                              result.getContextClass(),
                              contextClass);
        }
        return result;
    }

    public <T, D> StAXElementReader<T, D> getElementReader(Class<T> objectClass,
                                                           Class<D> contextClass) {
        final StAXElementReader<T, D> result = getElementReaderOrNull(objectClass, contextClass);
        return NotFoundException.onResult(result, "No reader found for " + objectClass.getCanonicalName());
    }

    public <T, C> T parseElement(AbstractStAXParser<?> parser,
                                 Function<String, Class<? extends T>> nameToClass,
                                 C context) throws XMLStreamException {
        parser.expectStartElement("StAXReading.parseElement(...)");
        final String name = parser.getReader().getLocalName();
        final Class<? extends T> objectClass = nameToClass.apply(name);
        @SuppressWarnings("unchecked")
        final Class<C> contextClass = (Class<C>) context.getClass();
        final StAXElementReader<? extends T, C> reader = getElementReader(objectClass, contextClass);
        return reader.parseElementRaw(parser, context);
    }
}