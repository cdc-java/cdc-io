package cdc.io.xml;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.Flushable;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.io.compress.CompressionUtils;
import cdc.io.compress.Compressor;
import cdc.io.utils.NonCloseableOutputStream;
import cdc.io.xml.XmlWriterContext.Type;
import cdc.util.lang.Checks;
import cdc.util.lang.IntMasks;
import cdc.util.lang.InvalidDataException;
import cdc.util.lang.InvalidStateException;
import cdc.util.strings.CaseConverter;

/**
 * Basic XML writer.
 * <p>
 * This class does not support all XML features.
 * It supports specific features:
 * <ul>
 * <li>Pretty printing.
 * <li>Name conversion for elements and attributes.
 * <li>Standard ('\n') or platform specific End of line.
 * <li>Standard compression algorithms.
 * <li>...
 * </ul>
 * <b>WARNING:</b> This class does not check full conformity of generated XML.
 * <p>
 * <b>Pretty printing</b><br>
 * Pretty printing may produce unexpected results with mixed content elements.<br>
 * This implementation tries to produce data as soon as possible.<br>
 * Discovering that an element has a mixed content may happen late, so that indentation
 * behavior changes after such discovery.<br>
 * If one knows that an element has a mixed content, it is advised to indicate it to the
 * writer by writing an empty string content first in that element.
 *
 * @author Damien Carbonne
 *
 */
public class XmlWriter implements XmlHandler, Flushable, Closeable {
    private static final Logger LOGGER = LogManager.getLogger(XmlWriter.class);
    private static final String CDATA_START = "<![CDATA[";
    private static final String CDATA_END = "]]>";
    protected static final Pattern CDATA_END_PATTERN = Pattern.compile(CDATA_END);
    private static final String SINGLE_QUOTE = "'";
    private static final String DOUBLE_QUOTE = "\"";
    public static final String LF = "&#xA;";
    public static final String CR = "&#xD;";
    public static final String TAB = "&#x9;";
    public static final String GT = "&gt;";
    public static final String LT = "&lt;";
    public static final String QUOT = "&quot;";
    public static final String APOS = "&apos;";
    public static final String AMP = "&amp;";
    /** The default string used to replace non printable characters. */
    public static final String DEFAULT_REPLACEMENT = "\uFFFD";

    public static final String XMLNS = "xmlns";
    public static final String XMLNS_XSI = "xmlns:xsi";
    public static final String XML_SCHEMA_INSTANCE = "http://www.w3.org/2001/XMLSchema-instance";
    public static final String XSI_SCHEMA_LOCATION = "xsi:schemaLocation";

    protected XmlVersion version = null;

    /** Mask of enabled features. */
    private int features = 0;

    /** {@link Feature#DONT_VALIDATE_NAMES} feature as boolean for better performances. */
    protected boolean dontValidateNames = false;
    /** {@link Feature#DONT_VALIDATE_CHARS} feature as boolean for better performances. */
    protected boolean dontValidateChars = false;
    /** {@link Feature#REPLACE_NON_PRINTABLE_CHARS} feature as boolean for better performances. */
    protected boolean replaceNonPrintableChars = false;
    /** {@link Feature#ALWAYS_ENTITIZE_ELEMENTS} feature as boolean for better performances. */
    protected boolean alwaysEntitizeElements = false;

    /** Attribute delimiter to use. */
    private String attributeDelimiter = DOUBLE_QUOTE;

    /** End Of Line string. */
    private String eol = System.lineSeparator();

    /** Current indentation level */
    private int level = 0;

    /** Indent string. */
    private String indent = "  ";

    private String nonPrintableCharReplacement = DEFAULT_REPLACEMENT;

    private XmlWriterContext context = new XmlWriterContext();

    /** Buffer used for comments and CDATA. */
    private final StringBuilder buffer = new StringBuilder();

    private final Writer out;

    /** Encoding used to encode characters. */
    private final String encoding;

    private CaseConverter converter = CaseConverter.builder().build();
    private boolean needsEOL = false;

    /**
     * Enumeration of features supported by the writer.
     *
     * @author Damien Carbonne
     *
     */
    public enum Feature {
        /** If set, pretty print output. */
        PRETTY_PRINT,

        /**
         * If set, use LF for EOL.
         * <p>
         * Otherwise, use platform way to mark EOL.<br>
         * This applies to text, comments and attributes.
         */
        USE_XML_EOL,

        /**
         * If set, and PRETTY_PRINT is also enabled, appends EOL on last line.
         * <p>
         * Otherwise, last line does not end with EOL.
         */
        APPEND_FINAL_EOL,

        /**
         * If set, use single quote instead of double quote for attribute delimiter.
         */
        USE_SINGLE_QUOTE,

        /**
         * If set, always replaces certain characters by their entity in attributes, even if this is not necessary.
         * <ul>
         * <li>{@code '} is replaced even if delimiter is {@code "}.
         * <li>{@code "} is replaced even if delimiter is {@code '}.
         * <li>{@code >} is replaced even if not mandatory.
         * </ul>
         * <b>WARNING:</b> This option has no effect of replacement of:
         * <ul>
         * <li>{@code \t}, which is always replaced.
         * <li>{@code \n}, which is always replaced.
         * <li>{@code \r}, which is always replaced.
         * <li>{@code &}, which is always replaced.
         * <li>{@code <}, which is always replaced.
         * </ul>
         */
        ALWAYS_ENTITIZE_ATTRIBUTES,

        /**
         * If set,always replaces certain characters by their corresponding entity in elements.
         */
        ALWAYS_ENTITIZE_ELEMENTS,

        /**
         * If set, use converter for element and attribute names.
         */
        USE_CONVERTER,

        /**
         * If enabled, partial xml writing is allowed.
         * <p>
         * Otherwise, only xml documents (and their content) can be produced.<br>
         * This option must be set to allow generating, for example:
         * <ul>
         * <li>A pure comment: {@code <!-- a comment -->}
         * <li>An element (and its content): {@code <element att="value"/>}
         * </ul>
         *
         */
        ALLOW_PARTIAL_XML,

        /**
         * If enabled, elements and attributes names are not checked.
         */
        DONT_VALIDATE_NAMES,

        /**
         * If enabled, elements content and attributes values are not checked.
         */
        DONT_VALIDATE_CHARS,

        /**
         * If enabled, non printable chars are replaced.
         * <p>
         * The replacement string is {@link XmlWriter#DEFAULT_REPLACEMENT}
         * or the value set with {@link XmlWriter#setNonPrintableCharReplacement(String)}.
         */
        REPLACE_NON_PRINTABLE_CHARS
    }

    private XmlWriter(OutputStream os,
                      String encoding,
                      boolean wrap,
                      Compressor compressor)
            throws IOException {
        Checks.isNotNull(encoding, "encoding");
        Checks.isNotNull(compressor, "compressor");
        this.encoding = encoding;
        if (wrap) {
            out = new BufferedWriter(new OutputStreamWriter(CompressionUtils.adapt(new NonCloseableOutputStream(os), compressor),
                                                            encoding));
        } else {
            out = new BufferedWriter(new OutputStreamWriter(CompressionUtils.adapt(os, compressor), encoding));
        }
    }

    /**
     * Creates an XmlWriter to a Writer with a given encoding.
     *
     * @param writer The writer.
     * @param encoding The encoding to use.
     */
    public XmlWriter(Writer writer,
                     String encoding) {
        this.out = writer;
        this.encoding = encoding;
    }

    /**
     * Creates an XmlWriter to a Writer, with UTF-8 encoding.
     *
     * @param writer The writer.
     */
    public XmlWriter(Writer writer) {
        this(writer, StandardCharsets.UTF_8.name());
    }

    /**
     * Creates an XmlWriter to System.out, with UTF-8 encoding.
     *
     * @throws UnsupportedEncodingException If UTF-8 is not supported, which should not happen.
     * @throws IOException When an IO exception occurs.
     */
    public XmlWriter() throws IOException {
        this(new BufferedOutputStream(NonCloseableOutputStream.NON_CLOSABLE_SYSTEM_OUT), StandardCharsets.UTF_8.name(), false,
             Compressor.NONE);
    }

    /**
     * Creates an XmlWriter to a PrintStream, with UTF-8 encoding.
     *
     * @param out The PrintStream to use. Must be closed be the caller.
     * @throws UnsupportedEncodingException If UTF-8 encoding is not supported, which should not happen.
     * @throws IOException When an IO exception occurs.
     */
    public XmlWriter(PrintStream out) throws IOException {
        this(new BufferedOutputStream(new NonCloseableOutputStream(out)), StandardCharsets.UTF_8.name(), false, Compressor.NONE);
    }

    /**
     * Creates an XmlWriter to an OutputStream with a given encoding.
     *
     * @param os The outputStream. Must be closed by caller.
     * @param encoding The encoding to use.
     * @throws UnsupportedEncodingException If the named encoding is not supported.
     * @throws IOException When an IO exception occurs.
     */
    public XmlWriter(OutputStream os,
                     String encoding)
            throws IOException {
        this(os, encoding, true, Compressor.NONE);
    }

    /**
     * Creates an XmlWriter to an OutputStream with UTF-8 encoding.
     *
     * @param os The outputStream. Must be closed by caller.
     * @throws UnsupportedEncodingException If the named encoding is not supported.
     * @throws IOException When an IO exception occurs.
     */
    public XmlWriter(OutputStream os)
            throws IOException {
        this(os, StandardCharsets.UTF_8.name());
    }

    /**
     * Creates an XmlWriter to a file with a given encoding and compressor.
     *
     * @param filename Name of the file.
     * @param encoding The encoding to use.
     * @param compressor The compressor.
     * @throws FileNotFoundException If the file exists but is a directory
     *             rather than a regular file, does not exist but cannot be created,
     *             or cannot be opened for any other reason.
     * @throws UnsupportedEncodingException If the named encoding is not supported.
     * @throws IOException When an IO exception occurs.
     */
    public XmlWriter(String filename,
                     String encoding,
                     Compressor compressor)
            throws IOException {
        this(new BufferedOutputStream(new FileOutputStream(filename)), encoding, false, compressor);
    }

    /**
     * Creates an XmlWriter to a file with a given encoding.
     *
     * @param filename Name of the file.
     * @param encoding The encoding to use.
     * @throws FileNotFoundException If the file exists but is a directory
     *             rather than a regular file, does not exist but cannot be created,
     *             or cannot be opened for any other reason.
     * @throws UnsupportedEncodingException If the named encoding is not supported.
     * @throws IOException When an IO exception occurs.
     */
    public XmlWriter(String filename,
                     String encoding)
            throws IOException {
        this(filename, encoding, Compressor.NONE);
    }

    /**
     * Creates an XmlWriter to a file with a UTF-8 encoding and compressor.
     *
     * @param filename Name of the file.
     * @param compressor The compressor.
     * @throws FileNotFoundException If the file exists but is a directory
     *             rather than a regular file, does not exist but cannot be created,
     *             or cannot be opened for any other reason.
     * @throws UnsupportedEncodingException If the named encoding is not supported.
     * @throws IOException When an IO exception occurs.
     */
    public XmlWriter(String filename,
                     Compressor compressor)
            throws IOException {
        this(filename, StandardCharsets.UTF_8.name(), compressor);
    }

    /**
     * Creates an XmlWriter to a file with UTF-8 encoding.
     *
     * @param filename Name of the file.
     * @throws FileNotFoundException If the file exists but is a directory
     *             rather than a regular file, does not exist but cannot be created,
     *             or cannot be opened for any other reason.
     * @throws UnsupportedEncodingException If the named encoding is not supported.
     * @throws IOException When an IO exception occurs.
     */
    public XmlWriter(String filename)
            throws IOException {
        this(filename, StandardCharsets.UTF_8.name(), Compressor.NONE);
    }

    /**
     * Creates an XmlWriter to a file with a given encoding and compressor.
     *
     * @param file File to use.
     * @param encoding The encoding to use.
     * @param compressor The compressor to use.
     * @throws FileNotFoundException If the file exists but is a directory
     *             rather than a regular file, does not exist but cannot be created,
     *             or cannot be opened for any other reason.
     * @throws UnsupportedEncodingException If the named encoding is not supported.
     * @throws IOException When an IO exception occurs.
     */
    public XmlWriter(File file,
                     String encoding,
                     Compressor compressor)
            throws IOException {
        this(new BufferedOutputStream(new FileOutputStream(file)), encoding, false, compressor);
    }

    /**
     * Creates an XmlWriter to a file with a given encoding.
     *
     * @param file File to use.
     * @param encoding The encoding to use.
     * @throws FileNotFoundException If the file exists but is a directory
     *             rather than a regular file, does not exist but cannot be created,
     *             or cannot be opened for any other reason.
     * @throws UnsupportedEncodingException If the named encoding is not supported.
     * @throws IOException When an IO exception occurs.
     */
    public XmlWriter(File file,
                     String encoding)
            throws IOException {
        this(file, encoding, Compressor.NONE);
    }

    /**
     * Creates an XmlWriter to a file with UTF-8 encoding and compressor.
     *
     * @param file File to use.
     * @param compressor The compressor to use.
     * @throws FileNotFoundException If the file exists but is a directory
     *             rather than a regular file, does not exist but cannot be created,
     *             or cannot be opened for any other reason.
     * @throws UnsupportedEncodingException If the named encoding is not supported.
     * @throws IOException When an IO exception occurs.
     */
    public XmlWriter(File file,
                     Compressor compressor)
            throws IOException {
        this(file, StandardCharsets.UTF_8.name(), compressor);
    }

    /**
     * Creates an XmlWriter to a file using UTF-8 encoding.
     *
     * @param file File to use.
     * @throws FileNotFoundException If the file exists but is a directory
     *             rather than a regular file, does not exist but cannot be created,
     *             or cannot be opened for any other reason.
     * @throws UnsupportedEncodingException If UTF-8 encoding is not supported, which should not happen.
     * @throws IOException When an IO exception occurs.
     */
    public XmlWriter(File file)
            throws IOException {
        this(file, StandardCharsets.UTF_8.name(), Compressor.NONE);
    }

    /**
     * Flushes and raises an InvalidStateException.
     *
     * @param message The exception message.
     * @throws InvalidStateException Systematically.
     */
    private void stateError(String message) {
        try {
            flush();
        } catch (final IOException e) {
            LOGGER.catching(e);
        }
        throw new InvalidStateException("Invalid state: " + context.getType() + ", when calling: " + message);
    }

    /**
     * @param message The exception message.
     * @throws InvalidDataException in all cases.
     */
    protected static void dataError(String message) {
        throw new InvalidDataException("Invalid data: " + message);
    }

    /**
     * Writes an attribute value.
     * <p>
     * If text validation is enabled and string is invalid, an exception is raised.
     *
     * @param s The attribute value.
     * @throws IOException When an IO error occurs.
     * @throws InvalidDataException When {@code s} is invalid and validation is enabled.
     */
    private void writeEscapedAttribute(String s) throws IOException {
        if (s != null) {
            final int length = s.length();
            int index = 0;
            while (index < length) {
                final int codePoint = s.codePointAt(index);
                switch (codePoint) {
                case '>':
                    if (isEnabled(Feature.ALWAYS_ENTITIZE_ATTRIBUTES)) {
                        out.append(GT);
                    } else {
                        out.write(codePoint);
                    }
                    break;

                case '<':
                    out.append(LT);
                    break;

                case '&':
                    out.append(AMP);
                    break;

                case '\'':
                    if (attributeDelimiter == SINGLE_QUOTE || isEnabled(Feature.ALWAYS_ENTITIZE_ATTRIBUTES)) {
                        // Only needs to be escaped in attributes enclosed by single
                        // quotes
                        out.append(APOS);
                    } else {
                        out.write(codePoint);
                    }
                    break;

                case '"':
                    if (attributeDelimiter == DOUBLE_QUOTE || isEnabled(Feature.ALWAYS_ENTITIZE_ATTRIBUTES)) {
                        // Only needs to be escaped in attributes enclosed by double
                        // quotes
                        out.append(QUOT);
                    } else {
                        out.write(codePoint);
                    }
                    break;

                case '\r':
                    if (index + 1 < length && s.charAt(index + 1) == '\n') {
                        index++;
                        if (isEnabled(Feature.USE_XML_EOL)) {
                            out.append(LF);
                        } else {
                            out.append(CR);
                            out.append(LF);
                        }
                    } else {
                        if (isEnabled(Feature.USE_XML_EOL)) {
                            out.append(LF);
                        } else {
                            out.append(CR);
                        }
                    }
                    break;

                case '\n':
                    out.append(LF);
                    break;

                case '\t':
                    out.append(TAB);
                    break;

                default:
                    if (dontValidateChars || XmlUtils.isXmlChar(codePoint, version)) {
                        out.write(codePoint);
                    } else {
                        if (replaceNonPrintableChars) {
                            out.append(nonPrintableCharReplacement);
                        } else {
                            dataError("Invalid xml charater: '" + XmlUtils.codePointToString(codePoint) + "'");
                        }
                    }
                    break;
                }
                index += Character.charCount(codePoint);
            }
        }
    }

    /**
     * Enumeration of possible contexts for text escaping.
     * <p>
     * Attribute content is excluded.
     *
     * @author Damien Carbonne
     */
    private enum EscapeContext {
        /** CDATA content. */
        CDATA,
        /** Comment content. */
        COMMENT,
        /** Element content. */
        ELEMENT_CONTENT,
        /** Element name or attribute name. */
        NAME,
        PI_CONTENT,
        PI_TARGET;

        /**
         * @return {@code true} when EOL is supported in this context.
         */
        public boolean supportsEOL() {
            return this == CDATA
                    || this == COMMENT
                    || this == ELEMENT_CONTENT;
        }

        /**
         * Checks that EOL is supported in this context and throws an exception
         * if this is not the case.
         *
         * @throws InvalidDataException if EOL is not supported in this context.
         */
        public void checkSupportsEOL() {
            if (!supportsEOL()) {
                dataError("Unexpected eol");
            }
        }

        /**
         * @return {@code true} when it is possible to write characters for which
         *         a predefined entity exists, either as a character or the corresponding entity.<br>
         *         This is <em>NOT</em> the case for {@link #NAME} and {@link #PI_TARGET}.
         */
        public boolean supportsEntitiesCandidates() {
            return this == CDATA
                    || this == COMMENT
                    || this == ELEMENT_CONTENT
                    || this == PI_CONTENT;
        }

        /**
         * Checks that writing a character for which a predefined entity exists is possible.
         * If this is not the case, an exception is raised.
         *
         * @throws InvalidDataException if writing a character associated
         *             to a predefined entity is not possible..
         */
        public void checkSupportsEntitiesCandidates() {
            if (!supportsEntitiesCandidates()) {
                dataError("Unexpected entity character");
            }
        }

        /**
         * @return {@code true} if redefined entities must be used instead
         *         of characters in this context.
         */
        public boolean needsEntities() {
            return this == ELEMENT_CONTENT;
        }

        /**
         * Replaces a string by a valid equivalent
         * or raises an exception when it is invalid and can not be fixed.
         *
         * @param s The (non null) string.
         * @param caller The XmlWriter calling this method. Used to access features.
         * @return A valid equivalent of {@code s}.
         * @throws InvalidDataException when {@code s} is invalid and can not be fixed.
         */
        public String validateNonNull(String s,
                                      XmlWriter caller) {
            if (this == EscapeContext.NAME) {
                if (!caller.dontValidateNames && !XmlUtils.isValidName(s)) {
                    dataError("Invalid name: '" + s + "'");
                }
                return s;
            } else {
                if (!caller.dontValidateChars && !XmlUtils.isValidXml(s, caller.version)) {
                    if (caller.replaceNonPrintableChars) {
                        s = XmlUtils.replaceInvalidChars(s, caller.version, caller.nonPrintableCharReplacement);
                    } else {
                        dataError("Invalid chars: '" + s + "'");
                    }
                }
                if (!caller.dontValidateChars && !XmlUtils.isValidXml(s, caller.version)) {
                    dataError("Invalid chars: '" + s + "'");
                }
                switch (this) {
                case CDATA:
                    final Matcher m = CDATA_END_PATTERN.matcher(s);
                    if (m.find()) {
                        return m.replaceAll("]]" + CDATA_END + CDATA_START + ">");
                    } else {
                        return s;
                    }

                case COMMENT:
                    if (s.contains("--")) {
                        dataError("Unexpected '--' in comment");
                    }
                    return s;

                default:
                    return s;
                }
            }
        }
    }

    /**
     * Writes any string, except attribute value.
     *
     * @param s The string.
     * @param context The context.
     * @throws IOException When an IO error occurs.
     */
    private void writeEscaped(String s,
                              EscapeContext context) throws IOException {
        if (s != null) {
            final String str = context.validateNonNull(s, this);
            final int length = str.length();
            int index = 0;
            while (index < length) {
                final int codePoint = str.codePointAt(index);
                switch (codePoint) {
                case '>':
                    context.checkSupportsEntitiesCandidates();
                    // In fact, never needs to be escaped !
                    if (context.needsEntities()) {
                        out.append(GT);
                    } else {
                        out.write(codePoint);
                    }
                    break;

                case '<':
                    context.checkSupportsEntitiesCandidates();
                    if (context.needsEntities()) {
                        out.append(LT);
                    } else {
                        out.write(codePoint);
                    }
                    break;

                case '&':
                    context.checkSupportsEntitiesCandidates();
                    if (context.needsEntities()) {
                        out.append(AMP);
                    } else {
                        out.write(codePoint);
                    }
                    break;

                case '\'':
                    context.checkSupportsEntitiesCandidates();
                    // In fact, only needs to be escaped in attributes enclosed by single quotes
                    if (context.needsEntities() && alwaysEntitizeElements) {
                        out.append(APOS);
                    } else {
                        out.write(codePoint);
                    }
                    break;

                case '"':
                    context.checkSupportsEntitiesCandidates();
                    // In fact, only needs to be escaped in attributes enclosed by double quotes
                    if (context.needsEntities() && alwaysEntitizeElements) {
                        out.append(QUOT);
                    } else {
                        out.write(codePoint);
                    }
                    break;

                case '\r':
                    context.checkSupportsEOL();
                    if (index + 1 < length && str.charAt(index + 1) == '\n') {
                        index++;
                    }
                    out.append(eol);
                    break;

                case '\n':
                    context.checkSupportsEOL();
                    out.append(eol);
                    break;

                default:
                    out.write(codePoint);
                    break;
                }
                index += Character.charCount(codePoint);
            }
        }
    }

    /**
     * Writes a string without transforming or checking it.
     * <p>
     * <b>WARNING:</b> this should be used with care, and exceptionally.
     *
     * @param s The string.
     * @throws IOException When an IO error occurs.
     */
    public void write(String s) throws IOException {
        flushln();
        out.append(s);
    }

    private void flushln() throws IOException {
        if (needsEOL) {
            out.append(eol);
            needsEOL = false;
        }
    }

    private void writeln() throws IOException {
        flushln();
        if (isEnabled(Feature.PRETTY_PRINT)) {
            needsEOL = true;
        }
    }

    private void indent() throws IOException {
        if (isEnabled(Feature.PRETTY_PRINT)) {
            for (int i = 0; i < level; i++) {
                write(indent);
            }
        }
    }

    /**
     * Enables or disables a feature.
     *
     * @param feature The feature to enable/disable.
     * @param enabled If true, enable the feature.
     */
    public final void setEnabled(Feature feature,
                                 boolean enabled) {
        features = IntMasks.setEnabled(features, feature, enabled);

        switch (feature) {
        case USE_SINGLE_QUOTE:
            if (isEnabled(Feature.USE_SINGLE_QUOTE)) {
                attributeDelimiter = SINGLE_QUOTE;
            } else {
                attributeDelimiter = DOUBLE_QUOTE;
            }
            break;

        case USE_XML_EOL:
            if (isEnabled(Feature.USE_XML_EOL)) {
                eol = "\n";
            } else {
                eol = System.lineSeparator();
            }
            break;

        case DONT_VALIDATE_CHARS:
            dontValidateChars = enabled;
            break;

        case REPLACE_NON_PRINTABLE_CHARS:
            replaceNonPrintableChars = enabled;
            break;

        case DONT_VALIDATE_NAMES:
            dontValidateNames = enabled;
            break;

        case ALWAYS_ENTITIZE_ELEMENTS:
            alwaysEntitizeElements = enabled;
            break;

        default:
            break;
        }
    }

    public final void setEnabled(Feature... features) {
        if (features != null) {
            for (final Feature feature : features) {
                setEnabled(feature, true);
            }
        }
    }

    /**
     * Returns {@code true} if a feature is enabled, {@code false} otherwise.
     *
     * @param feature The feature.
     * @return {@code true} if {@code feature} is enabled, {@code false} otherwise.
     */
    public final boolean isEnabled(Feature feature) {
        return IntMasks.isEnabled(features, feature);
    }

    /**
     * Set the indent string.
     *
     * @param s The indent string.
     */
    public final void setIndentString(String s) {
        this.indent = s == null ? "" : s;
    }

    public final String getIndentString() {
        return indent;
    }

    public final void setTabSize(int tabSize) {
        final StringBuilder builder = new StringBuilder();
        for (int index = 0; index < tabSize; index++) {
            builder.append(" ");
        }
        setIndentString(builder.toString());
    }

    /**
     * Sets the string to use in place of non printable (invalid) characters.
     * <p>
     * This is used if {@link Feature#REPLACE_NON_PRINTABLE_CHARS} is enabled.<br>
     * If not called, default value is {@link #DEFAULT_REPLACEMENT}.
     *
     * @param replacement The replacement string.
     */
    public final void setNonPrintableCharReplacement(String replacement) {
        this.nonPrintableCharReplacement = replacement;
    }

    public String getNonPrintableCharReplacement() {
        return nonPrintableCharReplacement;
    }

    /**
     * Set the string to use for end of lines.
     * <p>
     * This may be useful to force an OS specific symbol.<br>
     * This has interactions with {@link Feature#USE_XML_EOL}.<br>
     * <b>WARNING:</b> It is the responsibility of programmer to use a valid string.
     *
     * @param eol The end of line string.
     */
    public final void setEndOfLine(String eol) {
        this.eol = eol;
    }

    /**
     * @return The end of line string.
     */
    public final String getEndOfLine() {
        return eol;
    }

    /**
     * Returns the case converter. Used if USE_CONVERTER feature is enabled.
     *
     * @return The case converter.
     */
    public final CaseConverter getConverter() {
        return converter;
    }

    public final void setCaseConverter(CaseConverter converter) {
        Checks.isNotNull(converter, "converter");
        this.converter = converter;
    }

    /**
     * Begins an XML document, using writer's encoding.
     *
     * @param version The XML version to use.
     * @throws IOException If an I/O error occurs.
     */
    @Override
    public final void beginDocument(XmlVersion version) throws IOException {
        if (context.getType() == Type.IN_OPEN_STREAM) {
            context = context.pushContext(Type.IN_OPEN_DOC);
            this.version = version;
        } else {
            stateError("beginDocument()");
        }
        write("<?xml version=\"");
        write(version.getLabel());
        write("\"");
        write(" encoding=\"");
        write(encoding);
        write("\"");
        write("?>");
        writeln();
    }

    @Override
    public void addDTD(String dtd) throws IOException {
        if (context.getType() != Type.IN_OPEN_DOC || context.hasDTD()) {
            stateError("addDTD()");
        }
        context.setDTD();
        write(dtd);
        writeln();
    }

    @Override
    public final void beginProcessingInstruction(String target) throws IOException {
        switch (context.getType()) {
        case IN_OPEN_DOC:
        case IN_CLOSED_DOC:
        case IN_MIXED_CONTENT:
            context = context.pushContext(Type.IN_PI);
            break;
        case IN_SIMPLE_CONTENT:
            context = context.pushContext(Type.IN_PI);
            indent();
            break;
        case IN_TAG:
            write(">");
            context.setType(Type.IN_SIMPLE_CONTENT);
            context = context.pushContext(Type.IN_PI);
            writeln();
            indent();
            break;
        default:
            stateError("beginProcessingInstruction()");
            break;
        }
        write("<?");
        writeEscaped(target, EscapeContext.PI_TARGET);
    }

    @Override
    public final void addProcessingInstructionContent(String content) throws IOException {
        if (context.getType() != Type.IN_PI) {
            stateError("addProcessingInstructionContent()");
        }
        writeEscaped(content, EscapeContext.PI_CONTENT);
    }

    @Override
    public final void endProcessingInstruction() throws IOException {
        if (context.getType() == Type.IN_PI) {
            context = context.popContext();
        } else {
            stateError("endProcessingInstruction()");
        }
        write("?>");
        if (context.getType() != Type.IN_MIXED_CONTENT) {
            writeln();
        }
    }

    @Override
    public final void addProcessingInstruction(String target,
                                               String content) throws IOException {
        beginProcessingInstruction(target);
        write(" ");
        addProcessingInstructionContent(content);
        endProcessingInstruction();
    }

    /**
     * Starts the writing of a new comment.
     *
     * @throws IOException If an I/O error occurs.
     */
    @Override
    public final void beginComment() throws IOException {
        switch (context.getType()) {
        case IN_OPEN_DOC:
        case IN_CLOSED_DOC:
        case IN_MIXED_CONTENT:
            context = context.pushContext(Type.IN_COMMENT);
            break;
        case IN_SIMPLE_CONTENT:
            context = context.pushContext(Type.IN_COMMENT);
            indent();
            break;
        case IN_TAG:
            write(">");
            context.setType(Type.IN_SIMPLE_CONTENT);
            context = context.pushContext(Type.IN_COMMENT);
            writeln();
            indent();
            break;
        case IN_OPEN_STREAM:
            if (isEnabled(Feature.ALLOW_PARTIAL_XML)) {
                context = context.pushContext(Type.IN_COMMENT);
            } else {
                stateError("beginComment()");
            }
            break;
        default:
            stateError("beginComment()");
            break;
        }
        write("<!--");
    }

    /**
     * Adds content to current comment.
     *
     * @param content The text to add to current comment.
     */
    @Override
    public final void addCommentContent(String content) {
        if (context.getType() == Type.IN_COMMENT) {
            // Context does not change
            buffer.append(content);
        } else {
            stateError("addCommentContent()");
        }
    }

    /**
     * Closes the current comment.
     *
     * @throws IOException If an I/O error occurs.
     */
    @Override
    public final void endComment() throws IOException {
        if (context.getType() == Type.IN_COMMENT) {
            writeEscaped(buffer.toString(), EscapeContext.COMMENT);
            buffer.setLength(0);
            context = context.popContext();
        } else {
            stateError("endComment()");
        }
        write("-->");
        if (context.getType() != Type.IN_MIXED_CONTENT) {
            writeln();
        }
    }

    /**
     * Starts the writing of a new CData.
     *
     * @throws IOException If an I/O error occurs.
     */
    @Override
    public final void beginCData() throws IOException {
        switch (context.getType()) {
        case IN_SIMPLE_CONTENT:
            context = context.pushContext(Type.IN_CDATA);
            indent();
            break;
        case IN_MIXED_CONTENT:
            context = context.pushContext(Type.IN_CDATA);
            break;
        case IN_TAG:
            write(">");
            writeln();
            indent();
            context.setType(Type.IN_SIMPLE_CONTENT);
            context = context.pushContext(Type.IN_CDATA);
            break;
        default:
            stateError("beginCData()");
            break;
        }
        write(CDATA_START);
    }

    /**
     * Adds content to current CData.
     *
     * @param content The text to add to current CData.
     */
    @Override
    public final void addCDataContent(String content) {
        if (context.getType() == Type.IN_CDATA) {
            // Context does not change
            if (content != null) {
                buffer.append(content);
            }
        } else {
            stateError("addCDataContent()");
        }
    }

    /**
     * Closes the current CData.
     *
     * @throws IOException If an I/O error occurs.
     */
    @Override
    public final void endCData() throws IOException {
        if (context.getType() == Type.IN_CDATA) {
            writeEscaped(buffer.toString(), EscapeContext.CDATA);
            buffer.setLength(0);
            context = context.popContext();
        } else {
            stateError("endCData()");
        }
        write(CDATA_END);
        if (context.getType() != Type.IN_MIXED_CONTENT) {
            writeln();
        }
    }

    @Override
    public final void beginElement(String name) throws IOException {
        final String ename;
        if (isEnabled(Feature.USE_CONVERTER)) {
            ename = converter.splitAndConvert(name);
        } else {
            ename = name;
        }

        switch (context.getType()) {
        case IN_OPEN_DOC:
        case IN_SIMPLE_CONTENT:
            context = context.pushContext(Type.IN_TAG);
            context.setName(ename);
            indent();
            break;
        case IN_MIXED_CONTENT:
            context = context.pushContext(Type.IN_TAG);
            context.setName(ename);
            break;
        case IN_TAG:
            context.setType(Type.IN_SIMPLE_CONTENT);
            context = context.pushContext(Type.IN_TAG);
            context.setName(ename);
            write(">");
            writeln();
            indent();
            break;
        case IN_OPEN_STREAM:
            if (isEnabled(Feature.ALLOW_PARTIAL_XML)) {
                context = context.pushContext(Type.IN_TAG);
                context.setName(ename);
            } else {
                stateError("beginElement()");
            }
            break;
        default:
            stateError("beginElement()");
            break;
        }

        write("<");
        writeEscaped(ename, EscapeContext.NAME);
        level++;
    }

    /**
     * Adds a String attribute to current element.
     *
     * @param name Attribute name.
     * @param value Attribute value.
     * @throws IOException If an I/O error occurs.
     */
    @Override
    public final void addAttribute(String name,
                                   String value) throws IOException {
        addAttribute(name, value, true, true);
    }

    @Override
    public void addUnescapedAttribute(String name,
                                      String value) throws IOException {
        addAttribute(name, value, true, false);
    }

    private final void addAttribute(String name,
                                    String value,
                                    boolean allowConverter,
                                    boolean escape) throws IOException {
        final String ename;
        if (allowConverter && isEnabled(Feature.USE_CONVERTER) && context.getType() != Type.IN_PI) {
            ename = converter.splitAndConvert(name);
        } else {
            ename = name;
        }

        switch (context.getType()) {
        case IN_PI:
        case IN_TAG:
            // Context does not change
            break;
        default:
            stateError("addAttribute()");
            break;
        }
        write(" ");
        writeEscaped(ename, EscapeContext.NAME);
        write("=");
        write(attributeDelimiter);
        if (escape) {
            writeEscapedAttribute(value == null ? "" : value);
        } else {
            write(value == null ? "" : value);
        }
        write(attributeDelimiter);
    }

    @Override
    public final void addElementContent(String content) throws IOException {
        switch (context.getType()) {
        case IN_TAG:
            context.setType(Type.IN_MIXED_CONTENT);
            write(">");
            break;
        case IN_SIMPLE_CONTENT:
            context.setType(Type.IN_MIXED_CONTENT);
            break;
        case IN_MIXED_CONTENT:
        case IN_CLOSED_DOC:
            // Context does not change
            break;
        default:
            stateError("addElementContent()");
            break;
        }

        writeEscaped(content, EscapeContext.ELEMENT_CONTENT);
    }

    @Override
    public final void addUnescapedElementContent(String content) throws IOException {
        switch (context.getType()) {
        case IN_TAG:
            context.setType(Type.IN_MIXED_CONTENT);
            write(">");
            break;
        case IN_SIMPLE_CONTENT:
            context.setType(Type.IN_MIXED_CONTENT);
            break;
        case IN_MIXED_CONTENT:
        case IN_CLOSED_DOC:
            // Context does not change
            break;
        default:
            stateError("addElementContent()");
            break;
        }

        write(content == null ? "" : content);
    }

    @Override
    public final void endElement() throws IOException {
        level--;
        switch (context.getType()) {
        case IN_TAG:
            context = context.popContext();
            write("/>");
            writeln();
            break;
        case IN_SIMPLE_CONTENT:
            indent();
            write("</");
            writeEscaped(context.getName(), EscapeContext.NAME);
            write(">");
            writeln();
            context = context.popContext();
            break;
        case IN_MIXED_CONTENT:
            write("</");
            writeEscaped(context.getName(), EscapeContext.NAME);
            write(">");
            context = context.popContext();
            if (context.getType() == Type.IN_SIMPLE_CONTENT) {
                writeln();
            }
            break;

        default:
            stateError("endElement()");
            break;
        }
        if (context.getType() == Type.IN_OPEN_DOC) {
            context.setType(Type.IN_CLOSED_DOC);
        }
    }

    @Override
    public final void endDocument() throws IOException {
        switch (context.getType()) {
        case IN_OPEN_DOC:
        case IN_CLOSED_DOC:
            context = context.popContext();
            context.setType(Type.IN_CLOSED_STREAM);
            if (isEnabled(Feature.APPEND_FINAL_EOL)) {
                if (!needsEOL) {
                    writeln();
                }
                flushln();
            }
            break;
        default:
            stateError("endDocument()");
            break;
        }
        flush();
    }

    @Override
    public void flush() throws IOException {
        out.flush();
    }

    @Override
    public void close() throws IOException {
        switch (context.getType()) {
        case IN_CLOSED_STREAM:
            // Ignore: normal doc handling
            break;
        case IN_OPEN_STREAM:
            if (isEnabled(Feature.ALLOW_PARTIAL_XML)) {
                context.setType(Type.IN_CLOSED_STREAM);
                if (isEnabled(Feature.APPEND_FINAL_EOL)) {
                    if (!needsEOL) {
                        writeln();
                    }
                    flushln();
                }
            } else {
                stateError("close()");
            }
            break;
        default:
            stateError("close()");
            break;
        }

        out.close();
    }

    public void reset() {
        context = context.popAll();
        context.setType(Type.IN_OPEN_STREAM);
    }
}