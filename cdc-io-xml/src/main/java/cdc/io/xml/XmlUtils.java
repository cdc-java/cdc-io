package cdc.io.xml;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.util.lang.FailureReaction;

/**
 * Xml utilities.
 *
 * @author Damien Carbonne
 *
 */
public final class XmlUtils {
    private static final Logger LOGGER = LogManager.getLogger(XmlUtils.class);

    /**
     * Set of valid XML 1.0 characters.
     */
    private static final IntSet XML_1_0_CHARS =
            new IntSet(0x9, 0xA,
                       0xD, 0xD,
                       0x20, 0xD7FF,
                       0xE000, 0xFFFD,
                       0x10000, 0x10FFFF);

    /**
     * Set of valid XML 1.1 characters.
     */
    private static final IntSet XML_1_1_CHARS =
            new IntSet(0x0001, 0xD7FF,
                       0xE000, 0xFFFD,
                       0x10000, 0x10FFFF);

    /**
     * Set of valid first characters of names.
     * <p>
     * Same for XML 1.0 and XML 1.1.
     */
    private static final IntSet XML_NAME_START_CHAR =
            new IntSet(':', ':',
                       'A', 'Z',
                       '_', '_',
                       'a', 'z',
                       0xC0, 0xD6,
                       0xD8, 0xF6,
                       0xF8, 0x2FF,
                       0x370, 0x37D,
                       0x37F, 0x1FFF,
                       0x200C, 0x200D,
                       0x2070, 0x218F,
                       0x2C00, 0x2FEF,
                       0x3001, 0xD7FF,
                       0xF900, 0xFDCF,
                       0xFDF0, 0xFFFD,
                       0x10000, 0xEFFFF);

    /**
     * Set of valid characters of names.
     * <p>
     * Same for XML 1.0 and XML 1.1.
     */
    private static final IntSet XML_NAME_CHAR =
            new IntSet('-', /* '-', '.', */ '.',
                       '0', /* '9', ':', */ ':',
                       'A', 'Z',
                       '_', '_',
                       'a', 'z',
                       0xB7, 0xB7,
                       0xC0, 0xD6,
                       0xD8, 0xF6,
                       0xF8, /* 0x2FF, 0x300, 0x36F, 0x370, */ 0x37D,
                       0x37F, 0x1FFF,
                       0x200C, 0x200D,
                       0x203F, 0x2040,
                       0x2070, 0x218F,
                       0x2C00, 0x2FEF,
                       0x3001, 0xD7FF,
                       0xF900, 0xFDCF,
                       0xFDF0, 0xFFFD,
                       0x10000, 0xEFFFF);

    private XmlUtils() {
    }

    @FunctionalInterface
    private static interface CodePointEvaluator {
        public boolean apply(int codePoint);
    }

    private static boolean alwaysTrue(String s,
                                      CodePointEvaluator evaluator) {
        final int length = s.length();
        int index = 0;
        while (index < length) {
            final int codePoint = s.codePointAt(index);
            if ((!evaluator.apply(codePoint))) {
                return false;
            }
            index += Character.charCount(codePoint);
        }
        return true;
    }

    private static boolean alwaysTrue(char[] ch,
                                      int offset,
                                      int count,
                                      CodePointEvaluator evaluator) {
        int index = offset;
        while (index < offset + count) {
            final int codePoint = Character.codePointAt(ch, index);
            if (!evaluator.apply(codePoint)) {
                return false;
            }
            index += Character.charCount(codePoint);
        }
        return true;
    }

    public static String codePointToString(int codePoint) {
        final StringBuilder builder = new StringBuilder();
        if (Character.isBmpCodePoint(codePoint)) {
            builder.append((char) codePoint);
        } else if (Character.isValidCodePoint(codePoint)) {
            builder.append(Character.highSurrogate(codePoint));
            builder.append(Character.lowSurrogate(codePoint));
        } else {
            builder.append('?');
        }
        return builder.toString();
    }

    /**
     * Returns {@code true} if a code point is a white space.
     *
     * @param codePoint The code point.
     * @return {@code true} if {@code codePoint} is a white space.
     */
    public static boolean isWhiteSpace(int codePoint) {
        return codePoint == 0x9 // TAB
                || codePoint == 0xA // LF
                || codePoint == 0xD // CR
                || codePoint == 0x20; // Space
    }

    /**
     * Returns {@code true} if a string is {@code null} or contains only white spaces.
     *
     * @param s The string.
     * @return {@code true} if {@code s} is {@code null} or contains only white spaces.
     */
    public static boolean isWhiteSpace(String s) {
        return s == null || alwaysTrue(s, XmlUtils::isWhiteSpace);
    }

    public static boolean isWhiteSpace(char[] ch,
                                       int offset,
                                       int count) {
        return alwaysTrue(ch, offset, count, XmlUtils::isWhiteSpace);
    }

    /**
     * Returns {@code true} if a code point is valid for XML 1.0.
     * <p>
     * <b>Note:</b> A valid character may need escape.
     *
     * @param codePoint The code point.
     * @return {@code true} if {@code codePoint} is a valid XML 1.0 code point.
     */
    public static boolean isXml10Char(int codePoint) {
        return XML_1_0_CHARS.contains(codePoint);
    }

    /**
     * Returns {@code true} if a code point is valid for XML 1.1.
     * <p>
     * <b>Note:</b> A valid character may need escape.
     *
     * @param codePoint The code point.
     * @return {@code true} if {@code codePoint} is a valid XML 1.1 code point.
     */
    public static boolean isXml11Char(int codePoint) {
        return XML_1_1_CHARS.contains(codePoint);
    }

    public static boolean isXmlChar(int codePoint,
                                    XmlVersion version) {
        return version == XmlVersion.XML_1_1 ? isXml11Char(codePoint) : isXml10Char(codePoint);
    }

    /**
     * Returns {@code true} if a string is {@code null} or contains only valid XML 1.0 characters.
     * <p>
     * <b>Note:</b> A valid character may need escape.
     *
     * @param s The string.
     * @return {@code true} if {@code s} is {@code null} or contains only valid XML 1.0 characters.
     */
    public static boolean isValidXml10(String s) {
        return s == null || alwaysTrue(s, XmlUtils::isXml10Char);
    }

    /**
     * Returns {@code true} if a string is {@code null} or contains only valid XML 1.1 characters.
     * <p>
     * <b>Note:</b> A valid character may need escape.
     *
     * @param s The string.
     * @return {@code true} if {@code s} is {@code null} or contains only valid XML 1.1 characters.
     */
    public static boolean isValidXml11(String s) {
        return s == null || alwaysTrue(s, XmlUtils::isXml11Char);
    }

    public static boolean isValidXml(String s,
                                     XmlVersion version) {
        return version == XmlVersion.XML_1_1 ? isValidXml11(s) : isValidXml10(s);
    }

    /**
     * Returns {@code true} if a code point is valid as first character of an xml name.
     *
     * @param codePoint The code point.
     * @return {@code true} if {@code codePoint} is valid as first character of an xml name.
     */
    public static boolean isNameStartChar(int codePoint) {
        // Same for XML 1.0 and XML 1.1
        return XML_NAME_START_CHAR.contains(codePoint);
    }

    public static boolean isNameChar(int codePoint) {
        // Same for XML 1.0 and XML 1.1
        return XML_NAME_CHAR.contains(codePoint);
    }

    /**
     * Returns {@code true} if a string is a valid xml name.
     * <p>
     * If must not be null or empty, its first character must
     * be a valid start character, and other characters must be valid.
     *
     * @param s The string.
     * @return {@code true} if {@code s} is a valid xml name.
     */
    public static boolean isValidName(String s) {
        // Same for XML 1.0 and XML 1.1
        if (s == null || s.isEmpty()) {
            return false;
        } else {
            final int cp0 = s.codePointAt(0);
            if (!isNameStartChar(cp0)) {
                return false;
            }
            return alwaysTrue(s, XmlUtils::isNameChar);
        }
    }

    /**
     * Checks that a string contains only valid characters, and replaces invalid chars.
     *
     * @param s The string.
     * @param version The XML version.
     * @param reaction The reaction to adopt in when an invalid character is met.
     * @param def The default code point to use to replace invalid characters.
     * @return A validated string.
     * @throws IllegalArgumentException when invalid characters are met and {@code reaction} is FAIL.
     */
    public static String validate(String s,
                                  XmlVersion version,
                                  FailureReaction reaction,
                                  int def) {
        if (s == null) {
            return null;
        } else if (isValidXml(s, version)) {
            return s;
        } else {
            // If reaction is FAIL, we could throw an exception immediately
            // But we could not detail the invalid character
            final StringBuilder builder = new StringBuilder();
            final int length = s.length();
            int index = 0;
            while (index < length) {
                final int codePoint = s.codePointAt(index);
                if (isXmlChar(codePoint, version)) {
                    builder.appendCodePoint(codePoint);
                } else {
                    if (reaction == FailureReaction.DEFAULT) {
                        builder.appendCodePoint(def);
                    } else {
                        final String message = "Invalid xml character '" + codePointToString(codePoint) + "'";
                        if (reaction == FailureReaction.WARN) {
                            builder.appendCodePoint(def);
                            LOGGER.warn(message);
                        } else {
                            throw new IllegalArgumentException(message);
                        }
                    }
                }
                index += Character.charCount(codePoint);
            }
            return builder.toString();
        }
    }

    public static String validate(String s,
                                  XmlVersion version,
                                  FailureReaction reaction) {
        return validate(s, version, reaction, '?');
    }

    /**
     * Replaces all invalid characters.
     *
     * @param s The input string.
     * @param version The XML version.
     * @param replacement The replacement string.
     * @return {@code s} with all invalid characters replaced by {@code replacement}.
     */
    public static String replaceInvalidChars(String s,
                                             XmlVersion version,
                                             String replacement) {
        if (s == null || s.isEmpty()) {
            return s;
        } else {
            final StringBuilder builder = new StringBuilder();
            final int length = s.length();
            int index = 0;
            while (index < length) {
                final int codePoint = s.codePointAt(index);
                if (isXmlChar(codePoint, version)) {
                    builder.appendCodePoint(codePoint);
                } else {
                    builder.append(replacement);
                }
                index += Character.charCount(codePoint);
            }
            return builder.toString();
        }
    }

    /**
     * Escaping context.
     *
     * @author Damien Carbonne
     */
    public enum Context {
        /**
         * The text is in an element.
         */
        ELEMENT,

        /**
         * The text is in an attribute delimited by single quotes (''').
         */
        ATTRIBUTE_SINGLE_QUOTE,

        /**
         * The text is in an attribute delimited by double quotes ('"').
         */
        ATTRIBUTE_DOUBLE_QUOTE;

        public boolean isAttribute() {
            return this == ATTRIBUTE_SINGLE_QUOTE || this == ATTRIBUTE_DOUBLE_QUOTE;
        }
    }

    public enum EscapingPolicy {
        ESCAPE_IF_NECESSARY,
        ESCAPE_ALWAYS
    }

    /**
     * Returns {@code true} is a string must be escaped.
     * <ul>
     * <li>{@code <} and {@code &} must always be escaped.
     * <li>{@code >} does not need escape. It is escaped if required with policy.
     * <li>{@code "} needs to be escaped in attributes delimited by {@code "}. Otherwise, is escaped if required by policy.
     * <li>{@code '} needs to be escaped in attributes delimited by {@code '}. Otherwise, is escaped if required by policy.
     * <li>{@code \t}, {@code \n} and {@code \r} should be escaped in attributes. They are if escaped if required by policy.
     * </ul>
     *
     * @param s The string.
     * @param context The context.
     * @param policy The escaping policy.
     * @return {@code true} is {@code s} must be escaped in{@code context} and using {@code policy}.
     */
    public static boolean needsEscape(String s,
                                      Context context,
                                      EscapingPolicy policy) {
        if (s != null) {
            final int length = s.length();
            int index = 0;
            while (index < length) {
                final int codePoint = s.codePointAt(index);
                switch (codePoint) {
                case '<':
                case '&':
                    return true;
                case '>':
                    return policy == EscapingPolicy.ESCAPE_ALWAYS;
                case '"':
                    // Only needs to be escaped in attributes enclosed by double
                    // quotes
                    return policy == EscapingPolicy.ESCAPE_ALWAYS || context == Context.ATTRIBUTE_DOUBLE_QUOTE;
                case '\'':
                    // Only needs to be escaped in attributes enclosed by single
                    // quotes
                    return policy == EscapingPolicy.ESCAPE_ALWAYS || context == Context.ATTRIBUTE_SINGLE_QUOTE;
                case '\n':
                case '\t':
                case '\r':
                    return policy == EscapingPolicy.ESCAPE_ALWAYS && context.isAttribute();
                default:
                    // Ignore
                    break;
                }
                index += Character.charCount(codePoint);
            }
        }
        return false;
    }

    /**
     * Returns an escaped version of a string.
     *
     * @param s The string.
     * @param context The context.
     * @param policy The escaping policy.
     * @return The escaped version of {@code s} in{@code context} and using {@code policy}.
     */
    public static String escape(String s,
                                Context context,
                                EscapingPolicy policy) {
        if (s != null) {
            final StringBuilder builder = new StringBuilder();
            appendEscaped(builder, s, context, policy);
            return builder.toString();
        } else {
            return s;
        }
    }

    public static void appendEscaped(StringBuilder builder,
                                     String s,
                                     Context context,
                                     EscapingPolicy policy) {
        if (s != null) {
            final int length = s.length();
            int index = 0;
            while (index < length) {
                final int codePoint = s.codePointAt(index);
                switch (codePoint) {
                case '<':
                    builder.append(XmlWriter.LT);
                    break;

                case '&':
                    builder.append(XmlWriter.AMP);
                    break;

                case '>':
                    if (policy == EscapingPolicy.ESCAPE_ALWAYS) {
                        builder.append(XmlWriter.GT);
                    } else {
                        builder.appendCodePoint(codePoint);
                    }
                    break;

                case '"':
                    // Only needs to be escaped in attributes enclosed by double quotes
                    if (context == Context.ATTRIBUTE_DOUBLE_QUOTE || policy == EscapingPolicy.ESCAPE_ALWAYS) {
                        builder.append(XmlWriter.QUOT);
                    } else {
                        builder.appendCodePoint(codePoint);
                    }
                    break;

                case '\'':
                    // Only needs to be escaped in attributes enclosed by single quotes
                    if (context == Context.ATTRIBUTE_SINGLE_QUOTE || policy == EscapingPolicy.ESCAPE_ALWAYS) {
                        builder.append(XmlWriter.APOS);
                    } else {
                        builder.appendCodePoint(codePoint);
                    }
                    break;

                case '\n':
                    if (context.isAttribute() && policy == EscapingPolicy.ESCAPE_ALWAYS) {
                        builder.append(XmlWriter.LF);
                    } else {
                        builder.appendCodePoint(codePoint);
                    }
                    break;

                case '\t':
                    if (context.isAttribute() && policy == EscapingPolicy.ESCAPE_ALWAYS) {
                        builder.append(XmlWriter.TAB);
                    } else {
                        builder.appendCodePoint(codePoint);
                    }
                    break;

                case '\r':
                    if (context.isAttribute() && policy == EscapingPolicy.ESCAPE_ALWAYS) {
                        builder.append(XmlWriter.CR);
                    } else {
                        builder.appendCodePoint(codePoint);
                    }
                    break;

                default:
                    builder.appendCodePoint(codePoint);
                    break;
                }
                index += Character.charCount(codePoint);
            }
        }
    }
}