package cdc.io.xml;

import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamReader;

import cdc.util.strings.StringUtils;

public final class StAXSupport {
    private StAXSupport() {
    }

    public static final String REPORT_CDATA = "http://java.sun.com/xml/stream/properties/report-cdata-event";

    public static String eventTypeToString(int eventType) {
        switch (eventType) {
        case XMLStreamConstants.ATTRIBUTE:
            return "ATTRIBUTE";
        case XMLStreamConstants.CDATA:
            return "CDATA";
        case XMLStreamConstants.CHARACTERS:
            return "CHARACTERS";
        case XMLStreamConstants.COMMENT:
            return "COMMENT";
        case XMLStreamConstants.DTD:
            return "DTD";
        case XMLStreamConstants.END_DOCUMENT:
            return "END_DOCUMENT";
        case XMLStreamConstants.END_ELEMENT:
            return "END_ELEMENT";
        case XMLStreamConstants.ENTITY_DECLARATION:
            return "ENTITY_DECLARATION";
        case XMLStreamConstants.ENTITY_REFERENCE:
            return "ENTITY_REFERENCE";
        case XMLStreamConstants.NAMESPACE:
            return "NAMESPACE";
        case XMLStreamConstants.NOTATION_DECLARATION:
            return "NOTATION_DECLARATION";
        case XMLStreamConstants.PROCESSING_INSTRUCTION:
            return "PROCESSING_INSTRUCTION";
        case XMLStreamConstants.SPACE:
            return "SPACE";
        case XMLStreamConstants.START_DOCUMENT:
            return "START_DOCUMENT";
        case XMLStreamConstants.START_ELEMENT:
            return "START_ELEMENT";
        default:
            return "???";
        }
    }

    public static String toString(XMLStreamReader reader) {
        final StringBuilder builder = new StringBuilder();
        builder.append(eventTypeToString(reader.getEventType()));

        switch (reader.getEventType()) {
        case XMLStreamConstants.END_ELEMENT:
        case XMLStreamConstants.START_ELEMENT:
        case XMLStreamConstants.ENTITY_REFERENCE:
            builder.append(" (");
            builder.append(reader.getLocalName());
            builder.append(")");
            break;
        case XMLStreamConstants.CHARACTERS:
        case XMLStreamConstants.SPACE:
            builder.append(" [");
            builder.append(StringUtils.extract(reader.getText(), 30));
            builder.append("]");
            break;
        default:
            break;
        }

        if (reader.getEventType() == XMLStreamConstants.START_ELEMENT) {
            for (int index = 0; index < reader.getAttributeCount(); index++) {
                builder.append(' ');
                builder.append(reader.getAttributeLocalName(index));
                builder.append('=');
                builder.append(reader.getAttributeValue(index));
            }
        }

        return builder.toString();
    }
}