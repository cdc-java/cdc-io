package cdc.io.xml;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.net.URL;
import java.util.function.BiFunction;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.io.compress.CompressionUtils;
import cdc.io.compress.Compressor;

/**
 * Base class used to load an XML file.
 * <p>
 * Parsing is delegated to a parser that must be provided.
 *
 * @author Damien Carbonne
 *
 * @param <R> The parsing result type.
 */
public abstract class AbstractStAXLoader<R> {
    private final Logger logger;
    private final BiFunction<XMLStreamReader, String, ? extends AbstractStAXParser<R>> ctor;
    private static final String LOAD = "load({}, {})";

    protected AbstractStAXLoader(BiFunction<XMLStreamReader, String, ? extends AbstractStAXParser<R>> ctor) {
        this.ctor = ctor;
        this.logger = LogManager.getLogger(getClass());
    }

    /**
     * @return The logger to use.
     */
    public final Logger getLogger() {
        return logger;
    }

    public R load(Reader reader,
                  String systemId) throws IOException {
        final XMLInputFactory factory = XMLInputFactory.newInstance();
        factory.setProperty(XMLInputFactory.IS_COALESCING, true);
        try (final Reader buffered = reader instanceof BufferedReader ? reader : new BufferedReader(reader)) {
            final XMLStreamReader xmlsr = factory.createXMLStreamReader(buffered);
            final AbstractStAXParser<R> parser = ctor.apply(xmlsr, systemId);
            final R result = parser.parse();
            xmlsr.close();
            return result;
        } catch (final XMLStreamException e) {
            throw new IOException(e);
        }
    }

    public R load(InputStream is) throws IOException {
        return load(is, null, Compressor.NONE);
    }

    public R load(InputStream is,
                  String systemId) throws IOException {
        return load(is, systemId, Compressor.NONE);
    }

    public R load(InputStream is,
                  String systemId,
                  Compressor compressor) throws IOException {
        final XMLInputFactory factory = XMLInputFactory.newInstance();
        factory.setProperty(XMLInputFactory.IS_COALESCING, true);
        try (final InputStream buffered = is instanceof BufferedInputStream ? is : new BufferedInputStream(is);
                final InputStream adapted = CompressionUtils.adapt(buffered, compressor)) {
            final XMLStreamReader xmlsr = factory.createXMLStreamReader(systemId, adapted);
            final AbstractStAXParser<R> parser = ctor.apply(xmlsr, systemId);
            final R result = parser.parse();
            xmlsr.close();
            return result;
        } catch (final XMLStreamException e) {
            throw new IOException(e);
        }
    }

    public final R load(String filename,
                        Compressor compressor) throws IOException {
        logger.info(LOAD, filename, compressor);
        try (final InputStream is = new BufferedInputStream(CompressionUtils.adapt(new FileInputStream(filename), compressor))) {
            return load(is, filename);
        }
    }

    public final R load(String filename) throws IOException {
        return load(filename, Compressor.NONE);
    }

    public final R load(File file,
                        Compressor compressor) throws IOException {
        return load(file.getPath(), compressor);
    }

    public final R load(File file) throws IOException {
        return load(file, Compressor.NONE);
    }

    public final R load(URL url,
                        Compressor compressor) throws IOException {
        logger.info(LOAD, url, compressor);
        try (final InputStream is = new BufferedInputStream(CompressionUtils.adapt(url.openStream(), compressor))) {
            return load(is);
        }
    }

    public final R load(URL url) throws IOException {
        return load(url, Compressor.NONE);
    }
}