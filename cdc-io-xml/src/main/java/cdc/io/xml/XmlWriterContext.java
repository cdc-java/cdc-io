package cdc.io.xml;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Used to manage the writer current context. Contexts are chained and reused.
 * The number of created contexts matches the XML depth.
 *
 * @author Damien Carbonne
 */
final class XmlWriterContext {
    private static final Logger LOGGER = LogManager.getLogger(XmlWriterContext.class);

    public enum Type {
        /**
         * Initial state, before calling beginDocument.
         * <p>
         * Stream is empty.
         */
        IN_OPEN_STREAM,

        /**
         * After calling beginDocument
         * <p>
         * Stream contains the xml declaration.
         */
        IN_OPEN_DOC,

        /**
         * After calling beginElement.
         * <p>
         * One can add attributes to that element, or content (text, comments, ...) to it.
         */
        IN_TAG,

        /** After calling begingProcessingInstruction. */
        IN_PI,

        /**
         * Replaces IN_TAG and indicates that the element content is simple.
         */
        IN_SIMPLE_CONTENT,

        /**
         * Replaces IN_TAG and indicates that the element content is complex.
         */
        IN_MIXED_CONTENT,

        /** After calling beginComment. */
        IN_COMMENT,

        /** After calling beginCData. */
        IN_CDATA,

        /**
         * After closing of the root element.
         * <p>
         * We can not add more elements.
         */
        IN_CLOSED_DOC,

        /**
         * After calling endDocument.
         * <p>
         * One can not do anything after that, except closing the stream.
         */
        IN_CLOSED_STREAM
    }

    /** Type of this context. */
    private Type type;
    /** Element name. {@link Type#IN_TAG}, {@link Type#IN_SIMPLE_CONTENT}, {@link Type#IN_MIXED_CONTENT} */
    private String name;
    /** Set to true when the document has a DTD. {@link Type#IN_OPEN_DOC}. */
    private boolean dtd = false;
    private final XmlWriterContext parent;
    private XmlWriterContext child = null;

    private XmlWriterContext(Type type,
                             XmlWriterContext parent) {
        this.type = type;
        this.parent = parent;
        if (parent != null) {
            parent.child = this;
        }
    }

    XmlWriterContext() {
        this(Type.IN_OPEN_STREAM, null);
    }

    XmlWriterContext pushContext(Type type) {
        LOGGER.trace("pushContext({})", type);
        if (child == null) {
            new XmlWriterContext(type, this);
        } else {
            child.setType(type);
        }
        print(child);
        return child;
    }

    final XmlWriterContext popContext() {
        LOGGER.trace("popContext()");
        type = null;
        name = null;
        print(parent);
        return parent;
    }

    final XmlWriterContext popAll() {
        XmlWriterContext index = this;
        while (index.parent != null) {
            index = index.popContext();
        }
        return index;
    }

    final Type getType() {
        return type;
    }

    final void setType(Type type) {
        LOGGER.trace("setType({})", type);
        this.type = type;
        print(this);
    }

    final void setName(String name) {
        LOGGER.trace("setName({})", name);
        this.name = name;
        print(this);
    }

    final String getName() {
        return name;
    }

    final void setDTD() {
        LOGGER.trace("setDTD()");
        this.dtd = true;
    }

    final boolean hasDTD() {
        return dtd;
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append(type);
        if (name != null) {
            builder.append(" ").append(name);
        }
        return builder.toString();
    }

    private void appendRec(StringBuilder builder) {
        if (parent != null) {
            parent.appendRec(builder);
            builder.append("/");
        }
        builder.append(getType());
        if (name != null) {
            builder.append(" ")
                   .append(name);
        }
    }

    private static void print(XmlWriterContext context) {
        if (LOGGER.isTraceEnabled()) {
            final StringBuilder builder = new StringBuilder();
            builder.append("Stack: ");
            if (context != null) {
                context.appendRec(builder);
            }
            LOGGER.trace("   {}", builder);
        }
    }

    public void print(String message) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("---------------------");
            LOGGER.trace(message);
            print(this);
        }
    }
}