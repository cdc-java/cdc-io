package cdc.io.xml;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;

import cdc.util.lang.Checks;
import cdc.util.lang.NotFoundException;

public class XmlWriting {
    private final Map<Class<?>, XmlElementWriter<?, ?>> map = new HashMap<>();

    public XmlWriting() {
        super();
    }

    public <T, D> void register(XmlElementWriter<T, D> writer) {
        Checks.isNotNull(writer, "writer");
        Checks.doesNotContainKey(map, writer.getObjectClass(), "writer.getObjectClass()");

        map.put(writer.getObjectClass(), writer);
    }

    public <T, D> XmlElementWriter<T, D> getElementWriterOrNull(Class<T> objectClass,
                                                                Class<D> dataClass) {
        Checks.isNotNull(objectClass, "objectClass");
        Checks.isNotNull(dataClass, "dataClass");

        @SuppressWarnings("unchecked")
        final XmlElementWriter<T, D> result = (XmlElementWriter<T, D>) map.get(objectClass);
        if (result != null) {
            Checks.assertTrue(dataClass.isAssignableFrom(result.getDataClass()),
                              "Data class mismatch, expected: {}, found: {}",
                              dataClass,
                              result.getDataClass());
        }
        return result;
    }

    public <T, D> XmlElementWriter<T, D> getElementWriter(Class<T> objectClass,
                                                          Class<D> dataClass) {
        final XmlElementWriter<T, D> result = getElementWriterOrNull(objectClass, dataClass);
        return NotFoundException.onResult(result, "No printer found for " + objectClass.getCanonicalName());
    }

    public void writeElement(XmlHandler handler,
                             String name,
                             Object object,
                             Object data) throws IOException {
        Checks.isNotNull(handler, "handler");
        Checks.isNotNull(name, "name");

        @SuppressWarnings("unchecked")
        final XmlElementWriter<Object, Object> result =
                (XmlElementWriter<Object, Object>) getElementWriter(object.getClass(), Object.class);
        result.writeElementRaw(handler, name, object, data);
    }

    public void writeElement(XmlHandler handler,
                             String name,
                             Object object) throws IOException {
        writeElement(handler, name, object, null);
    }

    public <T> void writeElement(XmlHandler handler,
                                 Function<? super T, String> nameExtractor,
                                 T object,
                                 Object data) throws IOException {
        writeElement(handler, nameExtractor.apply(object), object, data);
    }

    public <T> void writeElement(XmlHandler handler,
                                 Function<? super T, String> nameExtractor,
                                 T object) throws IOException {
        writeElement(handler, nameExtractor, object, null);
    }

    public <T, D> void writeElement(XmlHandler handler,
                                    BiFunction<? super T, ? super D, String> nameExtractor,
                                    T object,
                                    D data) throws IOException {
        writeElement(handler, nameExtractor.apply(object, data), object, data);
    }

    public <T, D> void writeElement(XmlHandler handler,
                                    BiFunction<? super T, ? super D, String> nameExtractor,
                                    T object) throws IOException {
        writeElement(handler, nameExtractor.apply(object, null), object, null);
    }
}