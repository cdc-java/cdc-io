package cdc.io.xml;

import javax.xml.stream.XMLStreamException;

public interface StAXElementReader<T, C> {
    public Class<T> getObjectClass();

    public Class<C> getContextClass();

    public T parseElement(AbstractStAXParser<?> parser,
                          C context) throws XMLStreamException;

    public default T parseElementRaw(AbstractStAXParser<?> parser,
                                     Object context) throws XMLStreamException {
        return parseElement(parser, getContextClass().cast(context));
    }
}