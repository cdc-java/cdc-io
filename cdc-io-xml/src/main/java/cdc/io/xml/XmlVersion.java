package cdc.io.xml;

/**
 * Enumeration of available XML versions.
 *
 * @author Damien Carbonne
 *
 */
public enum XmlVersion {
    XML_1_0,
    XML_1_1;

    public String getLabel() {
        if (this == XML_1_0) {
            return "1.0";
        } else {
            return "1.1";
        }
    }
}