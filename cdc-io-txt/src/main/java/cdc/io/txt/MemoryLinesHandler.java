package cdc.io.txt;

import java.util.ArrayList;
import java.util.List;

import cdc.util.function.Evaluation;

/**
 * Implementation of {@link LinesHandler} that stores passed lines.
 *
 * @author Damien Carbonne
 */
public class MemoryLinesHandler implements LinesHandler {
    private final List<String> lines = new ArrayList<>();

    public List<String> getLines() {
        return lines;
    }

    @Override
    public void processBegin() {
        lines.clear();
    }

    @Override
    public Evaluation processLine(String line,
                                  int number) {
        lines.add(line);
        return Evaluation.CONTINUE;
    }

    @Override
    public void processEnd() {
        // Ignore
    }
}