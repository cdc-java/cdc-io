package cdc.io.txt;

import java.util.List;

public class Replacements {
    private final List<Replacement> list;

    public Replacements(Replacement... replacements) {
        this.list = List.of(replacements);
    }

    public String apply(String s) {
        for (final Replacement replacement : list) {
            s = replacement.apply(s);
        }
        return s;
    }
}