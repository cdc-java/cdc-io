package cdc.io.txt;

@FunctionalInterface
public interface LinePredicate {
    public static final LinePredicate TRUE = (line,
                                              number) -> true;

    /**
     * Invoked to test a line.
     *
     * @param line The line content.
     * @param number The line number, first line being numbered 1.
     * @return {@code true} if the passed {@code line} is accepted.
     */
    public boolean test(String line,
                        int number);
}