package cdc.io.txt;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.file.Files;

import cdc.util.function.Evaluation;

/**
 * Utilities to parse text sources and call {@link LinesHandler}.
 * <p>
 * <b>WARNING:</b> With current implementation, if the last line is empty, it will be skipped.<br>
 * This content:
 *
 * <pre>
 * {@code
 * [11111<eol>]
 * [22222<eol>]
 * []
 * }
 * </pre>
 * and this one:
 *
 * <pre>
 * {@code
 * [11111<eol>]
 * [22222]
 * }
 * </pre>
 * will appear identical (2 lines).
 * <p>
 * One may consider using {@link Files#lines(java.nio.file.Path)}
 * or {@link Files#lines(java.nio.file.Path, java.nio.charset.Charset)}.
 *
 * @author Damien Carbonne
 */
public final class LinesParser {
    private LinesParser() {
    }

    /**
     * Parses a reader and invokes a LineHandler.
     *
     * @param reader The reader.
     * @param handler The handler.
     * @throws IOException When an IO exception occurs
     */
    public static void parse(Reader reader,
                             LinesHandler handler) throws IOException {
        final BufferedReader r;
        if (reader instanceof final BufferedReader br) {
            r = br;
        } else {
            r = new BufferedReader(reader);
        }

        String line = null;
        int number = 0;
        handler.processBegin();
        Evaluation evaluation = Evaluation.CONTINUE;
        while (evaluation == Evaluation.CONTINUE && (line = r.readLine()) != null) {
            number++;
            evaluation = handler.processLine(line, number);
        }
        handler.processEnd();
    }

    /**
     * Parses an InputStream using a specified charset and invokes a LineHandler.
     *
     * @param in The input stream.
     * @param systemId The system id.
     * @param charset The name of the charset to use.
     * @param handler The handler.
     * @throws IOException When an IO exception occurs
     */
    public static void parse(InputStream in,
                             String systemId,
                             Charset charset,
                             LinesHandler handler) throws IOException {
        if (charset != null) {
            try (final Reader reader = new BufferedReader(new InputStreamReader(in, charset))) {
                parse(reader, handler);
            }
        } else {
            try (final Reader reader = new BufferedReader(new InputStreamReader(in))) {
                parse(reader, handler);
            }
        }
    }

    /**
     * Parses an InputStream using platform default charset and invokes a LineHandler.
     *
     * @param in The input stream.
     * @param systemId The system id.
     * @param handler The handler.
     * @throws IOException When an IO exception occurs
     */
    public static void parse(InputStream in,
                             String systemId,
                             LinesHandler handler) throws IOException {
        parse(in, systemId, null, handler);
    }

    /**
     * Parses a file using a specified charset and invokes a LineHandler.
     *
     * @param filename The file name.
     * @param charset The name of the charset to use.
     * @param handler The handler.
     * @throws IOException When an IO exception occurs
     */
    public static void parse(String filename,
                             Charset charset,
                             LinesHandler handler) throws IOException {
        try (final InputStream in = new BufferedInputStream(new FileInputStream(filename))) {
            parse(in, filename, charset, handler);
        }
    }

    /**
     * Parses a file using platform default charset and invokes a LineHandler.
     *
     * @param filename The file name.
     * @param handler The handler.
     * @throws IOException When an IO exception occurs
     */
    public static void parse(String filename,
                             LinesHandler handler) throws IOException {
        parse(filename, null, handler);
    }

    /**
     * Parses a file using a specified charset and invokes a LineHandler.
     *
     * @param file The file.
     * @param charset The name of the charset to use.
     * @param handler The handler.
     * @throws IOException When an IO exception occurs
     */
    public static void parse(File file,
                             Charset charset,
                             LinesHandler handler) throws IOException {
        try (final InputStream in = new BufferedInputStream(new FileInputStream(file))) {
            parse(in, file.getPath(), charset, handler);
        }
    }

    /**
     * Parses a file using platform default charset and invokes a LineHandler.
     *
     * @param file The file.
     * @param handler The handler.
     * @throws IOException When an IO exception occurs
     */
    public static void parse(File file,
                             LinesHandler handler) throws IOException {
        parse(file, null, handler);
    }
}