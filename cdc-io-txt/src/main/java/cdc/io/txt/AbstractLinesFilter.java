package cdc.io.txt;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.UnaryOperator;

import cdc.util.function.Evaluation;
import cdc.util.lang.Checks;

/**
 * Abstract base class used to create lines filters.
 *
 * @author Damien Carbonne
 */
public abstract class AbstractLinesFilter implements LinesHandler {
    protected final LinesHandler delegate;

    protected AbstractLinesFilter(LinesHandler delegate) {
        Checks.isNotNull(delegate, "delegate");

        this.delegate = delegate;
    }

    public final LinesHandler getDelegate() {
        return delegate;
    }

    @Override
    public void processBegin() {
        delegate.processBegin();
    }

    @Override
    public void processEnd() {
        delegate.processEnd();
    }

    static class Sorter extends AbstractLinesFilter {
        private final Comparator<String> comparator;
        private final List<String> lines = new ArrayList<>();

        public Sorter(LinesHandler delegate,
                      Comparator<String> comparator) {
            super(delegate);
            this.comparator = Checks.isNotNull(comparator, "comparator");
        }

        @Override
        public void processBegin() {
            delegate.processBegin();
        }

        @Override
        public Evaluation processLine(String line,
                                      int number) {
            lines.add(line);
            return Evaluation.CONTINUE;
        }

        @Override
        public void processEnd() {
            Collections.sort(lines, comparator);
            int number = 0;
            for (final String line : lines) {
                number++;
                final Evaluation evaluation = delegate.processLine(line, number);
                if (evaluation == Evaluation.PRUNE) {
                    break;
                }
            }
            delegate.processEnd();
        }
    }

    static class Filter extends AbstractLinesFilter {
        private final LinePredicate predicate;
        private int number = 0;

        public Filter(LinesHandler delegate,
                      LinePredicate predicate) {
            super(delegate);
            this.predicate = Checks.isNotNull(predicate, "predicate");
        }

        @Override
        public void processBegin() {
            this.number = 0;
            delegate.processBegin();
        }

        @Override
        public Evaluation processLine(String line,
                                      int number) {
            if (predicate.test(line, number)) {
                this.number++;
                return delegate.processLine(line, this.number);
            }
            return Evaluation.CONTINUE;
        }
    }

    static class Header extends AbstractLinesFilter {
        private final int count;

        protected Header(LinesHandler delegate,
                         int count) {
            super(delegate);
            this.count = count;
        }

        @Override
        public Evaluation processLine(String line,
                                      int number) {
            if (number <= count) {
                delegate.processLine(line, number);
                return Evaluation.CONTINUE;
            } else {
                return Evaluation.PRUNE;
            }
        }
    }

    static class Footer extends AbstractLinesFilter {
        private final int count;
        private final String[] lines;
        private int next = 0;
        private int total = 0;

        protected Footer(LinesHandler delegate,
                         int count) {
            super(delegate);
            this.count = count;
            this.lines = new String[count];
        }

        @Override
        public Evaluation processLine(String line,
                                      int number) {
            if (count == 0) {
                return Evaluation.PRUNE;
            }
            total++;
            lines[next] = line;
            next = (++next) % count;
            return Evaluation.CONTINUE;
        }

        @Override
        public void processEnd() {
            // The number of lines to output
            final int size;
            // Index in lines of first line to output
            final int first;
            if (total <= count) {
                size = total;
                first = 0;
            } else {
                size = count;
                first = next;
            }
            for (int index = 0; index < size; index++) {
                delegate.processLine(lines[(first + index) % count],
                                     index + 1);
            }
            delegate.processEnd();
        }
    }

    static class Converter extends AbstractLinesFilter {
        private final UnaryOperator<String> operator;
        private final LinePredicate predicate;

        public Converter(LinesHandler delegate,
                         LinePredicate predicate,
                         UnaryOperator<String> converter) {
            super(delegate);
            this.operator = converter;
            this.predicate = predicate;
        }

        @Override
        public Evaluation processLine(String line,
                                      int number) {
            if (predicate.test(line, number)) {
                return delegate.processLine(operator.apply(line), number);
            } else {
                return delegate.processLine(line, number);
            }
        }
    }

    static class Uniq extends AbstractLinesFilter {
        private String previous = null;
        private int next = 0;

        protected Uniq(LinesHandler delegate) {
            super(delegate);
        }

        @Override
        public Evaluation processLine(String line,
                                      int number) {
            if (!line.equals(previous)) {
                previous = line;
                next++;
                return delegate.processLine(line, next);
            } else {
                return Evaluation.CONTINUE;
            }
        }
    }
}