package cdc.io.txt;

import java.util.Comparator;
import java.util.function.UnaryOperator;

import cdc.util.function.Evaluation;

/**
 * Interface implemented by objects that can process ordered lines.
 * <p>
 * The caller <b>MUST</b> proceed in this order:
 * <ol>
 * <li>{@code processBegin}
 * <li>{@code processLine*}
 * <li>{@code processEnd}
 * </ol>
 *
 * @author D. Carbonne
 */
public interface LinesHandler {
    /**
     * Called at beginning of lines processing.
     */
    public default void processBegin() {
        // Ignore
    }

    /**
     * Called once for each line. The line object may be reused between calls.
     *
     * @param line The line to process.
     * @param number The line number, starting at 1.
     * @return {@link Evaluation#CONTINUE} when parsing must continue, {@link Evaluation#PRUNE} otherwise.
     */
    public Evaluation processLine(String line,
                                  int number);

    /**
     * Called at the end of lines processing.
     * <p>
     * This must be called even when processLine() returned {@link Evaluation#PRUNE} or there are no more lines.
     */
    public default void processEnd() {
        // Ignore
    }

    /**
     * Creates an {@link AbstractLinesFilter} that sorts lines.
     *
     * @param delegate The delegate.
     * @return A new {@link AbstractLinesFilter} that sorts lines and send them to {@code delegate}.
     */
    public static AbstractLinesFilter sorter(LinesHandler delegate) {
        return new AbstractLinesFilter.Sorter(delegate, Comparator.comparing(String::toString));
    }

    /**
     * Creates an {@link AbstractLinesFilter} that sorts lines.
     *
     * @param delegate The delegate.
     * @param comparator The comparator.
     * @return A new {@link AbstractLinesFilter} that sorts lines using {@code comparator} and send them to {@code delegate}.
     */
    public static AbstractLinesFilter sorter(LinesHandler delegate,
                                             Comparator<String> comparator) {
        return new AbstractLinesFilter.Sorter(delegate, comparator);
    }

    /**
     * Creates an {@link AbstractLinesFilter} that filter lines.
     *
     * @param delegate The delegate.
     * @param predicate The predicate.
     * @return A new {@link AbstractLinesFilter} that filters lines using {@code predicate} and send them to {@code delegate}.
     */
    public static AbstractLinesFilter filter(LinesHandler delegate,
                                             LinePredicate predicate) {
        return new AbstractLinesFilter.Filter(delegate, predicate);
    }

    /**
     * Creates an {@link AbstractLinesFilter} that keeps first lines.
     *
     * @param delegate The delegate.
     * @param count The number of lines to keep.
     * @return A new {@link AbstractLinesFilter} that keeps at most {@code count} first lines and send them to {@code delegate}.
     */
    public static AbstractLinesFilter header(LinesHandler delegate,
                                             int count) {
        return new AbstractLinesFilter.Header(delegate, count);
    }

    /**
     * Creates an {@link AbstractLinesFilter} that keeps last lines.
     *
     * @param delegate The delegate.
     * @param count The number of lines to keep.
     * @return A new {@link AbstractLinesFilter} that keeps at most {@code count} last lines and send them to {@code delegate}.
     */
    public static AbstractLinesFilter footer(LinesHandler delegate,
                                             int count) {
        return new AbstractLinesFilter.Footer(delegate, count);
    }

    /**
     * Creates an {@link AbstractLinesFilter} that converts lines matching a predicate.
     *
     * @param delegate The delegate.
     * @param predicate The predicate.
     * @param converter The converter.
     * @return A new {@link AbstractLinesFilter} that converts lines and send them to {@code delegate}.
     */
    public static AbstractLinesFilter converter(LinesHandler delegate,
                                                LinePredicate predicate,
                                                UnaryOperator<String> converter) {
        return new AbstractLinesFilter.Converter(delegate, predicate, converter);
    }

    /**
     * Creates an {@link AbstractLinesFilter} that converts all lines.
     *
     * @param delegate The delegate.
     * @param converter The converter.
     * @return A new {@link AbstractLinesFilter} that converts lines and send them to {@code delegate}.
     */
    public static AbstractLinesFilter converter(LinesHandler delegate,
                                                UnaryOperator<String> converter) {
        return converter(delegate, LinePredicate.TRUE, converter);
    }

    /**
     * Creates an {@link AbstractLinesFilter} that skips consecutive identical lines.
     *
     * @param delegate The delegate.
     * @return A new {@link AbstractLinesFilter} that skips consecutive identical lines.
     */
    public static AbstractLinesFilter uniq(LinesHandler delegate) {
        return new AbstractLinesFilter.Uniq(delegate);
    }

    /**
     * Creates an {@link AbstractLinesFilter} that applies pattern replacements to lines matching a predicate.
     *
     * @param delegate The delegate.
     * @param predicate The predicate.
     * @param replacements The replacements.
     * @return A new {@link AbstractLinesFilter} that applies pattern replacements to matching lines
     *         and send them to {@code delegate}.
     */
    public static AbstractLinesFilter replacer(LinesHandler delegate,
                                               LinePredicate predicate,
                                               Replacement... replacements) {
        final Replacements r = new Replacements(replacements);
        return new AbstractLinesFilter.Converter(delegate, predicate, r::apply);
    }

    /**
     * Creates an {@link AbstractLinesFilter} that applies pattern replacements to all lines.
     *
     * @param delegate The delegate.
     * @param replacements The replacements.
     * @return A new {@link AbstractLinesFilter} that applies pattern replacements to all lines
     *         and send them to {@code delegate}.
     */
    public static AbstractLinesFilter replacer(LinesHandler delegate,
                                               Replacement... replacements) {
        return replacer(delegate, LinePredicate.TRUE, replacements);
    }

    /**
     * Creates an {@link AbstractLinesFilter} that applies pattern replacements to lines matching a predicate.
     *
     * @param delegate The delegate.
     * @param predicate The predicate.
     * @param pattern The pattern.
     * @param by The replacement.
     * @return A new {@link AbstractLinesFilter} that applies pattern replacements to matching lines
     *         and send them to {@code delegate}.
     */
    public static AbstractLinesFilter replacer(LinesHandler delegate,
                                               LinePredicate predicate,
                                               String pattern,
                                               String by) {
        return replacer(delegate, predicate, new Replacement(pattern, by));
    }

    /**
     * Creates an {@link AbstractLinesFilter} that applies pattern replacements to all lines.
     *
     * @param delegate The delegate.
     * @param pattern The pattern.
     * @param by The replacement.
     * @return A new {@link AbstractLinesFilter} that applies pattern replacement to all lines
     *         and send them to {@code delegate}.
     */
    public static AbstractLinesFilter replacer(LinesHandler delegate,
                                               String pattern,
                                               String by) {
        return replacer(delegate, LinePredicate.TRUE, pattern, by);
    }
}