package cdc.io.txt;

import cdc.util.function.Evaluation;

/**
 * Implementation of {@link LinesHandler} that does nothing.
 *
 * @author Damien Carbonne
 */
public class VoidLinesHandler implements LinesHandler {
    @Override
    public void processBegin() {
        // Ignore
    }

    @Override
    public Evaluation processLine(String line,
                                  int number) {
        return Evaluation.CONTINUE;
    }

    @Override
    public void processEnd() {
        // Ignore
    }
}