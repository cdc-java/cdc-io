package cdc.io.txt;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.util.function.Evaluation;

/**
 * Implementation of {@link LinesHandler} that traces calls and calls a delegate.
 *
 * @author Damien Carbonne
 */
public class VerboseLinesHandler extends AbstractLinesFilter {
    private static final Logger LOGGER = LogManager.getLogger(VerboseLinesHandler.class);

    public VerboseLinesHandler(LinesHandler delegate) {
        super(delegate);
    }

    @Override
    public void processBegin() {
        LOGGER.info("processBegin()");
        delegate.processBegin();
    }

    @Override
    public Evaluation processLine(String line,
                                  int number) {
        LOGGER.info("processLine({}, {})", line, number);
        return delegate.processLine(line, number);
    }

    @Override
    public void processEnd() {
        LOGGER.info("processEnd()");
        delegate.processEnd();
    }
}