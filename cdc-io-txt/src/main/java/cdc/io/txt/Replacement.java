package cdc.io.txt;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Replacement {
    private final Pattern pattern;
    private final String by;

    public Replacement(String pattern,
                       String by) {
        this.pattern = Pattern.compile(pattern);
        this.by = by;
    }

    public String apply(String to) {
        final Matcher m = pattern.matcher(to);
        return m.replaceAll(by);
    }

    public Pattern getPattern() {
        return pattern;
    }

    public String getBy() {
        return by;
    }
}