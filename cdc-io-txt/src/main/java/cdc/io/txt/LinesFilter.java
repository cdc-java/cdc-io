package cdc.io.txt;

import cdc.util.function.Evaluation;
import cdc.util.lang.Checks;

/**
 * Implementation of {@link LinesHandler} that filters lines and calls a delegate.
 *
 * @author Damien Carbonne
 */
public class LinesFilter extends AbstractLinesFilter {
    private final LinePredicate predicate;
    private int number = 0;

    public LinesFilter(LinesHandler delegate,
                       LinePredicate predicate) {
        super(delegate);
        this.predicate = Checks.isNotNull(predicate, "predicate");
    }

    public final LinePredicate getPredicate() {
        return predicate;
    }

    @Override
    public Evaluation processLine(String line,
                                  int number) {
        if (predicate.test(line, number)) {
            this.number++;
            return delegate.processLine(line, this.number);
        } else {
            return Evaluation.CONTINUE;
        }
    }
}