package cdc.io.txt;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.util.function.Evaluation;

class LinesParserTest {
    protected static final Logger LOGGER = LogManager.getLogger(LinesParserTest.class);

    private static final String NL = "\n";
    private static final String CRNL = "\r\n";
    private static final String CR = "\r";
    private static final String[] EOLS = { NL, CRNL, CR };

    private final List<String> lines = new ArrayList<>();
    private final File filePlatform = new File("target", getClass().getSimpleName() + "-platform.txt");

    LinesParserTest() throws IOException {
        lines.add("Line 1: aaa");
        lines.add("Line 2: ààà");
        lines.add("Line 3");

        // Generate a Platform file
        try (PrintStream out = new PrintStream(filePlatform)) {
            for (final String line : lines) {
                out.println(line);
            }
            out.close();
        }
    }

    private static String toNames(String s) {
        final StringBuilder builder = new StringBuilder();
        builder.append("[");
        for (int index = 0; index < s.length(); index++) {
            if (index > 0) {
                builder.append(", ");
            }
            builder.append(Character.getName(s.charAt(index)));
        }
        builder.append("]");
        return builder.toString();
    }

    private static String toString(String eol,
                                   String... lines) {
        final StringBuilder builder = new StringBuilder();
        boolean first = true;
        for (final String line : lines) {
            if (first) {
                first = false;
            } else {
                builder.append(eol);
            }
            builder.append(line);
        }
        return builder.toString();
    }

    private static List<String> toList(String... lines) {
        final List<String> result = new ArrayList<>();

        for (final String line : lines) {
            result.add(line);
        }
        // Remove last line if it is empty
        if (!result.isEmpty() && result.get(result.size() - 1).isEmpty()) {
            result.remove(result.size() - 1);
        }
        return result;
    }

    private static class Handler implements LinesHandler {
        public List<String> lines = new ArrayList<>();

        public Handler() {
            super();
        }

        @Override
        public void processBegin() {
            // Ignore
            LOGGER.debug("processBegin()");
        }

        @Override
        public Evaluation processLine(String line,
                                      int number) {
            LOGGER.debug("processLine({}, {})", line, number);
            lines.add(line);
            return Evaluation.CONTINUE;
        }

        @Override
        public void processEnd() {
            // Ignore
            LOGGER.debug("processEnd()");
        }
    }

    private static void testParseString(String eol,
                                        int expectedLines,
                                        String... lines) throws Exception {
        LOGGER.debug("testParse({}, {} lines)", toNames(eol), lines.length);
        for (final String line : lines) {
            LOGGER.debug("   '{}' {}", line, toNames(line));
        }

        final String s = toString(eol, lines);
        LOGGER.debug("s: {}", toNames(s));
        final StringReader reader = new StringReader(s);
        final Handler handler = new Handler();
        LinesParser.parse(reader, handler);
        assertSame(expectedLines, handler.lines.size());
        assertEquals(toList(lines), handler.lines);
    }

    private static void testParseText(int expectedLines,
                                      String s) throws Exception {
        final StringReader reader = new StringReader(s);
        final Handler handler = new Handler();
        LinesParser.parse(reader, handler);
        assertSame(expectedLines, handler.lines.size());
    }

    @Test
    void testParseString() throws Exception {
        for (final String eol : EOLS) {
            testParseString(eol, 0);
            testParseString(eol,
                            1,
                            "l1");
            testParseString(eol,
                            0,
                            "");
            testParseString(eol,
                            2,
                            "l1",
                            "l2");
            testParseString(eol,
                            2,
                            "",
                            "l2");
            testParseString(eol,
                            3,
                            "l1",
                            "l2",
                            "l3");
            testParseString(eol,
                            3,
                            "",
                            "l2",
                            "l3");
            testParseString(eol,
                            3,
                            "",
                            "",
                            "l3");
            testParseString(eol,
                            2,
                            "",
                            "",
                            "");
        }
    }

    private void testParseFile(File file,
                               Charset charset) throws Exception {
        LOGGER.debug("testParseFile({}, {})", file, charset);

        final Handler handler = new Handler();
        LinesParser.parse(file, charset, handler);
        assertEquals(lines, handler.lines);
    }

    @Test
    void testParseFile() throws Exception {
        testParseFile(filePlatform, null);
        // testParseFile(fileUtf8, "UTF-8");
        // testParseFile(fileUtf16, "UTF-16");
    }

    @Test
    void testParseText() throws Exception {
        testParseText(0, "");
        testParseText(1, "\n");
        testParseText(1, "aa");
        testParseText(1, "aa\n");
        testParseText(2, "aa\na");
        testParseText(2, "aa\n\n");
        testParseText(3, "aa\n\n\n");
    }
}