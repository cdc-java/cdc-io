package cdc.io.txt;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;

import org.junit.jupiter.api.Test;

import cdc.util.lang.CollectionUtils;

class LinesHandlerTest {
    private static void test(AbstractLinesFilter filter,
                             List<String> lines,
                             List<String> expected) throws IOException {
        final StringBuilder builder = new StringBuilder();
        boolean first = true;
        for (final String line : lines) {
            if (first) {
                first = false;
            } else {
                builder.append("\n");
            }
            builder.append(line);
        }
        final StringReader reader = new StringReader(builder.toString());
        LinesParser.parse(reader, filter);
        final MemoryLinesHandler mem = (MemoryLinesHandler) filter.getDelegate();
        assertEquals(expected, mem.getLines());
    }

    @Test
    void testSort() throws IOException {
        test(LinesHandler.sorter(new MemoryLinesHandler()),
             CollectionUtils.toList("A", "B", "A", "C"),
             CollectionUtils.toList("A", "A", "B", "C"));
    }

    @Test
    void testUniq() throws IOException {
        test(LinesHandler.uniq(new MemoryLinesHandler()),
             CollectionUtils.toList("A", "B", "A", "C"),
             CollectionUtils.toList("A", "B", "A", "C"));

        test(LinesHandler.uniq(new MemoryLinesHandler()),
             CollectionUtils.toList("A", "A", "B", "C"),
             CollectionUtils.toList("A", "B", "C"));

        test(LinesHandler.uniq(new MemoryLinesHandler()),
             CollectionUtils.toList("A", "A", "B", "B"),
             CollectionUtils.toList("A", "B"));
    }

    @Test
    void testFilter() throws IOException {
        test(LinesHandler.filter(new MemoryLinesHandler(),
                                 (s,
                                  n) -> true),
             CollectionUtils.toList("A", "B", "C", "D"),
             CollectionUtils.toList("A", "B", "C", "D"));

        test(LinesHandler.filter(new MemoryLinesHandler(),
                                 (s,
                                  n) -> false),
             CollectionUtils.toList("A", "B", "C", "D"),
             CollectionUtils.toList());

        test(LinesHandler.filter(new MemoryLinesHandler(),
                                 (s,
                                  n) -> n % 2 == 0),
             CollectionUtils.toList("A", "B", "C", "D"),
             CollectionUtils.toList("B", "D"));
    }

    @Test
    void testConverter() throws IOException {
        test(LinesHandler.converter(new MemoryLinesHandler(),
                                    String::toLowerCase),
             CollectionUtils.toList("A", "B", "C", "D"),
             CollectionUtils.toList("a", "b", "c", "d"));
    }

    @Test
    void testHeader() throws IOException {
        test(LinesHandler.header(new MemoryLinesHandler(), 0),
             CollectionUtils.toList("A", "B", "C", "D"),
             CollectionUtils.toList());

        test(LinesHandler.header(new MemoryLinesHandler(), 1),
             CollectionUtils.toList("A", "B", "C", "D"),
             CollectionUtils.toList("A"));

        test(LinesHandler.header(new MemoryLinesHandler(), 2),
             CollectionUtils.toList("A", "B", "C", "D"),
             CollectionUtils.toList("A", "B"));

        test(LinesHandler.header(new MemoryLinesHandler(), 3),
             CollectionUtils.toList("A", "B", "C", "D"),
             CollectionUtils.toList("A", "B", "C"));

        test(LinesHandler.header(new MemoryLinesHandler(), 4),
             CollectionUtils.toList("A", "B", "C", "D"),
             CollectionUtils.toList("A", "B", "C", "D"));

        test(LinesHandler.header(new MemoryLinesHandler(), 5),
             CollectionUtils.toList("A", "B", "C", "D"),
             CollectionUtils.toList("A", "B", "C", "D"));
    }

    @Test
    void testFooter() throws IOException {
        test(LinesHandler.footer(new MemoryLinesHandler(), 0),
             CollectionUtils.toList("A", "B", "C", "D"),
             CollectionUtils.toList());

        test(LinesHandler.footer(new MemoryLinesHandler(), 1),
             CollectionUtils.toList("A", "B", "C", "D"),
             CollectionUtils.toList("D"));

        test(LinesHandler.footer(new MemoryLinesHandler(), 2),
             CollectionUtils.toList("A", "B", "C", "D"),
             CollectionUtils.toList("C", "D"));

        test(LinesHandler.footer(new MemoryLinesHandler(), 3),
             CollectionUtils.toList("A", "B", "C", "D"),
             CollectionUtils.toList("B", "C", "D"));

        test(LinesHandler.footer(new MemoryLinesHandler(), 4),
             CollectionUtils.toList("A", "B", "C", "D"),
             CollectionUtils.toList("A", "B", "C", "D"));

        test(LinesHandler.footer(new MemoryLinesHandler(), 5),
             CollectionUtils.toList("A", "B", "C", "D"),
             CollectionUtils.toList("A", "B", "C", "D"));

        test(LinesHandler.footer(new MemoryLinesHandler(), 0),
             CollectionUtils.toList("A"),
             CollectionUtils.toList());

        test(LinesHandler.footer(new MemoryLinesHandler(), 0),
             CollectionUtils.toList(),
             CollectionUtils.toList());

        test(LinesHandler.footer(new MemoryLinesHandler(), 1),
             CollectionUtils.toList(),
             CollectionUtils.toList());
    }
}