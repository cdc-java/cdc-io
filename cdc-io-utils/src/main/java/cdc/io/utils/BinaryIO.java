package cdc.io.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class BinaryIO {
    private static final Logger LOGGER = LogManager.getLogger(BinaryIO.class);

    private BinaryIO() {
    }

    public static <E extends Enum<E>> void writeEnumAsByte(E value,
                                                           DataOutput out) throws IOException {
        if (value == null) {
            out.writeByte((byte) -1);
        } else {
            out.writeByte((byte) value.ordinal());
        }
    }

    public static <E extends Enum<E>> E readByteAsEnum(Class<E> klass,
                                                       DataInput in) throws IOException {
        final int ordinal = in.readByte();
        if (ordinal == -1) {
            return null;
        } else {
            return klass.getEnumConstants()[ordinal];
        }
    }

    public static <E extends Enum<E>> E readByteAsEnum(E[] values,
                                                       DataInput in) throws IOException {
        final int ordinal = in.readByte();
        if (ordinal == -1) {
            return null;
        } else {
            return values[ordinal];
        }
    }

    public static <E extends Enum<E>> void writeEnumAsShort(E value,
                                                            DataOutput out) throws IOException {
        if (value == null) {
            out.writeShort((short) -1);
        } else {
            out.writeShort((short) value.ordinal());
        }
    }

    public static <E extends Enum<E>> E readShortAsEnum(Class<E> klass,
                                                        DataInput in) throws IOException {
        final int ordinal = in.readShort();
        if (ordinal == -1) {
            return null;
        } else {
            return klass.getEnumConstants()[ordinal];
        }
    }

    public static <E extends Enum<E>> E readShortAsEnum(E[] values,
                                                        DataInput in) throws IOException {
        final int ordinal = in.readShort();
        if (ordinal == -1) {
            return null;
        } else {
            return values[ordinal];
        }
    }

    public static <E extends Enum<E>> void writeEnumAsInt(E value,
                                                          DataOutput out) throws IOException {
        if (value == null) {
            out.writeInt(-1);
        } else {
            out.writeInt(value.ordinal());
        }
    }

    public static <E extends Enum<E>> E readIntAsEnum(Class<E> klass,
                                                      DataInput in) throws IOException {
        final int ordinal = in.readInt();
        if (ordinal == -1) {
            return null;
        } else {
            return klass.getEnumConstants()[ordinal];
        }
    }

    public static <E extends Enum<E>> E readIntAsEnum(E[] values,
                                                      DataInput in) throws IOException {
        final int ordinal = in.readInt();
        if (ordinal == -1) {
            return null;
        } else {
            return values[ordinal];
        }
    }

    public static void saveSerialized(Object object,
                                      String filename) throws IOException {
        try (final FileOutputStream fos = new FileOutputStream(filename);
                final BufferedOutputStream bos = new BufferedOutputStream(fos);
                final ObjectOutputStream oos = new ObjectOutputStream(bos)) {
            oos.writeObject(object);
            oos.flush();
        } catch (final IOException e) {
            LOGGER.catching(e);
            throw e;
        }
    }

    public static Object loadSerialized(String filename) throws IOException, ClassNotFoundException {
        try (final FileInputStream fis = new FileInputStream(filename);
                final BufferedInputStream bis = new BufferedInputStream(fis);
                final ObjectInputStream ois = new ObjectInputStream(bis)) {
            return ois.readObject();
        } catch (final ClassNotFoundException | IOException e) {
            LOGGER.catching(e);
            throw e;
        }
    }

    public static <T> T loadSerialized(String filename,
                                       Class<T> klass) throws IOException, ClassNotFoundException {
        return klass.cast(loadSerialized(filename));
    }
}