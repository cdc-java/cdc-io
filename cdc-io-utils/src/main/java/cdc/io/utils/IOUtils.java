package cdc.io.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

public final class IOUtils {
    private IOUtils() {
    }

    /**
     * Wraps or converts an OutputStream into a BufferedOutputStream.
     *
     * @param out The output stream.
     * @return {@code out} wrapped into or converted to a BufferedOutputStream.
     */
    public static BufferedOutputStream toBuffered(OutputStream out) {
        return out instanceof BufferedOutputStream
                ? (BufferedOutputStream) out
                : new BufferedOutputStream(out);
    }

    /**
     * Wraps or converts an OutputStream into a BufferedOutputStream.
     *
     * @param out The output stream.
     * @param size The buffer size, if created.
     * @return {@code out} wrapped into or converted to a BufferedOutputStream.
     */
    public static BufferedOutputStream toBuffered(OutputStream out,
                                                  int size) {
        return out instanceof BufferedOutputStream
                ? (BufferedOutputStream) out
                : new BufferedOutputStream(out, size);
    }

    /**
     * Wraps or converts an InputStream into a BufferedInputStream.
     *
     * @param in The input stream.
     * @return {@code in} wrapped into or converted to a BufferedInputStream.
     */
    public static BufferedInputStream toBuffered(InputStream in) {
        return in instanceof BufferedInputStream
                ? (BufferedInputStream) in
                : new BufferedInputStream(in);
    }

    /**
     * Wraps or converts an InputStream into a BufferedInputStream.
     *
     * @param in The input stream.
     * @param size The buffer size, if created.
     * @return {@code in} wrapped into or converted to a BufferedInputStream.
     */
    public static BufferedInputStream toBuffered(InputStream in,
                                                 int size) {
        return in instanceof BufferedInputStream
                ? (BufferedInputStream) in
                : new BufferedInputStream(in, size);
    }
}