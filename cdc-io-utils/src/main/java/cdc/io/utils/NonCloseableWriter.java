package cdc.io.utils;

import java.io.IOException;
import java.io.Writer;

/**
 * Writer wrapper that doesn't close the underlying Writer.
 *
 * @author Damien Carbonne
 */
public class NonCloseableWriter extends Writer {
    private final Writer delegate;

    public NonCloseableWriter(Writer delegate) {
        this.delegate = delegate;
    }

    @Override
    public void write(int c) throws IOException {
        delegate.write(c);
    }

    @Override
    public void write(char[] cbuf) throws IOException {
        delegate.write(cbuf);
    }

    @Override
    public void write(char[] cbuf,
                      int off,
                      int len) throws IOException {
        delegate.write(cbuf, off, len);
    }

    @Override
    public void write(String str) throws IOException {
        delegate.write(str);
    }

    @Override
    public void write(String str,
                      int off,
                      int len) throws IOException {
        delegate.write(str, off, len);
    }

    @Override
    public NonCloseableWriter append(char c) throws IOException {
        delegate.append(c);
        return this;
    }

    @Override
    public NonCloseableWriter append(CharSequence csq) throws IOException {
        delegate.append(csq);
        return this;
    }

    @Override
    public NonCloseableWriter append(CharSequence csq,
                                     int start,
                                     int end) throws IOException {
        delegate.append(csq, start, end);
        return this;
    }

    @Override
    public void flush() throws IOException {
        delegate.flush();
    }

    @Override
    public void close() throws IOException {
        // Ignore
    }
}