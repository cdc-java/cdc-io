package cdc.io.utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

import cdc.util.events.ProgressController;
import cdc.util.events.ProgressSupplier;
import cdc.util.lang.Checks;

public class ProgressControllerInputStream extends FilterInputStream {
    private final ProgressSupplier supplier;

    public ProgressControllerInputStream(InputStream in,
                                         ProgressController controller,
                                         long total) {
        super(in);
        this.supplier = new ProgressSupplier(controller);
        this.supplier.setTotal(total);
    }

    public ProgressControllerInputStream(File file,
                                         ProgressController controller)
            throws IOException {
        this(new BufferedInputStream(new FileInputStream(Checks.isNotNull(file, "file"))),
             controller,
             file.length());
    }

    public ProgressControllerInputStream(String filename,
                                         ProgressController controller)
            throws IOException {
        this(new File(filename),
             controller);
    }

    @Override
    public int read() throws IOException {
        final int c = in.read();
        if (c >= 0) {
            supplier.incrementValue();
        }
        supplier.checkCancelled();
        return c;
    }

    @Override
    public int read(byte[] b) throws IOException {
        final int n = in.read(b);
        if (n > 0) {
            supplier.incrementValue(n);
        }
        supplier.checkCancelled();
        return n;
    }

    @Override
    public int read(byte[] b,
                    int off,
                    int len) throws IOException {
        final int n = in.read(b, off, len);
        if (n > 0) {
            supplier.incrementValue(n);
        }
        supplier.checkCancelled();
        return n;
    }

    @Override
    public long skip(long n) throws IOException {
        final long skipped = in.skip(n);
        if (skipped > 0) {
            supplier.incrementValue(skipped);
        }
        supplier.checkCancelled();
        return skipped;
    }
}