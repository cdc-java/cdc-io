package cdc.io.utils;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Output stream wrapper that does't close the underlying stream.
 * <p>
 * This is useful with System.out or System.err.
 *
 * @author Damien Carbonne
 */
public class NonCloseableOutputStream extends FilterOutputStream {
    public static final NonCloseableOutputStream NON_CLOSABLE_SYSTEM_OUT = new NonCloseableOutputStream(System.out);
    public static final NonCloseableOutputStream NON_CLOSABLE_SYSTEM_ERR = new NonCloseableOutputStream(System.err);

    public NonCloseableOutputStream(OutputStream out) {
        super(out);
    }

    /**
     * Simply flushes the underlying output stream, without closing it.
     *
     * @throws IOException When an I/O error occurs.
     */
    @Override
    public void close() throws IOException {
        out.flush();
    }

    @Override
    public void write(byte[] b,
                      int off,
                      int len) throws IOException {
        out.write(b, off, len);
    }
}