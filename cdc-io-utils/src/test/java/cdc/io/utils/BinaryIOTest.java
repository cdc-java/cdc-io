package cdc.io.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;

import org.junit.jupiter.api.Test;

class BinaryIOTest {

    void testSaveLoad(Object object,
                      String filename) throws IOException, ClassNotFoundException {
        BinaryIO.saveSerialized(object, filename);
        final Object back = BinaryIO.loadSerialized(filename);
        assertEquals(object, back);

    }

    @Test
    void testSaveLoad() throws ClassNotFoundException, IOException {
        testSaveLoad("Hello world", "test1.dat");
    }
}