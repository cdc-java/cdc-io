# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [Unreleased]
### Added
- Added the `--dissolve` option to `XmlNormalizer`. #13

### Changed
- Updated dependencies:
    - org.junit-5.12.0


## [0.53.2] - 2025-01-11
### Fixed
- Use '/' as file separator when creating archive entries.
  Platform file separator used to work, but some changes made 7-zip fail when using `\\` on Windows. #11


## [0.53.1] - 2025-01-03
### Fixed
- Fixed a recursion bug in `LineParser.parse(in, systemeId, handler)`.


## [0.53.0] - 2025-01-03
### Changed
- Updated dependencies:
    - cdc-utils-0.54.0
    - commons-io-2.18.0
    - jackson-2.18.2
    - org.apache.log4j-2.24.3
    - org.junit-5.11.4

### Removed
- Removed code that was deprecated in 2023.


## [0.52.1] - 2024-09-28
### Changed
- Updated dependencies:
    - cdc-utils-0.53.0
    - commons-cli-1.9.0
    - commons-compress-1.27.1
    - commons-io-2.17.0
    - jackson-2.18.0
    - org.junit-5.11.1
    - saxon-12.5


## [0.52.0] - 2024-05-18
### Added
- Created `XmlDataReader.Builder`.

### Changed
- Updated dependencies:
    - cdc-utils-0.52.1
    - commons-cli-1.7.0
    - commons-io-2.16.1
    - jackson-2.17.1
- Updated maven plugins.

### Deprecated
- Deprecated public constructors of, and methods that modify `XmlDataReader`.  
  One should now use `XmlDataRedader.Builder`.


## [0.51.2] - 2024-03-23
### Changed
- Updated dependencies:
    - jackson-2.17.0
    - org.apache.log4j-2.23.1


## [0.51.1] - 2024-03-10
### Changed
- Updated dependencies:
    - org.junit-5.10.2

### Security
- Updated dependencies:
    - commons-compress-1.26.1


## [0.51.0] - 2024-01-01
### Changed
- Updated dependencies:
    - cdc-utils-0.52.0
    - jackson-2.16.1
    - saxon-12.4
- Updated maven plugins.


## [0.50.0] - 2023-11-25
### Changed
- Moved to Java 17
- Updated dependencies:
    - cdc-utils-0.50.0


## [0.27.3] - 2023-11-18
### Changed
- Updated dependencies:
    - cdc-utils-0.33.2
    - commons-compress-1.25.0
    - jackson-2.16.0
    - org.junit-5.10.1


## [0.27.2] - 2023-10-21
### Changed
- Updated dependencies:
    - cdc-utils-0.33.1
    - jackson-2.15.3


## [0.27.1] - 2023-10-01
### Changed
- Updated dependencies:
    - commons-compress-1.24.0
    - org.junit-5.10.0
- Updated maven plugins.
- Added maven enforcer plugin.
- Used `java.version` and `maven.version` properties in pom. 


## [0.27.0] - 2023-07-15
### Added
- Added new methods to `Parent` to access relative element using a `Path`.
- Added `Part.of(String)` and `Path.of(String)`.

### Changed
- Updated dependencies:
    - cdc-utils-0.33.0
    - saxon-12.3

### Deprecated
- Deprecated `Part.getName()` and `Path.getName()`. They are replaced by `Part.getText()` and `Path.getText()`.


## [0.26.2] - 2023-06-11
### Changed
- Updated dependencies:
    - jackson-2.15.2
    - saxon-12.2


## [0.26.1] - 2023-05-01
### Changed
- Updated dependencies:
    - cdc-utils-0.32.0
    - jackson-2.15.0
    - org.junit-5.9.3


## [0.26.0] - 2023-03-23
### Added
- Re-added DTD code that was added by #8 and removed.  
  It may be useful for ASD code.

### Changed
- `XmlWriter.write(String)` is now public for specific needs.
- Updated dependencies:
    - commons-compress-1.23.0
    - saxon-12.1

### Fixed
- Fixed a NPE in `Element.removeAttributes()`.


## [0.25.1] - 2023-02-25
### Changed
- Updated dependencies:
    - cdc-util-0.31.0
    - jackson-2.14.2
    - org.apache.log4j-2.20.0


## [0.25.0] - 2023-01-29
### Removed
- Removed most DTD code introduced by #8  
  Releasing this code was an error.


## [0.24.0] - 2023-01-28
### Added
- Added the ability to write a DTD to `XmlWriter`. The construction of the DTD is left to the caller. #8
- Added DTD to `Document`, `XmlDataWriter` and `XmlDataReader`in StAX mode. #8
- Added partial DTD to `XmlDataReader`in SAX mode. This use is discouraged. There are some incompatibilities. #8
- Added writing of unescaped attribute value and element content to `XmlWriter`.

### Changed
- Updated dependencies:
    - cdc-util-0.29.0
    - org.junit-5.9.2
    - saxon-12.0


## [0.23.2] - 2023-02-02
### Changed
- Updated dependencies:
    - cdc-util-0.28.2
    - jackson-2.14.1


## [0.23.1] - 2022-11-08
### Changed
- Updated dependencies:
    - cdc-util-0.28.1
    - commons-compress-1.22
    - jackson-2.14.0
    - org.apache.log4j-2.19.0
    - org.junit-5.9.1


## [0.23.0] - 2022-08-23
### Changed
- `AbstractStAXParser.getAttributeValue(String)` now fails when the attribute is missing.
- Updated dependencies:
    - cdc-util-0.28.0
    - org.junit-5.9.0
    - saxon-11.4


## [0.22.0] - 2022-07-07
### Added
- Created  `replacer` function in `LinesHandler`.

### Changed
- Renamed `LineParser` to `LinesParser`. #6
- Moved construction functions from `AbstractLinesFilter` to `LinesHandler`.
- Used `LinePredicate` instead of `BiPredicate<String Integer>`.
- Updated dependencies:
    - cdc-util-0.27.0
    - org.apache.log4j-2.18.0
    - org.junit-5.9.0-RC1


## [0.21.3] - 2022-06-18
### Changed
- Updated dependencies:
    - cdc-util-0.26.0


## [0.21.2] - 2022-05-19
### Changed
- Updated maven plugins
- Updated dependencies:
    - cdc-util-0.25.0
    - jackson-2.13.3
    - saxon-11.3


## [0.21.1] - 2022-03-11
### Changed
- Updated dependencies:
    - cdc-util-0.23.0
    - jackson-2.13.2
    - org.apache.log4j-2.17.2
    - saxon-11.2
- `Config` data is now retrieved from Manifest.


## [0.21.0] - 2022-02-13
### Added 
- Created `NonCloseableWriter`.

### Changed
- Updated dependencies:
    - cdc-util-0.20.0


## [0.20.0] - 2022-02-04
### Changed
- Updated maven plugins
- Upgraded to Java 11
- Updated dependencies:
    - saxon-11.1.1


## [0.13.2] - 2022-01-03
### Security
- Updated dependencies:
    - cdc-util-0.14.2
    - org.apache.log4j-2.17.1 #4


## [0.13.1] - 2021-12-28
### Security
- Updated dependencies:
    - cdc-util-0.14.1
    - jackson-2.13.1
    - org.apache.log4j-2.17.0 #4


## [0.13.0] - 2021-12-14
### Fixed
- Fixed warnings.

### Security
- Updated dependencies:
    - cdc-util-0.14.0
    - org.apache.log4j-2.16.0 #4


## [0.12.0] - 2021-10-02
### Added
- Added `AbstractJsonParser.skipToEnd`.


## [0.11.0] - 2021-07-23
### Added
- Created JsonException and JsonParserWrapper.
- Added new helper methods to AbstractJsonParser.
- Added traverseDepthFirst methods to DataUtils. #2

### Changed
- Used Charset instead of String in LineParser.
- Added systemId parameter to LineParser. 


## [0.10.1] - 2021-06-05
### Added
- **AbstractLinesFilter** and some standard filters.
- It is now possible to configure **XmlWriter** to replace non printable characters
  by a user defined string. #1
- **html** module was created.


## [0.10.0] - 2021-05-03
### Added
- First release, extracted from [cdc-utils](https://gitlab.com/cdc-java/cdc-util). cdc-java/cdc-util#43