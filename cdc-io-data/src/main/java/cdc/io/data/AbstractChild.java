package cdc.io.data;

/**
 * Base class of nodes that are children (they have a parent node).
 *
 * @author Damien Carbonne
 *
 */
abstract class AbstractChild implements Child {
    private Parent parent;

    protected AbstractChild() {
        super();
    }

    @Override
    public abstract AbstractChild clone(boolean recurse);

    @Override
    public final Document getDocument() {
        final Element root = getRootElement();
        if (root != null) {
            return (Document) root.getParent();
        } else {
            return (Document) getParent();
        }
    }

    @Override
    public final Element getRootElement() {
        final Node root = getRootChild();
        if (root.getType() == NodeType.DOCUMENT) {
            // Root is a document
            return ((Document) root).getRootElement();
        } else if (root.getType() == NodeType.ELEMENT) {
            // Root is an element
            return (Element) root;
        } else {
            // Root is either a comment or a text.
            return null;
        }
    }

    @Override
    public final Parent getParent() {
        return parent;
    }

    protected final void resetParent() {
        parent = null;
    }

    @Override
    public AbstractChild setParent(Parent parent) {
        if (parent == this.parent) {
            return this;
        } else if (parent == null || parent.canAddChild(this)) {
            // We always allow setting a null parent
            // Detach current parent
            if (this.parent != null) {
                this.parent.getModifiableChildren().remove(this);
                this.parent = null;
            }
            // Attach new parent
            if (parent != null) {
                parent.getModifiableChildren().add(this);
                this.parent = parent;
            }
            return this;
        } else {
            throw new IllegalStateException("Can not set parent. child: " + getType() + " parent: " + parent.getType());
        }
    }

    @Override
    public String toString() {
        return getType().name();
    }

    @Override
    public final int hashCode() {
        return super.hashCode();
    }

    @Override
    public final boolean equals(Object object) {
        return this == object;
    }
}