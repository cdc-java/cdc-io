package cdc.io.data;

/**
 * Enumeration of possible node types.
 *
 * @author Damien Carbonne
 *
 */
public enum NodeType {
    /** The node is a document (it is a {@link Parent}). */
    DOCUMENT,
    /** The node is a comment (it is a {@link Child} and a {@link Leaf}). */
    COMMENT,
    /** The node is an element (with attributes, it is a {@link Child} and a {@link Parent}). */
    ELEMENT,
    /** The node is a text content (it is a {@link Child} and a {@link Leaf}). */
    TEXT
}