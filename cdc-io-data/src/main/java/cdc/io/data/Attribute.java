package cdc.io.data;

import java.util.Comparator;
import java.util.Set;
import java.util.function.Predicate;

import cdc.util.lang.Checks;
import cdc.util.lang.CollectionUtils;
import cdc.util.lang.Operators;
import cdc.util.strings.StringConversion;

/**
 * XML like attribute ([name, value] pair).
 * <p>
 * An attribute name can not be changed after creation. Its value can be modified.
 *
 * @author Damien Carbonne
 *
 */
public final class Attribute {
    private final String name;
    private String value;

    /**
     * A Comparator of attributes that compares names.
     */
    public static final Comparator<Attribute> NAME_COMPARATOR = (o1,
                                                                 o2) -> o1.name.compareTo(o2.name);

    public Attribute(String name) {
        Checks.isNotNull(name, "name");
        this.name = name;
    }

    public Attribute(Attribute other) {
        this(other.name,
             other.value);
    }

    public Attribute(String name,
                     Object value) {
        this(name);
        setValue(value);
    }

    public <E extends Enum<E>> Attribute(String name,
                                         E value) {
        this(name);
        setValue(value);
    }

    public Attribute(String name,
                     String value) {
        this(name);
        setValue(value);
    }

    public Attribute(String name,
                     boolean value) {
        this(name);
        setValue(value);
    }

    public Attribute(String name,
                     long value) {
        this(name);
        setValue(value);
    }

    public Attribute(String name,
                     int value) {
        this(name);
        setValue(value);
    }

    public Attribute(String name,
                     short value) {
        this(name);
        setValue(value);
    }

    public Attribute(String name,
                     byte value) {
        this(name);
        setValue(value);
    }

    public Attribute(String name,
                     double value) {
        this(name);
        setValue(value);
    }

    public Attribute(String name,
                     float value) {
        this(name);
        setValue(value);
    }

    public void setValue(Object value) {
        this.value = value == null ? null : value.toString();
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setValue(boolean value) {
        this.value = StringConversion.asString(value);
    }

    public void setValue(long value) {
        this.value = StringConversion.asString(value);
    }

    public void setValue(int value) {
        this.value = StringConversion.asString(value);
    }

    public void setValue(short value) {
        this.value = StringConversion.asString(value);
    }

    public void setValue(byte value) {
        this.value = StringConversion.asString(value);
    }

    public void setValue(double value) {
        this.value = StringConversion.asString(value);
    }

    public void setValue(float value) {
        this.value = StringConversion.asString(value);
    }

    public <E extends Enum<E>> void setValue(E value) {
        this.value = StringConversion.asString(value);
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    public boolean getValueAsBoolean() {
        return Boolean.parseBoolean(value);
    }

    public long getValueAsLong() {
        return Long.parseLong(value);
    }

    public int getValueAsInt() {
        return Integer.parseInt(value);
    }

    public short getValueAsShort() {
        return Short.parseShort(value);
    }

    public byte getValueAsByte() {
        return Byte.parseByte(value);
    }

    public double getValueAsDouble() {
        return Double.parseDouble(value);
    }

    public float getValueAsFloat() {
        return Float.parseFloat(value);
    }

    public <E extends Enum<E>> E getValueAsEnum(Class<E> enumClass) {
        return StringConversion.asEnum(value, enumClass);
    }

    @Override
    public int hashCode() {
        return name.hashCode()
                + Operators.hashCode(value);
    }

    @Override
    public boolean equals(Object object) {
        if (object == this) {
            return true;
        }
        if (!(object instanceof Attribute)) {
            return false;
        }
        final Attribute other = (Attribute) object;
        return name.equals(other.name)
                && Operators.equals(value, other.value);
    }

    @Override
    public String toString() {
        return name + "=" + value;
    }

    /**
     * Creates a Predicate that accepts Attributes with a given name.
     *
     * @param name The name.
     * @return A new Predicate that accepts Attributes that are named {@code name}.
     */
    public static Predicate<Attribute> named(String name) {
        return att -> att.getName().equals(name);
    }

    /**
     * Creates a Predicate that accepts Attributes with a name belonging to a set.
     *
     * @param names The names.
     * @return A new Predicate that accepts Attributes whose name belongs to {@code names}.
     */
    public static Predicate<Attribute> named(Set<String> names) {
        return att -> names.contains(att.getName());
    }

    /**
     * Creates a Predicate that accepts Attributes with a name belonging to an array.
     *
     * @param names The names.
     * @return A new Predicate that accepts Attributes whose name belongs to {@code names}.
     */
    public static Predicate<Attribute> named(String... names) {
        return named(CollectionUtils.toSet(names));
    }
}