package cdc.io.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.io.data.paths.SPath;
import cdc.util.function.IterableUtils;
import cdc.util.lang.Checks;
import cdc.util.lang.CollectionUtils;
import cdc.util.lang.FailureReaction;
import cdc.util.lang.NotFoundException;
import cdc.util.lang.Operators;
import cdc.util.lang.UnexpectedValueException;
import cdc.util.strings.StringConversion;

/**
 * XML like Element node.
 * <p>
 * It has a name, can have children (elements, comments or texts).<br>
 * It can also have attributes.
 *
 * @author Damien Carbonne
 *
 */
public final class Element extends AbstractChild implements Parent {
    private static final Logger LOGGER = LogManager.getLogger(Element.class);
    private String name;
    private List<Attribute> attributes = null;
    private List<Child> children = null;

    /**
     * A comparator of elements using their names.
     */
    public static final Comparator<Element> NAME_COMPARATOR = (o1,
                                                               o2) -> {
        final String name1 = o1 == null ? null : o1.getName();
        final String name2 = o2 == null ? null : o2.getName();
        return Operators.compare(name1, name2);
    };

    /**
     * Returns a comparator that compares the values of a particular attribute.
     *
     * @param attributeName The name of the attribute to use.
     * @return A comparator comparing the value of attributes named {@code attributeName}.
     */
    public static Comparator<Element> compareAttribute(String attributeName) {
        return (o1,
                o2) -> {
            final String value1 = o1 == null ? null : o1.getAttributeValue(attributeName, null, FailureReaction.WARN);
            final String value2 = o2 == null ? null : o2.getAttributeValue(attributeName, null, FailureReaction.WARN);
            return Operators.compare(value1, value2);
        };
    }

    /**
     * Returns a comparator that compares the names then the values of a particular attribute.
     *
     * @param attributeName The name of the attribute to use.
     * @return A comparator that compares the names then the values of attributes named {@code attributeName}.
     */
    public static Comparator<Element> compareNameAndAttribute(String attributeName) {
        return (o1,
                o2) -> {
            final String name1 = o1 == null ? null : o1.getName();
            final String name2 = o2 == null ? null : o2.getName();
            final int nameCmp = Operators.compare(name1, name2);
            if (nameCmp == 0) {
                final String value1 = o1.getAttributeValue(attributeName, null, FailureReaction.WARN);
                final String value2 = o2.getAttributeValue(attributeName, null, FailureReaction.WARN);
                return Operators.compare(value1, value2);
            } else {
                return nameCmp;
            }
        };
    }

    /**
     * Creates an element.
     *
     * @param name The element name.
     * @throws IllegalArgumentException When {@code name} is invalid.
     */
    public Element(String name) {
        setName(name);
    }

    /**
     * Creates an element.
     *
     * @param parent The element's parent.
     * @param name The element name.
     * @throws IllegalArgumentException When {@code name} is invalid.
     * @throws IllegalStateException When {@code parent} can not not be set.
     */
    public Element(Parent parent,
                   String name) {
        setName(name);
        setParent(parent);
    }

    /**
     * Creates an element by cloning another one.
     * <p>
     * Cloning is shallow.<br>
     * For deep cloning, use {@link #clone(boolean)}.
     *
     * @param other The element to clone.
     */
    public Element(Element other) {
        this(null, other.getName());
        for (int index = 0; index < other.getAttributesCount(); index++) {
            addAttribute(new Attribute(other.getAttributeAt(index)));
        }
    }

    @Override
    public NodeType getType() {
        return NodeType.ELEMENT;
    }

    @Override
    public Element clone(boolean recurse) {
        final Element result = new Element(this);
        if (recurse) {
            for (final Child child : getChildren()) {
                result.addChild(child.clone(true));
            }
        }
        return result;
    }

    private boolean localEquals(Element element) {
        if (!Operators.equals(name, element.name)
                || getAttributesCount() != element.getAttributesCount()) {
            return false;
        }

        // Same name
        // Same number of attributes: need to do a deep compare of attributes
        if (getAttributesCount() != 0) {
            final List<Attribute> atts0 = new ArrayList<>(attributes);
            final List<Attribute> atts1 = new ArrayList<>(element.attributes);
            atts0.sort(Attribute.NAME_COMPARATOR);
            atts1.sort(Attribute.NAME_COMPARATOR);
            return atts0.equals(atts1);
        } else {
            return true;
        }
    }

    @Override
    public boolean deepEquals(Node node) {
        if (this == node) {
            return true;
        }
        if (!(node instanceof Element)) {
            return false;
        }
        final Element other = (Element) node;
        if (getChildrenCount() == other.getChildrenCount() && localEquals(other)) {
            for (int index = 0; index < getChildrenCount(); index++) {
                if (!getChildAt(index).deepEquals(other.getChildAt(index))) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String getQName() {
        if (isRoot()) {
            return "/" + getName();
        } else {
            return getParent().getQName() + "/" + getName();
        }
    }

    @Override
    public boolean canAddChild(Child child) {
        if (child == null) {
            return false;
        } else if (child.getType() == NodeType.ELEMENT) {
            // TODO check recursion
            return true;
        } else {
            return true;
        }
    }

    @Override
    public boolean hasChildren() {
        return children != null && !children.isEmpty();
    }

    @Override
    public List<Child> getModifiableChildren() {
        if (children == null) {
            children = new ArrayList<>();
        }
        return children;
    }

    @Override
    public List<Child> getChildren() {
        return children == null ? Collections.emptyList() : children;
    }

    /**
     * Sets the name of this element.
     *
     * @param name The name.
     * @throws IllegalArgumentException When {@code name} is invalid.
     */
    public void setName(String name) {
        Checks.isNotNullOrEmpty(name, "name");
        this.name = name;
    }

    /**
     * @return the name of this element.
     */
    public String getName() {
        return name;
    }

    /**
     * @return The content type of this element.
     */
    public ElementContentType getContentType() {
        if (children == null || children.isEmpty()) {
            return ElementContentType.EMPTY;
        } else {
            boolean hasText = false;
            boolean hasNonText = false;
            for (final Child child : children) {
                switch (child.getType()) {
                case COMMENT:
                case ELEMENT:
                    hasNonText = true;
                    break;
                case TEXT:
                    hasText = true;
                    break;
                default:
                    throw new UnexpectedValueException(child.getType());
                }
            }
            if (hasNonText) {
                if (hasText) {
                    return ElementContentType.MIXED;
                } else {
                    return ElementContentType.NON_TEXT;
                }
            } else {
                // It has necessarily only text children
                return ElementContentType.TEXT;
            }
        }
    }

    /**
     * @return {@code true} if this element has no children. It may have attributes.
     */
    public boolean isEmpty() {
        return getContentType() == ElementContentType.EMPTY;
    }

    /**
     * @return {@code true} if this element is empty and has no attributes.
     */
    public boolean isPure() {
        return isEmpty() && !hasAttributes();
    }

    /**
     * @return {@code true} if this element has only text children.
     */
    public boolean hasOnlyText() {
        return getContentType() == ElementContentType.TEXT;
    }

    /**
     * @return {@code true} if this element has only non-text (comments and elements) children.
     */
    public boolean hasOnlyNonText() {
        return getContentType() == ElementContentType.NON_TEXT;
    }

    /**
     * @return {@code true} if this element has text and non-text children.
     */
    public boolean isMixed() {
        return getContentType() == ElementContentType.MIXED;
    }

    /**
     * @return {@code true} if this element is a root element.
     */
    public boolean isRoot() {
        return getParent() == null || getParent().getType() == NodeType.DOCUMENT;
    }

    /**
     * @return A list of attributes of this element.
     */
    public List<Attribute> getAttributes() {
        return attributes == null ? Collections.emptyList() : attributes;
    }

    /**
     * @return A list of attributes of this element, sorted by names.
     */
    public List<Attribute> getSortedAttributes() {
        final List<Attribute> sorted = new ArrayList<>(getAttributes());
        if (sorted.size() > 1) {
            Collections.sort(sorted, Attribute.NAME_COMPARATOR);
        }
        return sorted;
    }

    /**
     * @return The number of attributes of this element.
     */
    public int getAttributesCount() {
        if (attributes == null) {
            return 0;
        } else {
            return attributes.size();
        }
    }

    /**
     * @return {@code true} if this element has attributes.
     */
    public boolean hasAttributes() {
        return attributes != null && !attributes.isEmpty();
    }

    /**
     * Returns the attribute at a given index.
     *
     * @param index The index.
     * @return The attribute at {@code index}.
     * @throws IndexOutOfBoundsException If the index is out of range
     *             {@code (index < 0 || index >= getAttributesCount())}
     */
    public Attribute getAttributeAt(int index) {
        return attributes.get(index);
    }

    /**
     * Returns {@code true} when an attribute with a particular name exists.
     *
     * @param name The name.
     * @return {@code true} when an attribute named {@code name} exists.
     */
    public boolean hasAttribute(String name) {
        return getAttribute(name) != null;
    }

    /**
     * Returns the attribute that has a particular name, or {@code null}.
     *
     * @param name The name.
     * @return The attribute named {@code name} or {@code null}.
     */
    public Attribute getAttribute(String name) {
        if (attributes != null) {
            for (final Attribute attribute : attributes) {
                if (attribute.getName().equals(name)) {
                    return attribute;
                }
            }
        }
        return null;
    }

    /**
     * Returns the index of the attribute that has a particular name or -1.
     *
     * @param name The name.
     * @return The index of attribute named {@code name} or -1.
     */
    public int getAttributeIndex(String name) {
        if (attributes != null) {
            for (int index = 0; index < attributes.size(); index++) {
                if (attributes.get(index).getName().equals(name)) {
                    return index;
                }
            }
        }
        return -1;
    }

    /**
     * Returns the value associated to an attribute.
     *
     * @param name The attribute name.
     * @param def The value to return if no attribute named {@code name} exists.
     * @param missingReaction The reaction to adopt when attribute is not found.
     * @return The value of attribute named {@code name} or {@code def}.
     * @throws NotFoundException when attribute is not found and {@code missingReaction} is {@link FailureReaction#FAIL}.
     */
    public String getAttributeValue(String name,
                                    String def,
                                    FailureReaction missingReaction) {
        final Attribute att = getAttribute(name);
        if (att == null) {
            if (missingReaction == FailureReaction.DEFAULT) {
                return def;
            } else if (missingReaction == FailureReaction.WARN) {
                LOGGER.warn("No attribute named '{}'", name);
                return def;
            } else {
                throw new NotFoundException("No attribute named '" + name + "'");
            }
        } else {
            return att.getValue();
        }
    }

    /**
     * Returns the value associated to an attribute as a string or a default value.
     *
     * @param name The attribute name.
     * @param def The value to return if no attribute named {@code name} exists.
     * @return The value of attribute named {@code name} or {@code def}.
     */
    public String getAttributeValue(String name,
                                    String def) {
        return getAttributeValue(name, def, FailureReaction.DEFAULT);
    }

    /**
     * Returns the value associated to an attribute as a string or {@code null}.
     *
     * @param name The attribute name.
     * @return The value of attribute named {@code name}.
     * @throws NotFoundException when attribute is not found.
     */
    public String getAttributeValue(String name) {
        return getAttributeValue(name, null, FailureReaction.FAIL);
    }

    /**
     * Returns the value associated to an attribute as a boolean or a default value.
     *
     * @param name The attribute name.
     * @param def The value to return if no attribute named {@code name} exists
     *            or if its value can not be converted to a boolean.
     * @return The value of attribute named {@code name} or {@code def}.<br>
     *         If the attribute exists but its value can not be converted
     *         to a boolean, then {@code def} is returned and a warning is issued.
     */
    public boolean getAttributeAsBoolean(String name,
                                         boolean def) {
        return StringConversion.asBoolean(getAttributeValue(name, null),
                                          def,
                                          FailureReaction.DEFAULT,
                                          FailureReaction.WARN);
    }

    /**
     * Returns the value associated to an attribute as a Boolean or a default value.
     *
     * @param name The attribute name.
     * @param def The value to return if no attribute named {@code name} exists
     *            or if its value can not be converted to a Boolean.
     * @return The value of attribute named {@code name} or {@code def}.<br>
     *         If the attribute exists but its value can not be converted
     *         to a Boolean, then {@code def} is returned and a warning is issued.
     */
    public Boolean getAttributeAsOptionalBoolean(String name,
                                                 Boolean def) {
        return StringConversion.asOptionalBoolean(getAttributeValue(name, null),
                                                  def,
                                                  FailureReaction.WARN);
    }

    /**
     * Returns the value associated to an attribute as a long.
     *
     * @param name The attribute name.
     * @param def The value to return if no attribute named {@code name} exists
     *            or if its value can not be converted to a long.
     * @return The value of attribute named {@code name} or {@code def}.<br>
     *         If the attribute exists but its value can not be converted
     *         to a long, then {@code def} is returned and a warning is issued.
     */
    public long getAttributeAsLong(String name,
                                   long def) {
        return StringConversion.asLong(getAttributeValue(name, null),
                                       def,
                                       FailureReaction.DEFAULT,
                                       FailureReaction.WARN);
    }

    /**
     * Returns the value associated to an attribute as a Long.
     *
     * @param name The attribute name.
     * @param def The value to return if no attribute named {@code name} exists
     *            or if its value can not be converted to a Long.
     * @return The value of attribute named {@code name} or {@code def}.<br>
     *         If the attribute exists but its value can not be converted
     *         to a Long, then {@code def} is returned and a warning is issued.
     */
    public Long getAttributeAsOptionalLong(String name,
                                           Long def) {
        return StringConversion.asOptionalLong(getAttributeValue(name, null),
                                               def,
                                               FailureReaction.WARN);
    }

    /**
     * Returns the value associated to an attribute as an int.
     *
     * @param name The attribute name.
     * @param def The value to return if no attribute named {@code name} exists
     *            or if its value can not be converted to an int.
     * @return The value of attribute named {@code name} or {@code def}.<br>
     *         If the attribute exists but its value can not be converted
     *         to an int, then {@code def} is returned and a warning is issued.
     */
    public int getAttributeAsInt(String name,
                                 int def) {
        return StringConversion.asInt(getAttributeValue(name, null),
                                      def,
                                      FailureReaction.DEFAULT,
                                      FailureReaction.WARN);
    }

    /**
     * Returns the value associated to an attribute as an Integer.
     *
     * @param name The attribute name.
     * @param def The value to return if no attribute named {@code name} exists
     *            or if its value can not be converted to an Integer.
     * @return The value of attribute named {@code name} or {@code def}.<br>
     *         If the attribute exists but its value can not be converted
     *         to an Integer, then {@code def} is returned and a warning is issued.
     */
    public Integer getAttributeAsOptionalInt(String name,
                                             Integer def) {
        return StringConversion.asOptionalInt(getAttributeValue(name, null),
                                              def,
                                              FailureReaction.WARN);
    }

    /**
     * Returns the value associated to an attribute as a short.
     *
     * @param name The attribute name.
     * @param def The value to return if no attribute named {@code name} exists
     *            or if its value can not be converted to a short.
     * @return The value of attribute named {@code name} or {@code def}.<br>
     *         If the attribute exists but its value can not be converted
     *         to a short, then {@code def} is returned and a warning is issued.
     */
    public short getAttributeAsShort(String name,
                                     short def) {
        return StringConversion.asShort(getAttributeValue(name, null),
                                        def,
                                        FailureReaction.DEFAULT,
                                        FailureReaction.WARN);
    }

    /**
     * Returns the value associated to an attribute as a Short.
     *
     * @param name The attribute name.
     * @param def The value to return if no attribute named {@code name} exists
     *            or if its value can not be converted to a Short.
     * @return The value of attribute named {@code name} or {@code def}.<br>
     *         If the attribute exists but its value can not be converted
     *         to a Short, then {@code def} is returned and a warning is issued.
     */
    public Short getAttributeAsOptionalShort(String name,
                                             Short def) {
        return StringConversion.asOptionalShort(getAttributeValue(name, null),
                                                def,
                                                FailureReaction.WARN);
    }

    /**
     * Returns the value associated to an attribute as a byte.
     *
     * @param name The attribute name.
     * @param def The value to return if no attribute named {@code name} exists
     *            or if its value can not be converted to a byte.
     * @return The value of attribute named {@code name} or {@code def}.<br>
     *         If the attribute exists but its value can not be converted
     *         to a byte, then {@code def} is returned and a warning is issued.
     */
    public byte getAttributeAsByte(String name,
                                   byte def) {
        return StringConversion.asByte(getAttributeValue(name, null),
                                       def,
                                       FailureReaction.DEFAULT,
                                       FailureReaction.WARN);
    }

    /**
     * Returns the value associated to an attribute as a Byte.
     *
     * @param name The attribute name.
     * @param def The value to return if no attribute named {@code name} exists
     *            or if its value can not be converted to a Byte.
     * @return The value of attribute named {@code name} or {@code def}.<br>
     *         If the attribute exists but its value can not be converted
     *         to a Byte, then {@code def} is returned and a warning is issued.
     */
    public Byte getAttributeAsOptionalByte(String name,
                                           Byte def) {
        return StringConversion.asOptionalByte(getAttributeValue(name, null),
                                               def,
                                               FailureReaction.WARN);
    }

    /**
     * Returns the value associated to an attribute as a double.
     *
     * @param name The attribute name.
     * @param def The value to return if no attribute named {@code name} exists
     *            or if its value can not be converted to a double.
     * @return The value of attribute named {@code name} or {@code def}.<br>
     *         If the attribute exists but its value can not be converted
     *         to a double, then {@code def} is returned and a warning is issued.
     */
    public double getAttributeAsDouble(String name,
                                       double def) {
        return StringConversion.asDouble(getAttributeValue(name, null),
                                         def,
                                         FailureReaction.DEFAULT,
                                         FailureReaction.WARN);
    }

    /**
     * Returns the value associated to an attribute as a Double.
     *
     * @param name The attribute name.
     * @param def The value to return if no attribute named {@code name} exists
     *            or if its value can not be converted to a Double.
     * @return The value of attribute named {@code name} or {@code def}.<br>
     *         If the attribute exists but its value can not be converted
     *         to a Double, then {@code def} is returned and a warning is issued.
     */
    public Double getAttributeAsOptionalDouble(String name,
                                               Double def) {
        return StringConversion.asOptionalDouble(getAttributeValue(name, null),
                                                 def,
                                                 FailureReaction.WARN);
    }

    /**
     * Returns the value associated to an attribute as a float.
     *
     * @param name The attribute name.
     * @param def The value to return if no attribute named {@code name} exists
     *            or if its value can not be converted to a float.
     * @return The value of attribute named {@code name} or {@code def}.<br>
     *         If the attribute exists but its value can not be converted
     *         to a float, then {@code def} is returned and a warning is issued.
     */
    public float getAttributeAsFloat(String name,
                                     float def) {
        return StringConversion.asFloat(getAttributeValue(name, null),
                                        def,
                                        FailureReaction.DEFAULT,
                                        FailureReaction.WARN);
    }

    /**
     * Returns the value associated to an attribute as a Float.
     *
     * @param name The attribute name.
     * @param def The value to return if no attribute named {@code name} exists
     *            or if its value can not be converted to a Float.
     * @return The value of attribute named {@code name} or {@code def}.<br>
     *         If the attribute exists but its value can not be converted
     *         to a Float, then {@code def} is returned and a warning is issued.
     */
    public Float getAttributeAsOptionalFloat(String name,
                                             Float def) {
        return StringConversion.asOptionalFloat(getAttributeValue(name, null),
                                                def,
                                                FailureReaction.WARN);
    }

    public Enum<?> getAttributeAsRawEnum(String name,
                                         Class<? extends Enum<?>> enumClass,
                                         Enum<?> def) {
        return StringConversion.asRawEnum(getAttributeValue(name, null),
                                          enumClass,
                                          def,
                                          FailureReaction.DEFAULT,
                                          FailureReaction.WARN);
    }

    public Enum<?> getAttributeAsOptionalRawEnum(String name,
                                                 Class<? extends Enum<?>> enumClass,
                                                 Enum<?> def) {
        return StringConversion.asOptionalRawEnum(getAttributeValue(name, null),
                                                  enumClass,
                                                  def,
                                                  FailureReaction.WARN);
    }

    /**
     * Returns the value associated to an attribute as an enum.
     *
     * @param <E> The enum type.
     * @param name The attribute name.
     * @param enumClass The enum class.
     * @param def The value to return if no attribute named {@code name} exists
     *            or if its value can not be converted to an enum.
     * @return The value of attribute named {@code name} or {@code def}.
     */
    public <E extends Enum<E>> E getAttributeAsEnum(String name,
                                                    Class<E> enumClass,
                                                    E def) {
        return StringConversion.asEnum(getAttributeValue(name, null),
                                       enumClass,
                                       def,
                                       FailureReaction.DEFAULT,
                                       FailureReaction.WARN);
    }

    public <E extends Enum<E>> E getAttributeAsOptionalEnum(String name,
                                                            Class<E> enumClass,
                                                            E def) {
        return StringConversion.asOptionalEnum(getAttributeValue(name, null),
                                               enumClass,
                                               def,
                                               FailureReaction.WARN);
    }

    /**
     * Adds an attribute.
     *
     * @param attribute The attribute.
     * @throws IllegalArgumentException When {@code attribute} is invalid
     *             or another attribute with same name already exists.
     */
    public void addAttribute(Attribute attribute) {
        Checks.isNotNull(attribute, "attribute");
        if (hasAttribute(attribute.getName())) {
            throw new IllegalArgumentException("Duplicate attribute");
        } else {
            if (attributes == null) {
                attributes = new ArrayList<>();
            }
            attributes.add(attribute);
        }
    }

    /**
     * Adds all attributes of a collection to this element.
     *
     * @param attributes The attributes to add.
     * @throws IllegalArgumentException When one of {@code attributes} is invalid
     *             or another attribute with same name already exists.
     */
    public void addAttributes(Collection<Attribute> attributes) {
        for (final Attribute attribute : attributes) {
            addAttribute(attribute);
        }
    }

    /**
     * Adds an array of attributes to this element.
     *
     * @param attributes The attributes to add.
     * @throws IllegalArgumentException When one of {@code attributes} is invalid
     *             or another attribute with same name already exists.
     */
    public void addAttributes(Attribute... attributes) {
        for (final Attribute attribute : attributes) {
            addAttribute(attribute);
        }
    }

    /**
     * Adds an object attribute.
     * <p>
     * The string value is constructed using value.toString().
     *
     * @param name The attribute name.
     * @param value The attribute value.
     * @throws IllegalArgumentException When {@code name} is invalid
     *             or another attribute with same name already exists.
     */
    public void addAttribute(String name,
                             Object value) {
        addAttribute(new Attribute(name, value));
    }

    /**
     * Adds a string attribute.
     *
     * @param name The attribute name.
     * @param value The attribute value.
     * @throws IllegalArgumentException When {@code name} is invalid
     *             or another attribute with same name already exists.
     */
    public void addAttribute(String name,
                             String value) {
        addAttribute(new Attribute(name, value));
    }

    /**
     * Adds a boolean attribute.
     *
     * @param name The attribute name.
     * @param value The attribute value.
     * @throws IllegalArgumentException When {@code name} is invalid
     *             or another attribute with same name already exists.
     */
    public void addAttribute(String name,
                             boolean value) {
        addAttribute(new Attribute(name, value));
    }

    /**
     * Adds a long attribute.
     *
     * @param name The attribute name.
     * @param value The attribute value.
     * @throws IllegalArgumentException When {@code name} is invalid
     *             or another attribute with same name already exists.
     */
    public void addAttribute(String name,
                             long value) {
        addAttribute(new Attribute(name, value));
    }

    /**
     * Adds an int attribute.
     *
     * @param name The attribute name.
     * @param value The attribute value.
     * @throws IllegalArgumentException When {@code name} is invalid
     *             or another attribute with same name already exists.
     */
    public void addAttribute(String name,
                             int value) {
        addAttribute(new Attribute(name, value));
    }

    /**
     * Adds a short attribute.
     *
     * @param name The attribute name.
     * @param value The attribute value.
     * @throws IllegalArgumentException When {@code name} is invalid
     *             or another attribute with same name already exists.
     */
    public void addAttribute(String name,
                             short value) {
        addAttribute(new Attribute(name, value));
    }

    /**
     * Adds a byte attribute.
     *
     * @param name The attribute name.
     * @param value The attribute value.
     * @throws IllegalArgumentException When {@code name} is invalid
     *             or another attribute with same name already exists.
     */
    public void addAttribute(String name,
                             byte value) {
        addAttribute(new Attribute(name, value));
    }

    /**
     * Adds a double attribute.
     *
     * @param name The attribute name.
     * @param value The attribute value.
     * @throws IllegalArgumentException When {@code name} is invalid
     *             or another attribute with same name already exists.
     */
    public void addAttribute(String name,
                             double value) {
        addAttribute(new Attribute(name, value));
    }

    /**
     * Adds a float attribute.
     *
     * @param name The attribute name.
     * @param value The attribute value.
     * @throws IllegalArgumentException When {@code name} is invalid
     *             or another attribute with same name already exists.
     */
    public void addAttribute(String name,
                             float value) {
        addAttribute(new Attribute(name, value));
    }

    /**
     * Removes an attribute identified by its name.
     *
     * @param name The name of the attribute to remove.
     * @return The removed attribute or null.
     */
    public Attribute removeAttribute(String name) {
        final int index = getAttributeIndex(name);
        if (index >= 0) {
            return attributes.remove(index);
        } else {
            return null;
        }
    }

    public void removeAttributes(Predicate<Attribute> predicate) {
        final Iterator<Attribute> atts = getAttributes().iterator();
        while (atts.hasNext()) {
            final Attribute att = atts.next();
            if (predicate.test(att)) {
                atts.remove();
            }
        }
    }

    public void removeAttributes(Predicate<Attribute> predicate,
                                 boolean recurse) {
        removeAttributes(predicate);
        if (recurse) {
            for (final Element child : getElements()) {
                child.removeAttributes(predicate, true);
            }
        }
    }

    public void removeAttributesNamed(String name) {
        removeAttributes(Attribute.named(name));
    }

    public void removeAttributesNamed(String name,
                                      boolean recurse) {
        removeAttributes(Attribute.named(name), recurse);
    }

    public void removeAttributesNamed(Set<String> names) {
        removeAttributes(Attribute.named(names));
    }

    public void removeAttributesNamed(Set<String> names,
                                      boolean recurse) {
        removeAttributes(Attribute.named(names), recurse);
    }

    public void removeAttributesNamed(String... names) {
        removeAttributes(Attribute.named(names));
    }

    /**
     * Remove all attributes of this element.
     */
    public void removeAttributes() {
        if (attributes != null) {
            attributes.clear();
        }
    }

    /**
     * Changes the value of attributes that have a given name.
     *
     * @param name The attribute name.
     * @param modifier A function that take value and returns new value.
     */
    public void changeAttributeValue(String name,
                                     UnaryOperator<String> modifier) {
        final Attribute att = getAttribute(name);
        if (att != null) {
            final String modified = modifier.apply(att.getValue());
            att.setValue(modified);
        }
    }

    /**
     * Changes the value of attributes that have a given name.
     *
     * @param name The attribute name.
     * @param modifier A function that take value and returns new value.
     * @param recurse If {@code true}, then this is applied recursively.
     */
    public void changeAttributeValue(String name,
                                     UnaryOperator<String> modifier,
                                     boolean recurse) {
        changeAttributeValue(name, modifier);
        if (recurse) {
            for (final Element child : getElements()) {
                child.changeAttributeValue(name, modifier, true);
            }
        }
    }

    /**
     * Changes the element name.
     *
     * @param modifier A function that take name and returns new name.
     */
    public void changeName(UnaryOperator<String> modifier) {
        final String newName = modifier.apply(getName());
        if (Operators.notEquals(getName(), newName)) {
            setName(newName);
        }
    }

    /**
     * Changes element names recursively.
     *
     * @param modifier A function that take name and returns new name.
     * @param recurse If {@code true}, then this is applied recursively.
     */
    public void changeName(UnaryOperator<String> modifier,
                           boolean recurse) {
        changeName(modifier);
        if (recurse) {
            for (final Element child : getElements()) {
                child.changeName(modifier, true);
            }
        }
    }

    /**
     * Adds a text as last child.
     *
     * @param content The text content.
     * @param kind The text kind.
     * @param merge If {@code true}, then if last child exists and is a text with same kind,
     *            {@code content} is added to this last child.<br>
     *            Otherwise, a text child is created and added to last position.
     * @return The modified or created text.
     */
    public Text addText(String content,
                        TextKind kind,
                        boolean merge) {
        if (merge) {
            final Child last = getLastChild();
            if (last != null
                    && last.getType() == NodeType.TEXT
                    && ((Text) last).getKind() == kind) {
                ((Text) last).appendContent(content);
                return (Text) last;
            }
        }
        final Text result = new Text(content, kind);
        addChild(result);
        return result;
    }

    public Text addText(String content,
                        boolean merge) {
        return addText(content, TextKind.STANDARD, merge);
    }

    /**
     * Adds a text as last child.
     * <p>
     * If last child exists and is a text, {@code content} is added to this last child.<br>
     * Otherwise, a text child is created and added to last position.
     *
     * @param content The text content.
     * @param kind The text kind.
     * @return The modified or created text.
     */
    public Text addText(String content,
                        TextKind kind) {
        return addText(content, kind, true);
    }

    public Text addText(String content) {
        return addText(content, TextKind.STANDARD);
    }

    /**
     * Returns the merged content of all children text nodes, if this node has no element children.
     * <p>
     * This will work when this node has only text and comment children.<br>
     * If it has element children, the default value is returned.
     *
     * @param def The default value.
     * @return The merged content of children texts (ignoring comments) or {@code def}.
     */
    public String getText(String def) {
        if (children == null || children.isEmpty()) {
            return def;
        } else if (!hasChildren(Element.class)) {
            // Ignore comments
            final Iterable<Text> texts = getChildren(Text.class);
            if (IterableUtils.isEmpty(texts)) {
                return def;
            } else {
                final StringBuilder sb = new StringBuilder();
                for (final Text text : texts) {
                    sb.append(text.getContent());
                }
                return sb.toString();
            }
        } else {
            return def;
        }
    }

    /**
     * Returns the merged content of all children text nodes, if this node has no element children.
     * <p>
     * This will work when this node has only text and comment children.<br>
     * If it has element children, {@code null} is returned.
     *
     * @return The merged content of children texts (ignoring comments) or {@code null}.
     */
    public String getText() {
        return getText(null);
    }

    public boolean getTextAsBoolean(boolean def) {
        return StringConversion.asBoolean(getText(),
                                          def,
                                          FailureReaction.DEFAULT,
                                          FailureReaction.WARN);
    }

    public Boolean getTextAsOptionalBoolean(Boolean def) {
        return StringConversion.asOptionalBoolean(getText(),
                                                  def,
                                                  FailureReaction.WARN);
    }

    public long getTextAsLong(long def) {
        return StringConversion.asLong(getText(),
                                       def,
                                       FailureReaction.DEFAULT,
                                       FailureReaction.WARN);
    }

    public Long getTextAsOptionalLong(Long def) {
        return StringConversion.asOptionalLong(getText(),
                                               def,
                                               FailureReaction.WARN);
    }

    public int getTextAsInt(int def) {
        return StringConversion.asInt(getText(),
                                      def,
                                      FailureReaction.DEFAULT,
                                      FailureReaction.WARN);
    }

    public Integer getTextAsOptionalInt(Integer def) {
        return StringConversion.asOptionalInt(getText(),
                                              def,
                                              FailureReaction.WARN);
    }

    public short getTextAsShort(short def) {
        return StringConversion.asShort(getText(),
                                        def,
                                        FailureReaction.DEFAULT,
                                        FailureReaction.WARN);
    }

    public Short getTextAsOptionalShort(Short def) {
        return StringConversion.asOptionalShort(getText(),
                                                def,
                                                FailureReaction.WARN);
    }

    public byte getTextAsByte(byte def) {
        return StringConversion.asByte(getText(),
                                       def,
                                       FailureReaction.DEFAULT,
                                       FailureReaction.WARN);
    }

    public Byte getTextAsOptionalByte(Byte def) {
        return StringConversion.asOptionalByte(getText(),
                                               def,
                                               FailureReaction.WARN);
    }

    public double getTextAsDouble(double def) {
        return StringConversion.asDouble(getText(),
                                         def,
                                         FailureReaction.DEFAULT,
                                         FailureReaction.WARN);
    }

    public Double getTextAsOptionalDouble(Double def) {
        return StringConversion.asOptionalDouble(getText(),
                                                 def,
                                                 FailureReaction.WARN);
    }

    public float getTextAsFloat(float def) {
        return StringConversion.asFloat(getText(),
                                        def,
                                        FailureReaction.DEFAULT,
                                        FailureReaction.WARN);
    }

    public Float getTextAsOptionalFloat(Float def) {
        return StringConversion.asOptionalFloat(getText(),
                                                def,
                                                FailureReaction.WARN);
    }

    public <E extends Enum<E>> E getTextAsEnum(Class<E> enumClass,
                                               E def) {
        return StringConversion.asEnum(getText(),
                                       enumClass,
                                       def,
                                       FailureReaction.DEFAULT,
                                       FailureReaction.WARN);
    }

    public <E extends Enum<E>> E getTextAsOptionalEnum(Class<E> enumClass,
                                                       E def) {
        return StringConversion.asOptionalEnum(getText(),
                                               enumClass,
                                               def,
                                               FailureReaction.WARN);
    }

    /**
     * Returns the text content of the first child element that has a given name.
     *
     * @param name The child name.
     * @param def The default value.
     * @return The text of the first child named {@code name} or {@code def}.
     */
    public String getElementNamedText(String name,
                                      String def) {
        final Element child = getElementNamed(name);
        if (child != null) {
            return child.getText(def);
        } else {
            return def;
        }
    }

    /**
     * Returns the text content of the first child element that has a given name.
     *
     * @param name The child name.
     * @return The text of the first child named {@code name} or {@code null}.
     */
    public String getElementNamedText(String name) {
        return getElementNamedText(name, null);
    }

    @Override
    public Element setParent(Parent parent) {
        return (Element) super.setParent(parent);
    }

    @Override
    public Element setIndex(int to) {
        return (Element) super.setIndex(to);
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("[" + getType() + " " + getName());
        for (final Attribute att : getAttributes()) {
            builder.append(" " + att.getName() + "='" + att.getValue() + "'");
        }
        builder.append("]");
        return builder.toString();
    }

    /**
     * Creates a Predicate that accepts Nodes that are element with a given name.
     *
     * @param name The name.
     * @return A new Predicate that accepts Nodes that are elements named {@code name}.
     */
    public static Predicate<Node> named(String name) {
        return node -> node.getType() == NodeType.ELEMENT && ((Element) node).getName().equals(name);
    }

    /**
     * Creates a Predicate that accepts Nodes that are element whose named belongs to a set.
     *
     * @param names The names.
     * @return A new Predicate that accepts Nodes that are elements whose name belongs to {@code names}.
     */
    public static Predicate<Node> named(Set<String> names) {
        return node -> node.getType() == NodeType.ELEMENT && names.contains(((Element) node).getName());
    }

    /**
     * Creates a Predicate that accepts Nodes that are element whose named belongs to an array.
     *
     * @param names The names.
     * @return A new Predicate that accepts Nodes that are elements whose name belongs to {@code names}.
     */
    public static Predicate<Node> named(String... names) {
        return named(CollectionUtils.toSet(names));
    }

    public static Predicate<Node> matching(Set<SPath> paths) {
        return node -> {
            if (node.getType() == NodeType.ELEMENT) {
                final Element element = (Element) node;
                for (final SPath path : paths) {
                    if (path.matchesElement(element)) {
                        return true;
                    }
                }
            }
            return false;
        };
    }

    /**
     * Creates a Predicate that accepts Nodes that are elements with a given name and have an attribute with a given value.
     *
     * @param name The element name.
     * @param attributeName The attribute name.
     * @param attributeValue The attribute value.
     * @return A new Predicate that accepts Nodes that are elements named {@code name}
     *         with an attribute named {@code attributeName} and containing {@code attributeValue}.
     */
    public static Predicate<Node> namedWithAttribute(String name,
                                                     String attributeName,
                                                     String attributeValue) {
        return node -> {
            if (node.getType() == NodeType.ELEMENT) {
                final Element e = (Element) node;
                return e.getName().equals(name)
                        && Operators.equals(e.getAttributeValue(attributeName, null), attributeValue);
            } else {
                return false;
            }
        };
    }

    /**
     * Creates a Predicate that accepts Nodes that are elements that have an attribute with a given value.
     *
     * @param attributeName The attribute name.
     * @param attributeValue The attribute value.
     * @return A new Predicate that accepts Nodes that are elements that have
     *         an attribute named {@code attributeName} and containing {@code attributeValue}.
     */
    public static Predicate<Node> hasAttribute(String attributeName,
                                               String attributeValue) {
        return node -> {
            if (node.getType() == NodeType.ELEMENT) {
                final Element e = (Element) node;
                return Operators.equals(e.getAttributeValue(attributeName, null), attributeValue);
            } else {
                return false;
            }
        };
    }

    /**
     * Creates a Predicate that accepts Nodes that are element with a given name, ignoring case.
     *
     * @param name The name.
     * @return A new Predicate that accepts Nodes that are elements named {@code name}, ignoring case.
     */
    public static Predicate<Node> namedIgnoreCase(String name) {
        return node -> node.getType() == NodeType.ELEMENT && ((Element) node).getName().equalsIgnoreCase(name);
    }
}
