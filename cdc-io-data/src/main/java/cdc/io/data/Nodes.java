package cdc.io.data;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;

import cdc.util.strings.StringUtils;

public final class Nodes {

    private Nodes() {
    }

    public static void print(Node node,
                             Logger logger,
                             Level level) {
        print(node, logger, level, 0);

    }

    private static void print(Node node,
                              Logger logger,
                              Level level,
                              int depth) {
        if (logger.isEnabled(level)) {
            logger.log(level, "{}{}", indent(depth), toString(node));
        }
        if (node instanceof Parent) {
            final Parent parent = (Parent) node;
            for (final Node child : parent.getChildren()) {
                print(child, logger, level, depth + 1);
            }
        }
    }

    private static String indent(int depth) {
        return StringUtils.toString(' ', depth * 3);
    }

    private static String toString(Node node) {
        final StringBuilder builder = new StringBuilder();
        builder.append(node.getType());
        switch (node.getType()) {
        case DOCUMENT:
            builder.append(" dtd:" + StringUtils.extract(((Document) node).getDTD(), 10));
            break;
        case COMMENT:
        case TEXT:
            builder.append(" '" + StringUtils.extract(((AbstractContentNode) node).getContent(), 10) + "' ");
            if (node.getType() == NodeType.TEXT) {
                builder.append(" " + ((Text) node).getKind());
            }
            break;
        case ELEMENT:
            builder.append(" " + ((Element) node).getName());
            break;
        default:
            break;
        }

        return builder.toString();
    }
}