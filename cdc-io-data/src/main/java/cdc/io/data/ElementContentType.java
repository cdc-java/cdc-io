package cdc.io.data;

/**
 * Enumeration of possible element content types.
 *
 * @author Damien Carbonne
 *
 */
public enum ElementContentType {
    /**
     * The element has no children.
     */
    EMPTY,

    /**
     * The element has only text children.
     */
    TEXT,

    /**
     * The element has only non-text children: elements or comments.
     */
    NON_TEXT,

    /**
     * The element has text and non-text (element or comment) children.
     */
    MIXED
}