package cdc.io.data;

import cdc.io.xml.XmlUtils;

/**
 * Text node.
 * <p>
 * It has a text content and can be attached to a Document or Element.
 *
 * @author Damien Carbonne
 *
 */
public final class Text extends AbstractContentNode {
    private TextKind kind = TextKind.STANDARD;

    public Text(Parent parent,
                String content,
                TextKind kind) {
        super(parent, content);
        this.kind = kind;
    }

    public Text() {
        this(null, null, TextKind.STANDARD);
    }

    public Text(Parent parent) {
        this(parent, null, TextKind.STANDARD);
    }

    public Text(String content,
                TextKind kind) {
        this(null, content, kind);
    }

    public Text(String content) {
        this(content, TextKind.STANDARD);
    }

    public Text(Parent parent,
                String content) {
        this(parent, content, TextKind.STANDARD);
    }

    public Text(Text other) {
        this(null, other.getContent(), other.kind);
    }

    @Override
    public NodeType getType() {
        return NodeType.TEXT;
    }

    @Override
    public Text clone(boolean recurse) {
        return new Text(this);
    }

    @Override
    public String getQName() {
        return getParent().getQName() + "/text()";
    }

    public TextKind getKind() {
        return kind;
    }

    public boolean isCData() {
        return getKind() == TextKind.CDATA;
    }

    public boolean isStandard() {
        return getKind() == TextKind.STANDARD;
    }

    public Text setKind(TextKind kind) {
        this.kind = kind;
        return this;
    }

    public boolean isIgnorable() {
        return XmlUtils.isWhiteSpace(getContent());
    }

    @Override
    public Text setParent(Parent parent) {
        return (Text) super.setParent(parent);
    }

    @Override
    public Text setIndex(int to) {
        return (Text) super.setIndex(to);
    }

    @Override
    public Text setContent(String content) {
        return (Text) super.setContent(content);
    }

    @Override
    public Text clearContent() {
        return (Text) super.clearContent();
    }

    @Override
    public Text appendContent(String content) {
        return (Text) super.appendContent(content);
    }

    @Override
    public Text appendContent(CharSequence cs,
                              int start,
                              int end) {
        return (Text) super.appendContent(cs, start, end);
    }

    @Override
    public Text appendContent(char[] str,
                              int offset,
                              int len) {
        return (Text) super.appendContent(str, offset, len);
    }

}