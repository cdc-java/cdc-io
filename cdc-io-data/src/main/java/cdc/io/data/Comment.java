package cdc.io.data;

/**
 * Comment node.
 * <p>
 * It has a text content and can be attached to a Document or Element.
 *
 * @author Damien Carbonne
 *
 */
public final class Comment extends AbstractContentNode {
    public Comment(Parent parent,
                   String content) {
        super(parent, content);
    }

    public Comment() {
        this(null, null);
    }

    public Comment(Parent parent) {
        this(parent, null);
    }

    public Comment(String content) {
        this(null, content);
    }

    public Comment(Comment other) {
        this(null, other.getContent());
    }

    @Override
    public NodeType getType() {
        return NodeType.COMMENT;
    }

    @Override
    public Comment clone(boolean recurse) {
        return new Comment(this);
    }

    @Override
    public String getQName() {
        return getParent().getQName() + "/comment()";
    }

    @Override
    public Comment setParent(Parent parent) {
        return (Comment) super.setParent(parent);
    }

    @Override
    public Comment setIndex(int to) {
        return (Comment) super.setIndex(to);
    }

    @Override
    public Comment setContent(String content) {
        return (Comment) super.setContent(content);
    }

    @Override
    public Comment clearContent() {
        return (Comment) super.clearContent();
    }

    @Override
    public Comment appendContent(String content) {
        return (Comment) super.appendContent(content);
    }

    @Override
    public Comment appendContent(CharSequence cs,
                                 int start,
                                 int end) {
        return (Comment) super.appendContent(cs, start, end);
    }

    @Override
    public Comment appendContent(char[] str,
                                 int offset,
                                 int len) {
        return (Comment) super.appendContent(str, offset, len);
    }
}