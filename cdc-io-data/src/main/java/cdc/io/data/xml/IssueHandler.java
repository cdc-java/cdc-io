package cdc.io.data.xml;

public interface IssueHandler {
    /**
     * Invoked to notify a warning.
     *
     * @param message The message.
     * @param systemId The system id.
     * @param publicId The public id.
     * @param lineNumber The line number.
     * @param columnNumber The column number.
     */
    public void warning(String message,
                        String systemId,
                        String publicId,
                        int lineNumber,
                        int columnNumber);

    public void error(String message,
                      String systemId,
                      String publicId,
                      int lineNumber,
                      int columnNumber);

    public void fatal(String message,
                      String systemId,
                      String publicId,
                      int lineNumber,
                      int columnNumber);
}