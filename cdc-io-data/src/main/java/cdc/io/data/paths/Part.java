package cdc.io.data.paths;

import java.util.regex.Pattern;

// ITEM ::= YYY | YYY[@YYY='ZZZ'] | @XXX | . | ..
// PATH ::= ITEM (/ITEM)*
/**
 * Elementary parts that constitute a Path.
 * <p>
 * Currently a Part can have the following forms:
 * <ul>
 * <li>{@code .} to designate the node itself.
 * <li>{@code ..} to designate the parent of the node.
 * <li>{@code AAA} to designate a child element named {@code AAA}.
 * <li>{@code AAA[@BBB='CCC']} to designate a child element named {@code AAA}
 * that has an attribute named {@code BBB} with value {@code CCC}.
 * <li>{@code @AAA} to designate an attribute named {@code AAA}.
 * </ul>
 *
 * @author Damien Carbonne
 */
public class Part {
    private static final Pattern DOT_PATTERN = Pattern.compile("^.$");
    private static final Pattern DOT_DOT_PATTERN = Pattern.compile("^..$");
    private static final Pattern ELEMENT_PATTERN = Pattern.compile("^[\\p{Alnum}_ -]+$");
    private static final Pattern ATTRIBUTE_PATTERN = Pattern.compile("^@[\\p{Alnum}_ -]+$");
    private static final Pattern SELECTOR_PATTERN = Pattern.compile("^[\\p{Alnum}_-]+\\[@[\\p{Alnum}_-]+='[\\p{Alnum}_ -]+'\\]$");

    public static final Part DOT = new Part(".");
    public static final Part DOT_DOT = new Part("..");

    /**
     * Enumeration of possible part types.
     *
     * @author Damien Carbonne
     *
     */
    public enum Type {
        /**
         * The part designates an attribute.
         */
        ATTRIBUTE,

        /**
         * The part designates a named element.
         */
        ELEMENT,

        /**
         * The part designates an element selector, using an attribute.
         */
        SELECTOR,
        // ATTRIBUTE_SELECTOR
        // ELEMENT_INDEX_SELECTOR
        // TEXT
        // COMMENT
        DOT,
        DOT_DOT
    }

    private final Type type;
    private final String text;

    public Part(String text) {
        this.type = getType(text);
        this.text = text;
        if (type == null) {
            throw new IllegalArgumentException("Invalid part text:'" + text + "'");
        }
    }

    public static Part of(String text) {
        return new Part(text);
    }

    public static Type getType(String text) {
        if (ELEMENT_PATTERN.matcher(text).find()) {
            return Type.ELEMENT;
        } else if (ATTRIBUTE_PATTERN.matcher(text).find()) {
            return Type.ATTRIBUTE;
        } else if (SELECTOR_PATTERN.matcher(text).find()) {
            return Type.SELECTOR;
        } else if (DOT_PATTERN.matcher(text).find()) {
            return Type.DOT;
        } else if (DOT_DOT_PATTERN.matcher(text).find()) {
            return Type.DOT_DOT;
        } else {
            return null;
        }
    }

    /**
     * @return The text representation of this Part.
     */
    public String getText() {
        return text;
    }

    /**
     * @return The {@link Type type} of this Part.
     */
    public Type getType() {
        return type;
    }

    /**
     * @return The name of the designated element or {@code null}.
     *         Meaningful with {@link Type#ELEMENT} and {@link Type#SELECTOR}.
     */
    public String getElementName() {
        if (type == Type.ELEMENT) {
            return text;
        } else if (type == Type.SELECTOR) {
            return text.substring(0, text.indexOf('['));
        } else {
            return null;
        }
    }

    /**
     * @return The name of the designated attribute.
     *         Meaningful with {@link Type#ATTRIBUTE}.
     */
    public String getAttributeName() {
        if (type == Type.ATTRIBUTE) {
            return text.substring(1);
        } else {
            return null;
        }
    }

    /**
     * @return The name of the designated selector attribute.
     *         Meaningful with {@link Type#SELECTOR}.
     */
    public String getSelectorName() {
        if (type == Type.SELECTOR) {
            final int start = text.indexOf('@') + 1;
            final int end = text.indexOf('=');
            return text.substring(start, end);
        } else {
            return null;
        }
    }

    /**
     * @return The value of the designated selector attribute.
     *         Meaningful with {@link Type#SELECTOR}.
     */
    public String getSelectorValue() {
        if (type == Type.SELECTOR) {
            final int start = text.indexOf('=') + 2;
            final int end = text.indexOf(']') - 1;
            return text.substring(start, end);
        } else {
            return null;
        }
    }

    @Override
    public String toString() {
        return text;
    }
}