package cdc.io.data;

/**
 * Interface implemented by leaf nodes (they have no children).
 *
 * @author Damien Carbonne
 *
 */
public interface Leaf extends Child {
    // Nothing to declare
}