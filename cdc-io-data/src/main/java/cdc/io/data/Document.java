package cdc.io.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * XML like Document node.
 *
 * @author Damien Carbonne
 *
 */
public final class Document implements Parent {
    private String dtd;
    /**
     * Children of this document.
     */
    private final List<Child> children = new ArrayList<>();

    public Document() {
        super();
    }

    @Override
    public NodeType getType() {
        return NodeType.DOCUMENT;
    }

    public void setDTD(String dtd) {
        this.dtd = dtd;
    }

    public String getDTD() {
        return dtd;
    }

    public boolean hasDTD() {
        return dtd != null;
    }

    @Override
    public Document clone(boolean recurse) {
        final Document result = new Document();
        result.setDTD(getDTD());
        if (recurse) {
            for (final Child child : getChildren()) {
                result.addChild(child.clone(true));
            }
        }
        return result;
    }

    @Override
    public boolean deepEquals(Node node) {
        if (this == node) {
            return true;
        }
        if (!(node instanceof Document)) {
            return false;
        }
        final Document other = (Document) node;
        if (!Objects.equals(getDTD(), other.getDTD())) {
            return false;
        }
        if (getChildrenCount() == other.getChildrenCount()) {
            for (int index = 0; index < getChildrenCount(); index++) {
                if (!getChildAt(index).deepEquals(other.getChildAt(index))) {
                    return false;
                }
            }
        } else {
            return false;
        }
        return true;
    }

    @Override
    public String getQName() {
        return "/";
    }

    @Override
    public Document getDocument() {
        return this;
    }

    @Override
    public Element getRootElement() {
        for (final Child child : children) {
            if (child.getType() == NodeType.ELEMENT) {
                return (Element) child;
            }
        }
        return null;
    }

    /**
     * Returns the root element of a document.
     * <p>
     * If {@code document} is {@code null}, then {@code null} is returned.
     *
     * @param document The document (possibly {@code null}).
     * @return The root element of {@code document}.
     */
    public static Element getRootElement(Document document) {
        return document == null ? null : document.getRootElement();
    }

    @Override
    public boolean canAddChild(Child child) {
        if (child == null) {
            return false;
        } else if (child.getType() == NodeType.ELEMENT) {
            return getRootElement() == null;
        } else {
            return true;
        }
    }

    @Override
    public List<Child> getModifiableChildren() {
        return children;
    }

    @Override
    public List<Child> getChildren() {
        return getModifiableChildren();
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object object) {
        return this == object;
    }
}