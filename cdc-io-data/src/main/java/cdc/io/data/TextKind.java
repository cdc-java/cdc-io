package cdc.io.data;

public enum TextKind {
    STANDARD,
    CDATA
}