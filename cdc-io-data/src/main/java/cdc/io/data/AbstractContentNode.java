package cdc.io.data;

import java.util.Objects;

/**
 * Base class for nodes with content: text and comment.
 *
 * @author Damien Carbonne
 *
 */
abstract class AbstractContentNode extends AbstractChild implements Leaf {
    private final StringBuilder content = new StringBuilder();

    protected AbstractContentNode(Parent parent,
                                  String content) {
        super();
        setParent(parent);
        setContent(content);
    }

    public AbstractContentNode setContent(String content) {
        if (content != null) {
            this.content.setLength(0);
            this.content.append(content);
        }
        return this;
    }

    public AbstractContentNode clearContent() {
        content.setLength(0);
        return this;
    }

    public AbstractContentNode appendContent(String text) {
        if (text != null) {
            this.content.append(text);
        }
        return this;
    }

    public AbstractContentNode appendContent(CharSequence cs,
                                             int start,
                                             int end) {
        content.append(cs, start, end);
        return this;
    }

    public AbstractContentNode appendContent(char[] str,
                                             int offset,
                                             int len) {
        this.content.append(str, offset, len);
        return this;
    }

    public final String getContent() {
        return content.toString();
    }

    @Override
    public final boolean deepEquals(Node node) {
        if (node == this) {
            return true;
        }
        if (node == null || !getClass().equals(node.getClass())) {
            return false;
        }
        final AbstractContentNode other = (AbstractContentNode) node;
        return Objects.equals(getContent(), other.getContent());
    }
}