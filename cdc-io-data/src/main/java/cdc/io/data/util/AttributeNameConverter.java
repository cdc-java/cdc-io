package cdc.io.data.util;

import java.util.Map;
import java.util.function.UnaryOperator;

import cdc.io.data.Element;
import cdc.io.data.paths.SPath;
import cdc.util.lang.Checks;

/**
 * Interface used to convert (replace) an attribute name.
 *
 * @author Damien Carbonne
 *
 */
@FunctionalInterface
public interface AttributeNameConverter {

    /**
     * An attribute name converter that returns the name unchanged.
     */
    public static final AttributeNameConverter IDENTITY = (e,
                                                           n) -> n;

    /**
     * Returns the name that should be used instead of the original name.
     *
     * @param element The element.
     * @param name The attribute name.
     * @return The name to use for attribute, instead of name.
     */
    public String convertAttributeName(Element element,
                                       String name);

    /**
     * Returns a converter that first applies {@code other} then this converter.
     *
     * @param other The other converter.
     * @return A converter that first applies {@code other} then this converter.
     * @throws IllegalArgumentException When {@code other} is {@code null}.
     */
    public default AttributeNameConverter compose(AttributeNameConverter other) {
        Checks.isNotNull(other, "other");
        return (Element element,
                String name) -> convertAttributeName(element, other.convertAttributeName(element, name));
    }

    /**
     * Returns a converter that first applies this converter and then {@code other}.
     *
     * @param other The other converter.
     * @return A converter that first applies this converter and then {@code other}.
     * @throws IllegalArgumentException When {@code other} is {@code null}.
     */
    public default AttributeNameConverter andThen(AttributeNameConverter other) {
        Checks.isNotNull(other, "other");
        return (Element element,
                String name) -> other.convertAttributeName(element, convertAttributeName(element, name));
    }

    /**
     * Creates a new AttributeNameConverter from a name converter function.
     * <p>
     * Conversion is independent of element.
     *
     * @param function The function used to convert attribute names.
     * @return A new AttributeValueConverter from {@code function}.
     * @throws IllegalArgumentException When {@code function} is {@code null}.
     */
    public static AttributeNameConverter fromNameFunction(UnaryOperator<String> function) {
        Checks.isNotNull(function, "function");
        return (Element element,
                String name) -> function.apply(name);
    }

    /**
     * Creates a new AttributeNameConverter from a Map of SPath to converter functions.
     *
     * @param map The Map of SPath to converter functions.
     * @return A new AttributeValueConverter from {@code map}.
     * @throws IllegalArgumentException When {@code map} is {@code null}.
     */
    public static AttributeNameConverter fromPathNameFunctionMap(Map<SPath, UnaryOperator<String>> map) {
        Checks.isNotNull(map, "map");
        return (Element element,
                String name) -> DataUtils.applyOnAttribute(map, element, name, name);
    }
}