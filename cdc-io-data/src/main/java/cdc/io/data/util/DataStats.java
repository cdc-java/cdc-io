package cdc.io.data.util;

import java.io.PrintStream;

import cdc.io.data.Element;
import cdc.io.data.Node;
import cdc.io.data.NodeType;
import cdc.io.data.Parent;
import cdc.util.debug.Printable;
import cdc.util.debug.Printables;
import cdc.util.strings.CaseConverter;

public class DataStats implements Printable {
    private final int[] counts = new int[NodeType.values().length];
    private int atts = 0;

    public DataStats(Node node) {
        count(node);
    }

    public int getNodesCount(NodeType type) {
        return counts[type.ordinal()];
    }

    public int getAttributesCount() {
        return atts;
    }

    @Override
    public void print(PrintStream out,
                      int level) {
        for (final NodeType type : NodeType.values()) {
            Printables.indent(out, level);
            out.println(CaseConverter.toCapital(type.name()) + "(s): " + getNodesCount(type));
        }
        Printables.indent(out, level);
        out.println("Attribute(s): " + getAttributesCount());
    }

    public static void print(Node node,
                             PrintStream out,
                             int level) {
        final DataStats stats = new DataStats(node);
        stats.print(out, level);
    }

    private void count(Node node) {
        counts[node.getType().ordinal()]++;
        if (node instanceof Element) {
            atts += ((Element) node).getAttributesCount();
        }
        if (node instanceof Parent) {
            for (final Node child : ((Parent) node).getChildren()) {
                count(child);
            }
        }
    }
}