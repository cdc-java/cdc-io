package cdc.io.data.util;

import java.util.Map;
import java.util.function.UnaryOperator;

import cdc.io.data.Element;
import cdc.io.data.Parent;
import cdc.io.data.paths.SPath;
import cdc.util.lang.Checks;
import cdc.util.strings.StringUtils;

@FunctionalInterface
public interface TextContentConverter {
    public static final TextContentConverter IDENTITY = (p,
                                                         c) -> c;

    public String convertTextContent(Parent parent,
                                     String content);

    /**
     * Returns a converter that first applies {@code other} then this converter.
     *
     * @param other The other converter.
     * @return A converter that first applies {@code other} then this converter.
     * @throws IllegalArgumentException When {@code other} is {@code null}.
     */
    public default TextContentConverter compose(TextContentConverter other) {
        Checks.isNotNull(other, "other");
        return (Parent parent,
                String content) -> convertTextContent(parent, other.convertTextContent(parent, content));
    }

    /**
     * Returns a converter that first applies this converter and then {@code other}.
     *
     * @param other The other converter.
     * @return A converter that first applies this converter and then {@code other}.
     * @throws IllegalArgumentException When {@code other} is {@code null}.
     */
    public default TextContentConverter andThen(TextContentConverter other) {
        Checks.isNotNull(other, "other");
        return (Parent parent,
                String content) -> other.convertTextContent(parent, convertTextContent(parent, content));
    }

    public static TextContentConverter fromString(String s) {
        return (Parent parent,
                String content) -> s;
    }

    public static TextContentConverter scramble(boolean preserveSpaces) {
        return (Parent parent,
                String content) -> StringUtils.scrambleWithLettersOrDigits(content, preserveSpaces);
    }

    /**
     * Creates a new TextContentConverter from a content converter function.
     * <p>
     * Conversion is independent of element.
     *
     * @param function The function used to convert content.
     * @return A new TextContentConverter from {@code function}.
     * @throws IllegalArgumentException When {@code function} is {@code null}.
     */
    public static TextContentConverter fromContentFunction(UnaryOperator<String> function) {
        Checks.isNotNull(function, "function");
        return (Parent parent,
                String content) -> function.apply(content);
    }

    public static TextContentConverter fromPathContentFunctionMap(Map<SPath, UnaryOperator<String>> map) {
        Checks.isNotNull(map, "map");
        return (Parent parent,
                String content) -> DataUtils.applyOnElement(map, parent instanceof Element ? (Element) parent : null, content);
    }
}