package cdc.io.data.util;

import java.io.PrintStream;

import cdc.io.data.Comment;
import cdc.io.data.Element;
import cdc.io.data.Node;
import cdc.io.data.Parent;
import cdc.io.data.Text;

public final class DataDebug {
    private DataDebug() {
    }

    private static void indent(PrintStream out,
                               int level) {
        for (int i = 0; i < level; i++) {
            out.print("  ");
        }
    }

    public static void print(Node node,
                             Node highlight1,
                             Node highlight2,
                             int level,
                             PrintStream out) {
        indent(out, level);
        out.print(node.getType());
        if (node == highlight1) {
            out.print(" *");
        }
        if (node == highlight2) {
            out.print(" +");
        }
        switch (node.getType()) {
        case COMMENT:
            final Comment comment = (Comment) node;
            out.print(" " + comment.getContent().length());
            break;
        case DOCUMENT:
            break;
        case ELEMENT:
            final Element element = (Element) node;
            out.print(" " + element.getName());
            break;
        case TEXT:
            final Text text = (Text) node;
            out.print(" " + text.getContent().length());
            break;
        default:
            // Ignore
            break;
        }
        out.println();
        if (node instanceof Parent) {
            for (final Node child : ((Parent) node).getChildren()) {
                print(child, highlight1, highlight2, level + 1, out);
            }
        }
    }

    public static void print(Node node,
                             int level,
                             PrintStream out) {
        print(node, null, null, level, out);
    }

}