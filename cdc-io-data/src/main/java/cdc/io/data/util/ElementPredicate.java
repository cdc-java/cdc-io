package cdc.io.data.util;

import java.util.Collection;
import java.util.function.Predicate;

import cdc.io.data.Element;
import cdc.io.data.Parent;
import cdc.io.data.paths.SPath;
import cdc.util.lang.Checks;

/**
 * Interface used to filter elements.
 *
 * @author Damien Carbonne
 *
 */
@FunctionalInterface
public interface ElementPredicate {

    /**
     * An element predicate that always returns {@code true}.
     */
    public static final ElementPredicate ANY_ELEMENT = (p,
                                                        e) -> true;
    /**
     * An element predicate that returns {@code true} when the element is empty.
     */
    public static final ElementPredicate EMPTY_ELEMENT = (p,
                                                          e) -> e.isEmpty();

    /**
     * An element predicate that returns {@code true} when the element is empty and has no attribute.
     */
    public static final ElementPredicate PURE_ELEMENT = (p,
                                                         e) -> e.isPure();

    /**
     * Returns true when an element must be kept.
     * <p>
     * Element (and its parents) must have been created before this can be called.<br>
     * However, its children are not necessarily existing at time of filtering.
     *
     * @param parent The element parent.
     * @param element The element.
     * @return True if element must be kept, false otherwise.
     */
    public boolean accepts(Parent parent,
                           Element element);

    /**
     * @return The negation of this predicate.
     */
    public default ElementPredicate not() {
        return (p,
                e) -> !accepts(p, e);
    }

    /**
     * Returns a predicate that is the logical {@code and} combination of this one and another one.
     *
     * @param other The other predicate.
     * @return A predicate that is the logical {@code and} combination of this one and {@code other}.
     * @throws IllegalArgumentException When {@code other} is {@code null}.
     */
    public default ElementPredicate and(ElementPredicate other) {
        Checks.isNotNull(other, "other");
        return (p,
                e) -> accepts(p, e) && other.accepts(p, e);
    }

    /**
     * Returns a predicate that is the logical {@code or} combination of this one and another one.
     *
     * @param other The other predicate.
     * @return A predicate that is the logical {@code or} combination of this one and {@code other}.
     * @throws IllegalArgumentException When {@code other} is {@code null}.
     */
    public default ElementPredicate or(ElementPredicate other) {
        Checks.isNotNull(other, "other");
        return (p,
                e) -> accepts(p, e) || other.accepts(p, e);
    }

    /**
     * Returns a predicate that returns {@code true} when the element name belongs to a collection.
     * <p>
     * It is advised to use an efficient collection, typically a Set.
     *
     * @param names The collection of accepted names.
     * @return A predicate that returns {@code true} when the element name belongs {@code names}.
     * @throws IllegalArgumentException When {@code names} is {@code null}.
     */
    public static ElementPredicate fromNames(Collection<String> names) {
        Checks.isNotNull(names, "names");
        return (Parent parent,
                Element element) -> names.contains(element.getName());
    }

    public static ElementPredicate fromPaths(Collection<SPath> paths) {
        Checks.isNotNull(paths, "paths");
        return (Parent parent,
                Element element) -> {
            for (final SPath path : paths) {
                if (path.matchesElement(parent, element.getName())) {
                    return true;
                }
            }
            return false;
        };
    }

    public static ElementPredicate fromPredicate(Predicate<? super Element> predicate) {
        Checks.isNotNull(predicate, "predicate");
        return (Parent parent,
                Element element) -> predicate.test(element);
    }
}