package cdc.io.data.util;

import java.util.function.UnaryOperator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatternReplacement implements UnaryOperator<String> {
    private final String replacement;
    private final Pattern p;

    public PatternReplacement(String regex,
                              String replacement) {
        this.p = Pattern.compile(regex);
        this.replacement = replacement;
    }

    public Pattern getPattern() {
        return p;
    }

    public String getRegex() {
        return p.pattern();
    }

    public String getReplacement() {
        return replacement;
    }

    @Override
    public String apply(String s) {
        final Matcher matcher = p.matcher(s);
        return matcher.replaceAll(replacement);
    }
}