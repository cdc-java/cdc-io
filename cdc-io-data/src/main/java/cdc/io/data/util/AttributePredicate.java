package cdc.io.data.util;

import java.util.Collection;

import cdc.io.data.Element;
import cdc.io.data.paths.SPath;
import cdc.util.lang.Checks;

/**
 * Interface used to filter attributes.
 *
 * @author Damien Carbonne
 *
 */
@FunctionalInterface
public interface AttributePredicate {

    /**
     * An attribute predicate that always returns {@code true}.
     */
    public static final AttributePredicate ANY_ATTRIBUTE = (e,
                                                            n,
                                                            v) -> true;

    /**
     * An attribute predicate that returns {@code true} when an attribute value is {@code null} or empty.
     */
    public static final AttributePredicate IS_EMPTY_ATTRIBUTE = (e,
                                                                 n,
                                                                 v) -> v == null || v.isEmpty();

    /**
     * An attribute predicate that returns {@code true} when an attribute value is neither {@code null} nor empty.
     */
    public static final AttributePredicate IS_NOT_EMPTY_ATTRIBUTE = IS_EMPTY_ATTRIBUTE.not();

    /**
     * Returns {@code true} when the attribute must be kept.
     *
     * @param element The element containing the attribute.
     * @param name The attribute name.
     * @param value The attribute value.
     * @return True if attribute must be kept, false otherwise.
     */
    public boolean accepts(Element element,
                           String name,
                           String value);

    /**
     * @return The negation of this predicate.
     */
    public default AttributePredicate not() {
        return (e,
                n,
                v) -> !accepts(e, n, v);
    }

    /**
     * Returns a predicate that is the logical {@code and} combination of this one and another one.
     *
     * @param other The other predicate.
     * @return A predicate that is the logical {@code and} combination of this one and {@code other}.
     * @throws IllegalArgumentException When {@code other} is {@code null}.
     */
    public default AttributePredicate and(AttributePredicate other) {
        Checks.isNotNull(other, "other");
        return (e,
                n,
                v) -> accepts(e, n, v) && other.accepts(e, n, v);
    }

    /**
     * Returns a predicate that is the logical {@code or} combination of this one and another one.
     *
     * @param other The other predicate.
     * @return A predicate that is the logical {@code or} combination of this one and {@code other}.
     * @throws IllegalArgumentException When {@code other} is {@code null}.
     */
    public default AttributePredicate or(AttributePredicate other) {
        Checks.isNotNull(other, "other");
        return (e,
                n,
                v) -> accepts(e, n, v) || other.accepts(e, n, v);
    }

    /**
     * Returns an AttributePredicate that returns true when the attribute has a names belonging to a collection.
     * <p>
     * It is advised to use an efficient collection, typically a Set.
     *
     * @param names The collections of names. <em>MUST NOT</em> be null.
     * @return A new instance of AttributePredicate that returns true when the attribute name belong to names.
     * @throws IllegalArgumentException When {@code names} is {@code null}.
     */
    public static AttributePredicate fromNames(Collection<String> names) {
        Checks.isNotNull(names, "names");
        return (Element element,
                String name,
                String value) -> names.contains(name);
    }

    public static AttributePredicate fromPaths(Collection<SPath> paths) {
        Checks.isNotNull(paths, "paths");
        return (Element element,
                String name,
                String value) -> {
            for (final SPath path : paths) {
                if (path.matchesAttribute(element, name)) {
                    return true;
                }
            }
            return false;
        };
    }
}