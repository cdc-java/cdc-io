package cdc.io.data.util;

import java.util.Map;
import java.util.function.UnaryOperator;

import cdc.io.data.Element;
import cdc.io.data.paths.SPath;
import cdc.util.lang.Checks;
import cdc.util.strings.StringUtils;

/**
 * Interface used to convert (replace) an attribute value.
 *
 * @author Damien Carbonne
 *
 */
@FunctionalInterface
public interface AttributeValueConverter {

    /**
     * An attribute value converter that returns the value unchanged.
     */
    public static final AttributeValueConverter INDENTITY = (e,
                                                             n,
                                                             v) -> v;

    /**
     * Returns the value that should be used instead of the original value.
     *
     * @param element The element containing the attribute.
     * @param name The attribute name.
     * @param value The attribute value.
     * @return The value to use for the attribute (instead of value).
     */
    public String convertAttributeValue(Element element,
                                        String name,
                                        String value);

    /**
     * Returns a converter that first applies {@code other} then this converter.
     *
     * @param other The other converter.
     * @return A converter that first applies {@code other} then this converter.
     * @throws IllegalArgumentException When {@code other} is {@code null}.
     */
    public default AttributeValueConverter compose(AttributeValueConverter other) {
        Checks.isNotNull(other, "other");
        return (Element element,
                String name,
                String value) -> convertAttributeValue(element, name, other.convertAttributeValue(element, name, value));
    }

    /**
     * Returns a converter that first applies this converter and then {@code other}.
     *
     * @param other The other converter.
     * @return A converter that first applies this converter and then {@code other}.
     * @throws IllegalArgumentException When {@code other} is {@code null}.
     */
    public default AttributeValueConverter andThen(AttributeValueConverter other) {
        Checks.isNotNull(other, "other");
        return (Element element,
                String name,
                String value) -> other.convertAttributeValue(element, name, convertAttributeValue(element, name, value));
    }

    public static AttributeValueConverter fromString(String s) {
        return (Element element,
                String name,
                String value) -> s;
    }

    public static AttributeValueConverter scramble(boolean preserveSpaces) {
        return (Element element,
                String name,
                String value) -> StringUtils.scrambleWithLettersOrDigits(value, preserveSpaces);
    }

    /**
     * Creates a new AttributeValueConverter from a value converter function.
     * <p>
     * Conversion is independent of element and attribute name.
     *
     * @param function The function used to convert values.
     * @return A new AttributeValueConverter from {@code function}.
     * @throws IllegalArgumentException When {@code function} is {@code null}.
     */
    public static AttributeValueConverter fromValueFunction(UnaryOperator<String> function) {
        Checks.isNotNull(function, "function");
        return (Element element,
                String name,
                String value) -> function.apply(value);
    }

    public static AttributeValueConverter fromPathValueFunctionMap(Map<SPath, UnaryOperator<String>> map) {
        Checks.isNotNull(map, "map");
        return (Element element,
                String name,
                String value) -> DataUtils.applyOnAttribute(map, element, name, value);
    }
}