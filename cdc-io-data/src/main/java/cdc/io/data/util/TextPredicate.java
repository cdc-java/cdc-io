package cdc.io.data.util;

import java.util.Collection;

import cdc.io.data.Element;
import cdc.io.data.Parent;
import cdc.io.data.Text;
import cdc.io.data.paths.SPath;
import cdc.util.lang.Checks;

/**
 * Interface used to filter text nodes.
 *
 * @author Damien Carbonne
 *
 */
@FunctionalInterface
public interface TextPredicate {

    public static final TextPredicate ANY_TEXT = (Parent parent,
                                                  Text text) -> true;

    public static final TextPredicate IGNORABLE_TEXT = (Parent parent,
                                                        Text text) -> text.isIgnorable();

    public static final TextPredicate NON_IGNORABLE_TEXT = (Parent parent,
                                                            Text text) -> !text.isIgnorable();

    /**
     * Returns {@code true} when a text must be kept.
     * <p>
     * Text (and its parents) must have been created before this can be called.<br>
     *
     * @param parent The text parent.
     * @param text The text node.
     * @return {@code true} if {@code text} must be kept, {@code false} otherwise.
     */
    public boolean accepts(Parent parent,
                           Text text);

    /**
     * @return The negation of this predicate.
     */
    public default TextPredicate not() {
        return (p,
                t) -> !accepts(p, t);
    }

    /**
     * Returns a predicate that is the logical {@code and} combination of this one and another one.
     *
     * @param other The other predicate.
     * @return A predicate that is the logical {@code and} combination of this one and {@code other}.
     * @throws IllegalArgumentException When {@code other} is {@code null}.
     */
    public default TextPredicate and(TextPredicate other) {
        Checks.isNotNull(other, "other");
        return (p,
                t) -> accepts(p, t) && other.accepts(p, t);
    }

    /**
     * Returns a predicate that is the logical {@code or} combination of this one and another one.
     *
     * @param other The other predicate.
     * @return A predicate that is the logical {@code or} combination of this one and {@code other}.
     * @throws IllegalArgumentException When {@code other} is {@code null}.
     */
    public default TextPredicate or(TextPredicate other) {
        Checks.isNotNull(other, "other");
        return (p,
                t) -> accepts(p, t) || other.accepts(p, t);
    }

    /**
     * Returns an TextPredicate that returns true when the text id defined in an element whose name belongs to a collection.
     * <p>
     * It is advised to use an efficient collection, typically a Set.
     *
     * @param names The collections of names. <em>MUST NOT</em> be null.
     * @return A new instance of TextPredicate that returns {@code true} when the text is in an element whose name belong to {@code names}.
     * @throws IllegalArgumentException When {@code names} is {@code null}.
     */
    public static TextPredicate fromElementNames(Collection<String> names) {
        Checks.isNotNull(names, "names");
        return (Parent parent,
                Text text) -> {
            if (parent instanceof Element) {
                return names.contains(((Element) parent).getName());
            } else {
                return false;
            }
        };
    }

    public static TextPredicate fromElementPaths(Collection<SPath> paths) {
        Checks.isNotNull(paths, "paths");
        return (Parent parent,
                Text text) -> {
            for (final SPath path : paths) {
                if (path.matchesElement(parent)) {
                    return true;
                }
            }
            return false;
        };
    }
}