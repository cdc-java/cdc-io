package cdc.io.data.util;

import java.util.Map;
import java.util.function.UnaryOperator;

import cdc.io.data.Parent;
import cdc.io.data.paths.SPath;
import cdc.util.lang.Checks;

/**
 * Interface used to convert an element name.
 *
 * @author Damien Carbonne
 *
 */
@FunctionalInterface
public interface ElementNameConverter {
    /**
     * An element name converter that returns the name unchanged.
     */
    public static final ElementNameConverter IDENTITY = (p,
                                                         n) -> n;

    /**
     * Returns the element name to use instead of the original name.
     *
     * @param parent The element parent.
     * @param name The element name.
     * @return The name to use for the element, instead of name.
     */
    public String convertElementName(Parent parent,
                                     String name);

    /**
     * Returns a converter that first applies {@code other} then this converter.
     *
     * @param other The other converter.
     * @return A converter that first applies {@code other} then this converter.
     * @throws IllegalArgumentException When {@code other} is {@code null}.
     */
    public default ElementNameConverter compose(ElementNameConverter other) {
        Checks.isNotNull(other, "other");
        return (Parent parent,
                String name) -> convertElementName(parent, other.convertElementName(parent, name));
    }

    /**
     * Returns a converter that first applies this converter and then {@code other}.
     *
     * @param other The other converter.
     * @return A converter that first applies this converter and then {@code other}.
     * @throws IllegalArgumentException When {@code other} is {@code null}.
     */
    public default ElementNameConverter andThen(ElementNameConverter other) {
        Checks.isNotNull(other, "other");
        return (Parent parent,
                String name) -> other.convertElementName(parent, convertElementName(parent, name));
    }

    /**
     * Creates a new ElementNameConverter from a name converter function.
     * <p>
     * Conversion is independent of element.
     *
     * @param function The function used to convert elements names.
     * @return A new ElementNameConverter from {@code function}.
     * @throws IllegalArgumentException When {@code function} is {@code null}.
     */
    public static ElementNameConverter fromNameFunction(UnaryOperator<String> function) {
        Checks.isNotNull(function, "function");
        return (Parent parent,
                String name) -> function.apply(name);
    }

    public static ElementNameConverter fromPathNameFunctionMap(Map<SPath, UnaryOperator<String>> map) {
        Checks.isNotNull(map, "map");
        return (Parent parent,
                String name) -> DataUtils.applyOnElement(map, parent, name, name);
    }
}