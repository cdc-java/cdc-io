package cdc.io.data;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.io.PrintStream;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.io.IoBuilder;
import org.junit.jupiter.api.Test;

import cdc.io.data.paths.Path;
import cdc.io.data.xml.XmlDataWriter;
import cdc.io.xml.XmlWriter;
import cdc.util.function.IterableUtils;
import cdc.util.lang.ComparatorUtils;

class ParentTest {
    private static final Logger LOGGER = LogManager.getLogger(ParentTest.class);
    private static final PrintStream OUT = IoBuilder.forLogger(LOGGER).setLevel(Level.INFO).buildPrintStream();
    private static final String NAME = "name";
    private static final String NAME1 = "name1";
    private static final String NAME2 = "name2";
    private static final String VALUE = "value";

    protected static void print(Node node) {
        try {
            OUT.println("=====================");
            XmlDataWriter.print(node, false, OUT, "  ", false, XmlWriter.Feature.APPEND_FINAL_EOL);
            OUT.println();
        } catch (final IOException e) {
            LOGGER.catching(e);
        }
    }

    @Test
    void testGetElementWithPath() {
        final Element root = new Element(NAME);
        final Element child1 = root.addElement(NAME1);
        final Element child11 = child1.addElement(NAME1);
        final Element child111 = child11.addElement(NAME1);
        final Element child112 = child11.addElement(NAME1);
        child112.addAttribute(NAME2, VALUE);

        assertSame(root, root.getElement(Path.of(".")));
        assertSame(child1, root.getElement(Path.of("name1")));
        assertSame(child11, root.getElement(Path.of("name1/name1")));
        assertSame(child111, root.getElement(Path.of("name1/name1/name1")));
        assertSame(child112, root.getElement(Path.of("name1/name1/name1[@name2='value']")));
    }

    @Test
    void testGetChildren() {
        final Element root = new Element(NAME);
        assertEquals(null, root.getLastChild());

        final Element child1 = root.addElement(NAME1);
        final Element child2 = root.addElement(NAME2);
        final Element child3 = root.addElement(NAME1);
        child3.addAttribute(NAME, VALUE);
        final Comment child4 = root.addComment("Hello");
        assertEquals(4, root.getChildrenCount());
        assertTrue(root.hasChildren(Element.class));
        assertTrue(root.hasChildren(Comment.class));
        assertFalse(root.hasChildren(Text.class));
        assertEquals(child4, root.getLastChild());

        assertEquals(child1, root.getChild(Element.class));
        assertEquals(child1, root.getElementNamed(NAME1));
        assertEquals(3, root.getChildrenCount(Element.class));
        assertEquals(2, root.getChildrenCount(Element.class, Element.named(NAME1)));
        assertEquals(child2, root.getChild(Element.class, Element.named(NAME2)));
        assertEquals(child2, root.getChildAt(Element.class, Element.named(NAME2), 0));
        assertEquals(null, root.getChildAt(Element.class, Element.named(NAME2), 1));
        assertEquals(child3, root.getChildAt(2));
        assertEquals(child3, root.getChildAt(Element.class, 2));
        assertEquals(child4, root.getChildAt(Comment.class, 0));
        assertEquals(3, IterableUtils.size(root.getElements()));
        assertEquals(1, IterableUtils.size(root.getComments()));
        assertEquals(0, IterableUtils.size(root.getTexts()));
        assertEquals(child3, root.getElementNamedWithAttribute(NAME1, NAME, VALUE));
    }

    @Test
    void testRemoveChildren() {
        final Element root = new Element(NAME);
        final Element child = root.addElement(NAME);

        root.removeChildren();
        assertEquals(0, root.getChildrenCount());
        assertFalse(root.removeChild(child));

        root.addElement(NAME);
        root.addComment("Hello");
        root.addElement(NAME);
        root.addText("World");
        root.addElement(NAME);
        root.removeComments();
        assertEquals(4, root.getChildrenCount());
        root.removeTexts();
        assertEquals(3, root.getChildrenCount());
        root.addElement(NAME1);
        root.removeElementsNamed(NAME);
        assertEquals(1, root.getChildrenCount());
    }

    @Test
    void testMerge() {
        final Element root = new Element(NAME);
        root.addElement(NAME);
        root.addComment("Hello");
        assertEquals(1, root.getChildrenCount(Comment.class));
        root.addComment("World");
        assertEquals(1, root.getChildrenCount(Comment.class));
        root.addComment("Foo", false);
        assertEquals(2, root.getChildrenCount(Comment.class));
        root.mergeComments();
        assertEquals(1, root.getChildrenCount(Comment.class));

        root.removeChildren();
        root.addElement(NAME);
        root.addComment("Hello", false);
        root.addElement(NAME);
        root.addComment("Hello", false);
        root.addComment("Hello", false);
        root.addText("Hello", false);
        root.addComment("Hello", false);
        root.addComment("Hello", false);
        root.addText("Hello", false);
        root.addText("Hello", false);

        assertEquals(5, root.getChildrenCount(Comment.class));
        assertEquals(3, root.getChildrenCount(Text.class));
        root.mergeComments();
        assertEquals(3, root.getChildrenCount(Comment.class));
        assertEquals(3, root.getChildrenCount(Text.class));
        root.mergeTexts();
        assertEquals(3, root.getChildrenCount(Comment.class));
        assertEquals(2, root.getChildrenCount(Text.class));

        root.removeChildren();
        root.addComment("hello", false);
        root.addComment("hello", false);
        root.addText("Hello", false);
        root.addText("Hello", false);
        root.addComment("hello", false);
        root.addComment("hello", false);
        root.addText("Hello", false);
        root.addText("Hello", false);

        final Element child = root.addElement("child");
        child.addComment("hello", false);
        child.addComment("hello", false);
        child.addText("Hello", false);
        child.addText("Hello", false);
        child.addComment("hello", false);
        child.addComment("hello", false);
        child.addText("Hello", false);
        child.addText("Hello", false);

        root.addComment("hello", false);
        root.addComment("hello", false);
        root.addText("Hello", false);
        root.addText("Hello", false);
        root.addComment("hello", false);
        root.addComment("hello", false);
        root.addText("Hello", false);
        root.addText("Hello", false);

        root.mergeComments(true);
        root.mergeTexts(true);
        assertEquals(4, root.getChildrenCount(Comment.class));
        assertEquals(2, child.getChildrenCount(Comment.class));
        assertEquals(4, root.getChildrenCount(Text.class));
        assertEquals(2, child.getChildrenCount(Text.class));
    }

    @Test
    void testChangeTexts() {
        final Element root = new Element(NAME);
        root.addText("hello", false);
        root.addText("HELLO", false);

        root.changeTexts(String::toUpperCase);
        assertEquals(2, root.getChildrenCount());
        assertEquals("HELLO", root.getChildAt(Text.class, 0).getContent());
        assertEquals("HELLO", root.getChildAt(Text.class, 1).getContent());

        root.removeChildren();
        final Element child1 = root.addElement(NAME1);
        final Element child2 = root.addElement(NAME2);
        final Element child3 = root.addElement(NAME1);
        child1.addText("hello");
        child2.addText("hello");
        child3.addText("hel");
        child3.addText("lo", false);
        assertEquals(2, child3.getChildrenCount());
        root.changeTexts(String::toUpperCase);
        assertEquals(2, child3.getChildrenCount());
        root.changeNamedTexts(NAME1, String::toUpperCase);
        assertEquals(1, child3.getChildrenCount());
        assertEquals("HELLO", child1.getText());
        assertEquals("HELLO", child3.getText());
    }

    @Test
    void testChangeComments() {
        final Element root = new Element(NAME);
        root.addComment("hello", false);
        root.addComment("HELLO", false);

        root.changeComments(String::toUpperCase);
        assertEquals(2, root.getChildrenCount());
        assertEquals("HELLO", root.getChildAt(Comment.class, 0).getContent());
        assertEquals("HELLO", root.getChildAt(Comment.class, 1).getContent());

        root.removeChildren();
        final Element child1 = root.addElement(NAME1);
        final Element child2 = root.addElement(NAME2);
        final Element child3 = root.addElement(NAME1);
        child1.addComment("hello");
        child2.addComment("hello");
        child3.addComment("hel");
        child3.addComment("lo", false);
        assertEquals(2, child3.getChildrenCount());
        root.changeComments(String::toUpperCase, true);
        assertEquals(2, child3.getChildrenCount());
        assertEquals("HELLO", child1.getChild(Comment.class).getContent());
        assertEquals("HEL", child3.getChildAt(Comment.class, 0).getContent());
    }

    @Test
    void testSortNoChildren() {
        final Element root = new Element("root");
        root.sortChildren(ComparatorUtils.adapt(Element.class, Element.NAME_COMPARATOR));
    }

    @Test
    void testSortName() {
        final Element root = new Element("root");
        for (int i = 9; i >= 0; i--) {
            root.addElement("child-" + i);
        }
        assertEquals(root.getElementNamed("child-0"), root.getChildAt(9));
        root.sortChildren(ComparatorUtils.adapt(Element.class, Element.NAME_COMPARATOR));
        assertEquals(root.getElementNamed("child-0"), root.getChildAt(0));
    }

    @Test
    void testSortAttribute() {
        final Element root = new Element("root");
        for (int i = 9; i >= 0; i--) {
            final Element child = root.addElement("child");
            child.addAttribute("id", i);
        }
        assertEquals(root.getElementNamedWithAttribute("child", "id", "0"), root.getChildAt(9));
        root.sortChildren(ComparatorUtils.adapt(Element.class, Element.compareAttribute("id")));
        assertEquals(root.getElementNamedWithAttribute("child", "id", "0"), root.getChildAt(0));
    }
}