package cdc.io.data;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.util.lang.FailureReaction;
import cdc.util.lang.NotFoundException;

class ElementTest {
    private static final String NAME = "name";
    private static final String NAME1 = "name1";
    private static final String NAME2 = "name2";
    private static final String NAME3 = "name3";
    private static final String NAME4 = "name4";
    private static final String VALUE = "value";

    @Test
    void testConstructors() {
        final Element e = new Element(NAME);
        assertEquals(null, e.getParent());
        assertEquals(ElementContentType.EMPTY, e.getContentType());
        e.addComment("comment", true);
        assertEquals(ElementContentType.NON_TEXT, e.getContentType());
    }

    @Test
    void testClone() {
        final Element e1 = new Element(NAME);
        e1.addAttribute(NAME1, VALUE);
        e1.addAttribute(NAME2, VALUE);
        final Element e2 = e1.clone(true);
        assertTrue(e1.deepEquals(e2));
    }

    @Test
    void testDeepEquals() {
        final Element e1 = new Element(NAME);
        e1.addAttribute(NAME1, VALUE);
        e1.addAttribute(NAME2, VALUE);
        final Element e2 = new Element(NAME);
        e2.addAttribute(NAME2, VALUE);
        e2.addAttribute(NAME1, VALUE);
        assertTrue(e1.deepEquals(e2));
    }

    @Test
    void testContentType() {
        final Element e = new Element(NAME);
        assertEquals(ElementContentType.EMPTY, e.getContentType());
        e.addComment("Hello");
        assertEquals(ElementContentType.NON_TEXT, e.getContentType());
        e.removeChildren();
        assertEquals(ElementContentType.EMPTY, e.getContentType());
        e.addText("Hello");
        assertEquals(ElementContentType.TEXT, e.getContentType());
        e.addText("World");
        assertEquals(ElementContentType.TEXT, e.getContentType());
        e.addComment("Hello");
        assertEquals(ElementContentType.MIXED, e.getContentType());
        e.removeChildren();
        assertEquals(ElementContentType.EMPTY, e.getContentType());
        e.addElement("child");
        assertEquals(ElementContentType.NON_TEXT, e.getContentType());
        e.addComment("Hello");
        assertEquals(ElementContentType.NON_TEXT, e.getContentType());
        e.addText("World");
        assertEquals(ElementContentType.MIXED, e.getContentType());
    }

    @Test
    void testAttributes() {
        final Element e = new Element(NAME);
        assertEquals(0, e.getAttributesCount());
        assertFalse(e.hasAttribute(NAME1));
        assertEquals(-1, e.getAttributeIndex(NAME1));
        assertTrue(e.getAttributes().isEmpty());
        assertThrows(NotFoundException.class,
                     () -> e.getAttributeValue(NAME1));

        e.addAttribute(NAME1, null);
        assertTrue(e.hasAttribute(NAME1));
        assertEquals(null, e.getAttributeValue(NAME1));
        assertEquals(0, e.getAttributeIndex(NAME1));
        assertEquals(-1, e.getAttributeIndex(NAME2));
        assertFalse(e.hasAttribute(NAME2));
        assertEquals(1, e.getAttributes().size());
        assertEquals(Boolean.TRUE, e.getAttributeAsOptionalBoolean(NAME1, Boolean.TRUE));
        assertEquals(Long.valueOf(0L), e.getAttributeAsOptionalLong(NAME1, 0L));
        assertEquals(Integer.valueOf(0), e.getAttributeAsOptionalInt(NAME1, 0));
        assertEquals(Short.valueOf((short) 0), e.getAttributeAsOptionalShort(NAME1, (short) 0));
        assertEquals(Byte.valueOf((byte) 0), e.getAttributeAsOptionalByte(NAME1, (byte) 0));
        assertEquals(Double.valueOf(0.0), e.getAttributeAsOptionalDouble(NAME1, 0.0));
        assertEquals(Float.valueOf(0.0F), e.getAttributeAsOptionalFloat(NAME1, 0.0F));
        assertEquals(FailureReaction.DEFAULT, e.getAttributeAsOptionalEnum(NAME1, FailureReaction.class, FailureReaction.DEFAULT));
    }

    @Test
    void testChildren() {
        final Element e = new Element(NAME);
        assertEquals(null, e.getDocument());
        assertEquals(e, e.getRootElement());
        assertEquals(e, e.getRootChild());

        final Element c0 = e.addElement(NAME1);
        final Element c1 = e.addElement(NAME1);
        final Text c2 = e.addText("Hello");
        final Comment c3 = e.addComment("Hello");
        assertEquals(c0, e.getChildAt(0));
        assertEquals(c1, e.getChildAt(1));
        assertEquals(c0, e.getElementNamed(NAME1));
        assertEquals(null, c2.getDocument());
        assertEquals(e, c3.getRootElement());
        assertEquals(e, c3.getRootChild());

        assertEquals(e, c2.getParent());
        assertEquals(e, c2.getParent(Element.class));

        assertEquals(4, e.getChildrenCount());
        assertEquals(4, e.getChildrenCount(Node.class));
        assertEquals(2, e.getChildrenCount(Element.class));
        assertEquals(1, e.getChildrenCount(Comment.class));
        assertEquals(1, e.getChildrenCount(Text.class));
        assertEquals(2, e.getChildrenCount(Parent.class));
        assertEquals(4, e.getChildrenCount(Child.class));
        assertEquals(2, e.getChildrenCount(Leaf.class));
    }

    @Test
    void testGetText() {
        final Element e = new Element(NAME);
        assertEquals(null, e.getText());
        e.addComment("Hello");
        assertEquals(null, e.getText());
        e.removeChildAt(0);
        assertEquals(null, e.getText());
        e.addComment("Comment");
        e.addText("Hello");
        assertEquals("Hello", e.getText());
        e.addText("World");
        assertEquals("HelloWorld", e.getText());
        e.addElement("Child");
        assertEquals(null, e.getText());

        e.removeChildren();
        e.addText("Hello", false);
        e.addComment("Comment");
        e.addText("World", false);
        assertEquals("HelloWorld", e.getText());

        e.removeChildren();
        e.addText("true");
        assertEquals(true, e.getTextAsBoolean(false));
        assertEquals(true, e.getTextAsOptionalBoolean(null));

        e.removeChildren();
        e.addText("10");
        assertEquals(10L, e.getTextAsLong(0));
        assertEquals(Long.valueOf(10L), e.getTextAsOptionalLong(null));
        assertEquals(10, e.getTextAsInt(0));
        assertEquals(Integer.valueOf(10), e.getTextAsOptionalInt(null));
        assertEquals((short) 10, e.getTextAsShort((short) 0));
        assertEquals(Short.valueOf((short) 10), e.getTextAsOptionalShort(null));
        assertEquals((byte) 10, e.getTextAsByte((byte) 0));
        assertEquals(Byte.valueOf((byte) 10), e.getTextAsOptionalByte(null));
    }

    @Test
    void testRemoveChildren1() {
        final Element root = new Element(NAME);
        final Element child1 = root.addElement(NAME1);
        final Element child2 = root.addElement(NAME2);
        final Element child3 = root.addElement(NAME3);

        assertEquals(child1, root.getChildAt(0));
        assertEquals(child2, root.getChildAt(1));
        assertEquals(child3, root.getChildAt(2));
        assertEquals(1, root.getChildrenCount(Element.class, Element.named(NAME1)));
        assertEquals(1, root.getChildrenCount(Element.class, Element.named(NAME2)));
        assertEquals(1, root.getChildrenCount(Element.class, Element.named(NAME3)));

        root.removeChildAt(0);
        assertEquals(child2, root.getChildAt(0));
        assertEquals(child3, root.getChildAt(1));
        assertEquals(0, root.getChildrenCount(Element.class, Element.named(NAME1)));
        assertEquals(1, root.getChildrenCount(Element.class, Element.named(NAME2)));
        assertEquals(1, root.getChildrenCount(Element.class, Element.named(NAME3)));

        root.removeChildAt(0);
        assertEquals(child3, root.getChildAt(0));
        assertEquals(0, root.getChildrenCount(Element.class, Element.named(NAME1)));
        assertEquals(0, root.getChildrenCount(Element.class, Element.named(NAME2)));
        assertEquals(1, root.getChildrenCount(Element.class, Element.named(NAME3)));
        root.removeChildAt(0);

        root.addChild(child1);
        root.addChild(child2);
        root.addChild(child3);
        assertEquals(child1, root.getChildAt(0));
        assertEquals(child2, root.getChildAt(1));
        assertEquals(child3, root.getChildAt(2));
        assertEquals(1, root.getChildrenCount(Element.class, Element.named(NAME1)));
        assertEquals(1, root.getChildrenCount(Element.class, Element.named(NAME2)));
        assertEquals(1, root.getChildrenCount(Element.class, Element.named(NAME3)));

        root.removeChildAt(2);
        assertEquals(child1, root.getChildAt(0));
        assertEquals(child2, root.getChildAt(1));

        root.removeChildAt(0);
        assertEquals(child2, root.getChildAt(0));
    }

    @Test
    void testRemoveChildren2() {
        final Element root = new Element(NAME);
        final Element child1 = root.addElement(NAME1);
        final Element child2 = root.addElement(NAME1);
        final Element child3 = root.addElement(NAME3);
        final Element child4 = root.addElement(NAME4);
        final Element child5 = root.addElement(NAME1);
        assertEquals(3, root.getChildrenCount(Element.class, Element.named(NAME1)));
        assertEquals(0, root.getChildrenCount(Element.class, Element.named(NAME2)));
        assertEquals(5, root.getChildrenCount(Element.class));
        assertEquals(child1, root.getChildAt(0));
        assertEquals(child2, root.getChildAt(1));
        assertEquals(child3, root.getChildAt(2));
        assertEquals(child4, root.getChildAt(3));
        assertEquals(child5, root.getChildAt(4));

        root.removeChildAt(0);
        assertEquals(2, root.getChildrenCount(Element.class, Element.named(NAME1)));
        assertEquals(0, root.getChildrenCount(Element.class, Element.named(NAME2)));
        assertEquals(4, root.getChildrenCount(Element.class));
        assertEquals(child2, root.getChildAt(0));
        assertEquals(child3, root.getChildAt(1));
        assertEquals(child4, root.getChildAt(2));
        assertEquals(child5, root.getChildAt(3));

        root.removeChildAt(3);
        assertEquals(1, root.getChildrenCount(Element.class, Element.named(NAME1)));
        assertEquals(0, root.getChildrenCount(Element.class, Element.named(NAME2)));
        assertEquals(3, root.getChildrenCount(Element.class));
        assertEquals(child2, root.getChildAt(0));
        assertEquals(child3, root.getChildAt(1));
        assertEquals(child4, root.getChildAt(2));

        root.removeChildAt(2);
        root.removeChildAt(0);
    }

    @Test
    void testRemoveChildren() {
        final Element root = new Element(NAME);
        root.addElement(NAME1);
        root.addElement(NAME1);
        root.addElement(NAME2);
        root.addElement(NAME1);
        root.addElement(NAME1);

        assertEquals(5, root.getChildrenCount(Element.class));
        assertEquals(4, root.getChildrenCount(Element.class, Element.named(NAME1)));

        root.removeChildAt(0);
        assertEquals(4, root.getChildrenCount(Element.class));
        assertEquals(3, root.getChildrenCount(Element.class, Element.named(NAME1)));

        root.removeElementsNamed(NAME1);
        assertEquals(1, root.getChildrenCount(Element.class));
        assertEquals(0, root.getChildrenCount(Element.class, Element.named(NAME1)));
    }
}