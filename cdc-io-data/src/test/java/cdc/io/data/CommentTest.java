package cdc.io.data;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class CommentTest {
    private static final String HELLO = "Hello";
    private static final String NAME = "name";

    @Test
    void testConstructors1() {
        final Comment x = new Comment();
        assertEquals(NodeType.COMMENT, x.getType());
        assertEquals(null, x.getParent());
        assertEquals("", x.getContent());
        x.setContent(HELLO);
        assertEquals(HELLO, x.getContent());
        x.clearContent();
        assertEquals("", x.getContent());
        x.appendContent(HELLO);
        assertEquals(HELLO, x.getContent());
        x.appendContent(null);
        assertEquals(HELLO, x.getContent());
        assertEquals(null, x.getRootElement());
        assertEquals(null, x.getDocument());
        assertEquals(x, x.getRootChild());

        assertTrue(x.deepEquals(x));
        assertFalse(x.deepEquals(null));
    }

    @Test
    void testConstructors2() {
        final Document doc = new Document();
        final Comment x = new Comment(doc);
        assertEquals(NodeType.COMMENT, x.getType());
        assertEquals(doc, x.getParent());
        assertEquals("", x.getContent());
        x.setContent(HELLO);
        assertEquals(HELLO, x.getContent());
        x.clearContent();
        assertEquals("", x.getContent());
        x.appendContent(HELLO);
        assertEquals(HELLO, x.getContent());
        x.appendContent(null);
        assertEquals(HELLO, x.getContent());
        assertEquals(null, x.getRootElement());
        assertEquals(doc, x.getDocument());
        assertEquals(x, x.getRootChild());
    }

    @Test
    void testClone() {
        final Comment x1 = new Comment(null, HELLO);
        final Comment x2 = x1.clone(false);
        final Comment x3 = x1.clone(true);
        assertNotEquals(x1, x2);
        assertNotEquals(x1, x3);
        assertTrue(x1.deepEquals(x2));
        assertTrue(x2.deepEquals(x1));
    }

    @Test
    void testGetRootElement() {
        final Comment x = new Comment(HELLO);
        assertEquals(null, x.getRootElement());
        final Element root = new Element(NAME);
        root.addChild(x);
        assertEquals(root, x.getRootElement());
    }
}