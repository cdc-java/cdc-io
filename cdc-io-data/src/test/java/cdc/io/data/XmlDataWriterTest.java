package cdc.io.data;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.io.StringWriter;
import java.util.function.Consumer;

import org.junit.jupiter.api.Test;

import cdc.io.data.xml.XmlDataWriter;
import cdc.io.xml.XmlWriter;

class XmlDataWriterTest {
    private static final String XML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
    private static final String NL = "\n";
    private static final String I = "  ";
    private static final String SCDATA = "<![CDATA[";
    private static final String ECDATA = "]]>";

    private static void check(Node node,
                              Consumer<XmlDataWriter> configurator,
                              String expected) throws IOException {
        try (final StringWriter result = new StringWriter();
                final XmlDataWriter writer = new XmlDataWriter(result)) {
            configurator.accept(writer);
            writer.write(node);
            writer.flush();

            assertEquals(expected, result.toString());
        }
    }

    private static void checkPretty(Node node,
                                    String expected) throws IOException {
        check(node, writer -> {
            writer.getXmlWriter().setEnabled(XmlWriter.Feature.PRETTY_PRINT, true);
            writer.getXmlWriter().setIndentString(I);
        },
              expected);
    }

    @Test
    void testPrettyDoc() throws IOException {
        final Document doc = new Document();
        checkPretty(doc, XML);
    }

    @Test
    void testPrettyRoot() throws IOException {
        final Document doc = new Document();
        doc.addChild(new Element("root"));
        checkPretty(doc,
                    XML + NL
                            + "<root/>");
    }

    @Test
    void testPrettyRootXTextY() throws IOException {
        final Document doc = new Document();
        final Element root = new Element("root").setParent(doc);
        root.addText("text");
        checkPretty(doc,
                    XML + NL
                            + "<root>text</root>");
    }

    @Test
    void testPrettyRootXCDataY() throws IOException {
        final Document doc = new Document();
        final Element root = new Element("root").setParent(doc);
        root.addText("text").setKind(TextKind.CDATA);
        checkPretty(doc,
                    XML + NL
                            + "<root>" + SCDATA + "text" + ECDATA + "</root>");
    }

    @Test
    void testPrettyRootXTextCDataY() throws IOException {
        final Document doc = new Document();
        final Element root = new Element("root").setParent(doc);
        root.addText("text");
        root.addText("text", TextKind.CDATA);
        checkPretty(doc,
                    XML + NL
                            + "<root>text" + SCDATA + "text" + ECDATA + "</root>");
    }

    @Test
    void testPrettyRootXChildY() throws IOException {
        final Document doc = new Document();
        final Element root = doc.addChild(new Element("root"));
        final Element child = new Element("child");
        root.addChild(child);
        checkPretty(doc,
                    XML + NL
                            + "<root>" + NL
                            + I + "<child/>" + NL
                            + "</root>");
    }

    @Test
    void testPrettyRootXChildXTextYY() throws IOException {
        final Document doc = new Document();
        final Element root = doc.addChild(new Element("root"));
        final Element child = new Element("child");
        child.addText("text");
        root.addChild(child);
        checkPretty(doc,
                    XML + NL
                            + "<root>" + NL
                            + I + "<child>text</child>" + NL
                            + "</root>");
    }

    @Test
    void testPrettyRootXChildXCDataYY() throws IOException {
        final Document doc = new Document();
        final Element root = doc.addChild(new Element("root"));
        final Element child = new Element("child");
        child.addText("text", TextKind.CDATA);
        root.addChild(child);
        checkPretty(doc,
                    XML + NL
                            + "<root>" + NL
                            + I + "<child>" + SCDATA + "text" + ECDATA + "</child>" + NL
                            + "</root>");
    }

    @Test
    void testPrettyRootXTextChildY() throws IOException {
        final Document doc = new Document();
        final Element root = doc.addChild(new Element("root"));
        root.addText("text");
        final Element child = new Element("child");
        root.addChild(child);
        checkPretty(doc,
                    XML + NL
                            + "<root>text" + "<child/>" + NL
                            + "</root>");
    }

    // @Test
    // FIXME XmlWriter must be fixed first
    void testPrettyRootXChildTextY() throws IOException {
        final Document doc = new Document();
        final Element root = doc.addChild(new Element("root"));
        final Element child = new Element("child");
        root.addChild(child);
        root.addText("text");
        checkPretty(doc,
                    XML + NL
                            + "<root>" + NL
                            + I + "<child/>text</root>");
    }
}