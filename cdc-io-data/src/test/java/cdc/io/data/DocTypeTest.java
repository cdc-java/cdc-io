package cdc.io.data;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;

import org.junit.jupiter.api.Test;

import cdc.io.data.xml.XmlDataReader;

class DocTypeTest {
    @Test
    void testReadMissingDoctypeOK() throws IOException {
        XmlDataReader.load("src/test/resources/test-missing-doctype.xml", XmlDataReader.Feature.DUMMY_ENTITY_RESOLVER);
        assertTrue(true);
    }

    @Test
    void testReadMissingDoctypeKO() throws IOException {
        assertThrows(IOException.class,
                     () -> XmlDataReader.load("src/test/resources/test-missing-doctype.xml"));
    }
}