package cdc.io.data;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

import cdc.io.data.paths.SPath;

class SPathTest {
    private static void testConstructor(String text,
                                        List<String> expectedParts,
                                        boolean expectedAttribute) {
        final SPath path = new SPath(text);
        assertEquals(expectedAttribute, path.isAttribute());
        assertEquals(expectedParts, path.getParts());
    }

    @Test
    void testConstructor() {
        testConstructor("hello", Arrays.asList("hello"), false);
        testConstructor("hello/world", Arrays.asList("hello", "world"), false);
        testConstructor("hello/world/how", Arrays.asList("hello", "world", "how"), false);
        testConstructor("@hello", Arrays.asList("hello"), true);
        testConstructor("hello@world", Arrays.asList("hello", "world"), true);
        testConstructor("hello/world@how", Arrays.asList("hello", "world", "how"), true);
    }

    private static void testElementMatches(String path,
                                           boolean expected,
                                           String... names) {
        Element parent = null;
        for (final String name : names) {
            parent = new Element(parent, name);
        }

        final SPath spath = new SPath(path);
        assertEquals(expected, spath.matchesElement(parent));
    }

    @Test
    void testElementMatches() {
        testElementMatches("root", false, "ROOT");
        testElementMatches("root", true, "root");
        testElementMatches("child", true, "root", "child");
        testElementMatches("@child", false, "root", "child");
        testElementMatches("child", true, "root", "child", "child");
        testElementMatches("child/child", true, "root", "child", "child");
        testElementMatches("root/child", false, "root", "child", "child");
        testElementMatches("root/child/child", true, "root", "child", "child");
        testElementMatches("child", true, "root", "child");
        testElementMatches("child", true, "root", "child", "child");
        testElementMatches("child/child", true, "root", "child", "child");
        testElementMatches("child/child/child", false, "root", "child", "child");
    }

    private static void testAttributeMatches(String path,
                                             boolean expected,
                                             String... names) {
        Element parent = null;
        for (final String name : names) {
            parent = new Element(parent, name);
        }
        parent = (Element) parent.getParent();
        parent.addAttribute(names[names.length - 1], "");

        final SPath spath = new SPath(path);
        assertEquals(expected, spath.matchesAttribute(parent, names[names.length - 1]));
    }

    @Test
    void testAttributeMatches() {
        testAttributeMatches("@att", true, "root", "att");
        testAttributeMatches("@att", false, "root", "att1");
        testAttributeMatches("@att", true, "root", "child", "att");
        testAttributeMatches("child@att", true, "root", "child", "att");
        testAttributeMatches("root/child@att", true, "root", "child", "att");
        testAttributeMatches("root/child@att", false, "root", "child", "att1");
    }
}