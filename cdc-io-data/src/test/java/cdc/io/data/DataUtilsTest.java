package cdc.io.data;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.junit.jupiter.api.Test;

import cdc.io.data.util.AttributeNameConverter;
import cdc.io.data.util.AttributePredicate;
import cdc.io.data.util.AttributeValueConverter;
import cdc.io.data.util.DataUtils;
import cdc.io.data.util.ElementNameConverter;
import cdc.io.data.util.TextContentConverter;
import cdc.io.data.util.TextPredicate;
import cdc.io.data.xml.XmlDataReader;
import cdc.io.data.xml.XmlDataWriter;
import cdc.io.xml.XmlWriter;
import cdc.util.function.Predicates;

class DataUtilsTest {
    private static final String XML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
    private static final String INDENT = null;
    private static final String EOL = "";

    @Test
    void testSetNameOfMatchingAttributes() throws IOException {
        final String source = XML + EOL
                + "<root x='a'>" + EOL
                + "<child y='a'/>" + EOL
                + "</root>";
        final String expected = XML + EOL
                + "<root X='a'>" + EOL
                + "<child Y='a'/>" + EOL
                + "</root>";
        final Document doc = XmlDataReader.load(source, StandardCharsets.UTF_8);
        DataUtils.setNameOfMatchingAttributes(doc,
                                              AttributePredicate.ANY_ATTRIBUTE,
                                              AttributeNameConverter.fromNameFunction(String::toUpperCase),
                                              DataUtils.RECURSE);
        final String target = XmlDataWriter.toString(doc, INDENT, XmlWriter.Feature.USE_SINGLE_QUOTE);
        assertEquals(expected, target);
    }

    @Test
    void testSetValueOfMatchingAttributes() throws IOException {
        final String source = XML + EOL
                + "<root x='a'>" + EOL
                + "<child y='a'/>" + EOL
                + "</root>";
        final String expected = XML + EOL
                + "<root x='A'>" + EOL
                + "<child y='A'/>" + EOL
                + "</root>";
        final Document doc = XmlDataReader.load(source, StandardCharsets.UTF_8);
        DataUtils.setValueOfMatchingAttributes(doc,
                                               AttributePredicate.ANY_ATTRIBUTE,
                                               AttributeValueConverter.fromValueFunction(String::toUpperCase),
                                               DataUtils.RECURSE);
        final String target = XmlDataWriter.toString(doc, INDENT, XmlWriter.Feature.USE_SINGLE_QUOTE);
        assertEquals(expected, target);
    }

    @Test
    void testSortAttributes() throws IOException {
        final String source = XML + EOL
                + "<root y='b' x='a'>" + EOL
                + "<child y='b' x='a'/>" + EOL
                + "</root>";
        final String expected = XML + EOL
                + "<root x='a' y='b'>" + EOL
                + "<child x='a' y='b'/>" + EOL
                + "</root>";
        final Document doc = XmlDataReader.load(source, StandardCharsets.UTF_8);
        DataUtils.sortAttributes(doc,
                                 Attribute.NAME_COMPARATOR,
                                 DataUtils.RECURSE);
        final String target = XmlDataWriter.toString(doc, INDENT, XmlWriter.Feature.USE_SINGLE_QUOTE);
        assertEquals(expected, target);
    }

    @Test
    void testRemoveEmptyAttributes() throws IOException {
        final String source = XML + EOL
                + "<root x='' y='b'>" + EOL
                + "<child x='' y=''/>" + EOL
                + "</root>";
        final String expected = XML + EOL
                + "<root y='b'>" + EOL
                + "<child/>" + EOL
                + "</root>";
        final Document doc = XmlDataReader.load(source, StandardCharsets.UTF_8);
        DataUtils.removeEmptyAttributes(doc,
                                        DataUtils.RECURSE);
        final String target = XmlDataWriter.toString(doc, INDENT, XmlWriter.Feature.USE_SINGLE_QUOTE);
        assertEquals(expected, target);
    }

    @Test
    void testRemovePureElements() throws IOException {
        final String source = XML + EOL
                + "<root x='a'>" + EOL
                + "<child>" + EOL
                + "<child/>" + EOL
                + "<child/>" + EOL
                + "</child>" + EOL
                + "</root>";
        final String expected = XML + EOL
                + "<root x='a'/>";
        final Document doc = XmlDataReader.load(source, StandardCharsets.UTF_8);
        DataUtils.removePureElements(doc, DataUtils.RECURSE);
        final String target = XmlDataWriter.toString(doc, INDENT, XmlWriter.Feature.USE_SINGLE_QUOTE);
        assertEquals(expected, target);
    }

    @Test
    void testSetNameOfMatchingElements() throws IOException {
        final String source = XML + EOL
                + "<root x='a'>" + EOL
                + "<child x='a'/>" + EOL
                + "</root>";
        final String expected = XML + EOL
                + "<ROOT x='a'>" + EOL
                + "<CHILD x='a'/>" + EOL
                + "</ROOT>";
        final Document doc = XmlDataReader.load(source, StandardCharsets.UTF_8);
        DataUtils.setNameOfMatchingElements(doc,
                                            Predicates.alwaysTrue(),
                                            ElementNameConverter.fromNameFunction(String::toUpperCase),
                                            DataUtils.RECURSE);
        final String target = XmlDataWriter.toString(doc, INDENT, XmlWriter.Feature.USE_SINGLE_QUOTE);
        assertEquals(expected, target);
    }

    @Test
    void testSortChildrenElementsByName() throws IOException {
        final String source = XML + EOL
                + "<root x='a'>" + EOL
                + "<child2 x='a'/>" + EOL
                + "<child1 x='a'/>" + EOL
                + "</root>";
        final String expected = XML + EOL
                + "<root x='a'>" + EOL
                + "<child1 x='a'/>" + EOL
                + "<child2 x='a'/>" + EOL
                + "</root>";
        final Document doc = XmlDataReader.load(source, StandardCharsets.UTF_8);
        DataUtils.sortChildren(doc,
                               Predicates.alwaysTrue(),
                               Node.ELEMENT_NAME_COMPARATOR,
                               DataUtils.RECURSE);
        final String target = XmlDataWriter.toString(doc, INDENT, XmlWriter.Feature.USE_SINGLE_QUOTE);
        assertEquals(expected, target);
    }

    @Test
    void testSortChildrenElementsByNameAndAttributes() throws IOException {
        final String source = XML + EOL
                + "<root x='a'>" + EOL
                + "<child x='b'/>" + EOL
                + "<child x='a'/>" + EOL
                + "</root>";
        final String expected = XML + EOL
                + "<root x='a'>" + EOL
                + "<child x='a'/>" + EOL
                + "<child x='b'/>" + EOL
                + "</root>";
        final Document doc = XmlDataReader.load(source, StandardCharsets.UTF_8);
        DataUtils.sortChildren(doc,
                               Predicates.alwaysTrue(),
                               Node.ELEMENT_NAME_AND_ATTRIBUTES_COMPARATOR,
                               DataUtils.RECURSE);
        final String target = XmlDataWriter.toString(doc, INDENT, XmlWriter.Feature.USE_SINGLE_QUOTE);
        assertEquals(expected, target);
    }

    @Test
    void testSetContentOfMatchingTexts() throws IOException {
        final String source = XML + EOL
                + "<root x='a'>" + EOL
                + "<child y='a'>aaa</child>" + EOL
                + "</root>";
        final String expected = XML + EOL
                + "<root x='a'>" + EOL
                + "<child y='a'>AAA</child>" + EOL
                + "</root>";
        final Document doc = XmlDataReader.load(source, StandardCharsets.UTF_8);
        DataUtils.setContentOfMatchingTexts(doc,
                                            TextPredicate.ANY_TEXT,
                                            TextContentConverter.fromContentFunction(String::toUpperCase),
                                            DataUtils.RECURSE);
        final String target = XmlDataWriter.toString(doc, INDENT, XmlWriter.Feature.USE_SINGLE_QUOTE);
        assertEquals(expected, target);
    }
}