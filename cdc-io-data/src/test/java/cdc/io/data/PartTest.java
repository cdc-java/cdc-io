package cdc.io.data;

import static org.junit.jupiter.api.Assertions.assertSame;

import org.junit.jupiter.api.Test;

import cdc.io.data.paths.Part;

class PartTest {
    @Test
    void testType() {
        assertSame(Part.Type.DOT, Part.of(".").getType());
        assertSame(Part.Type.DOT_DOT, Part.of("..").getType());
        assertSame(Part.Type.ELEMENT, Part.of("AAA").getType());
        assertSame(Part.Type.ATTRIBUTE, Part.of("@AAA").getType());
        assertSame(Part.Type.SELECTOR, Part.of("AAA[@YYY='ZZZ']").getType());
    }
}
