package cdc.io.data;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class AttributeTest {

    private static final String NAME = "name";

    @Test
    void testMisc() {
        final Attribute att = new Attribute(NAME);
        assertEquals(att, att);
        assertEquals(NAME, att.getName());
        final Attribute att2 = new Attribute(att);
        assertEquals(att, att2);
        att2.setValue("XXX");
        assertNotEquals(att, att2);
        assertNotEquals(att, new Attribute("NAME"));
    }

    @Test
    void testObject() {
        final Attribute att1 = new Attribute(NAME, (Object) null);
        assertEquals(null, att1.getValue());
        final Attribute att2 = new Attribute(NAME, getClass());
        assertEquals(getClass().toString(), att2.getValue());
    }

    @Test
    void testString() {
        final Attribute att = new Attribute(NAME, "SSS");
        assertEquals("SSS", att.getValue());
    }

    @Test
    void testBoolean() {
        final Attribute att = new Attribute(NAME, true);
        assertTrue(att.getValueAsBoolean());
        assertEquals("true", att.getValue());
    }

    @Test
    void testByte() {
        final Attribute att = new Attribute(NAME, (byte) 10);
        assertEquals((byte) 10, att.getValueAsByte());
        assertEquals("10", att.getValue());
    }

    @Test
    void testShort() {
        final Attribute att = new Attribute(NAME, (short) 10);
        assertEquals((short) 10, att.getValueAsShort());
        assertEquals("10", att.getValue());
    }

    @Test
    void testInteger() {
        final Attribute att = new Attribute(NAME, 10);
        assertEquals(10, att.getValueAsInt());
        assertEquals("10", att.getValue());
    }

    @Test
    void testLong() {
        final Attribute att = new Attribute(NAME, 10L);
        assertEquals(10L, att.getValueAsLong());
        assertEquals("10", att.getValue());
    }

    @Test
    void testFloat() {
        final Attribute att = new Attribute(NAME, 10.0f);
        assertEquals(10.0f, att.getValueAsFloat(), 1.0e-10);
        assertEquals("10.0", att.getValue());
    }

    @Test
    void testDouble() {
        final Attribute att = new Attribute(NAME, 10.0);
        assertEquals(10.0, att.getValueAsDouble(), 1.0e-10);
        assertEquals("10.0", att.getValue());
    }

    private enum Enum {
        A,
        B
    }

    @Test
    void testEnum() {
        final Attribute att = new Attribute(NAME, Enum.A);
        assertEquals(Enum.A, att.getValueAsEnum(Enum.class));
        assertEquals(Enum.A.name(), att.getValue());
    }
}