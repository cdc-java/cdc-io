package cdc.io.data;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class DocumentTest {
    static final String NAME = "name";
    private static final String VALUE = "value";

    @Test
    void testContructors() {
        final Document doc = new Document();
        assertEquals(doc, doc.getDocument());
        assertEquals(null, doc.getRootElement());
        assertEquals(0, doc.getChildrenCount());
        assertTrue(doc.deepEquals(doc));
        assertEquals(null, doc.getLastChild());
    }

    @Test
    void testDeepEquals() {
        final Document doc1 = new Document();
        final Document doc2 = new Document();
        assertTrue(doc1.deepEquals(doc2));
        doc1.addChild(new Comment());
        assertFalse(doc1.deepEquals(doc2));
        doc2.addChild(new Comment());
        assertTrue(doc1.deepEquals(doc2));
        doc1.addChild(new Element(NAME));
        assertFalse(doc1.deepEquals(doc2));
        doc2.addChild(new Element(NAME));
        assertTrue(doc1.deepEquals(doc2));
        ((Element) doc1.getLastChild()).addAttribute(NAME, VALUE);
        assertFalse(doc1.deepEquals(doc2));
        ((Element) doc2.getLastChild()).addAttribute(NAME, VALUE);
        assertTrue(doc1.deepEquals(doc2));

        final Document doc3 = doc1.clone(true);
        assertTrue(doc1.deepEquals(doc3));

        doc3.removeChildren();
        assertEquals(0, doc3.getChildrenCount());
    }

    @Test
    void testGetRootElement() {
        assertEquals(null, Document.getRootElement(null));
        final Document doc = new Document();
        assertEquals(null, doc.getRootElement());
        assertEquals(null, Document.getRootElement(doc));
        final Element root = new Element(NAME);
        doc.addChild(root);
        assertEquals(root, doc.getRootElement());

        assertEquals(root, doc.getRootElement());
        assertEquals(root, Document.getRootElement(doc));
    }

    @Test
    void testCanAddChild() {
        final Document doc = new Document();
        assertFalse(doc.canAddChild(null));
        final Element root = doc.addElement("root");
        assertFalse(doc.canAddChild(root));
    }

    @Test
    void testRoot() {
        final Document doc = new Document();
        assertEquals(null, doc.getRootElement());
        final Element root = new Element("root");
        assertEquals(null, root.getParent());
        assertTrue(doc.canAddChild(root));
        doc.addChild(root);
        assertFalse(doc.canAddChild(root));
        assertEquals(root, doc.getRootElement());
        root.setParent(null);
        assertEquals(null, root.getParent());
        assertEquals(null, doc.getRootElement());
    }
}