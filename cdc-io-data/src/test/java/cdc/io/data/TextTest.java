package cdc.io.data;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class TextTest {
    private static final String HELLO = "Hello";
    private static final String NAME = "name";

    @Test
    void testConstructors1() {
        final Text x = new Text();
        assertEquals(NodeType.TEXT, x.getType());
        assertEquals(null, x.getParent());
        assertEquals("", x.getContent());
        x.setContent(HELLO);
        assertEquals(HELLO, x.getContent());
        x.clearContent();
        assertEquals("", x.getContent());
        x.appendContent(HELLO);
        assertEquals(HELLO, x.getContent());
        x.appendContent(null);
        assertEquals(HELLO, x.getContent());
        assertEquals(null, x.getRootElement());
        assertEquals(null, x.getDocument());
        assertEquals(x, x.getRootChild());

        assertFalse(x.isCData());

        assertTrue(x.deepEquals(x));
        assertFalse(x.deepEquals(null));
    }

    @Test
    void testConstructors2() {
        final Document doc = new Document();
        final Text x = new Text(doc);
        assertEquals(NodeType.TEXT, x.getType());
        assertEquals(doc, x.getParent());
        assertEquals("", x.getContent());
        x.setContent(HELLO);
        assertEquals(HELLO, x.getContent());
        x.clearContent();
        assertEquals("", x.getContent());
        x.appendContent(HELLO);
        assertEquals(HELLO, x.getContent());
        x.appendContent(null);
        assertEquals(HELLO, x.getContent());
        assertEquals(null, x.getRootElement());
        assertEquals(doc, x.getDocument());
        assertEquals(x, x.getRootChild());
        assertFalse(x.isCData());
    }

    @Test
    void testClone() {
        final Text x1 = new Text(null, HELLO);
        final Text x2 = x1.clone(false);
        final Text x3 = x1.clone(true);
        assertNotEquals(x1, x2);
        assertNotEquals(x1, x3);
        assertTrue(x1.deepEquals(x2));
        assertTrue(x2.deepEquals(x1));
        assertFalse(x1.isCData());
        assertFalse(x2.isCData());
        assertFalse(x3.isCData());
    }

    @Test
    void testIsIgnorableWhiteSpace() {
        final Text x = new Text();
        assertTrue(x.isIgnorable());
        x.setContent(" ");
        assertTrue(x.isIgnorable());
        x.setContent("\t");
        assertTrue(x.isIgnorable());
        x.setContent("\n");
        assertTrue(x.isIgnorable());
        x.setContent("\r");
        assertTrue(x.isIgnorable());
        x.setContent("a");
        assertFalse(x.isIgnorable());
    }

    @Test
    void testGetRootElement() {
        final Text x = new Text(HELLO);
        assertEquals(null, x.getRootElement());
        final Element root = new Element(NAME);
        root.addChild(x);
        assertEquals(root, x.getRootElement());
    }
}