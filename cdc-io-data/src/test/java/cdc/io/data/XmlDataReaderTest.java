package cdc.io.data;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.URL;
import java.util.function.Consumer;
import java.util.function.Predicate;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.io.IoBuilder;
import org.junit.jupiter.api.Test;

import cdc.io.data.util.DataStats;
import cdc.io.data.util.DataUtils;
import cdc.io.data.xml.XmlDataReader;
import cdc.io.data.xml.XmlDataWriter;
import cdc.io.xml.XmlWriter;
import cdc.util.debug.Memory;
import cdc.util.files.Files;
import cdc.util.time.Chronometer;

class XmlDataReaderTest {
    private static final Logger LOGGER = LogManager.getLogger(XmlDataReaderTest.class);
    private static final String DTD_EOL = "\n  ";
    private static final PrintStream OUT = IoBuilder.forLogger(LOGGER).setLevel(Level.DEBUG).buildPrintStream();
    private static final String DTD = "\n<!DOCTYPE root [" + DTD_EOL
            + "<!ELEMENT root (#PCDATA|child)*>" + DTD_EOL
            + "<!ATTLIST root att CDATA #IMPLIED>" + DTD_EOL
            + "<!-- comment -->" + DTD_EOL
            + "<!ENTITY xxx \"my entity value\">" + DTD_EOL
            // + "<!ENTITY name SYSTEM \"URI/URL\">" + DTD_EOL
            // + "<!ENTITY name SYSTEM \"URI\" NDATA name>" + DTD_EOL
            // + "<?PI xxx?>" + DTD_EOL
            + "<!NOTATION GIF PUBLIC \"ppp\">" + DTD_EOL
            // + "<!NOTATION GIF PUBLIC \"ppp\" \"sss\">" + DTD_EOL
            // + "<!NOTATION GIF SYSTEM \"sss\">" + DTD_EOL
            + "]>\n";

    private static void generateTestData(String filename) {
        final int folders = 20;
        final int objects = 500;
        final int attributes = 100;
        try (final XmlWriter writer = new XmlWriter(filename, "UTF-8")) {
            writer.setEnabled(XmlWriter.Feature.PRETTY_PRINT, true);
            writer.beginDocument();
            writer.beginElement("root");
            for (int findex = 0; findex < folders; findex++) {
                writer.beginElement("folder");
                writer.addAttribute("type", "type" + findex);
                for (int oindex = 0; oindex < objects; oindex++) {
                    writer.beginElement("object");
                    writer.addAttribute("id", findex + "." + oindex);
                    writer.addAttribute("type", "type" + findex);
                    for (int aindex = 0; aindex < attributes; aindex++) {
                        writer.beginElement("attribute");
                        writer.addAttribute("name", "att" + aindex);
                        writer.addAttribute("value", "value" + aindex);
                        writer.endElement();
                    }
                    writer.endElement();
                }
                writer.endElement();
            }
            writer.endElement();
            writer.endDocument();
            writer.flush();
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    void testAttributeNameConversion() throws IOException {
        final XmlDataReader reader =
                XmlDataReader.builder()
                             .feature(XmlDataReader.Feature.LOAD_SPACES, false)
                             .attributeNameConverter((element,
                                                      name) -> name.toUpperCase())
                             .build();

        final URL url = getClass().getClassLoader().getResource("test-reader.xml");
        final Document doc = reader.read(Files.toFile(url).getPath());
        assertTrue(DataUtils.hasAllDescendantsMatching(doc,
                                                       node -> {
                                                           if (node instanceof Element) {
                                                               final Element e = (Element) node;
                                                               for (final Attribute a : e.getAttributes()) {
                                                                   if (!a.getName().toUpperCase().equals(a.getName())) {
                                                                       return false;
                                                                   }
                                                               }
                                                               return true;
                                                           } else {
                                                               return true;
                                                           }
                                                       },
                                                       true));
        LOGGER.debug("Upper case attribute names");
        XmlDataWriter.print(doc, OUT, "  ", false);
    }

    @Test
    void testAttributeValueConversion() throws IOException {
        final XmlDataReader reader =
                XmlDataReader.builder()
                             .feature(XmlDataReader.Feature.LOAD_SPACES, false)
                             .attributeValueConverter((element,
                                                       name,
                                                       value) -> value.toUpperCase())
                             .build();

        final URL url = getClass().getClassLoader().getResource("test-reader.xml");
        final Document doc = reader.read(Files.toFile(url).getPath());

        LOGGER.debug("Upper case attribute values");
        XmlDataWriter.print(doc, OUT, "  ", false);
        assertTrue(true);
    }

    @Test
    void testAttributeFiltering() throws IOException {
        final XmlDataReader reader =
                XmlDataReader.builder()
                             .feature(XmlDataReader.Feature.LOAD_SPACES, false)
                             .attributeFilter((element,
                                               name,
                                               value) -> "id".equals(name))
                             .build();

        final URL url = getClass().getClassLoader().getResource("test-reader.xml");
        final Document doc = reader.read(Files.toFile(url).getPath());

        LOGGER.debug("Only id attributes");
        XmlDataWriter.print(doc, OUT, "  ", false);
        assertTrue(true);
    }

    @Test
    void testElementNameConversion() throws IOException {
        final XmlDataReader reader =
                XmlDataReader.builder()
                             .feature(XmlDataReader.Feature.LOAD_SPACES, false)
                             .elementNameConverter((parent,
                                                    name) -> name.toUpperCase())
                             .build();

        final URL url = getClass().getClassLoader().getResource("test-reader.xml");
        final Document doc = reader.read(Files.toFile(url).getPath());

        LOGGER.debug("Upper case element names");
        XmlDataWriter.print(doc, OUT, "  ", false);
        assertTrue(true);
    }

    @Test
    void testElementFiltering() throws IOException {
        final XmlDataReader reader =
                XmlDataReader.builder()
                             .feature(XmlDataReader.Feature.LOAD_SPACES, false)

                             .elementPreFilter((parent,
                                                element) -> "root".equals(element.getName())
                                                        || "E1".equals(element.getAttributeValue("id", ""))
                                                        || "E1.1".equals(element.getAttributeValue("id", ""))
                                                        || "E2".equals(element.getAttributeValue("id", "")))
                             .build();

        final URL url = getClass().getClassLoader().getResource("test-reader.xml");
        final Document doc = reader.read(Files.toFile(url).getPath());

        LOGGER.debug("Only E1, E1.1 and E2");
        XmlDataWriter.print(doc, OUT, "  ", false);
        assertTrue(true);
    }

    @Test
    void testLargeElementFiltering() throws IOException {
        final File file = File.createTempFile("cdc-test", "xml");
        final Chronometer chrono = new Chronometer();
        chrono.start();
        generateTestData(file.getPath());
        chrono.suspend();
        LOGGER.debug("Generation time: {}", chrono);
        Memory.warmUp();
        Memory.runGC();
        final long init = Memory.usedMemory();

        final Predicate<Node> p = node -> {
            if (node instanceof Element) {
                final Element e = (Element) node;
                return "folder".equals(e.getName()) && "type0".equals(e.getAttributeValue("type", ""));
            } else {
                return false;
            }
        };

        final XmlDataReader reader =
                XmlDataReader.builder()
                             .feature(XmlDataReader.Feature.LOAD_SPACES, false)
                             // feature(XmlDataReader.Feature.SHARE_ATTRIBUTE_NAMES, true)
                             // feature(XmlDataReader.Feature.SHARE_ELEMENT_NAMES, true)
                             .elementPreFilter((parent,
                                                element) -> {
                                 final boolean ok = "root".equals(element.getName())
                                         || p.test(element)
                                         || DataUtils.hasAncestorMatching(parent, p);
                                 return ok;
                             })
                             .build();
        chrono.start();
        final Document doc = reader.read(file);
        Memory.runGC();
        final long end = Memory.usedMemory();
        chrono.suspend();
        LOGGER.debug("Reading time: {} {}", chrono, (end - init));
        DataStats.print(doc, OUT, 0);
    }

    private static InputStream createInputStream(String s) {
        return new ByteArrayInputStream(s.getBytes());
    }

    private static void test(String expected,
                             String source,
                             Consumer<Document> docChecker,
                             XmlDataReader.Feature... features) throws IOException {
        LOGGER.debug("test({})", source);
        final InputStream is = createInputStream(source);
        final Document doc = XmlDataReader.load(is, features);
        if (docChecker != null) {
            docChecker.accept(doc);
        }
        final String result = XmlDataWriter.toString(doc, null, XmlWriter.Feature.USE_XML_EOL);
        assertEquals(expected, result);
    }

    @Test
    void testFeaturesDefault0() throws IOException {
        test("<?xml version=\"1.0\" encoding=\"UTF-8\"?><root/>",
             "<?xml version=\"1.0\" encoding=\"UTF-8\"?><!-- comment --><root/><!-- comment -->",
             doc -> {
                 assertEquals(1, doc.getChildrenCount());
                 assertEquals("root", doc.getRootElement().getName());
                 assertEquals(0, doc.getRootElement().getChildrenCount());
             });
    }

    @Test
    void testFeaturesDefault1() throws IOException {
        test("<?xml version=\"1.0\" encoding=\"UTF-8\"?><root/>",
             "<?xml version=\"1.0\" encoding=\"UTF-8\"?><!-- comment --><root></root><!-- comment -->",
             doc -> {
                 assertEquals(1, doc.getChildrenCount());
                 assertEquals("root", doc.getRootElement().getName());
                 assertEquals(0, doc.getRootElement().getChildrenCount());
             });
    }

    @Test
    void testFeaturesDefault2() throws IOException {
        test("<?xml version=\"1.0\" encoding=\"UTF-8\"?><root>   </root>",
             "<?xml version=\"1.0\" encoding=\"UTF-8\"?><!-- comment --><root> <!-- comment -->  </root><!-- comment -->",
             doc -> {
                 assertEquals(1, doc.getChildrenCount());
                 assertEquals("root", doc.getRootElement().getName());
                 assertEquals(1, doc.getRootElement().getChildrenCount());
             });
    }

    @Test
    void testFeaturesDefault3() throws IOException {
        test("<?xml version=\"1.0\" encoding=\"UTF-8\"?><root>aaa</root>",
             "<?xml version=\"1.0\" encoding=\"UTF-8\"?><!-- comment --><root><!-- comment -->aaa</root><!-- comment -->",
             doc -> {
                 assertEquals(1, doc.getChildrenCount());
                 assertEquals("root", doc.getRootElement().getName());
                 assertEquals(1, doc.getRootElement().getChildrenCount());
             });
    }

    // @Test
    void testFeaturesDefaultUnexpectedMixed1() throws IOException {
        assertThrows(IOException.class, () -> {
            final InputStream is = createInputStream("<?xml version=\"1.0\" encoding=\"UTF-8\"?><root>text<child/></root>");
            XmlDataReader.load(is);
        });
    }

    // @Test
    void testFeaturesDefaultUnexpectedMixed2() throws IOException {
        assertThrows(IOException.class, () -> {
            final InputStream is = createInputStream("<?xml version=\"1.0\" encoding=\"UTF-8\"?><root><child/>text</root>");
            XmlDataReader.load(is);
        });
    }

    @Test
    void testFeaturesDefault4() throws IOException {
        test("<?xml version=\"1.0\" encoding=\"UTF-8\"?><root><child> </child></root>",
             "<?xml version=\"1.0\" encoding=\"UTF-8\"?> \n  <root> \n <child> </child> \n </root>",
             doc -> {
                 assertEquals(1, doc.getChildrenCount());
                 assertEquals("root", doc.getRootElement().getName());
                 assertEquals(1, doc.getRootElement().getChildrenCount());
                 assertEquals(1, doc.getRootElement().getChildAt(Element.class, 0).getChildrenCount());
             });
    }

    @Test
    void testFeaturesDefault5() throws IOException {
        test("<?xml version=\"1.0\" encoding=\"UTF-8\"?><root><child/></root>",
             "<?xml version=\"1.0\" encoding=\"UTF-8\"?> \n  <root> \n <child></child> \n </root>",
             doc -> {
                 assertEquals(1, doc.getChildrenCount());
                 assertEquals("root", doc.getRootElement().getName());
                 assertEquals(1, doc.getRootElement().getChildrenCount());
                 assertEquals(0, doc.getRootElement().getChildAt(Element.class, 0).getChildrenCount());
             });
    }

    @Test
    void testFeaturesDefault6() throws IOException {
        test("<?xml version=\"1.0\" encoding=\"UTF-8\"?><root><child/></root>",
             "<?xml version=\"1.0\" encoding=\"UTF-8\"?><root><child/></root>",
             doc -> {
                 assertEquals(1, doc.getChildrenCount());
                 assertEquals("root", doc.getRootElement().getName());
                 assertEquals(1, doc.getRootElement().getChildrenCount());
             });
    }

    @Test
    void testFeaturesDefault7() throws IOException {
        test("<?xml version=\"1.0\" encoding=\"UTF-8\"?><root><child/></root>",
             "<?xml version=\"1.0\" encoding=\"UTF-8\"?> \n  <root> \n <child/> \n </root>",
             doc -> {
                 assertEquals(1, doc.getChildrenCount());
                 assertEquals("root", doc.getRootElement().getName());
                 assertEquals(1, doc.getRootElement().getChildrenCount());
             });
    }

    @Test
    void testFeaturesComments() throws IOException {
        test("<?xml version=\"1.0\" encoding=\"UTF-8\"?><!--comment--><root><child/></root>",
             "<?xml version=\"1.0\" encoding=\"UTF-8\"?> \n <!--comment--> <root> \n <child/> \n </root>",
             doc -> {
                 assertEquals(2, doc.getChildrenCount());
                 assertEquals(1, doc.getChildrenCount(Comment.class));
             },
             XmlDataReader.Feature.LOAD_COMMENTS);
    }

    @Test
    void testFeaturesCommentsMixed1() throws IOException {
        test("<?xml version=\"1.0\" encoding=\"UTF-8\"?><!--comment--><root>  aaa\n  aaa<child/>  aaa\n  aaa</root>",
             "<?xml version=\"1.0\" encoding=\"UTF-8\"?><!--comment--><root>  aaa\n  aaa<child/>  aaa\n  aaa</root>",
             doc -> {
                 assertEquals(2, doc.getChildrenCount());
                 assertEquals(1, doc.getChildrenCount(Comment.class));
                 assertEquals(3, doc.getRootElement().getChildrenCount());
                 assertEquals(2, doc.getRootElement().getChildrenCount(Text.class));
             },
             XmlDataReader.Feature.LOAD_COMMENTS,
             XmlDataReader.Feature.LOAD_SPACES,
             XmlDataReader.Feature.ALLOW_MIXED_CONTENT);
    }

    @Test
    void testFeaturesCommentsMixed2() throws IOException {
        test("<?xml version=\"1.0\" encoding=\"UTF-8\"?><!--comment--><root>aaa\naaa<child/>aaa\naaa</root>",
             "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<!--comment--><root>aaa\naaa<child/>aaa\naaa</root>",
             doc -> {
                 Nodes.print(doc, LOGGER, Level.DEBUG);
                 assertEquals(2, doc.getChildrenCount());
                 assertEquals(1, doc.getChildrenCount(Comment.class));
                 assertEquals(3, doc.getRootElement().getChildrenCount());
                 assertEquals(2, doc.getRootElement().getChildrenCount(Text.class));
             },
             XmlDataReader.Feature.LOAD_COMMENTS,
             XmlDataReader.Feature.LOAD_SPACES,
             XmlDataReader.Feature.ALLOW_MIXED_CONTENT);
    }

    @Test
    void testSAXTextNoPreserveCDATA() throws IOException {
        test("<?xml version=\"1.0\" encoding=\"UTF-8\"?><root>aaabbb</root>",
             "<?xml version=\"1.0\" encoding=\"UTF-8\"?><root>aaa<![CDATA[bbb]]></root>",
             doc -> {
                 Nodes.print(doc, LOGGER, Level.DEBUG);
                 assertEquals(1, doc.getChildrenCount());
                 assertEquals(1, doc.getRootElement().getChildrenCount());
                 assertEquals(1, doc.getRootElement().getChildrenCount(Text.class));
             },
             XmlDataReader.Feature.ALLOW_MIXED_CONTENT);
    }

    @Test
    void testStAXTextNoPreserveCDATA() throws IOException {
        test("<?xml version=\"1.0\" encoding=\"UTF-8\"?><root>aaabbb</root>",
             "<?xml version=\"1.0\" encoding=\"UTF-8\"?><root>aaa<![CDATA[bbb]]></root>",
             doc -> {
                 Nodes.print(doc, LOGGER, Level.DEBUG);
                 assertEquals(1, doc.getChildrenCount());
                 assertEquals(1, doc.getRootElement().getChildrenCount());
                 assertEquals(1, doc.getRootElement().getChildrenCount(Text.class));
             },
             XmlDataReader.Feature.USE_STAX,
             XmlDataReader.Feature.ALLOW_MIXED_CONTENT);
    }

    @Test
    void testSAXTextPreserveCDATA() throws IOException {
        test("<?xml version=\"1.0\" encoding=\"UTF-8\"?><root>aaa<![CDATA[bbb]]></root>",
             "<?xml version=\"1.0\" encoding=\"UTF-8\"?><root>aaa<![CDATA[bbb]]></root>",
             doc -> {
                 Nodes.print(doc, LOGGER, Level.INFO);
                 assertEquals(1, doc.getChildrenCount());
                 assertEquals(2, doc.getRootElement().getChildrenCount());
                 assertEquals(2, doc.getRootElement().getChildrenCount(Text.class));
             },
             XmlDataReader.Feature.USE_SAX,
             XmlDataReader.Feature.ALLOW_MIXED_CONTENT,
             XmlDataReader.Feature.PRESERVE_CDATA);
    }

    @Test
    void testStAXTextPreserveCDATA() throws IOException {
        test("<?xml version=\"1.0\" encoding=\"UTF-8\"?><root>aaa<![CDATA[bbb]]></root>",
             "<?xml version=\"1.0\" encoding=\"UTF-8\"?><root>aaa<![CDATA[bbb]]></root>",
             doc -> {
                 Nodes.print(doc, LOGGER, Level.INFO);
                 assertEquals(1, doc.getChildrenCount());
                 assertEquals(2, doc.getRootElement().getChildrenCount());
                 assertEquals(2, doc.getRootElement().getChildrenCount(Text.class));
             },
             XmlDataReader.Feature.USE_STAX,
             XmlDataReader.Feature.ALLOW_MIXED_CONTENT,
             XmlDataReader.Feature.PRESERVE_CDATA);
    }

    @Test
    void testStAXDTD() throws IOException {
        test("<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + DTD + "<root>aaa</root>",
             "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + DTD + "\n<root>aaa</root>",
             doc -> {
                 Nodes.print(doc, LOGGER, Level.INFO);
                 assertEquals(1, doc.getChildrenCount());
                 assertEquals(1, doc.getRootElement().getChildrenCount());
                 assertEquals(1, doc.getRootElement().getChildrenCount(Text.class));
             },
             XmlDataReader.Feature.USE_STAX,
             XmlDataReader.Feature.LOAD_COMMENTS,
             XmlDataReader.Feature.LOAD_SPACES,
             XmlDataReader.Feature.ALLOW_MIXED_CONTENT,
             XmlDataReader.Feature.LOAD_DTD);

        test("<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + "<root>aaa</root>",
             "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + DTD + "<root>aaa</root>",
             doc -> {
                 Nodes.print(doc, LOGGER, Level.INFO);
                 assertEquals(1, doc.getChildrenCount());
                 assertEquals(1, doc.getRootElement().getChildrenCount());
                 assertEquals(1, doc.getRootElement().getChildrenCount(Text.class));
             },
             XmlDataReader.Feature.USE_STAX,
             XmlDataReader.Feature.LOAD_COMMENTS,
             XmlDataReader.Feature.LOAD_SPACES,
             XmlDataReader.Feature.ALLOW_MIXED_CONTENT);
    }

    @Test
    void testSAXDTD() throws IOException {
        test("<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + DTD + "<root>aaa</root>",
             "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + DTD + "<root>aaa</root>",
             doc -> {
                 Nodes.print(doc, LOGGER, Level.INFO);
                 assertEquals(1, doc.getChildrenCount());
                 assertEquals(1, doc.getRootElement().getChildrenCount());
                 assertEquals(1, doc.getRootElement().getChildrenCount(Text.class));
             },
             XmlDataReader.Feature.USE_SAX,
             XmlDataReader.Feature.LOAD_COMMENTS,
             XmlDataReader.Feature.LOAD_SPACES,
             XmlDataReader.Feature.ALLOW_MIXED_CONTENT,
             XmlDataReader.Feature.LOAD_DTD);
    }
}